package com.pingu.party;


import com.pingu.party.launcher.LaunchUI;
import com.pingu.party.gui.test.*;

import javax.swing.*;

/**
 * This class is used to launch the game.
 *
 * @author Pho3
 * @author Author 2
 * @author Author 3
 */
public class Launcher{

	/**
	 * Test gui.test Graphics00
	 */
	public static void TestGui00() {
		new Graphics00().setVisible(true);
	}

	/**
	 * Launches the game.
	 *
	 * @param args
	 * 		Not used here.
	 */
	public static void main(String[] args)
	{	//TODO to be completed
		System.setProperty("com.sun.management.jmxremote","true");
		System.setProperty("com.sun.management.jmxremote.local.only","false");
		System.out.println("Pingu Launcher is starting . . .");
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new LaunchUI();
				//TestGui00();
			}
		});
		System.out.println("Or not xD");
	}
}
