package com.pingu.party.network;
import java.util.Scanner;
import java.util.Random;
import java.util.Vector;
import com.pingu.party.game.Game_Console;
import com.pingu.party.game.Card;
import com.pingu.party.game.Player;
import com.pingu.party.game.Game;
import com.pingu.party.game.AIOne;
import com.pingu.party.game.Board;
import com.pingu.party.game.Init;


import java.util.ArrayList;
import java.util.HashMap;

import com.pingu.party.network.dataobjects.EndGame;
import com.pingu.party.network.dataobjects.Color;
import com.pingu.party.network.dataobjects.NextTurn;
import com.pingu.party.network.dataobjects.Move;
import com.pingu.party.network.dataobjects.PlayerInformation;
import com.pingu.party.network.dataobjects.ProfileData;
import com.pingu.party.network.dataobjects.ProfileStatData;
import com.pingu.party.network.dataobjects.Result;
import com.pingu.party.network.dataobjects.RoundStart;
import com.pingu.party.network.dataobjects.StatData;

import java.io.IOException;

public class GameServer extends Game
{
	private static ArrayList<ProfileStatData> psd;
	public static PlayerServer[] allPlayer;
	public MultiThreadServer mts;
	
	GameServer(int n,ArrayList<ProfileStatData> p,MultiThreadServer msts)
	{
		mts=msts;
		psd=p;
		allPlayer= new PlayerServer[n];
		nbPlayer=n;
		for(int i=0;i<nbPlayer;i++)
		{
			allPlayer[i]=new PlayerServer(n,i+1,mts);
		}
		nbPlayer=n;
		
		System.out.println(allPlayer.length);
		B.createTable(n);
	}
	public void setALPSD(ArrayList<ProfileStatData> p)
	{
		psd=p;
	}
	public PlayerServer[] getAllPlayer()
	{
		return allPlayer;
	}
	public int getDis()
	{
		return B.getDis();
	}
	public String[] distribution(clientThread[] t)
	{
		Random rand = new Random();
		int max=35;
		int min=0;
		int[] Super_Ultimate_Drawer={1,1,1,1,1,1,1,2,2,2,2,2,2,2,3,3,3,3,3,3,3,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5};
		int o=0;
		int x;
		String[] s=new String[allPlayer[0].getMaxCard()];
		Card P;
		while(o!=allPlayer[0].getMaxCard())
		{
			for(int k=0;k<nbPlayer;k++)
			{
				x=rand.nextInt(max - min + 1) + min;
				max--;
				P=new Card(Super_Ultimate_Drawer[x]);
				allPlayer[k].addCard(P);
				Super_Ultimate_Drawer=numberAdvance(Super_Ultimate_Drawer, x);
				if(o==0)
				{
					s[k]=""+convertIntToLetter(P.getColor());
				}
				else
				{	
					s[k]=s[k]+convertIntToLetter(P.getColor());
				}
			}
			o++;
		}
		if(nbPlayer==5)
		{
			B.changeDis(new Card(Super_Ultimate_Drawer[0]));
		}
		for(int i=0;i<allPlayer.length;i++)
		{
			allPlayer[i].sortingHand();
		}
		/*for(int i=0;i<t.length;i++)
		{	
			if(t[i]!=null)
				System.out.println("");
				//t[i].send(s[i]);
		}*/
		return s;
	}
	/*public void reset(clientThread[] t)
	{
		for(int i=0;i<allPlayer.length;i++)
		{
			allPlayer[i].changeNbCard(0);
			allPlayer[i].changeTurnDeath(0);
			allPlayer[i].resetDefeat();
		}
		for(int i=0;i<t.length;i++)
		{
			if(t[i]!=null)
				t[i].send("r");
		}
		nbSlain=0;
		turn=0;
		B.createTable(allPlayer.length);
	}*/
	public void Turn(clientThread[] t,int j,Move s)
	{
		Vector K=new Vector();
		int c=allPlayer[j].doAction(j,t,B,s);
		if(c==5)
		{
			allPlayer[j].increaseGreen();
		}
		if(c==0)
		{
			allPlayer[j].doDefeatS(t[j],t);
			nbSlain++;
			allPlayer[j].changeTurnDeath(turn);
		}
		else
		{
			allPlayer[j].removeCard(c);
		}
	}
	public void Round(clientThread[] t,int ro)
	{
		int r=0;
		while(allPlayer.length!=nbSlain)
		{
			System.out.println("Turn "+turn);
			System.out.println("nbSlain "+nbSlain);
			//System.out.println(allPlayer.length);
			for(int i=lastFight;i<allPlayer.length;i++)
			{
				if((!allPlayer[i].getDefeat())&&(!allPlayer[i].getKicked()))
				{
					//Pa.toDisplayTempString("Your turn");
					r++;
					try
					{
						t[i].send(new NextTurn ( allPlayer[i].getUUID() , null , new PlayerInformation( allPlayer[i].getUUID(),(float) -1.0, null, allPlayer[i].getNbCard())));
					}
					catch(IOException e)
					{
						t[i].kickPlayer(t,mts);
					}
						Move s=t[i].listener();
						Turn(t,i,s);
					//}
					/*catch(IOException e)
					{
						nbSlain++;
						allPlayer[i].kicked();
						//t[i].send("k");
						t[i]=null;
					}*/
					
					
					
					
					//if(!(s.equals("kkk")))
				}
			}
			turn++;
			if(lastFight!=0)
			{
				turn--;
			}
			lastFight=0;
		}
		for(int y=0;y<allPlayer.length;y++)
		{
			allPlayer[y].incScore(allPlayer[y].getNbCard());
		}
		lastFighter(ro);
	}
	public void lastFighter(int roun)
	{
		int max=0;
		for(int i=0;i<allPlayer.length;i++)
		{
			if(max<allPlayer[i].getTurnDeath())
			{
				max=allPlayer[i].getTurnDeath();
			}
		}
		for(int i=0;i<allPlayer.length;i++)
		{
			if(max==allPlayer[i].getTurnDeath())
			{
				allPlayer[i].incScoreRound();
				allPlayer[i].incScoreRoundByRound(roun);
			}
		}
	}
	public void mainGame(clientThread[] t)
	{
		int o=0;
		int min;
		for(int d=0;d<t.length;d++)
		{
			allPlayer[d].setUUID(t[d].getUUID());
		}
		while(o!=nbPlayer)
		{
			String[] handss=distribution(t);
			for(int i=0;i<t.length;i++)
			{
				try
				{
					t[i].send(new RoundStart(allPlayer.length,handss[i] ,convertIntToColor(B.getDis()),(float)-1.0,null));
				}
				catch(IOException e)
					{
						t[i].kickPlayer(t,mts);
					}
			}
			min=999999;
			nbSlain=0;
			for(int i=0;i<allPlayer.length;i++)
			{
				if(allPlayer[i].getKicked())
				{
					nbSlain++;
				}
			}
			Round(t,o);
			for(int i=0;i<nbPlayer;i++)
			{
				if(min>allPlayer[i].getTurnDeath())
				{
					min=allPlayer[i].getTurnDeath();
				}
			}
			for(int i=0;i<nbPlayer;i++)
			{
				if(min==allPlayer[i].getTurnDeath())
				{
					lastFight=i;
				}
			}
			//reset(t);
			o++;
		}
		End(t);
	}
	@SuppressWarnings("unchecked")
	public void End(clientThread[] t)
	{
		int PlayerManage=0;
		Vector Ranking=new Vector();
		int minScore;
		int minGeneralScore=-1;
		while(PlayerManage<allPlayer.length)
		{
			minScore=9999;
			for(int i=0;i<allPlayer.length;i++)
			{
				if((minScore>allPlayer[i].getScore())&&(minGeneralScore<allPlayer[i].getScore()))
				{
					minScore=allPlayer[i].getScore();
				}
			}
			minGeneralScore=minScore;
			Vector Transition1=new Vector();
			for(int i=0;i<nbPlayer;i++)
			{
				if(minScore==allPlayer[i].getScore())
				{
					Transition1.add(allPlayer[i]);
				}
			}
			if(Transition1.size()==1)
			{
				PlayerServer First[]=new PlayerServer[1];
				First[0]=(PlayerServer)Transition1.get(0);
				Transition1.removeAllElements();
				Ranking.add(First);
				PlayerManage++;
			}
			else
			{
				while(Transition1.size()>0)
				{
					PlayerServer TransitionRound[]=new PlayerServer[Transition1.size()];
					for(int i=0;i<Transition1.size();i++)
					{
						TransitionRound[i]=(PlayerServer)Transition1.get(i);
					}
					int max=-1;
					for(int i=0;i<TransitionRound.length;i++)
					{
						if(max<TransitionRound[i].getScoreRound())
						{
							max=TransitionRound[i].getScoreRound();
						}
					}
					Vector Transition2=new Vector();
					for(int i=TransitionRound.length-1;i>=0;i--)
					{
						if(max==TransitionRound[i].getScoreRound())
						{
							Transition1.remove(i);
							Transition2.add(TransitionRound[i]);
						}
					}
					if(Transition2.size()==1)
					{
						PlayerServer First[]=new PlayerServer[1];
						First[0]=(PlayerServer)Transition2.get(0);
						Transition2.removeAllElements();
						Ranking.add(First);
						PlayerManage++;
					}
					else
					{
						while(Transition2.size()>0)
						{
							PlayerServer TransitionGreen[]=new PlayerServer[Transition2.size()];
							for(int i=0;i<Transition2.size();i++)
							{
								TransitionGreen[i]=(PlayerServer)Transition2.get(i);
							}
							int minG=9999;
							for(int i=0;i<TransitionGreen.length;i++)
							{
								if(minG>TransitionGreen[i].getGreen())
								{
									minG=TransitionGreen[i].getGreen();
								}
							}
							Vector Transition3=new Vector();
							for(int i=TransitionGreen.length-1;i>=0;i--)
							{
								if(minG==TransitionGreen[i].getGreen())
								{
									Transition3.add(TransitionGreen[i]);
									Transition2.remove(i);
								}
							}
							if(Transition3.size()==1)
							{
								PlayerServer First[]=new PlayerServer[1];
								First[0]=(PlayerServer)Transition3.get(0);
								Transition3.removeAllElements();
								Ranking.add(First);
								PlayerManage++;
							}
							else
							{
								PlayerServer First[]=new PlayerServer[Transition3.size()];
								for(int i=0;i<First.length;i++)
								{
									First[i]=(PlayerServer)Transition3.get(i);
								}
								PlayerManage=PlayerManage+Transition3.size();
								Transition3.removeAllElements();
								Transition2.removeAllElements();
								Transition1.removeAllElements();
								Ranking.add(First);
							}
						}
					}
				}
			}
		}
		if(nbPlayer==PlayerManage)
		{
			PlayerServer[][] P=new PlayerServer[Ranking.size()][];
			for(int i=0;i<Ranking.size();i++)
			{
				P[i]=(PlayerServer[])Ranking.get(i);
			}
			System.out.println("");
			System.out.println("");
			System.out.println("Ranking :");
			System.out.println("");
			/*for(int i=0;i<t.length;i++)
			{
				if(t[i]!=null)
				{
					t[i].send("c");
					t[i].send("c");
					t[i].send("cRanking :");
					t[i].send("c");
				}
			}*/
			int no=0;
			String SuperEnd;
			ArrayList<Result> rk=new  ArrayList<Result>();
			for(int i=0;i<P.length;i++)
			{
				if((i!=0)&&(P[i].length>1))
					no+=P[i].length;
				/*for(int o=0;o<t.length;o++)
					{
						if(t[o]!=null)
						{
							t[o].send("c	" + (i+1+no) + "-	");
						}
					}*/
				//System.out.print("	" + (i+1+no) + "-	");
				SuperEnd="m";
				SuperEnd+=(i+1+no);
				for(int j=0;j<P[i].length;j++)
				{
					/*if((P[i].length>1)&&(j>0))
					{
						System.out.print("		");
					}*/
					//System.out.println("	   "+P[i][j].getName());
					//System.out.println("	 	  score : "+P[i][j].getScore());
					//System.out.println("	 	  Nb of rounds win : "+P[i][j].getScoreRound());
					//System.out.println("	 	  Nb of green cards used : "+P[i][j].getGreen());
				//	System.out.println("");
					
					
					Result r=new Result(P[i][j].getUUID(), i+1+no ,P[i][j].getScore(), P[i][j].getGreen(), P[i][j].getArrayRound());
					SuperEnd+="m";
					SuperEnd+=P[i][j].getScore();
					SuperEnd+="m";
					SuperEnd+=P[i][j].getScoreRound();
					SuperEnd+="m";
					SuperEnd+=P[i][j].getGreen();
					//SuperEnd+="m";
					SuperEnd+="m"+P[i][j].getName();
					/*for(int o=0;o<t.length;o++)
					{
						if(t[o]!=null)
						{
						///	t[o].send(SuperEnd);														TO DO:ENVOIE ligne
							try { Thread.sleep(60); } catch (Exception e) { }*/
							/*if((P[i].length>1)&&(j>0))
							{
								System.out.print("		");
							}
							t[o].send("c		   "+P[i][j].getName());
							t[o].send("c	 	  score : "+P[i][j].getScore());
							t[o].send("c	 	  Nb of rounds win : "+P[i][j].getScoreRound());
							t[o].send("c	 	  Nb of green cards used : "+P[i][j].getGreen());
							t[o].send("c");*/
						//}
					//}
					rk.add(r);
				}
			}
			for(int o=0;o<t.length;o++)
					{
						if(t[o]!=null)
						{
							//System.out .println("");					// TO DO:t[o].send("y");
							try
							{
								t[o].send(new EndGame (getStatData(psd) ,rk ));
							}
							catch(IOException e)
							{
								t[o].kickPlayer(t,mts);
							}
						}
					}
		}
		else
		{
			System.err.println("Some players aren't in the Ranking !");
		}
	}
	public int convertColorToInt(Color c)
		{
			if(c==Color.UNDEFINED)
				return 0;
			else if(c==Color.RED)
				return 1;
			else if(c==Color.PURPLE)
				return 2;
			else if(c==Color.BLUE)
				return 3;
			else if(c==Color.YELLOW)
				return 4;
			else if(c==Color.GREEN)
				return 5;
			return 0;
		}
		public int convertLetterToInt(char c)
		{
			if(c=='U')
				return 0;
			else if(c=='R')
				return 1;
			else if(c=='P')
				return 2;
			else if(c=='B')
				return 3;
			else if(c=='Y')
				return 4;
			else if(c=='G')
				return 5;
			return 0;
		}
		public char convertIntToLetter(int c)
		{
			if(c==0)
				return 'U';
			else if(c==1)
				return 'R';
			else if(c==2)
				return 'P';
			else if(c==3)
				return 'B';
			else if(c==4)
				return 'Y';
			else if(c==5)
				return 'G';
			return 'U';
		}
		public Color convertIntToColor(int c)
		{
			if(c==0)
				return Color.UNDEFINED;
			else if(c==1)
				return Color.RED;
			else if(c==2)
				return Color.PURPLE;
			else if(c==3)
				return Color.BLUE;
			else if(c==4)
				return Color.YELLOW;
			else if(c==5)
				return Color.GREEN;
			return Color.UNDEFINED;
		}
		public int convertTwoCoordToOne(int x,int y)
		{
			int nbPlayer=allPlayer.length;
			int cc=0;
			if(((x==6)&&(nbPlayer!=2))||(x==5)&&(nbPlayer==2))
				cc+=1;
			else if(((x==5)&&(nbPlayer!=2))||(x==4)&&(nbPlayer==2))
				cc+=3;
			else if(((x==4)&&(nbPlayer!=2))||(x==3)&&(nbPlayer==2))
				cc+=6;
			else if(((x==3)&&(nbPlayer!=2))||(x==2)&&(nbPlayer==2))
				cc+=10;
			else if(((x==2)&&(nbPlayer!=2))||(x==1)&&(nbPlayer==2))
				cc+=15;
			else if(((x==1)&&(nbPlayer!=2))||(x==0)&&(nbPlayer==2))
				cc+=21;
			else if(((x==0)&&(nbPlayer!=2)))
				cc+=28;
			cc+=y;
			return cc;
		}
		public int[] convertOneCoordToTwo(int i)
		{
			
			int nbPlayer=allPlayer.length;
			int[] cc=new int[2];
			int incr=1;
			if(nbPlayer==2)
				incr--;
			if(i==0)
			{
				cc[0]=6+incr;
				cc[1]=0;
			}
			else if((i==2)||(i==1))
			{
				cc[0]=5+incr;
				cc[1]=i-1;
			}
			else if((i<=5)&&(i>=3))
			{
				cc[0]=4+incr;
				cc[1]=i-3;
			}
			else if((i<=9)&&(i>=6))
			{
				cc[0]=3+incr;
				cc[1]=i-6;
			}
			else if((i<=14)&&(i>=10))
			{
				cc[0]=2+incr;
				cc[1]=i-10;
			}
			else if((i<=20)&&(i>=15))
			{
				cc[0]=1+incr;
				cc[1]=i-15;
			}
			else if((i<=27)&&(i>=21))
			{
				cc[0]=0+incr;
				cc[1]=i-21;
			}
			else if((nbPlayer!=2)&&((i<=35)&&(i>=28)))
			{
				cc[0]=0;
				cc[1]=i-28;
			}
			else
			{
				System.out.println("Valeur incohérente : NbPlayer :"+nbPlayer+" Valeur :"+ i);
				cc[0]=-1;
				cc[1]=-1;
			}
			return cc;
		}
    public ArrayList<StatData> getStatData(ArrayList<ProfileStatData> psd)
	{
		ArrayList<StatData> ret=new ArrayList<>();
		for(int i=0;i<psd.size();i++)
		{
			ret.add(psd.get(i).stat);
		}
		return ret;
	}
}
class PlayerServer extends Player
{
	private boolean kicked=false;
	private int turnKicked=0;
	private String uuid;
	MultiThreadServer mts;
	private ArrayList roundAr=new ArrayList<>();
	
	PlayerServer(int nbPlayer,int number,MultiThreadServer mt)
	{
		super(nbPlayer,number);
		mts=mt;
	}
	public void setUUID(String uuid)
	{
		this.uuid=uuid;
	}
	public String getUUID()
	{
		return uuid;
	}
	public ArrayList getArrayRound()
	{
		return roundAr;
	}
	public void incScoreRoundByRound(int i)
	{
		roundAr.add(i);
	}
	public void doDefeatS(clientThread t,clientThread[] threads)
	{
		if(t!=null)
		{
			try
			{
				t.send("|||You have be slain, sorry "+getName());
			}
			catch(IOException e)
			{
				t.kickPlayer(threads,mts);
				//getKicked();
			}
		}
		defeated=true;
	}
	public boolean getKicked()
	{
		return kicked;
	}
	public void kicked()
	{
		kicked=true;
		//turnKicked=t;
	}
	/*public String addCardS(Card NCard)
	{
		if(!kicked)
		{
			hand[nbCard]=NCard;
			nbCard++;
			return "h"+NCard.getColor();
		}
		return null;
	}*/
	public int doAction(int i,clientThread[] t,Board T,Move m)
	{
		/*System.out.println("WOW");
		System.out.println("WOW"+line);
		int x=Character.getNumericValue(line.charAt(1));
		int y=Character.getNumericValue(line.charAt(2));
		int color=Character.getNumericValue(line.charAt(3));*/
		int[] coor=convertOneCoordToTwo(m.position);
		int x=coor[0];
		int y=coor[1];
		int color=convertColorToInt(m.color);
		if(color!=0)
		{
			T.changeColorBoard(x,y,color);
			for(int o=0;o<t.length;o++)
			{
				if(o!=i)
				{
					if(t[o]!=null)
					{
						try
						{
						t[o].send(new NextTurn ( getUUID() , null , new PlayerInformation( getUUID(), (float)-1.0, new Move (convertTwoCoordToOne(x,y) , convertIntToColor(color)), getNbCard())));	
						}
						catch(IOException e)
							{
								t[o].kickPlayer(t,mts);
							}
					}
						//t[o].send("j"+x+y+color);					//send action
				}
			}
		}
		return color;
	}
	public int convertColorToInt(Color c)
		{
			if(c==Color.UNDEFINED)
				return 0;
			else if(c==Color.RED)
				return 1;
			else if(c==Color.PURPLE)
				return 2;
			else if(c==Color.BLUE)
				return 3;
			else if(c==Color.YELLOW)
				return 4;
			else if(c==Color.GREEN)
				return 5;
			return 0;
		}
		public int convertLetterToInt(char c)
		{
			if(c=='U')
				return 0;
			else if(c=='R')
				return 1;
			else if(c=='P')
				return 2;
			else if(c=='B')
				return 3;
			else if(c=='Y')
				return 4;
			else if(c=='G')
				return 5;
			return 0;
		}
		public char convertIntToLetter(int c)
		{
			if(c==0)
				return 'U';
			else if(c==1)
				return 'R';
			else if(c==2)
				return 'P';
			else if(c==3)
				return 'B';
			else if(c==4)
				return 'Y';
			else if(c==5)
				return 'G';
			return 'U';
		}
		public Color convertIntToColor(int c)
		{
			if(c==0)
				return Color.UNDEFINED;
			else if(c==1)
				return Color.RED;
			else if(c==2)
				return Color.PURPLE;
			else if(c==3)
				return Color.BLUE;
			else if(c==4)
				return Color.YELLOW;
			else if(c==5)
				return Color.GREEN;
			return Color.UNDEFINED;
		}
		public int convertTwoCoordToOne(int x,int y)
		{
			int nbPlayer=nbpla;
			int cc=0;
			if(((x==6)&&(nbPlayer!=2))||(x==5)&&(nbPlayer==2))
				cc+=1;
			else if(((x==5)&&(nbPlayer!=2))||(x==4)&&(nbPlayer==2))
				cc+=3;
			else if(((x==4)&&(nbPlayer!=2))||(x==3)&&(nbPlayer==2))
				cc+=6;
			else if(((x==3)&&(nbPlayer!=2))||(x==2)&&(nbPlayer==2))
				cc+=10;
			else if(((x==2)&&(nbPlayer!=2))||(x==1)&&(nbPlayer==2))
				cc+=15;
			else if(((x==1)&&(nbPlayer!=2))||(x==0)&&(nbPlayer==2))
				cc+=21;
			else if(((x==0)&&(nbPlayer!=2)))
				cc+=28;
			cc+=y;
			return cc;
		}
		public int[] convertOneCoordToTwo(int i)
		{
			int nbPlayer=nbpla;
			int[] cc=new int[2];
			int incr=1;
			if(nbPlayer==2)
				incr--;
			if(i==0)
			{
				cc[0]=6+incr;
				cc[1]=0;
			}
			else if((i==2)||(i==1))
			{
				cc[0]=5+incr;
				cc[1]=i-1;
			}
			else if((i<=5)&&(i>=3))
			{
				cc[0]=4+incr;
				cc[1]=i-3;
			}
			else if((i<=9)&&(i>=6))
			{
				cc[0]=3+incr;
				cc[1]=i-6;
			}
			else if((i<=14)&&(i>=10))
			{
				cc[0]=2+incr;
				cc[1]=i-10;
			}
			else if((i<=20)&&(i>=15))
			{
				cc[0]=1+incr;
				cc[1]=i-15;
			}
			else if((i<=27)&&(i>=21))
			{
				cc[0]=0+incr;
				cc[1]=i-21;
			}
			else if((nbPlayer!=2)&&((i<=35)&&(i>=28)))
			{
				cc[0]=0;
				cc[1]=i-28;
			}
			else
			{
				System.out.println("Valeur incohérente : NbPlayer :"+nbPlayer+" Valeur :"+ i);
				cc[0]=-1;
				cc[1]=-1;
			}
			return cc;
		}
        
}
/*
class AIServer extends AI
{
	AIServer(int nbPlayer,int number)
	{
		super(nbPlayer,number);
	}
	public void doDefeatS(clientThread t)
	{
		t.send("c");
		t.send("cYou have be slain, sorry "+getName());
		defeated=true;
	}
	public String addCardS(Card NCard)
	{
		hand[nbCard]=NCard;
		nbCard++;
		return "h"+NCard.getColor();
	}
	public int doAction(int i,clientThread[] t,Board T,String line)
	{
		int x=Character.getNumericValue(line.charAt(1));
		int y=Character.getNumericValue(line.charAt(2));
		int color=Character.getNumericValue(line.charAt(3));
		if(color!=0)
		{
			T.changeColorBoard(x,y,color);
			for(int o=0;o<t.length;o++)
			{
				if(o!=i)
				{
					t[o].send("j"+x+y+color);
				}
			}
		}
		return color;
	}
}*/