package com.pingu.party.network.dataobjects;

import java.io.Serializable;

public class PlayerInformation implements Serializable
{
	public String id;
	public float remainingTime;
	public Move lastMovePlayed;
	public int currentScore;
	
	public PlayerInformation( String UID , float rTime, Move m, int score)
	{
		this.id = UID ;
		this.remainingTime = rTime;
		this.lastMovePlayed = m ;
		this.currentScore = score;
	}
}
