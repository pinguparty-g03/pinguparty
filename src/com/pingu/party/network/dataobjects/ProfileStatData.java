package com.pingu.party.network.dataobjects;

import java.io.Serializable;

public class ProfileStatData  implements Serializable
{
	public ProfileData profile;
	public StatData stat;
	
	public ProfileStatData ( ProfileData p , StatData s)
	{
		profile = p ;
		stat = s;
	}
}
