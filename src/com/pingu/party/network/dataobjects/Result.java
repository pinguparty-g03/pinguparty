package com.pingu.party.network.dataobjects;

import java.util.ArrayList;
import java.io.Serializable;

public class Result implements Serializable
{
	public String id;
	public int rank;
	public int score;
	public int greenCardsPlayed;
	public ArrayList<Integer> roundWinnerId;
	
	public Result ( String uid , int r , int s , int gc , ArrayList<Integer> rwid)
	{
		id = uid ;
		rank = r;
		score = s;
		greenCardsPlayed = gc;
		roundWinnerId  = rwid;
	}
}
