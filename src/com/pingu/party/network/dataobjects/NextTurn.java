package com.pingu.party.network.dataobjects;

import java.util.ArrayList;
import java.util.HashMap;
import java.io.Serializable;

public class NextTurn implements Serializable
{
	public String currentPlayerID ;
	public  HashMap<Color,ArrayList<Integer>> availableMoves;
	public PlayerInformation lastPlayerInfo;
	
	public NextTurn ( String id , HashMap<Color,ArrayList<Integer>> moves , PlayerInformation pI )
	{
		currentPlayerID = id;
		availableMoves = moves;
		lastPlayerInfo = pI;
	}
	
}
