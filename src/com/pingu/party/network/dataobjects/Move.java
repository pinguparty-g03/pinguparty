package com.pingu.party.network.dataobjects;

import java.io.Serializable;
public class Move  implements Serializable
{
	public int position;
	public Color color;
	
	public Move (int pos , Color c)
	{
		position = pos;
		color = c;
	}
}
