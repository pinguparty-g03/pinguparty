package com.pingu.party.network.dataobjects;

import java.util.ArrayList;
import java.io.Serializable;

public class EndGame implements Serializable
{
	public ArrayList<StatData> stats;
	public ArrayList<Result> ranking;
	
	public EndGame ( ArrayList<StatData> psd ,  ArrayList<Result> rk)
	{
		stats = psd;
		ranking = rk;
	}
}
