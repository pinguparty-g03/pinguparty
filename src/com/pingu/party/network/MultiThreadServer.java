package com.pingu.party.network;
import java.io.PrintStream;
import java.io.IOException;
import java.net.Socket;
import java.net.ServerSocket;
import java.util.Scanner;
import java.util.Random;
import java.util.Vector;
import com.pingu.party.game.Game_Console;
import com.pingu.party.game.Card;
import com.pingu.party.game.Player;
import com.pingu.party.game.Game;
import com.pingu.party.game.AIOne;
import com.pingu.party.game.Board;
import com.pingu.party.game.Init;

import java.net.InetAddress; 

import com.pingu.party.activities.Online2;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.pingu.party.network.dataobjects.EndGame;
import com.pingu.party.network.dataobjects.Color;
import com.pingu.party.network.dataobjects.NextTurn;
import com.pingu.party.network.dataobjects.Move;
import com.pingu.party.network.dataobjects.PlayerInformation;
import com.pingu.party.network.dataobjects.ProfileData;
import com.pingu.party.network.dataobjects.ProfileStatData;
import com.pingu.party.network.dataobjects.Result;
import com.pingu.party.network.dataobjects.RoundStart;
import com.pingu.party.network.dataobjects.StatData;

import java.util.ArrayList;
import java.util.HashMap;

public class MultiThreadServer {
	private  ServerSocket serverSocket = null;
	private  Socket clientSocket = null;
	private  int maxClientsCount = 2;	
    private  clientThread[] threads;
    private  GameServer GS;
	
	
	private  int portNumber;
	
	
	private  boolean g=true;
	
	private  ArrayList<ProfileStatData> psd=new ArrayList<ProfileStatData>();
	
	 class testIfSomeoneDisappeared extends Thread {
			clientThread[] tr;
			MultiThreadServer fo;
			private volatile boolean running = true;
			testIfSomeoneDisappeared(clientThread[] t,MultiThreadServer f)
			{
				tr=t;
				fo=f;
			}
			public void stopit()
			{
				running=false;
			}
			@Override
			public void run() 
			{
				while(running)
				{
					for(int i=0;i<tr.length;i++)
					{
						if(tr[i]!=null)
						{
							try
							{
								tr[i].send("Jigme Khesar Namgyel Wangchuck");
							}
							catch(IOException e)
							{
								tr[i].closeListenerProfile();
								tr[i].closeListener();
								//tr[i]=null;
								tr[i].kickPlayer(tr,fo);
							}
						}
					}
					try{Thread.sleep(1000);}catch(Exception e){}
				}
			}
	}
	 
	 class Update extends Thread {
			private clientThread[] client;
			private MultiThreadServer fo;
			
			private volatile boolean running = true;
			Update(clientThread[] t,MultiThreadServer f)
			{
				client=t;
				fo=f;
			}
			
			public void stopit()
			{
				running=false;
			}
			
			@Override
			public void run() 
			{
				while(running)
				{
					for(int i=0;i< client.length;i++)
					{
						if(client[i] != null)
						{
							try
							{
								client[i].send("Jigme Khesar Namgyel Wangchuck");
							}
							catch(IOException e)
							{
								client[i].closeListenerProfile();
								client[i].closeListener();
								client[i].kickPlayer(client,fo);
							}
						}
					}
					try{Thread.sleep(500);}catch(Exception e){}
				}
			}
	}

	public  void removeU(String uuid)
	{
		for(int i=0;i<psd.size();i++)
		{
			if(psd.get(i).profile.id==uuid)
			{
				System.out.println("UUID "+uuid+" removed");
				psd.remove(i);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public  void main(String args[]) 
	{
		//Scanner sc = new Scanner(System.in);
		portNumber = 4242;
		String str;
		maxClientsCount=Integer.parseInt(args[0]);
		System.out.println("Max Client :" + maxClientsCount);
		threads = new clientThread[maxClientsCount];
		GS = new GameServer(maxClientsCount,psd,this);
		Player[] allPlayer0=GS.getAllPlayer();
		if (args.length < 2) 
		{
		  System.out.println("Port=" + portNumber);
		}
		else 
		{
		  portNumber = Integer.valueOf(args[1]).intValue();
		}
		try 
		{
		  serverSocket = new ServerSocket(portNumber);
		} 
		catch (IOException e) 
		{
		  System.out.println(e);
		}
		testIfSomeoneDisappeared testT=new testIfSomeoneDisappeared(threads,this);
		testT.start();
		while(g) 
		{
		  try
		  {
			clientSocket = serverSocket.accept();
			if(g)
			{
				int i = 0;
				for (i = 0; i < maxClientsCount; i++)
				{
				  if(threads[i] == null)
				  {
					System.out.println("connection");
					(threads[i] = new clientThread(clientSocket, threads,i)).start();
					try{Thread.sleep(1000);}catch(Exception e){}
					
					try
					{
						threads[i].send(psd);
					}
					catch(IOException e)
					{
						threads[i].closeListenerProfile();
						threads[i].closeListener();
						threads[i].kickPlayer(threads,this);
						break;
					}
					ProfileStatData gg=threads[i].listenerProfile();
					System.out.println("Connection 3"+(gg==null)+","+gg.profile.id+","+gg.profile.nickname);
					threads[i].setUUID(gg.profile.id);
					threads[i].setCName(gg.profile.nickname);
					psd.add(gg);
					break;
				  }
				}
				for (int j = 0; j < maxClientsCount; j++)
				{
				  if(threads[j] != null)
				  {
					System.out.println("Spot "+j+" = "+threads[j].getClientName());
					Online2.setUsersCard(threads[j].getClientName(),true, j);
				  }
				  else
				  {
					 System.out.println("Spot "+j+" = FREE");
					 Online2.setUsersCard("",false, j);
				  }
				}
			/*if (i == maxClientsCount-1) 
			{
				 g=false;
			}*/
			}
		  }
		  catch (IOException e) 
		  {
			System.out.println(e);
		  }
		}
		//GS = new GameServer(maxClientsCount);
		

	}
	public  void launchs()
	{
		g=false;
		try{Thread.sleep(600);}catch(Exception e){}
		Socket socketC=null;
		try
		{
			InetAddress localhost = InetAddress.getLocalHost(); 
			System.out.println("System IP Address : " + (localhost.getHostAddress()).trim());
			socketC = new Socket((localhost.getHostAddress()).trim(), portNumber);
		}
		catch(Exception e)
		{
			System.err.println(e);
		}
		try{socketC.close();}catch(Exception E){}
		socketC=null;
		
		for(int i=0;i<threads.length;i++)
		{
			threads[i].closeListenerProfile();
		}
		GS.setALPSD(psd);
		mixThreads();
		int dis=GS.getDis();
		GS.mainGame(threads);
	}
	@SuppressWarnings("unchecked")
	public  void mixThreads()
	{
		Vector V=new Vector();
		Random rand = new Random();
		clientThread[] threads2=new clientThread[threads.length];
		for(int i=0;i<threads.length;i++)
		{
			V.add(threads[i]);
		}
		int mx=threads.length-1;
		int mn=0;
		int x;
		for(int i=0;i<threads.length;i++)
		{
			x=rand.nextInt(mx - mn + 1) + mn;
			threads2[i]=(clientThread)V.get(x);
			V.remove(x);
			mx--;
		}
		for(int i=0;i<threads.length;i++)
		{
			threads[i]=threads2[i];
		}
	}
	
	public  void init(ArrayList<ProfileStatData> list) 
	{
		for(ProfileStatData p : list)
		{
			psd.add(p);
		}
	}
  }
