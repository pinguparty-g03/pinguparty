package com.pingu.party.network;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import com.pingu.party.game.Game_Console;
import com.pingu.party.game.Card;
import com.pingu.party.game.Player;
import com.pingu.party.game.Game;
import com.pingu.party.game.AIOne;
import com.pingu.party.game.Board;
import com.pingu.party.game.Init;


import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.pingu.party.network.dataobjects.EndGame;
import com.pingu.party.network.dataobjects.Color;
import com.pingu.party.network.dataobjects.NextTurn;
import com.pingu.party.network.dataobjects.Move;
import com.pingu.party.network.dataobjects.PlayerInformation;
import com.pingu.party.network.dataobjects.ProfileData;
import com.pingu.party.network.dataobjects.ProfileStatData;
import com.pingu.party.network.dataobjects.Result;
import com.pingu.party.network.dataobjects.RoundStart;
import com.pingu.party.network.dataobjects.StatData;

public class clientThread extends Thread
{
  private String clientName = null;
  private ObjectInputStream is = null;
  private ObjectOutputStream os = null;
  private Socket clientSocket = null;
  private final clientThread[] threads;
  private int maxClientsCount;
  private int number;
  private String uuid="";
  
  private boolean finish=false;
  private boolean finish2=false;
	public void setCName(String Nam)
	{
		clientName=Nam;
	}
  public clientThread(Socket clientSocket, clientThread[] threads,int i) 
  {
    this.clientSocket = clientSocket;
    this.threads = threads;
    maxClientsCount = threads.length;
    clientName="Player"+i;
	number=i;
	try
	 {
		  is = new ObjectInputStream(clientSocket.getInputStream());
		  os = new ObjectOutputStream(clientSocket.getOutputStream());
	  }
	   catch(IOException e)
	  {
		  
	  }
  }
  public void setUUID(String g)
  {
	  uuid=g;
  }
    public String getUUID()
  {
	  return uuid;
  }
  public void send(Object s) throws IOException
  {
	  if(this!=null)
	  {
		try
		{
			this.os.writeObject(s);
			this.os.flush();
		}
		catch(IOException e)
		{
			System.out.println("Deconnection");
			throw new IOException();
		}
	  }
  }
  public void kickPlayer(clientThread[] threads,MultiThreadServer mts)
  {
	  try
	  {
		clientSocket.close();
	  }
	  catch(IOException e)
	  {
	  }
	   for(int i=0;i<threads.length;i++)
	  {
		if(threads[i]==this)	
			threads[i]=null;
	  }
	  for(int i=0;i<threads.length;i++)
	  {
		if(threads[i]!=null)
		{
			try
			{
				threads[i].send("kick:"+threads[i].getUUID());
				mts.removeU(threads[i].getUUID());
			}
			catch(IOException e)
			{
				threads[i].kickPlayer(threads,mts);
			}
		}
	  }
  }
  public Move listener() 
  {
    //int maxClientsCount = this.maxClientsCount;
    //clientThread[] threads = this.threads;
    try 
    {
	  //send("j1");
	  //System.out.println("Wow");
      String name=clientName;
      while (!finish) {
       Object message = is.readObject();
		//System.out.println(line);
		System.out.println("Message : " + message.getClass().getName());
            if(message instanceof Move)
			{
				   synchronized (this)
					   {
						   return (Move)message;
					   }
			}
        }
    }
    catch (Exception e) 
    {
    }
	return null;
 }
 public ProfileStatData listenerProfile() 
  {
    //int maxClientsCount = this.maxClientsCount;
    //clientThread[] threads = this.threads;
    try 
    {
	  //send("j1");
	  //System.out.println("Wow");
      String name=clientName;
      while (!finish2) {
       Object message = is.readObject();
		//System.out.println(line);
		System.out.println("Message : " + message.getClass().getName());	
		//System.out.println("Ca fonctionne ?");
            if(message instanceof ProfileStatData)
			{
			//	System.out.println("Oui !");
				   synchronized (this)
					  {
						   return (ProfileStatData)message;
					  }
			}
        }
    }
    catch (Exception e) 
    {
		System.err.println("Error at listenerProfile "+e);
    }
	return null;
 }
 
 public void closeListener()
 {
	try
	{
		clientSocket.close();
	}
	catch(IOException e)
	{
	}
	finish=true;
 }
  public void closeListenerProfile()
 {
	finish2=true;
 }
 /*
   public String listenerEnd() 
  {
    try 
    {
      String name=clientName;
      while (true) {
        String line = is.readLine();
		//System.out.println(line);
		System.out.println("listenedEnd : " + line);
            if (!line.isEmpty()) 
            {
			   if(line.startsWith("k"))
			   {
				   
				   synchronized (this)
					   {
						   return line;
					   }
			   }
            }
      }
    }
    catch (IOException e) 
    {
    }
	return null;
 }*/
  public String getClientName()
  {
	  return clientName;
  }
}