package com.pingu.party.network;

import java.util.ArrayList;

import com.pingu.party.engine.game.AIProfile;
import com.pingu.party.engine.game.DistantProfile;
import com.pingu.party.engine.game.Profile;
import com.pingu.party.network.dataobjects.ProfileData;
import com.pingu.party.network.dataobjects.ProfileStatData;
import com.pingu.party.network.dataobjects.StatData;
import com.pingu.party.stats.Stat;

/**
 * @author florian
 *
 */
public class DataForProfilesAndStats {
	ArrayList<ProfileStatData> listProfilesAndStats;
		
	/**
	 * CONSTRUCTOR
	 */
	public DataForProfilesAndStats()
	{
		listProfilesAndStats = new ArrayList<ProfileStatData>();
	}
	
	/**
	 * create a new ProfileData with the database
	 * in the list
	 * @param UUID player's UUID
	 */
	public void addOldProfileAndStat(String UUID)
	{
		if(!Profile.verifyUUID(UUID))
		{
			Profile p = new Profile(UUID); // loading profile with DB
			Stat s = new Stat(UUID); // loading stats of the selected profile
			ProfileData prdata = new ProfileData(UUID, !(p.getAI()), p.getDistant(), p.getName(), p.getImage());		
			StatData stdata = new StatData(UUID, s.getWin(), s.getQuit(), s.getLoose(), s.getTotalScore(), s.getTime(), (float)s.getElo());
			
			
			ProfileStatData profileToAdd = new ProfileStatData(prdata,stdata);
			listProfilesAndStats.add(profileToAdd);
		}
		else
		{
			System.out.println("Profile "+UUID+" does not exist in the database");
		}
	}
	
	/**
	 * create new distant user in database
	 */
	public void addNewProfileAndStat(ProfileStatData psd)
	{
		if(psd.profile.isDistant && !Profile.verifyUUID(psd.profile.id))
		{
			Profile p;
			if(psd.profile.isHuman)
			{
				p = new DistantProfile(psd.profile.id,psd.profile.nickname,psd.profile.avatarUrl);
				
			}
			else
			{
				p = new AIProfile(psd.profile.id,true, psd.profile.nickname, psd.profile.avatarUrl, 666, "Unknown");
			}
			int played = psd.stat.gameWon+psd.stat.gameLost+psd.stat.gameDraw;
			Stat s = new Stat(psd.stat.id,played,psd.stat.gameWon,psd.stat.gameLost,psd.stat.gameDraw,(int)psd.stat.totalTimePlayed,(int)psd.stat.elo,psd.stat.totalScore);
			p.createUser(s);
			
			ProfileData prdata = new ProfileData(p.getUUID(), !(p.getAI()), p.getDistant(), p.getName(), p.getImage());		
			StatData stdata = new StatData(s.getUUID(), s.getWin(), s.getQuit(), s.getLoose(), s.getTotalScore(), s.getTime(), (float)s.getElo());
			ProfileStatData profileToAdd = new ProfileStatData(prdata,stdata);
			listProfilesAndStats.add(profileToAdd);
		}
	}
	
	/**
	 * @return list of data profiles and stats
	 */
	public ArrayList<ProfileStatData> getList()
	{
		return this.listProfilesAndStats;
	}
	
	public ProfileStatData getProfileStatData(String uuid)
	{
		for(ProfileStatData p : listProfilesAndStats)
		{
			if(p.profile.id.equals(uuid))
			{
				return p;
			}
		}
		System.out.println("Profile "+uuid+" does not exist in the list");
		return null;
	}
	
	public Profile getProfile(String uuid)
	{
		for(ProfileStatData p : listProfilesAndStats)
		{
			if(p.profile.id.equals(uuid))
			{
				return new Profile(uuid);
			}
		}
		System.out.println("Profile "+uuid+" does not exist in the list");
		return null;
	}
}
