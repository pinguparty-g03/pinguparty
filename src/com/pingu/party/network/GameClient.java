package com.pingu.party.network;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.Scanner;
import java.util.Vector;
import com.pingu.party.game.Game_Console;
import com.pingu.party.game.Card;
import com.pingu.party.game.Player;
import com.pingu.party.game.Game;
import com.pingu.party.game.AIOne;
import com.pingu.party.game.Board;
import com.pingu.party.game.Init;
import com.pingu.party.activities.Partie;
import com.pingu.party.engine.structs.SScores;
import com.pingu.party.activities.Menu;

import com.pingu.party.engine.game.Profile;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.pingu.party.network.dataobjects.EndGame;
import com.pingu.party.network.dataobjects.Color;
import com.pingu.party.network.dataobjects.NextTurn;
import com.pingu.party.network.dataobjects.Move;
import com.pingu.party.network.dataobjects.PlayerInformation;
import com.pingu.party.network.dataobjects.ProfileData;
import com.pingu.party.network.dataobjects.ProfileStatData;
import com.pingu.party.network.dataobjects.Result;
import com.pingu.party.network.dataobjects.RoundStart;
import com.pingu.party.network.dataobjects.StatData;

import java.util.ArrayList;
import java.util.HashMap;


public class GameClient {
	private static GameAccess access;
	public int getNumber()
	{
		return access.getNumber();
	}
	public void close()
	{
		access.close();
	}
    static class GameAccess
    {
        private Socket socket;
        private ObjectOutputStream outputStream;
		private ObjectInputStream in;
        private int number=0;
        private Board B;
        private Player allPlayer;
        private int nbPlayer;
		private boolean AI=false;
		
		private int round=0;
		
		private Profile p;
		
		Thread receivingThread;
		private HashMap<String,String> named=new HashMap<String,String>();
		
		////////////////////////////////////
		//autre profile ?
		////////////////////////////////////
		
		//pivate int difficulty=0;
		
		boolean finish=false;
		
		private Partie Pa;
		SScores scores = new SScores();
		
		
		class testIfServerDisappeared extends Thread {
			GameAccess tr;
			private volatile boolean running = true;
			testIfServerDisappeared(GameAccess t)
			{
				tr=t;
			}
			public void stopit()
			{
				running=false;
			}
			@Override
			public void run() 
			{
				while(running)
				{
							try
							{
								tr.notSaveSend("Jigme Khesar Namgyel Wangchuck");
							}
							catch(Exception e)
							{
								tr.close();
								running=false;
								this.stop();
							}
							try{Thread.sleep(3000);}catch(Exception e){}
				}
			}
	}
		
		
		
		public void setProfile(Profile pk)
		{
			p=pk;
		}
		public int getNumber()
		{
			return number;
		}
		public void putPa(Partie n)
		{
			Pa=n;
		}
        public void InitSocket(String server, int port,boolean IA) throws IOException 
        {
			this.p=p;
			AI=IA;
            socket = new Socket(server, port);
            outputStream = new ObjectOutputStream(socket.getOutputStream());
			in=new ObjectInputStream(socket.getInputStream());
			testIfServerDisappeared testTh=new testIfServerDisappeared(this);
			testTh.start();
            receivingThread = new Thread() 
            {
                @Override
                public void run() 
                {
                    try 
                    {
						System.out.println("\n\n\n\n\n\n\n");
                        Object message=null;
						DataForProfilesAndStats d=new DataForProfilesAndStats();
						d.addOldProfileAndStat(p.getUUID());
						ProfileStatData myProfile=d.getProfileStatData(p.getUUID());						// TO DO: send profile
						//System.out.println("envoie ca : "+(myProfile==null)+p.getUUID());						
						send(myProfile);
						while (!finish)
                        {
							try
							{
								message = in.readObject();	
							}
							catch (SocketException ex) 
							{
								close();
							}
							catch(Exception e)
							{
								System.err.println("Error at ReadObject"+e);
							}
							System.out.println("Object received : "+message.getClass().getName());
							if(message instanceof EndGame)
							{
								System.out.println("EndGameAction");
								EndGame transfert=(EndGame)message;
								ArrayList<StatData> stats=transfert.stats;
								ArrayList<Result> ranking=transfert.ranking;
								for(int i=0;i<stats.size();i++)
								{
									System.out.println("Stat Data: id :"+stats.get(i).id+" gameWon :"+stats.get(i).gameWon+" gameDraw : "+stats.get(i).gameDraw+" gameLost :"+stats.get(i).gameLost+" totalScore "+stats.get(i).totalScore+" totalTimePlayer "+stats.get(i).totalTimePlayed+" elo "+stats.get(i).elo);
								}
								for(int i=0;i<ranking.size();i++)
								{
									System.out.println("id : "+ranking.get(i).id+" rank"+ranking.get(i).rank+" score"+ranking.get(i).score+" green"+ranking.get(i).greenCardsPlayed+" nbRoundWin"+ranking.get(i).roundWinnerId.size());
									String name="";																																																						//TO DO: get name by uuid
									scores.addScore(name,ranking.get(i).score,ranking.get(i).roundWinnerId.size(),ranking.get(i).greenCardsPlayed,ranking.get(i).rank);
									Pa.Endo(scores);
								}
								
							}
							else if(message instanceof NextTurn)
							{
								System.out.println("NextTurnAction");
								NextTurn transfert=(NextTurn)message;
								System.out.println("Player uid :"+transfert.currentPlayerID+" Number playe :"+transfert.availableMoves.size()+" Player info : id :"+transfert.lastPlayerInfo.id+" remainingTime :"+transfert.lastPlayerInfo.remainingTime+" movePosition :"+transfert.lastPlayerInfo.lastMovePlayed.position+" color :"+transfert.lastPlayerInfo.lastMovePlayed.color+" currentScore : "+transfert.lastPlayerInfo.currentScore);
								if((transfert.availableMoves==null)||(transfert.availableMoves.size()==0))
								{   int[] coord=convertOneCoordToTwo(transfert.lastPlayerInfo.lastMovePlayed.position);
									if(coord[0]!=-1)
									{
										B.changeColorBoard(coord[0],coord[1],convertColorToInt(transfert.lastPlayerInfo.lastMovePlayed.color));
									}
								}
								else
								{
									Pa.toDisplayTempString("Your turn");
									Thread turn = new Thread() 
									{
										@Override
										public void run() 
										{
											Turn();
										}
									};
									turn.start();
								}
							}
							else if(message instanceof PlayerInformation)
							{
								System.out.println("PlayerInformation");
								PlayerInformation transfert=(PlayerInformation)message;
								System.out.println(transfert.id+" remainingTime :"+transfert.remainingTime+" movePosition :"+transfert.lastMovePlayed.position+" color :"+transfert.lastMovePlayed.color+" currentScore : "+transfert.currentScore);
							}
							else if(message instanceof ProfileStatData)
							{
								System.out.println("ProfileStatData");
								ProfileStatData transfert=(ProfileStatData)message;
								ProfileData transfert1=transfert.profile;
								StatData transfert2=transfert.stat;
								System.out.println("ProfileData id :"+transfert1.id+" isHuman"+transfert1.isHuman+" isDistant"+transfert1.isDistant+" nickname"+transfert1.nickname+" avatarUrl"+transfert1.avatarUrl);
								System.out.println("Stat Data: id :"+transfert2.id+" gameWon :"+transfert2.gameWon+" gameDraw : "+transfert2.gameDraw+" gameLost :"+transfert2.gameLost+" totalScore "+transfert2.totalScore+" totalTimePlayer "+transfert2.totalTimePlayed+" elo "+transfert2.elo);
								
								Pa.toDisplayTempString(transfert1.nickname+" est connecté");
								named.put(transfert1.id,transfert1.nickname);
								//////////////////////////////////////////////
								//TO DO: Ajout de profile distant
								//////////////////////////////////////////////
							}
							else if(message instanceof Result)
							{
								System.out.println("ResultAction");
								Result transfert=(Result)message;
								System.out.println("id : "+transfert.id+" rank"+transfert.rank+" score"+transfert.score+" green"+transfert.greenCardsPlayed+" nbRoundWin"+transfert.roundWinnerId.size());
							}
							else if(message instanceof RoundStart)
							{
								System.out.println("RoundStartAction");
								RoundStart transfert=(RoundStart)message;
								System.out.println("Post");
								System.out.println("nbPlayer : "+transfert.nbPlayers+" hand"+transfert.hand+" ColorDis "+transfert.excludedCard+" blitz "+transfert.blitzTimer+" lastRoundWinnerID"+transfert.lastRoundWinnerID+" lol"+transfert.misc.getClass().getName());
								nbPlayer=transfert.nbPlayers;
								reset();
								round++;
								Pa.toDisplayTempString("Round "+round);
								B.changeDis(new Card(convertColorToInt(transfert.excludedCard)));
								String hand=transfert.hand;
								for(int i=0;i<hand.length();i++)
								{
									allPlayer.addCard(new Card(Character.getNumericValue(convertLetterToInt(hand.charAt(i)))));
								}
								System.out.println("Post_RoundStart");
							}
							/*else if(message instanceof StatData)
							{
								System.out.println("RoundStartAction");
								StatData transfert=(StatData)message;
								System.out.println("Stat Data: id :"+transfert.id+" gameWon :"+transfert.gameWon+" gameDraw : "+transfert.gameDraw+" gameLost :"+transfert.gameLost+" totalScore "+transfert.totalScore+" totalTimePlayer "+transfert.totalTimePlayed+" elo "+transfert.elo);
							}*/
							else if(message instanceof ArrayList)
							{
								ArrayList<ProfileStatData> transfert=(ArrayList<ProfileStatData>)message;
								//System.out.println("nbPlayer : "+transfert.nbPlayers+" hand"+transfert.hand+" ColorDis "+transfert.excludedCard+" blitz "+transfert.blitzTimer+" lastRoundWinnerID"+transfert.lastRoundWinnerID+" lol"+transfert.misc.getClass().getName());
								System.out.println("hey ! "+transfert.size());
								for(int i=0;i<transfert.size();i++)
								{
									ProfileStatData transfert2=transfert.get(i);
									ProfileData transfert4=transfert2.profile;
									StatData transfert3=transfert2.stat;
									Pa.toDisplayTempString(transfert4.nickname+" est connecté");
									named.put(transfert4.id,transfert4.nickname);
									System.out.println("arr	y ! "+transfert4.id+" "+named.size());
									//Ajout profile
								}
								System.out.println("Post_RoundStart");
							}
							else if(message instanceof String)
							{
								//System.out.println("It's a String : "+(String)message);
								String transfert=(String)message;
								if(transfert.startsWith("kick:"))
								{
									//System.out.println("Kick "+transfert);
									String uuidPlayerKicked="";
									for(int i=5;i<transfert.length();i++)
									{
										uuidPlayerKicked+=transfert.charAt(i);
									}
									/*System.out.println("Kick "+transfert+"    "+named.containsKey(uuidPlayerKicked));
									
									for (String key : named.keySet()) {
										String value = named.get(key);
										System.out.println(" Key :"+key+"  Value:"+value);
									}
									
									*/
									if(named.containsKey(uuidPlayerKicked))
									{
										Pa.toDisplayTempString(named.get(uuidPlayerKicked)+" est déconnecté");
										named.remove(uuidPlayerKicked);
									}
								}
								else if(transfert.startsWith("|||"))
								{
									String text="";
									for(int i=3;i<transfert.length();i++)
									{
										text+=transfert.charAt(i);
									}
									Pa.toDisplayTempString(text);
								}
							}
							else
								System.out.println("Cette Classe"+message.getClass().getName()+"n'est pas gérer");
						}
                            
                    } 
					catch (Exception ex) 
                    {
						System.out.println(ex);
                    }
                }
            };
            receivingThread.run();
			//System.out.println("HEY PostCrash");
			//T5.stopit();
        }
		public void reset()
		{
			allPlayer.changeNbCard(0);
			allPlayer.changeTurnDeath(0);
			allPlayer.resetDefeat();
			B.createTable(nbPlayer);
		}
		public void Turn()
				{
					/*System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("");*/
					Pa.toDisplayTempString("It's your turn "+allPlayer.getName());
					Vector K=new Vector();
					K=allPlayer.possibilityPlayer(B);
					int a=(int)K.get(0);
					int b=(int)K.get(1);
					int c=(int)K.get(2);
					if(c==5)
					{
						allPlayer.increaseGreen();
					}
					if(c==0)
					{
						//send("j990");
						send(new Move(convertTwoCoordToOne(a,b),Color.UNDEFINED));
					}
					else
					{
						allPlayer.removeCard(c);
						B.changeColorBoard(a,b,c);
						send(new Move(convertTwoCoordToOne(a,b),convertIntToColor(c)));
						B.removeX();
					}
					
				//	B.toDisplay();
		}
		public int convertColorToInt(Color c)
		{
			if(c==Color.UNDEFINED)
				return 0;
			else if(c==Color.RED)
				return 1;
			else if(c==Color.PURPLE)
				return 2;
			else if(c==Color.BLUE)
				return 3;
			else if(c==Color.YELLOW)
				return 4;
			else if(c==Color.GREEN)
				return 5;
			return 0;
		}
		public int convertLetterToInt(char c)
		{
			if(c=='U')
				return 0;
			else if(c=='R')
				return 1;
			else if(c=='P')
				return 2;
			else if(c=='B')
				return 3;
			else if(c=='Y')
				return 4;
			else if(c=='G')
				return 5;
			return 0;
		}
		public char convertIntToLetter(int c)
		{
			if(c==0)
				return 'U';
			else if(c==1)
				return 'R';
			else if(c==2)
				return 'P';
			else if(c==3)
				return 'B';
			else if(c==4)
				return 'Y';
			else if(c==5)
				return 'G';
			return 'U';
		}
		public Color convertIntToColor(int c)
		{
			if(c==0)
				return Color.UNDEFINED;
			else if(c==1)
				return Color.RED;
			else if(c==2)
				return Color.PURPLE;
			else if(c==3)
				return Color.BLUE;
			else if(c==4)
				return Color.YELLOW;
			else if(c==5)
				return Color.GREEN;
			return Color.UNDEFINED;
		}
		public int convertTwoCoordToOne(int x,int y)
		{
			int cc=0;
			if(((x==6)&&(nbPlayer!=2))||(x==5)&&(nbPlayer==2))
				cc+=1;
			else if(((x==5)&&(nbPlayer!=2))||(x==4)&&(nbPlayer==2))
				cc+=3;
			else if(((x==4)&&(nbPlayer!=2))||(x==3)&&(nbPlayer==2))
				cc+=6;
			else if(((x==3)&&(nbPlayer!=2))||(x==2)&&(nbPlayer==2))
				cc+=10;
			else if(((x==2)&&(nbPlayer!=2))||(x==1)&&(nbPlayer==2))
				cc+=15;
			else if(((x==1)&&(nbPlayer!=2))||(x==0)&&(nbPlayer==2))
				cc+=21;
			else if(((x==0)&&(nbPlayer!=2)))
				cc+=28;
			cc+=y;
			return cc;
		}
		public int[] convertOneCoordToTwo(int i)
		{
			int[] cc=new int[2];
			int incr=1;
			if(nbPlayer==2)
				incr--;
			if(i==0)
			{
				cc[0]=6+incr;
				cc[1]=0;
			}
			else if((i==2)||(i==1))
			{
				cc[0]=5+incr;
				cc[1]=i-1;
			}
			else if((i<=5)&&(i>=3))
			{
				cc[0]=4+incr;
				cc[1]=i-3;
			}
			else if((i<=9)&&(i>=6))
			{
				cc[0]=3+incr;
				cc[1]=i-6;
			}
			else if((i<=14)&&(i>=10))
			{
				cc[0]=2+incr;
				cc[1]=i-10;
			}
			else if((i<=20)&&(i>=15))
			{
				cc[0]=1+incr;
				cc[1]=i-15;
			}
			else if((i<=27)&&(i>=21))
			{
				cc[0]=0+incr;
				cc[1]=i-21;
			}
			else if((nbPlayer!=2)&&((i<=35)&&(i>=28)))
			{
				cc[0]=0;
				cc[1]=i-28;
			}
			else
			{
				System.out.println("Valeur incohérente : NbPlayer :"+nbPlayer+" Valeur :"+ i);
				cc[0]=-1;
				cc[1]=-1;
			}
			return cc;
		}
        //private static final String CRLF = "\r\n"; 
        /*public void send(String text)
         {
            try 
            {
                outputStream.write((text + CRLF).getBytes());
                outputStream.flush();
            } 
            catch (IOException ex) 
            {
            }
        }*/
		public void send(Object obj)
         {
            try 
            {
                outputStream.writeObject(obj);
                outputStream.flush();
            } 
			catch (SocketException ex) 
			{
						close();
			}
            catch (IOException ex) 
            {
				////////////////Deconnection
				close();
            }
        }
		public void notSaveSend(Object obj) throws IOException
         {
			 try{
                outputStream.writeObject(obj);
                outputStream.flush();
			 }
			 catch(IOException e)
			 {
				 throw new IOException();
			 }
        }
        public void close() 
        {
            try 
            {
				finish=true;
				receivingThread.stop();
				Pa.toDisplayTempString("Closing Thread");
                socket.close();
				Pa.Endo(scores);
            } 
            catch (IOException ex)
            {
            }
        }
    }
    public void init(String server,int port,Profile p,Partie ppp)
    {
        access = new GameAccess();
		access.putPa(ppp);
		access.setProfile(p);
        try 
        {
            access.InitSocket(server,port,p.getAI());
        } 
        catch (IOException ex)
        {
            System.out.println("Cannot connect to " + server + ":" + port);
            ex.printStackTrace();
            System.exit(0);
        }
    }
}
