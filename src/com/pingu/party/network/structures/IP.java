package com.pingu.party.network.structures;

public class IP {
  private int v4binary = 2130706433;
  private boolean v4 = true;

  public IP(int a, int b, int c, int d) {
    this.v4 = true;
    this.v4binary = (a<<24)+(b<<16)+(c<<8)+d;
  }
  public IP(IP i) {
    this.v4 = i.isV4();
    if(this.v4) {
      this.v4binary = i.getRaw();
    }
  }

  public static boolean isValid(String s) {
    if(s==null || s.isEmpty() || s.compareTo("")==0) return false;
    if (s.endsWith(".")) return false;
    String[] blocks = s.split("\\.");
    if(blocks.length !=4) return false; //TODO: v6 ?
    for (String b : blocks) {
      try {
        if(b==null ||b.isEmpty()) return false;
        int i = Integer.parseInt(b);
        if(i>=255||i<0) return false;
      } catch (Exception e) {
        return false;
      }
    }
    return true;
  }

  public static IP parseIP(String s) {
    if(isValid(s)) {
      String[] blocks = s.split("\\.");
      int[] b = new int[4];
      for (int i = 0;i<4 ;i++ ) b[i] = Integer.parseInt(blocks[i]);
      return new IP(b[0],b[1],b[2],b[3]);
    }
    return null;
  }

  public boolean isV4() {
    return this.v4;
  }

  public int getBlock(int b) {
    if(this.v4) {
      if(b<4) {
        return ((this.v4binary>>24-b*8)&255);
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  }

  public int getRaw() {
    if(this.v4) {
      return this.v4binary;
    }
    return 0;
  }

	/**
	* Create string representation of IP for printing
	* @return
	*/
	@Override
	public String toString() {
    if(this.v4) {
      return "" + getBlock(0)+"."+getBlock(1)+"."+getBlock(2)+"."+getBlock(3);
    }
    return null;
	}
}
