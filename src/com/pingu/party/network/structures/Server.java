package com.pingu.party.network.structures;

public class Server {
  String name;
  IP adress;
  Status state;

  private int port = 4242;

  public static enum Status {
    OPEN,
    IN_GAME,
    OFFLINE
  }

  public Server(String name, IP adress) {
    this.name = name;
    this.adress = adress;
    this.state = Status.OFFLINE;
  }

  public Status getStatus() {
    return this.state;
  }

  public String  getName() {
    return this.name;
  }

  public String getIPText() {
    return this.adress.toString();
  }

	/**
	* Create string representation of Server for printing
	* @return
	*/
	@Override
	public String toString() {
		return this.name+"@"+this.adress.toString()+":"+this.port+"("+this.state+")";
	}
}
