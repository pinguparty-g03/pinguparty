package com.pingu.party.launcher.Utils;

import java.io.FileInputStream;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.BufferedReader;
import com.pingu.party.engine.config.Config;
import com.pingu.party.engine.core.StringManager;

public class StringComputer {
  String localBase = "en_T";
  public StringComputer() {
    InitAll();
  }

  public void setLocation() {
    switch (Config.LANG) { // de, en, fr, jp
      case "de": localBase = "de_T"; break;
      case "en": localBase = "en_T"; break;
      case "fr": localBase = "fr_T"; break;
      case "jp": localBase = "jp_T"; break;
      default: localBase = "en_T"; break;
    }
  }

  private String getConfigFile() {
    switch (this.localBase) { // de, en, fr, jp
      case "de_T": return "config/locale/de.lang";
      case "en_T": return "config/locale/en.lang";
      case "fr_T": return "config/locale/fr.lang";
      case "jp_T": return "config/locale/jp.lang";
      default: return "config/locale/en.lang";
    }
  }

  private void setupCoreStrings() {


    try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(getConfigFile()), "UTF-8"))) {
      for(String line; (line = br.readLine()) != null; ) {
        if (line.contains("=") && line.length()>4 && line.indexOf("=")<line.length()-1) {
          StringManager.add(line.split("=",2)[0],line.split("=",2)[1]);
        }
      }
    } catch (Exception e) { }
    System.out.println("Strings Done !");

  }

  private void InitAll() {
    setLocation();
    setupCoreStrings();
  }
}
