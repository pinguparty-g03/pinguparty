package com.pingu.party.launcher.Utils.Font;

import com.pingu.party.engine.config.Config;
import java.io.File;

import java.awt.GraphicsEnvironment;
import java.awt.Font;

public class Roboto {
  public int f_size = 16;
  private Font _get(String W) {

    try {
      Font rtrn = null;
      // Actually using noto because roboto seems to have antialisaing issues
      if(Config.LANG.contains("jp")) {
        switch (W) {
          case "Bold": rtrn = Font.createFont(Font.TRUETYPE_FONT, new File("res/Fonts/mgenp/rounded-mgenplus-2m-bold.ttf")).deriveFont((float)this.f_size); break;
          case "Light": rtrn = Font.createFont(Font.TRUETYPE_FONT, new File("res/Fonts/mgenp/rounded-mgenplus-2m-light.ttf")).deriveFont((float)this.f_size); break;
          case "Medium": rtrn = Font.createFont(Font.TRUETYPE_FONT, new File("res/Fonts/mgenp/rounded-mgenplus-2m-medium.ttf")).deriveFont((float)this.f_size); break;
          case "Thin": rtrn = Font.createFont(Font.TRUETYPE_FONT, new File("res/Fonts/mgenp/rounded-mgenplus-2m-thin.ttf")).deriveFont((float)this.f_size); break;
          default: rtrn = Font.createFont(Font.TRUETYPE_FONT, new File("res/Fonts/mgenp/rounded-mgenplus-2m-regular.ttf")).deriveFont((float)this.f_size); break;
        }
      } else {
        rtrn = Font.createFont(Font.TRUETYPE_FONT, new File("res/Fonts/NotoSans-"+W+".ttf")).deriveFont((float)this.f_size);
      }
      GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(rtrn);
      rtrn = rtrn.deriveFont((int)this.f_size);
      return rtrn;
    } catch(Exception ex) {
      ex.printStackTrace(); //TODO: Error manager
      return new Font("sans-serif", Font.PLAIN, (int)f_size);
    }

  }

  public Font get() {
    return this._get("Regular");
  }
  public Font get(int size) {
    return this._get("Regular").deriveFont((size>2)?((float)size):(16));
  }
  public Font get_regular() {
    return this._get("Regular");
  }
  public Font get_regular(int size) {
    return this._get("Regular").deriveFont((size>2)?((float)size):(16));
  }
  public Font get_black() {
    return this._get("Black");
  }
  public Font get_black(int size) {
    return this._get("Black").deriveFont((size>2)?((float)size):(16));
  }
  public Font get_blackitalic() {
    return this._get("BlackItalic");
  }
  public Font get_bold() {
    return this._get("Bold");
  }
  public Font get_bold(int size) {
    return this._get("Bold").deriveFont((size>2)?((float)size):(16));
  }
  public Font get_boldcondensed() {
    return this._get("CondensedBold");
  }
  public Font get_boldcondensed(int size) {
    return this._get("CondensedBold").deriveFont((size>2)?((float)size):(16));
  }
  public Font get_boldcondenseditalic() {
    return this._get("BoldCondensedItalic");
  }
  public Font get_bolditalic() {
    return this._get("BoldItalic");
  }
  public Font get_italic() {
    return this._get("Italic");
  }
  public Font get_italic(int size) {
    return this._get("Italic").deriveFont((size>2)?((float)size):(16));
  }
  public Font get_light() {
    return this._get("Light");
  }
  public Font get_light(int size) {
    return this._get("Light").deriveFont((size>2)?((float)size):(16));
  }
  public Font get_lightitalic() {
    return this._get("LightItalic");
  }
  public Font get_medium() {
    return this._get("Medium");
  }
  public Font get_medium(int size) {
    return this._get("Medium").deriveFont((size>2)?((float)size):(16));
  }
  public Font get_mediumitalic() {
    return this._get("MediumItalic");
  }
  public Font get_thin() {
    return this._get("Thin");
  }
  public Font get_thinitalic() {
    return this._get("ThinItalic");
  }
}
