package com.pingu.party.launcher.Utils.Font;

import java.io.File;

import java.awt.GraphicsEnvironment;
import java.awt.Font;

public class MaterialIcon {
  public int f_size = 16;

  public Font get(float size) {

    try {
      Font rtrn = null;
      rtrn = Font.createFont(Font.TRUETYPE_FONT, new File("res/Fonts/MaterialIcons-Regular.ttf")).deriveFont((size>2)?(size):(float)this.f_size);

      GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(rtrn);
      rtrn = rtrn.deriveFont((size>2)?(size):(float)this.f_size);
      return rtrn;
    } catch(Exception ex) {
      ex.printStackTrace(); //TODO: Error manager
      return new Font("sans-serif", Font.PLAIN, (int)f_size);
    }
  }
  public Font get() {

    try {
      Font rtrn = null;
      rtrn = Font.createFont(Font.TRUETYPE_FONT, new File("res/Fonts/MaterialIcons-Regular.ttf")).deriveFont((float)this.f_size);

      GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(rtrn);
      rtrn = rtrn.deriveFont((float)this.f_size);
      return rtrn;
    } catch(Exception ex) {
      ex.printStackTrace(); //TODO: Error manager
      return new Font("sans-serif", Font.PLAIN, (int)f_size);
    }
  }
}
