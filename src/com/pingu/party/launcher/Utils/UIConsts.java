package com.pingu.party.launcher.Utils;

import com.pingu.party.launcher.Utils.Font.MaterialIcon;
import com.pingu.party.launcher.Utils.Font.Roboto;

import java.awt.Font;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.RenderingHints;

public class UIConsts {
  // INT Constants
  public static final int LPANEL_WIDTH = 800;
  public static final int LPANEL_HEIGHT = 400;
  public static final int L_MARGIN = 48;
  public static final int SEPARATOR_WIDTH = 4;
  public static final int EXITB_WIDTH = 80;

  // STRING Constants
  public static final String LPANEL_TITLE = "Pingu Launcher";
  public static final String LOADING_TEXT = "WAITING";

  // COLOR Constants
  public static final Color COLOR_BACKGROUND = new Color(36, 54, 61);
  public static final Color COLOR_BLUE = new Color(87, 161, 211);
  public static final Color COLOR_GREY = new Color(74, 111, 125);
  public static final Color COLOR_MIDGREY = new Color(132, 142, 146);
  public static final Color COLOR_MIDGREY_A = new Color(132, 142, 146, 128);
  public static final Color COLOR_BUTTON = new Color(72, 87, 92);
  public static final Color COLOR_SEMI_BUTTON = new Color(72, 87, 92, 128);
  public static final Color COLOR_BUTTON_BORDER = new Color(86, 99, 104);
  public static final Color COLOR_BUTTON_HOVER = new Color(88, 125, 144);
  public static final Color COLOR_WHITE = new Color(231, 231, 231);
  public static final Color COLOR_FOREGROUND = new Color(193, 198, 200);

  public static final Color COLOR_DEBUG_RED = new Color(194, 59, 4, 42);
  public static final Color COLOR_DEBUG_GREEN = new Color(71, 198, 2, 42);

  public static final Color COLOR_AXIS_1 = new Color(110, 70, 150);
  public static final Color COLOR_AXIS_2 = new Color(118, 158, 174);
  public static final Color COLOR_AXIS_3 = new Color(195, 150, 61);
  public static final Color COLOR_AXIS_4 = new Color(173, 83, 83);
  public static final Color COLOR_AXIS_R1 = new Color(131, 91, 157);
  public static final Color COLOR_AXIS_R2 = new Color(95, 113, 85);
  public static final Color COLOR_AXIS_R3 = new Color(123, 89, 75);
  public static final Color COLOR_AXIS_R4 = new Color(120, 83, 105);

  //public Font _roboto_light = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("Fonts/Roboto-Light.ttf").openStream());
  // FONT Constants
  public static final Font ROBOTO_LIGHT = new Roboto().get_light();
  public static final Font ROBOTO_BOLD = new Roboto().get_bold();
  public static final Font ROBOTO_ITALIC = new Roboto().get_italic();
  public static final Font FONT_TITLE = new Roboto().get_light();
  public static final Font BUTTON_TEXT = new Roboto().get_bold(28);
  public static final Font MATERIALICONS = new MaterialIcon().get(24);
  public static final Font WINDOWS_TITLE = new Font("Segoe UI", Font.PLAIN, 16);

  // Generators
  public static Graphics2D l_G2D(Graphics gr) {
    Graphics2D rtrn = (Graphics2D) gr;
    RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    rh.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
    rtrn.addRenderingHints(rh);
    return rtrn;
  }

}
