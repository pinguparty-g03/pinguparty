package com.pingu.party.launcher.Utils ;


import java.awt.GraphicsEnvironment;
import java.awt.Font;

import java.io.IOException;

public class Fonts {
  protected float f_size = 16f;
  protected String f_name, f_face, f_type;
  private String f_path;

  //TODO: Generation like below

  public Font get() {
    try {
      Font rtrn = null;
      String path = this.f_path +"-"+this.f_face+this.f_type;
      rtrn = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource(path).openStream());
      GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(rtrn);
      rtrn = rtrn.deriveFont((int)this.f_size);
      return rtrn;
    } catch(Exception err) {
      err.printStackTrace(); //TODO: Error manager
      return new Font("sans-serif", Font.PLAIN, (int)f_size);
    }
  }

	/**
	* Returns value of font size
	* @return
	*/
	public float get_size() {
		return this.f_size;
	}

	/**
	* Sets new value of font size
	* @param float size
	*/
	public void set_size(float f_size) {
		this.f_size = (f_size>0)?f_size:(f_size<0)?(-f_size):16f;
	}

	/**
	* Default empty Fonts constructor
	*/
	public Fonts() {
		super();
    this.f_size = 16f;
    this.f_name = "Roboto";
    this.f_path = "Fonts/"+this.f_name;
    this.f_face = "Regular";
    this.f_type = "ttf";
	}

  /**
  * Default Fonts constructor
  */
  public Fonts(String f_name) {
    super();
    this.f_size = 16f;
    this.f_name = (f_name != "")?(f_name):"Roboto";
    this.f_path = "Fonts/"+this.f_name;
  }

	/**
	* Default Fonts constructor
	*/
	public Fonts(float f_size, String f_name) {
		super();
		this.f_size = 16f;
		this.f_name = (f_name != "")?(f_name):"Roboto";
    this.f_path = "Fonts/"+this.f_name;

    this.set_size(f_size);
	}
}
