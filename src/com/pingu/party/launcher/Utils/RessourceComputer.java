package com.pingu.party.launcher.Utils;

import com.pingu.party.gui.widget.WTitle;
import com.pingu.party.engine.config.Config;
import java.awt.Image;
import com.pingu.party.engine.core.AssetManager;
import com.pingu.party.gui.widget.WBackground;
import com.pingu.party.gui.widget.Widget;
import java.awt.Color;
import com.pingu.party.gui.widget.WTitleBar;

public class RessourceComputer {
  public RessourceComputer() {
    InitAll();
  }

  private void setupCoreAssets() {
    String localBase = "en";
    if(Config.LANG == null) Config.getFromFile();
    switch (Config.LANG) { // de, en, fr, jp
      case "de": localBase = "de_T"; break;
      case "en": localBase = "en_T"; break;
      case "fr": localBase = "fr_T"; break;
      case "jp": localBase = "jp_T"; break;
      default: localBase = "en_T"; break;
    }

    for (int i = 1; i<96;i++ ) {
      AssetManager.queueImage("splash"+i,"/UI/split-png/splash"+i+".png");
    }

    AssetManager.queueImage("logo-linear","/UI/menu/logo-linear.png");

    /* Buttons */

    AssetManager.queueImage("returnBHT","/UI/menu/buttons/Ht/"+localBase+"/returnBHT.png");
    AssetManager.queueImage("startBHT","/UI/menu/buttons/Ht/"+localBase+"/startBHT.png");
    AssetManager.queueImage("joinBHT","/UI/menu/buttons/Ht/"+localBase+"/joinBHT.png");
    AssetManager.queueImage("changeBHT","/UI/menu/buttons/Ht/"+localBase+"/changeBHT.png");

    AssetManager.queueImage("hostHT","/UI/menu/buttons/Ht/"+localBase+"/hostHT.png");
    AssetManager.queueImage("joinHT","/UI/menu/buttons/Ht/"+localBase+"/joinHT.png");

    AssetManager.queueImage("sort","/UI/menu/buttons/li/sort.png");
    AssetManager.queueImage("sort-136","/UI/menu/buttons/li/sort-136.png");
    AssetManager.queueImage("sort-96","/UI/menu/buttons/li/sort-96.png");
    AssetManager.queueImage("sortR","/UI/menu/buttons/li/sortR.png");
    AssetManager.queueImage("sortR-136","/UI/menu/buttons/li/sortR-136.png");
    AssetManager.queueImage("sortR-96","/UI/menu/buttons/li/sortR-96.png");
    AssetManager.queueImage("sortD","/UI/menu/buttons/li/sortD.png");
    AssetManager.queueImage("sortD-136","/UI/menu/buttons/li/sortD-136.png");
    AssetManager.queueImage("sortD-96","/UI/menu/buttons/li/sortD-96.png");

    AssetManager.queueImage("check","/UI/menu/buttons/Ht/check.png");
    AssetManager.queueImage("checked","/UI/menu/buttons/Ht/checked.png");

    AssetManager.queueImage("Bplay","/UI/menu/buttons/Bplay.png");
    AssetManager.queueImage("Bskip","/UI/menu/buttons/Bskip.png");

    AssetManager.queueImage("Snext","/UI/menu/buttons/Wi/Snext.png");
    AssetManager.queueImage("SnextD","/UI/menu/buttons/Wi/SnextD.png");
    AssetManager.queueImage("Sprev","/UI/menu/buttons/Wi/Sprev.png");
    AssetManager.queueImage("SprevD","/UI/menu/buttons/Wi/SprevD.png");

    AssetManager.queueImage("pingu1","/UI/pingus/pingu1.png");
    AssetManager.queueImage("pingu2","/UI/pingus/pingu2.png");
    AssetManager.queueImage("pingu3","/UI/pingus/pingu3.png");
    AssetManager.queueImage("pingu4","/UI/pingus/pingu4.png");
    AssetManager.queueImage("pingu5","/UI/pingus/pingu5.png");

    AssetManager.queueImage("own3","/UI/pingus/own3.png");
    AssetManager.queueImage("serv","/UI/menu/serv.png");

    AssetManager.queueImage("BnextH","/UI/menu/buttons/Ht/BnextH.png");
    AssetManager.queueImage("BmodesH","/UI/menu/buttons/Ht/BmodesH.png");
    AssetManager.queueImage("BmodesH2","/UI/menu/buttons/Ht/BmodesH2.png");
    AssetManager.queueImage("BstatsH","/UI/menu/buttons/Ht/BstatsH.png");
    AssetManager.queueImage("BprevH","/UI/menu/buttons/Ht/BprevH.png");
    AssetManager.queueImage("nextH","/UI/menu/buttons/Ht/nextH.png");
    AssetManager.queueImage("prevH","/UI/menu/buttons/Ht/prevH.png");
    AssetManager.queueImage("BnextHD","/UI/menu/buttons/Ht/BnextHD.png");
    AssetManager.queueImage("BprevHD","/UI/menu/buttons/Ht/BprevHD.png");
    AssetManager.queueImage("nextHD","/UI/menu/buttons/Ht/nextHD.png");
    AssetManager.queueImage("prevHD","/UI/menu/buttons/Ht/prevHD.png");
    AssetManager.queueImage("addH","/UI/menu/buttons/Ht/addH.png");
    AssetManager.queueImage("minH","/UI/menu/buttons/Ht/minH.png");
    AssetManager.queueImage("delH","/UI/menu/buttons/Ht/delH.png");
    AssetManager.queueImage("kickH","/UI/menu/buttons/Ht/kickH.png");

    AssetManager.queueImage("button-solo","/UI/menu/buttons/Ht/"+localBase+"/button-solo.png");
    AssetManager.queueImage("button-vs","/UI/menu/buttons/Ht/"+localBase+"/button-vs.png");
    AssetManager.queueImage("button-stats","/UI/menu/buttons/Ht/"+localBase+"/button-stats.png");
    AssetManager.queueImage("button-profile","/UI/menu/buttons/Ht/"+localBase+"/button-profile.png");

    AssetManager.queueImage("profile-rbtn","/UI/menu/buttons/profile-rbtn.png");

    AssetManager.queueImage("btn256-e","/UI/menu/buttons/btn256-e.png");

    /* Tiles */
    AssetManager.queueImage("tile-b","/UI/tiles/b.png");
    AssetManager.queueImage("tile-g","/UI/tiles/g.png");
    AssetManager.queueImage("tile-r","/UI/tiles/r.png");
    AssetManager.queueImage("tile-v","/UI/tiles/v.png");
    AssetManager.queueImage("tile-y","/UI/tiles/y.png");
    AssetManager.queueImage("tile-0","/UI/tiles/0.png");
    AssetManager.queueImage("tile-1","/UI/tiles/1.png");

    /* icons */
    AssetManager.queueImage("optionsT","/UI/menu/buttons/Ht/"+localBase+"/OptionsT.png");
    AssetManager.queueImage("editT","/UI/menu/buttons/Ht/"+localBase+"/EditT.png");
    AssetManager.queueImage("endT","/UI/menu/buttons/Ht/"+localBase+"/EndT.png");
    AssetManager.queueImage("ownT","/UI/menu/buttons/Ht/ownT.png");
    AssetManager.queueImage("pinguT","/UI/menu/buttons/Ht/pinguT.png");
    AssetManager.queueImage("profilsT","/UI/menu/buttons/Ht/"+localBase+"/ProfilesT.png");
    AssetManager.queueImage("statsT","/UI/menu/buttons/Ht/"+localBase+"/StatsT.png");
    AssetManager.queueImage("VersusT","/UI/menu/buttons/Ht/"+localBase+"/VersusT.png");
    AssetManager.queueImage("SoloT","/UI/menu/buttons/Ht/"+localBase+"/SoloT.png");

    AssetManager.queueColor("deepBlue", new Color(87, 161, 211));
    AssetManager.queueColor("deepWhite", new Color(193, 198, 200));
    AssetManager.queueColor("testGrey", new Color(132, 142, 146));
    AssetManager.queueColor("softBlue", new Color(118, 158, 174));
    AssetManager.queueColor("titleBlue", new Color(74, 111, 125));
    AssetManager.queueColor("almostGrey", new Color(72, 87, 92, 204));

    AssetManager.loadAll();

  }

  public void MakeWidgets() {
    Widget w = new Widget();


    WTitleBar wtb = new WTitleBar(80, AssetManager.getColor("almostGrey"));
    WBackground wb = new WBackground("bg");
    w.add(wb);
    Image im = AssetManager.getImage("logo-linear");
    int tx = (Config.WIDTH/5)*2-im.getWidth(null);
    if(tx<0) tx=0;
    WTitle wt = new WTitle(tx, 0, im);
    wtb.addTitle(wt);
    wtb.addButton();
    w.add(wtb);
    AssetManager.queueWidget("TitleBar", w);
  }

  private void InitAll() {
    setupCoreAssets();
  }
}
