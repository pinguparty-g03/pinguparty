package com.pingu.party.launcher.Component;

import javax.swing.SwingConstants;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.Color;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import com.pingu.party.launcher.Utils.UIConsts;
import javax.swing.JLabel;


public class Button extends JLabel {
  final Color[] SelectorColorButton = {UIConsts.COLOR_BLUE, UIConsts.COLOR_WHITE};
  protected String value;

  public Button(String text, Runnable Action) {
    super(text);
    this.value = text;
    setForeground(UIConsts.COLOR_WHITE);
    setBackground(UIConsts.COLOR_BLUE);
    setFont(UIConsts.BUTTON_TEXT);
    setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    setHorizontalAlignment(SwingConstants.CENTER);
    MouseAdapter ma = new MouseAdapter() {

      @Override
      public void mousePressed(MouseEvent me) {
        Action.run();
        System.out.println(Button.this.value);
      }

      @Override
      public void mouseEntered(MouseEvent me) {
        Button.this.SelectorColorButton[0] = UIConsts.COLOR_BUTTON_HOVER;
        repaint();
      }

      @Override
      public void mouseExited(MouseEvent me) {
        Button.this.SelectorColorButton[0] = UIConsts.COLOR_BLUE;
        repaint();
      }
    };
    addMouseListener(ma);
  }

  @Override
  protected void paintComponent(Graphics gr) {
    Graphics2D g2d = UIConsts.l_G2D(gr);
    g2d.setRenderingHint(
    RenderingHints.KEY_TEXT_ANTIALIASING,
    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
    super.paintComponent(g2d);

    g2d.setColor(this.SelectorColorButton[0]);
    g2d.fillRoundRect(0, 0, this.getWidth(), this.getHeight(), this.getHeight(), this.getHeight()); // x y w h br br

    g2d.setFont(UIConsts.BUTTON_TEXT);
    g2d.setColor(this.SelectorColorButton[1]);
    int sz = gr.getFontMetrics().stringWidth(this.value);
    g2d.drawString(this.value, (this.getWidth()-sz)/2, (this.getHeight()+16)/2);

  }
}
