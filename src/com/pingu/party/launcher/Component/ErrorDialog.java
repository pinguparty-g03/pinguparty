package com.pingu.party.launcher.Component;

import javax.swing.plaf.ComponentUI;
import javax.swing.JScrollBar;
import javax.swing.plaf.basic.BasicArrowButton;
import java.awt.Rectangle;
import javax.swing.JComponent;
import java.awt.Graphics;
import javax.swing.plaf.basic.BasicScrollBarUI;
import javax.swing.plaf.ColorUIResource;
import javax.swing.UIManager;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JButton;

import javax.swing.JScrollPane;
import java.awt.BorderLayout;

import java.awt.Insets;
import javax.swing.JTextArea;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JDialog;

class ScrollFlatUI extends BasicScrollBarUI {
  public static final Color COLOR_BACKGROUND = new Color(36, 54, 61);
  public static final Color COLOR_BUTTON = new Color(72, 87, 92);
  public static final Color COLOR_BUTTON_HOVER = new Color(88, 125, 144);
  public static final Color COLOR_FOREGROUND = new Color(193, 198, 200);

  public static ComponentUI createUI(JComponent c)    {
        return new ScrollFlatUI();
    }

  @Override
    protected void installComponents(){
        switch (scrollbar.getOrientation()) {
        case JScrollBar.VERTICAL:
            incrButton = createIncreaseButton(SOUTH);
            decrButton = createDecreaseButton(NORTH);
            break;

        case JScrollBar.HORIZONTAL:
            if (scrollbar.getComponentOrientation().isLeftToRight()) {
                incrButton = createIncreaseButton(EAST);
                decrButton = createDecreaseButton(WEST);
            } else {
                incrButton = createIncreaseButton(WEST);
                decrButton = createDecreaseButton(EAST);
            }
            break;
        }
        incrButton.setBorderPainted(false);
        incrButton.setBorder(null);
        incrButton.setContentAreaFilled(false);
        scrollbar.add(incrButton);

        decrButton.setBorderPainted(false);
        decrButton.setBorder(null);
        decrButton.setContentAreaFilled(false);
        scrollbar.add(decrButton);

        scrollbar.setEnabled(scrollbar.isEnabled());
    }

      @Override
      protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds)
      {
          if(thumbBounds.isEmpty() || !scrollbar.isEnabled())     {
              return;
          }

          int w = thumbBounds.width;
          int h = thumbBounds.height;
          int arc = 5;

          g.translate(thumbBounds.x, thumbBounds.y);

          g.setColor(COLOR_BUTTON_HOVER);
          g.drawRoundRect(0, 0, 1, 1, 8, 8);
          g.setColor(COLOR_BUTTON_HOVER);
          g.fillRoundRect(0, 0, w, h, 0, 0);

          g.translate(-thumbBounds.x, -thumbBounds.y);
      }
      @Override
     protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds)
     {
         g.setColor(COLOR_BUTTON);
         g.fillRect(trackBounds.x, trackBounds.y, trackBounds.width, trackBounds.height);
     }

     @Override
     protected JButton createDecreaseButton(int orientation)  {
      return new BasicArrowButton(orientation,COLOR_BUTTON,COLOR_BUTTON,COLOR_BUTTON,COLOR_BUTTON);
    }

    @Override
    protected JButton createIncreaseButton(int orientation)  {
        return new BasicArrowButton(orientation,COLOR_BUTTON,COLOR_BUTTON,COLOR_BUTTON,COLOR_BUTTON);
    }
  }

public class ErrorDialog extends JDialog {
  public static final Color COLOR_BACKGROUND = new Color(36, 54, 61);
  public static final Color COLOR_BUTTON = new Color(72, 87, 92);
  public static final Color COLOR_BLUE = new Color(87, 161, 211);
  public static final Color COLOR_FOREGROUND = new Color(193, 198, 200);

  public boolean exit = true;

  public ErrorDialog(String title, String msg, boolean exit) {
    setSize(600, 360);
    setTitle(title);
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    setBackground(COLOR_BACKGROUND);
    setForeground(COLOR_FOREGROUND);

    this.exit = exit;

    if (exit) setModal(true);

    setResizable(false);

    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    setLocation((screenSize.width-getWidth())/2, (screenSize.height-getHeight())/2);

    Container cnt = getContentPane();
    cnt.setLayout(null);
    cnt.setBackground(COLOR_BACKGROUND);
    cnt.setForeground(COLOR_FOREGROUND);

    JTextArea log = new JTextArea();
    log.setEditable(false);
    log.setText(msg);
    //log.setMargin(new Insets(48,8,8,8));
    log.setBackground(COLOR_BUTTON);
    log.setForeground(COLOR_FOREGROUND);
    log.setBounds(0, 0, 246, 64);
    log.setBorder(null);

    JScrollPane sp = new JScrollPane(log);
    BasicScrollBarUI bs = new BasicScrollBarUI();
    bs = (BasicScrollBarUI)ScrollFlatUI.createUI(sp);
    sp.getVerticalScrollBar().setUI( bs);
    sp.setBounds(16,16,562,267);
    sp.setBorder(null);
    sp.getVerticalScrollBar().setBackground(COLOR_BUTTON);
    sp.getVerticalScrollBar().setBorder(null);
    sp.getVerticalScrollBar().setForeground(COLOR_BLUE);
    //sp.getVerticalScrollBar().setUI((BasicScrollBarUI)ScrollFlatUI.createUI(sp));
    sp.setOpaque(true);
    sp.setBackground(COLOR_BUTTON);

    sp.getHorizontalScrollBar().setBackground(COLOR_BUTTON);
    sp.getHorizontalScrollBar().setUI((BasicScrollBarUI)ScrollFlatUI.createUI(sp));
    add(sp);

    JButton OkBtn = new JButton( new AbstractAction("OK") {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (exit) System.exit(1);
        else {
          dispose();
        }
      }
    });
    OkBtn.setBackground(COLOR_BLUE);
    OkBtn.setForeground(Color.white);
    OkBtn.setBorderPainted(false);
    OkBtn.setSelected(true);
    OkBtn.setBounds((int)(562-(this.getWidth()*0.2)), 291, (int)(this.getWidth()*0.2), 32);
    add(OkBtn);
  }

  /*public void dispose() {
    if (this.exit) System.exit(1);
  }*/

  public static String handle(String title, Exception e, boolean raise, boolean exit) {
    final StringBuilder StackB = new StringBuilder();
    StackB.append(" Exception "+e.toString()+" :\n");
    for (StackTraceElement st : e.getStackTrace()) {
      StackB.append("   "+st.toString()+"\n");
    }
    String stack = StackB.toString();
    if (raise) show(title, stack, exit);
    return stack;
  }
  public static String handle(Exception e, boolean raise, boolean exit) {
    return handle("Game has crashed",e, raise, exit);
  }
  public static String handle(Exception e, boolean raise) {
    return handle("Game has crashed", e, raise, true);
  }

  public static void show(String err, boolean exit) {
    show("Game has crashed",err, exit);
  }
  public static void show(String title, String err, boolean exit) {
    ErrorDialog eg = new ErrorDialog(title,err, exit);
    eg.setVisible(true);
  }
}
