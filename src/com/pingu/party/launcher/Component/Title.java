package com.pingu.party.launcher.Component;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import com.pingu.party.launcher.Utils.UIConsts;
import javax.swing.JLabel;


public class Title extends JLabel {

  protected String value;

  public Title(String text) {
    super(text);
    this.value = text;
    setFont(UIConsts.FONT_TITLE);
  }

  @Override
  protected void paintComponent(Graphics gr) {
    Graphics2D g2d = UIConsts.l_G2D(gr);
    g2d.setRenderingHint(
    RenderingHints.KEY_TEXT_ANTIALIASING,
    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
    super.paintComponent(g2d);

  }
}
