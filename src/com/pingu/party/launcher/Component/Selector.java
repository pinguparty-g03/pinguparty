package com.pingu.party.launcher.Component;

import java.awt.RenderingHints;

import javax.swing.SwingConstants;
import java.awt.event.MouseEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.Graphics;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Image;
import java.awt.Graphics2D;
import com.pingu.party.launcher.Utils.UIConsts;
import javax.swing.JLabel;
import java.awt.Dimension;
import java.awt.Cursor;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 * @author Pho3
 *
 *	Border Selector
 *
 */
class BordSel extends JLabel {
  /**
	 * Generated UID
	 */
	private static final long serialVersionUID = -2999828394518356177L;
  /**
 * Color container
 */
final Color[] SelectorColorButton = {UIConsts.COLOR_BUTTON};
  /**
 * Button type (left / right) => (true/false)
 */
final boolean type;

  /**
 * @param type boolean
 * @param Action runnable
 */
public BordSel(boolean type, Runnable Action) {
    super("");
    this.type = type;
    setForeground(UIConsts.COLOR_BUTTON);
    setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    MouseAdapter ma = new MouseAdapter() {

      @Override
      public void mousePressed(MouseEvent me) {
        Action.run();
        //System.out.println((type)?("Next"):("Prev"));
      }

      @Override
      public void mouseEntered(MouseEvent me) {
        BordSel.this.SelectorColorButton[0] = UIConsts.COLOR_BUTTON_HOVER;
        repaint();
      }

      @Override
      public void mouseExited(MouseEvent me) {
        BordSel.this.SelectorColorButton[0] = UIConsts.COLOR_BUTTON;
        repaint();
      }
    };
    addMouseListener(ma);
  }

  @Override
  protected void paintComponent(Graphics gr) {
    Graphics2D g2d = UIConsts.l_G2D(gr);
    super.paintComponent(g2d);

    g2d.setColor(this.SelectorColorButton[0]);
    g2d.fillRoundRect((this.type)?(-32):(0), 0, 64, 32, 32, 32);

    Image close=null;
    try { close = ImageIO.read(new File((this.type)?("res/UI/next.png"):("res/UI/prev.png"))); }
    catch(Exception ex) { ex.printStackTrace(); }
    g2d.drawImage(close, 11, 8, this);
  }

}

/**
 * @author Pho3
 *
 * Text Selector
 *
 */
class TextSel extends JLabel {
  /**
	 * Generated UID
	 */
	private static final long serialVersionUID = 2067913734697796788L;
/**
 * Selector Caption
 */
protected String caption;

  /**
 * @param cap String
 */
public TextSel(String cap) {
    super(cap);
    setOpaque(true);
    setBackground(UIConsts.COLOR_BUTTON);
    setForeground(UIConsts.COLOR_FOREGROUND);
    setFont(UIConsts.ROBOTO_BOLD);
    setHorizontalAlignment(SwingConstants.CENTER);

  }
  @Override
  protected void paintComponent(Graphics gr) {
    Graphics2D g2d = UIConsts.l_G2D(gr);
    super.paintComponent(g2d);

    g2d.setRenderingHint(
        RenderingHints.KEY_TEXT_ANTIALIASING,
        RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
  }

  /**
 * @param text String
 */
protected void SetText(String text) {
    this.caption = text;
    repaint();
  }
  /**
   */
protected String GetText() {
    return this.caption;
  }
}

/**
 * @author Pho3
 *
 *	Main selector class
 *
 */
public class Selector extends JPanel {
  /**
	 * Generated UID
	 */
	private static final long serialVersionUID = -4420190431804761278L;

/**
 * Items array
 */
private SItem SItems[] = {
    new SItem("Windowed","res/UI/ico.png"),
        new SItem("Fullscreen","res/UI/ico.png"),
        new SItem("Bordeless","res/UI/ico.png")
  };

  /**
 * Position in array
 */
int Position = 0;

  /**
 * @param i integer
 * @return String Array value
 */
protected String get(int i) {
    this.Position = (i>=0)?(i%this.SItems.length):(this.SItems.length-1);
    return this.SItems[this.Position].getText();
  }

  /**
 * Constructor
 */
public Selector() {
    setOpaque(true);
    setBackground(UIConsts.COLOR_BACKGROUND);
    setForeground(UIConsts.COLOR_FOREGROUND);
    setLayout(null);
    setSize(200, 32);
    setPreferredSize(new Dimension(200, 32));
    setMinimumSize(new Dimension(200, 32));

    JLabel val = new TextSel(get(this.Position));
    JLabel prev = new BordSel(false, () -> {val.setText(get(this.Position-1));});
    JLabel next = new BordSel(true, () -> {val.setText(get(this.Position+1));});

    prev.setBounds(0, 0, 32, 32);
    val.setBounds(32, 0, 136, 32);
    next.setBounds(168, 0, 32, 32);
    add(prev);
    add(val);
    add(next);

  }

	public String get() {
		return get(this.Position);
	}

}

/**
 * @author Pho3
 *
 * Selector Item
 *
 */
class SItem {
  /**
 * Strings constants
 */
private final String text,imgpath;
  /**
 * ImageIcon value
 */
private ImageIcon img;

  /**
 * @param text Caption String
 * @param img Path String
 */
public SItem(String text, String img) {
    this.text = text;
    this.imgpath = img;
  }

  /**
 * @return ImageIcon
 */
private ImageIcon _SetIcon() {
    if (this.img == null) {
      this.img = new ImageIcon(this.imgpath);
    }
    return this.img;
  }

  /**
 * @return ImageIcon
 */
public ImageIcon getIcon() {
    return (this.img == null)?(_SetIcon()):(this.img);
  }

  /**
 * @return String Text
 */
public String getText() {  return this.text; }
}
