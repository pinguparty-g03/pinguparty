package com.pingu.party.launcher.Component;

import java.awt.Insets;
import javax.swing.SwingConstants;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.Color;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import com.pingu.party.launcher.Utils.UIConsts;
import javax.swing.JLabel;

import com.pingu.party.launcher.Component.Button;


public class ProgressButton extends Button {
  final Color[] SelectorColorButton = {UIConsts.COLOR_BLUE, UIConsts.COLOR_WHITE};
  private int progress = 99;
  public ProgressButton(String text, Runnable Action) {
    super(text, Action);

    MouseAdapter ma = new MouseAdapter() {

      @Override
      public void mousePressed(MouseEvent me) {
        ProgressButton.this.setProgress(ProgressButton.this.getProgress()+5);
      }
    };

    addMouseListener(ma);


  }

  public void setProgress(int pr) {
    this.progress = (pr<0)?-pr%100:pr%100;
    repaint();
  }

  public int getProgress() {
    return this.progress;
  }

  private void CalcProg(Graphics2D g2d, int pr) {
    int w = this.getWidth();
    int h = this.getHeight();
    int d = w-(this.getWidth()*pr/100);
    //System.out.println(pr+"% " + w + ","+h+":"+d);
    if (pr>=95) {
      g2d.fillRoundRect(2, 2, 0, 0, 0, 0);
    } else if (pr<=5) {
        g2d.fillRoundRect(2, 2, this.getWidth()-4, this.getHeight()-4, this.getHeight(), this.getHeight());
    } else if (d<h/2) {
      g2d.fillArc(4+this.getWidth()-this.getHeight()-2,2,this.getHeight()-4, this.getHeight()-4, -90, 180); //int x, int y, int width, int height, int startAngle, int arcAngle
    } else if (d>h/2){
      g2d.fillRoundRect(2+this.getWidth()*pr/100, 2, this.getWidth()-this.getWidth()*pr/100-(this.getHeight()/2)-3, this.getHeight()-4, 0, 0);
      g2d.fillArc(this.getWidth()-this.getHeight()-1, 2 ,this.getHeight()-1, this.getHeight()-4, -90, 180);
    }
  }

  @Override
  protected void paintComponent(Graphics gr) {
    Graphics2D g2d = UIConsts.l_G2D(gr);
    g2d.setRenderingHint(
    RenderingHints.KEY_TEXT_ANTIALIASING,
    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
    super.paintComponent(g2d);

    g2d.setColor(this.SelectorColorButton[0]);
    g2d.fillRoundRect(0, 0, this.getWidth(), this.getHeight(), this.getHeight(), this.getHeight());

    Insets in = getInsets();
    g2d.setColor(UIConsts.COLOR_BACKGROUND);
    int pr = (progress>0)?progress%100:-progress%100;
    CalcProg(g2d, pr);

   this.SelectorColorButton[1] = (pr<99)?UIConsts.COLOR_FOREGROUND:UIConsts.COLOR_WHITE;
    g2d.setFont(UIConsts.BUTTON_TEXT);
    g2d.setColor(this.SelectorColorButton[1]);
    int sz = gr.getFontMetrics().stringWidth((pr<99)?UIConsts.LOADING_TEXT:this.value);
    g2d.drawString((pr<99)?UIConsts.LOADING_TEXT:this.value, (this.getWidth()-sz)/2, (this.getHeight()+16)/2);

  }
}
