package com.pingu.party.launcher;

import com.pingu.party.launcher.Component.*;
import com.pingu.party.launcher.Utils.StringComputer;
import javax.swing.DefaultComboBoxModel;

import com.pingu.party.launcher.Utils.RessourceComputer;

import com.pingu.party.activities.base.Activity;

import com.pingu.party.activities.base.GameWindow;

import com.pingu.party.engine.core.WindowMaker;

import com.pingu.party.engine.game.Test.Empty;
import com.pingu.party.activities.*;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import com.pingu.party.launcher.Utils.UIConsts;

import com.pingu.party.engine.core.GameCanvas;
import com.pingu.party.engine.config.Config;
import com.pingu.party.engine.config.Keys;
import javax.swing.JComboBox;
import java.awt.SystemColor;
import com.pingu.party.engine.game.Profile;
import com.pingu.party.engine.config.LoadInfo;

/**
 * @author bastien
 * @author florian
 */
public class LaunchUI extends JFrame {
	static RessourceComputer r = new RessourceComputer();
	static StringComputer sc = new StringComputer();
	Selector sl = new Selector();
	private WindowMaker w = null;
	private GameWindow gw = null;
	private Profile actualProfile = null;
	/**
	 * @param args
	 *
	 * Launches the UI
	 */
	public static void main(String[] args) {
		new LaunchUI();
	}

  /**
 * Launch UI function
 */
public LaunchUI() {
    System.out.println("Launcher Loading . . .");
    System.out.println("Parsing config file");
		Config.getFromFile();
		Keys.getFromFile();
    JPanel lpanel = LaunchPanel();

    //TODO: put into subclasses + launch options

    lpanel.add(Separator());
    lpanel.add(ExitButton());
    lpanel.add(Icon());
    lpanel.add(Logo());
    lpanel.add(Title()); //Placeholder
    lpanel.add(Play()); //Placeholder
		lpanel.add(UpdateI());

		//JSpinner s = new Switcher();
		//s.setBounds(525, 204, 48, 32);

		//lpanel.add(Switch());
		lpanel.add(Select(this.sl));

    getContentPane().add(lpanel);

    this.pack();
    this.setVisible(true);
    this.toFront();


    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    setLocation((screenSize.width-getWidth())/2, (screenSize.height-getHeight())/2);

		/* Init stuff */

  }

  /**
 * @return JPanel
 */
private JPanel LaunchPanel() {
	LoadInfo.localNoneAI(); // load local profiles
    Dimension wh = new Dimension(UIConsts.LPANEL_WIDTH, UIConsts.LPANEL_HEIGHT);
    JPanel lpanel = new JPanel();
    lpanel.setSize(wh);
    lpanel.setPreferredSize(wh);
    lpanel.setMinimumSize(wh);

    lpanel.setBackground(UIConsts.COLOR_BACKGROUND);

    //lpanel.setLayout(new FlowLayout ());
    lpanel.setLayout(null); // NULL Layout make you able to manually set bounds.

    // scrolling list with selection
    JComboBox comboBox = new JComboBox();
    comboBox.setModel(new DefaultComboBoxModel<Profile>());
    for(Profile p : LoadInfo.PROFILES)
    {
    	comboBox.addItem(p);
    }
    comboBox.addActionListener(new
    ActionListener()
    {
    	public void actionPerformed(ActionEvent e)
    	{
    		System.out.println("select profile => "+convertObjectToProfile(comboBox.getSelectedItem())); // print it !
    		LaunchUI.this.actualProfile = convertObjectToProfile(comboBox.getSelectedItem()); //  set it !

    	}
    });
    this.actualProfile = convertObjectToProfile(comboBox.getSelectedItem());

    comboBox.setBackground(UIConsts.COLOR_BUTTON);
    comboBox.setForeground(SystemColor.window);
    comboBox.setBounds(514, 122, 169, 24);
    lpanel.add(comboBox);

    MouseAdapter ma = new MouseAdapter() {
      int X, Y;

      @Override
      public void mousePressed(MouseEvent e) {
        this.X = e.getXOnScreen();
        this.Y = e.getYOnScreen();
      }

      @Override
      public void mouseDragged(MouseEvent e) {
        Point p = new Point(e.getXOnScreen(),e.getYOnScreen());
        setLocation(p.x+getLocationOnScreen().x-this.X, p.y+getLocationOnScreen().y-this.Y);
        this.X = p.x;
        this.Y = p.y;

      }
    };

    lpanel.addMouseListener(ma);
    lpanel.addMouseMotionListener(ma);

    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent we) {
        System.exit(0); //TODO: non closing (waiting for task to complete)
      }
    });

    this.setUndecorated(true); // Empty panel = no controls

    return lpanel;
  }

private JLabel UpdateI() {
	//TODO: True updater animated component
	JLabel ic = new JLabel("");
	ic.setText("\uE627");
	// cloud dl \uE2C0
	// sync \uE627
	// dl \uE2C4
	ic.setFont(UIConsts.MATERIALICONS);
	ic.setForeground(UIConsts.COLOR_BUTTON);
	ic.setBounds(525, 192, 48, 32);
	ic.setBounds(UIConsts.LPANEL_WIDTH-UIConsts.EXITB_WIDTH-48,0,UIConsts.EXITB_WIDTH,UIConsts.L_MARGIN); //x, y, w, h
	return ic;
}

  /**
 * @return JLabel Icon
 */
private JLabel Icon() {
    JLabel ic = new JLabel() {
      /**
		 * Generated UID
		 */
		private static final long serialVersionUID = 2843273974038682905L;

	@Override
      protected void paintComponent(Graphics gr) {
        Graphics2D g2d = UIConsts.l_G2D(gr);
        super.paintComponent(g2d);
        g2d.setColor(UIConsts.COLOR_BACKGROUND);
        Image ico=null;
        try { ico = ImageIO.read(new File("res/UI/ico.png")); }
        catch(Exception ex) { ex.printStackTrace(); }
        g2d.drawImage(ico, 0, 0, this);
      }
    };

    ic.setBounds(UIConsts.L_MARGIN/4,UIConsts.L_MARGIN/4,UIConsts.L_MARGIN/2,UIConsts.L_MARGIN/2);
    return ic;
  }

  /**
 * @return JLabel Title
 */
private Title Title() {
    Title tl = new Title(UIConsts.LPANEL_TITLE) {
      /**
		 * Generated UID
		 */
		private static final long serialVersionUID = -2010529050733447269L;

	/*@Override
      protected void paintComponent(Graphics gr) {
        Graphics2D g2d = UIConsts.l_G2D(gr);
        super.paintComponent(g2d);
        g2d.setColor(UIConsts.COLOR_GREY);
        Image ico=null;
        try { ico = ImageIO.read(new File("res/bg.png")); }
        catch(Exception ex) { ex.printStackTrace(); }
        g2d.drawImage(ico, 0, 0, this);
      }*/
    };
    tl.setBackground(UIConsts.COLOR_GREY);
		tl.setForeground(UIConsts.COLOR_FOREGROUND);
    tl.setBounds(UIConsts.L_MARGIN, UIConsts.L_MARGIN/4, UIConsts.LPANEL_WIDTH-(UIConsts.EXITB_WIDTH+UIConsts.L_MARGIN+UIConsts.L_MARGIN/4), UIConsts.L_MARGIN/2);
    return tl;
  }

public boolean setCanvas(GameCanvas gc) {
	try {
		if(this.w == null) {
			this.w = WindowMaker.make(gc);
		} else {
			WindowMaker.set(gc);
		}
		return true;
		//WindowMaker.make(new GameCanvas());
		//WindowMaker.make(new TV());
		//WindowMaker.make(new Pixels());
		//WindowMaker.make(new Empty());
		//WindowMaker.make(new Menu());
		//WindowMaker.make(new Bouncy());
	} catch (Exception e) {
		ErrorDialog.handle(e, true);
		return false;
	}
}

private void LaunchWith() {
	LoadInfo.ACTUALPROFILE = this.actualProfile;
	String ws = this.sl.get();
	System.out.print(ws);
	switch(ws) {
		case "Bordeless":		Config.BORDERED = false;
												Config.FULLSCREEN = false;
												break;
		case "Windowed": 		Config.BORDERED = true;
												Config.FULLSCREEN = false;
												break;
		case "Fullscreen": 	Config.BORDERED = false;
												Config.FULLSCREEN = true;
												break;

	}
	r.MakeWidgets(); //TODO: in load screen
	//WindowMaker(GameCanvas gc, String title, int w, int h)
	//WindowMaker w = new WindowMaker(new Pixels(), "Title", Config.WIDTH, Config.HEIGHT);
	try {

		this.w = WindowMaker.make(new Empty());
		this.gw = new GameWindow();

		Activity ac = new Activity(new Menu(), 2);
		//Activity ac = new Activity(new ProfileList(), 2);
		this.gw.register(ac);

		//this.w.set(new Pixels());
		//this.w.set(ac.getCanvas());
		WindowMaker.set(this.gw.Init().getCanvas());

		/*try {
			this.w.set(this.gw.Init().getCanvas());
		} catch (Exception e) {
			ErrorDialog.show("Exception while launching : "+e, false);
		}*/

		//WindowMaker.make(new GameCanvas());
		//WindowMaker.make(new TV());
		//WindowMaker.make(new Pixels());
		//WindowMaker.make(new Empty());
		//WindowMaker.make(new Menu());
		//WindowMaker.make(new Bouncy());
	} catch (Exception e) {
		ErrorDialog.handle(e, true);
	}
	/*
	SwingUtilities.invokeLater(new Runnable() {
		public void run() {
			//GameCanvas.test();
			//TestGui00();
		}
	});*/
}

  /**
 * @return JLabel Play button
 */
private JLabel Play() {
    Button pl = new Button("PLAY", ()-> {LaunchWith();});
		//ProgressButton pl = new ProgressButton("PLAY", () -> {LaunchWith();});
    pl.setBackground(UIConsts.COLOR_GREY);
    pl.setBounds(472, 304, 256, 64);
		//pl.setProgress(5);
    return pl;
  }

  /**
 * @return JLabel Logo
 */
private JLabel Logo() {
    JLabel lg = new JLabel() {
      /**
		 * Generated UID
		 */
		private static final long serialVersionUID = -1289465907841135336L;

	@Override
      protected void paintComponent(Graphics gr) {
        Graphics2D g2d = UIConsts.l_G2D(gr);
        super.paintComponent(g2d);
        g2d.setColor(UIConsts.COLOR_GREY);
        Image ico=null;
        try { ico = ImageIO.read(new File("res/UI/logo-launch.png")); }
        catch(Exception ex) { ex.printStackTrace(); }
        g2d.drawImage(ico, 0, 0, this);
      }
    };
    lg.setBackground(UIConsts.COLOR_GREY);
    lg.setBounds(48, 72, 304, 280);
    return lg;
  }

	/**
	 * @return Selector
	 */
	private Selector Select(Selector sl) {
		sl.setBounds(500, 225, 200, 32);
		return sl;
	}

  /**
 * @return JLabel Exit button
 */
private JLabel ExitButton() {
    final Color[] ExitSwitchColor = {UIConsts.COLOR_BUTTON};

    JLabel eb = new JLabel() {
      /**
		 * Generated UID
		 */
		private static final long serialVersionUID = 2262123827861728486L;

	@Override
      protected void paintComponent(Graphics gr) {
        Graphics2D g2d = UIConsts.l_G2D(gr);
        super.paintComponent(g2d);

        g2d.setColor(ExitSwitchColor[0]);
        //g2d.fillRoundRect(0, -24, 100, 72, 48, 48);
        // Make an oversized rounded button to get only the curve
        g2d.fillRoundRect(0, -(UIConsts.L_MARGIN/2), UIConsts.EXITB_WIDTH+UIConsts.L_MARGIN/2, UIConsts.L_MARGIN+UIConsts.L_MARGIN/2, UIConsts.L_MARGIN, UIConsts.L_MARGIN);

        Image close=null;
        try { close = ImageIO.read(new File("res/UI/close.png")); }
        catch(Exception ex) { ex.printStackTrace(); }
        g2d.drawImage(close, (UIConsts.EXITB_WIDTH-20)/2, (UIConsts.L_MARGIN-20)/2, this);
      }
    };

    MouseAdapter ma = new MouseAdapter() {

      @Override
      public void mousePressed(MouseEvent me) {
        System.exit(0);
      }

      @Override
      public void mouseEntered(MouseEvent me) {
        ExitSwitchColor[0] = UIConsts.COLOR_GREY;
        //eb.repaint();
      }

      @Override
      public void mouseExited(MouseEvent me) {
        ExitSwitchColor[0] = UIConsts.COLOR_BUTTON;
        //eb.repaint();
      }
    };
    eb.addMouseListener(ma);

    eb.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    //eb.setForeground(UIConsts.COLOR_GREY);
    //eb.setBackground(UIConsts.COLOR_GREY);
    eb.setBounds(UIConsts.LPANEL_WIDTH-UIConsts.EXITB_WIDTH,0,UIConsts.EXITB_WIDTH,UIConsts.L_MARGIN); //x, y, w, h

    return eb;
  }

  /**
 * @return JSeparator
 */
private JSeparator Separator() {
    JSeparator sep = new JSeparator();
    sep.setOrientation(SwingConstants.VERTICAL);
    sep.setForeground(UIConsts.COLOR_GREY);
    sep.setBackground(UIConsts.COLOR_GREY);
    //L_MARGIN = 48 LPANEL_HEIGHT = 400
    // LPANEL_HEIGHT - L_MARGIN*2.5
    sep.setBounds(UIConsts.LPANEL_HEIGHT-UIConsts.SEPARATOR_WIDTH/2, (int)(UIConsts.L_MARGIN*1.5), UIConsts.SEPARATOR_WIDTH , (int)(UIConsts.LPANEL_HEIGHT - UIConsts.L_MARGIN*2.5)); //x, y, w, h
    //sep.setBounds(398, 72, 2, 280);
    return sep;
  }

	/**
	 * convert an object to profile
	 * @param obj object to convert
	 * @return Profile
	 */
	private Profile convertObjectToProfile(Object obj)
	{
		return (Profile)obj;
	}
}
