package com.pingu.party.stats;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonWriter;

import com.pingu.party.engine.game.Profile;


/**
 * class Stat
 * @author florian RIBOU
 *
 */
public class Stat implements Comparable<Stat> {
	/**
	 * path of the stats database
	 */
	private static final String PATH = "src/com/pingu/party/engine/data/stats.json" ;
	
	/**
	 * user's uuid
	 */
	private String uuid;
	/**
	 * total played game(s) number
	 */
	private int played;
	/**
	 * number of won rounds
	 */
	private int won;
	/**
	 * number of lost rounds
	 */
	private int lost;
	/**
	 * left rounds/games
	 */
	private int left;
	/**
	 * total time played
	 */
	private int time;
	/**
	 * elo score
	 */
	private int elo;
	/**
	 * total score
	 */
	private int score; 
	/**
	 * data for each played rounds
	 */
	private List<Round> rounds;
	
	
	/**
	 * load existing stats
	 * @param uuid (String) user uuid
	 */
	public Stat(String uuid) {
		try {
			this.uuid = uuid ;
			this.played = loadPlayed();
			this.won = loadWon();
			this.lost = loadLost();
			this.left = loadLeft();
			this.time = loadTime();
			this.elo = loadElo();
			this.rounds = loadRounds();
			this.score = getTotalScore();
		} catch(IOException e) {
			System.out.println("Stream Error");
		}
	}
	
	public Stat(String uuid, int played, int won, int lost, int left, int time, int elo, int score) {
		this.uuid = uuid ;
		this.played = played;
		this.won = won;
		this.lost = lost;
		this.left = left;
		this.time = time;
		this.elo = elo;
		this.rounds = null;
		this.score = score;
	}
	
	
	/**
	 * add stats to the database at the end of the game
	 * @param won (boolean) true if the game is won
	 * @param score score of the game
	 * @param addTime integer value add to time
	 * @param elo integer value of elo score
	 * @throws IOException stream problem
	 */
	public void endRound(boolean win, int newScore, int addTime, int newElo) throws IOException {
		InputStream input = new FileInputStream(PATH) ;
		// create a JsonReader from Json
		JsonReader reader = Json.createReader(input) ;	
		// get the JsonObject
		JsonObject json = reader.readObject() ;
		input.close() ;
		reader.close() ;
		JsonArray profiles = json.getJsonArray("profiles");
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
		JsonArrayBuilder newProfiles = Json.createArrayBuilder();
		for(int i = 0; i < profiles.size(); i++){
			JsonObjectBuilder newProfile = Json.createObjectBuilder();
        	String uuid = profiles.getJsonObject(i).getString("uuid");
        	newProfile.add("uuid",uuid);
        	if(uuid.equals(this.uuid)) {
        		newProfile.add("uuid",uuid);
                int played = profiles.getJsonObject(i).getInt("played");
                newProfile.add("played",played+1);
                JsonArray rounds = profiles.getJsonObject(i).getJsonArray("rounds");
                JsonArrayBuilder newRounds = Json.createArrayBuilder();
                for(int j = 0; j < rounds.size(); j++) {
                	if(j == 0)
                	{
                		JsonObjectBuilder newRound = Json.createObjectBuilder();
                    	newRound.add("score",newScore) ;
        				newRound.add("elo",newElo) ;
        				if(win)
        				{
        					int won = rounds.getJsonObject(j).getInt("won");
            				newRound.add("won",won+1) ;
            				int lost = rounds.getJsonObject(j).getInt("lost");
            				newRound.add("lost",lost) ;
        				}
        				else
        				{
        					int won = rounds.getJsonObject(j).getInt("won");
            				newRound.add("won",won) ;
            				int lost = rounds.getJsonObject(j).getInt("lost");
            				newRound.add("lost",lost+1) ;
        				}
        				int left = rounds.getJsonObject(j).getInt("left");
        				newRound.add("left",left) ;
        				int time = rounds.getJsonObject(j).getInt("time");
        				newRound.add("time",time+addTime) ;
                        newRounds.add(newRound);
                	}
                	JsonObjectBuilder newRound = Json.createObjectBuilder();
                	int score = rounds.getJsonObject(j).getInt("score");
                	newRound.add("score",score) ;
    				int elo = rounds.getJsonObject(j).getInt("elo");
    				newRound.add("elo",elo) ;
    				int won = rounds.getJsonObject(j).getInt("won");
    				newRound.add("won",won) ;
    				int lost = rounds.getJsonObject(j).getInt("lost");
    				newRound.add("lost",lost) ;
    				int left = rounds.getJsonObject(j).getInt("left");
    				newRound.add("left",left) ;
    				int time = rounds.getJsonObject(j).getInt("time");
    				newRound.add("time",time) ;
                    newRounds.add(newRound);
                }
                newProfile.add("rounds",newRounds) ;
                newProfiles.add(newProfile);
            }
        	else
        	{
        		newProfile.add("uuid",uuid);
                int played = profiles.getJsonObject(i).getInt("played");
                newProfile.add("played",played);
                JsonArray rounds = profiles.getJsonObject(i).getJsonArray("rounds");
                JsonArrayBuilder newRounds = Json.createArrayBuilder();
                for(int j = 0; j < rounds.size(); j++) {
                	JsonObjectBuilder newRound = Json.createObjectBuilder();
                	int score = rounds.getJsonObject(j).getInt("score");
                	newRound.add("score",score) ;
    				int elo = rounds.getJsonObject(j).getInt("elo");
    				newRound.add("elo",elo) ;
    				int won = rounds.getJsonObject(j).getInt("won");
    				newRound.add("won",won) ;
    				int lost = rounds.getJsonObject(j).getInt("lost");
    				newRound.add("lost",lost) ;
    				int left = rounds.getJsonObject(j).getInt("left");
    				newRound.add("left",left) ;
    				int time = rounds.getJsonObject(j).getInt("time");
    				newRound.add("time",time) ;
                    newRounds.add(newRound);
                }
                newProfile.add("rounds",newRounds) ;
                newProfiles.add(newProfile);
        	}
		}
		jsonBuilder.add("profiles", newProfiles);
		JsonObject root = jsonBuilder.build();	
		OutputStream output = new FileOutputStream(PATH);
		JsonWriter writer = Json.createWriter(output);
		writer.writeObject(root);
		writer.close();
		output.close();
	}
	
	/**
	 * if the game is brutally left
	 * @throws IOException stream problem
	 */
	public void leftGame() throws IOException {
		InputStream input = new FileInputStream(PATH) ;
		// create a JsonReader from Json
		JsonReader reader = Json.createReader(input) ;	
		// get the JsonObject
		JsonObject json = reader.readObject() ;
		input.close() ;
		reader.close() ;
		JsonArray profiles = json.getJsonArray("profiles");
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
		JsonArrayBuilder newProfiles = Json.createArrayBuilder();
		for(int i = 0; i < profiles.size(); i++){
			JsonObjectBuilder newProfile = Json.createObjectBuilder();
        	String uuid = profiles.getJsonObject(i).getString("uuid");
        	if(uuid.equals(this.uuid)) {
        		newProfile.add("uuid",uuid);
                int played = profiles.getJsonObject(i).getInt("played");
                newProfile.add("played",played+1);
                JsonArray rounds = profiles.getJsonObject(i).getJsonArray("rounds");
                JsonArrayBuilder newRounds = Json.createArrayBuilder();
                for(int j = 0; j < rounds.size(); j++) {
                	if(j == 0)
                	{
                		JsonObjectBuilder newRound = Json.createObjectBuilder();
                    	int score = rounds.getJsonObject(j).getInt("score");
                    	newRound.add("score",score) ;
        				int elo = rounds.getJsonObject(j).getInt("elo");
        				newRound.add("elo",elo) ;
        				int won = rounds.getJsonObject(j).getInt("won");
        				newRound.add("won",won) ;
        				int lost = rounds.getJsonObject(j).getInt("lost");
        				newRound.add("lost",lost) ;
        				int left = rounds.getJsonObject(j).getInt("left");
        				newRound.add("left",left+1) ;
        				int time = rounds.getJsonObject(j).getInt("time");
        				newRound.add("time",time) ;
                        newRounds.add(newRound);
                	}
                	JsonObjectBuilder newRound = Json.createObjectBuilder();
                	int score = rounds.getJsonObject(j).getInt("score");
                	newRound.add("score",score) ;
    				int elo = rounds.getJsonObject(j).getInt("elo");
    				newRound.add("elo",elo) ;
    				int won = rounds.getJsonObject(j).getInt("won");
    				newRound.add("won",won) ;
    				int lost = rounds.getJsonObject(j).getInt("lost");
    				newRound.add("lost",lost) ;
    				int left = rounds.getJsonObject(j).getInt("left");
    				newRound.add("left",left) ;
    				int time = rounds.getJsonObject(j).getInt("time");
    				newRound.add("time",time) ;
                    newRounds.add(newRound);
                }
                newProfile.add("rounds",newRounds) ;
                newProfiles.add(newProfile);
            }
        	else
        	{
        		newProfile.add("uuid",uuid);
                int played = profiles.getJsonObject(i).getInt("played");
                newProfile.add("played",played);
                JsonArray rounds = profiles.getJsonObject(i).getJsonArray("rounds");
                JsonArrayBuilder newRounds = Json.createArrayBuilder();
                for(int j = 0; j < rounds.size(); j++) {
                	JsonObjectBuilder newRound = Json.createObjectBuilder();
                	int score = rounds.getJsonObject(j).getInt("score");
                	newRound.add("score",score) ;
    				int elo = rounds.getJsonObject(j).getInt("elo");
    				newRound.add("elo",elo) ;
    				int won = rounds.getJsonObject(j).getInt("won");
    				newRound.add("won",won) ;
    				int lost = rounds.getJsonObject(j).getInt("lost");
    				newRound.add("lost",lost) ;
    				int left = rounds.getJsonObject(j).getInt("left");
    				newRound.add("left",left) ;
    				int time = rounds.getJsonObject(j).getInt("time");
    				newRound.add("time",time) ;
                    newRounds.add(newRound);
                }
                newProfile.add("rounds",newRounds) ;
                newProfiles.add(newProfile);
        	}
        }
		jsonBuilder.add("profiles", newProfiles);
		JsonObject root = jsonBuilder.build();	
		OutputStream output = new FileOutputStream(PATH);
		JsonWriter writer = Json.createWriter(output);
		writer.writeObject(root);
		writer.close();
		output.close();
	}
	
	/**
	 * delete stats from this user in DB
	 * @throws IOException stream problem
	 */
	public void deleteStatsUser() throws IOException {
		InputStream input = new FileInputStream(PATH) ;
		// create a JsonReader from Json
		JsonReader reader = Json.createReader(input) ;	
		// get the JsonObject
		JsonObject json = reader.readObject() ;
		input.close() ;
		reader.close() ;
		JsonArray profiles = json.getJsonArray("profiles");
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
		JsonArrayBuilder newProfiles = Json.createArrayBuilder();
		for(int i = 0; i < profiles.size(); i++){
			JsonObjectBuilder newProfile = Json.createObjectBuilder();
        	String uuid = profiles.getJsonObject(i).getString("uuid");
        	if(!(uuid.equals(this.uuid))) {
        		newProfile.add("uuid",uuid);
                int played = profiles.getJsonObject(i).getInt("played");
                newProfile.add("played",played);
                JsonArray rounds = profiles.getJsonObject(i).getJsonArray("rounds");
                JsonArrayBuilder newRounds = Json.createArrayBuilder();
                for(int j = 0; j < rounds.size(); j++) {
                	JsonObjectBuilder newRound = Json.createObjectBuilder();
                	int score = rounds.getJsonObject(j).getInt("score");
                	newRound.add("score",score) ;
    				int elo = rounds.getJsonObject(j).getInt("elo");
    				newRound.add("elo",elo) ;
    				int won = rounds.getJsonObject(j).getInt("won");
    				newRound.add("won",won) ;
    				int lost = rounds.getJsonObject(j).getInt("lost");
    				newRound.add("lost",lost) ;
    				int left = rounds.getJsonObject(j).getInt("left");
    				newRound.add("left",left) ;
    				int time = rounds.getJsonObject(j).getInt("time");
    				newRound.add("time",time) ;
                    newRounds.add(newRound);
                }
                newProfile.add("rounds",newRounds) ;
                newProfiles.add(newProfile);
            }	
        	
        }
		jsonBuilder.add("profiles", newProfiles);
		JsonObject root = jsonBuilder.build();	
		OutputStream output = new FileOutputStream(PATH);
		JsonWriter writer = Json.createWriter(output);
		writer.writeObject(root);
		writer.close();
		output.close();
	}
	
	/**
	 * create for the first time user's stats in DB
	 * @param uuid user's uuid
	 * @throws IOException stream problem
	 */
	public static void createStatsUser(String uuid) throws IOException {
		InputStream input = new FileInputStream(PATH) ;
		// create a JsonReader from Json
		JsonReader reader = Json.createReader(input) ;	
		// get the JsonObject
		JsonObject json = reader.readObject() ;
		input.close() ;
		reader.close() ;
		JsonArray profiles = json.getJsonArray("profiles");
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
		JsonArrayBuilder newProfiles = Json.createArrayBuilder();
		for(int i = 0; i < profiles.size(); i++){
			JsonObjectBuilder newProfile = Json.createObjectBuilder();
			String id = profiles.getJsonObject(i).getString("uuid");
			newProfile.add("uuid",id);
			int played = profiles.getJsonObject(i).getInt("played");
			newProfile.add("played",played);
			JsonArray rounds = profiles.getJsonObject(i).getJsonArray("rounds");
			JsonArrayBuilder oldRounds = Json.createArrayBuilder();
			for(int j = 0; j < rounds.size(); j++) {
				JsonObjectBuilder oldRound = Json.createObjectBuilder();
				int score = rounds.getJsonObject(j).getInt("score");
				oldRound.add("score",score) ;
				int elo = rounds.getJsonObject(j).getInt("elo");
				oldRound.add("elo",elo) ;
				int won = rounds.getJsonObject(j).getInt("won");
				oldRound.add("won",won) ;
				int lost = rounds.getJsonObject(j).getInt("lost");
				oldRound.add("lost",lost) ;
				int left = rounds.getJsonObject(j).getInt("left");
				oldRound.add("left",left) ;
				int time = rounds.getJsonObject(j).getInt("time");
                oldRound.add("time",time) ;
				oldRounds.add(oldRound);
			}
			newProfile.add("rounds",oldRounds) ;
			newProfiles.add(newProfile);
        }
		JsonObjectBuilder newProfile = Json.createObjectBuilder();
		JsonArrayBuilder newRounds = Json.createArrayBuilder();
		JsonObjectBuilder newRound = Json.createObjectBuilder();
		newRound.add("score",0) ;
		newRound.add("elo",500) ;
		newRound.add("won",0) ;
		newRound.add("lost",0) ;
		newRound.add("left",0) ;
		newRound.add("time",0) ;
		newRounds.add(newRound);
		
		newProfile.add("uuid",uuid);
		newProfile.add("played",0);
		newProfile.add("rounds",newRounds) ;
		newProfiles.add(newProfile);
		jsonBuilder.add("profiles", newProfiles);
		JsonObject root = jsonBuilder.build();	
		OutputStream output = new FileOutputStream(PATH);
		JsonWriter writer = Json.createWriter(output);
		writer.writeObject(root);
		writer.close();
		output.close();
	}
	
	
	/**
	 * create user's stats in DB with stats
	 * @param uuid user's uuid
	 * @throws IOException stream problem
	 */
	public static void createStatsUser(Stat s) throws IOException {
		InputStream input = new FileInputStream(PATH) ;
		// create a JsonReader from Json
		JsonReader reader = Json.createReader(input) ;	
		// get the JsonObject
		JsonObject json = reader.readObject() ;
		input.close() ;
		reader.close() ;
		JsonArray profiles = json.getJsonArray("profiles");
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
		JsonArrayBuilder newProfiles = Json.createArrayBuilder();
		for(int i = 0; i < profiles.size(); i++){
			JsonObjectBuilder newProfile = Json.createObjectBuilder();
			String id = profiles.getJsonObject(i).getString("uuid");
			newProfile.add("uuid",id);
			int played = profiles.getJsonObject(i).getInt("played");
			newProfile.add("played",played);
			JsonArray rounds = profiles.getJsonObject(i).getJsonArray("rounds");
			JsonArrayBuilder oldRounds = Json.createArrayBuilder();
			for(int j = 0; j < rounds.size(); j++) {
				JsonObjectBuilder oldRound = Json.createObjectBuilder();
				int score = rounds.getJsonObject(j).getInt("score");
				oldRound.add("score",score) ;
				int elo = rounds.getJsonObject(j).getInt("elo");
				oldRound.add("elo",elo) ;
				int won = rounds.getJsonObject(j).getInt("won");
				oldRound.add("won",won) ;
				int lost = rounds.getJsonObject(j).getInt("lost");
				oldRound.add("lost",lost) ;
				int left = rounds.getJsonObject(j).getInt("left");
				oldRound.add("left",left) ;
				int time = rounds.getJsonObject(j).getInt("time");
                oldRound.add("time",time) ;
				oldRounds.add(oldRound);
			}
			newProfile.add("rounds",oldRounds) ;
			newProfiles.add(newProfile);
        }
		JsonObjectBuilder newProfile = Json.createObjectBuilder();
		JsonArrayBuilder newRounds = Json.createArrayBuilder();
		JsonObjectBuilder newRound = Json.createObjectBuilder();
		newRound.add("score",s.getTotalScore()) ;
		newRound.add("elo",s.getElo()) ;
		newRound.add("won",s.getWin()) ;
		newRound.add("lost",s.getLoose()) ;
		newRound.add("left",s.getQuit()) ;
		newRound.add("time",s.getTime()) ;
		newRounds.add(newRound);
		
		newProfile.add("uuid",s.getUUID());
		newProfile.add("played",s.getPlayedGames());
		newProfile.add("rounds",newRounds) ;
		newProfiles.add(newProfile);
		jsonBuilder.add("profiles", newProfiles);
		JsonObject root = jsonBuilder.build();	
		OutputStream output = new FileOutputStream(PATH);
		JsonWriter writer = Json.createWriter(output);
		writer.writeObject(root);
		writer.close();
		output.close();
	}
	
	/**
	 * @return number of played game(s)
	 * @throws IOException stream problem
	 */
	public int loadPlayed() throws IOException
	{
		InputStream input = new FileInputStream(PATH);
		// create a JsonReader from Json
		JsonReader reader = Json.createReader(input);	
		// get the JsonObject
		JsonObject json = reader.readObject();
		input.close() ;
		reader.close() ;
		JsonArray profiles = json.getJsonArray("profiles");
		for(int i = 0; i < profiles.size(); i++){
        	String uuid = profiles.getJsonObject(i).getString("uuid");
        	if(uuid.equals(this.uuid)) {
                int played = profiles.getJsonObject(i).getInt("played");
                return played;
            }
        }
        return 0;
	}
	
	/**
	 * @return number of games won
	 * @throws IOException stream problem
	 */
	public int loadWon() throws IOException
	{
		InputStream input = new FileInputStream(PATH);
		// create a JsonReader from Json
		JsonReader reader = Json.createReader(input);	
		// get the JsonObject
		JsonObject json = reader.readObject();
		input.close() ;
		reader.close() ;
		JsonArray profiles = json.getJsonArray("profiles");
		for(int i = 0; i < profiles.size(); i++){
        	String uuid = profiles.getJsonObject(i).getString("uuid");
        	if(uuid.equals(this.uuid)) {
        		JsonArray rounds = profiles.getJsonObject(i).getJsonArray("rounds");
        		int won = rounds.getJsonObject(0).getInt("won");
                return won;
            }
        }
        return 0;
	}
	
	/**
	 * @return number of lost games
	 * @throws IOException stream problem
	 */
	public int loadLost() throws IOException
	{
		InputStream input = new FileInputStream(PATH);
		// create a JsonReader from Json
		JsonReader reader = Json.createReader(input);	
		// get the JsonObject
		JsonObject json = reader.readObject();
		input.close() ;
		reader.close() ;
		JsonArray profiles = json.getJsonArray("profiles");
		for(int i = 0; i < profiles.size(); i++){
        	String uuid = profiles.getJsonObject(i).getString("uuid");
        	if(uuid.equals(this.uuid)) {
        		JsonArray rounds = profiles.getJsonObject(i).getJsonArray("rounds");
        		int lost = rounds.getJsonObject(0).getInt("lost");
                return lost;
            }
        }
        return 0;
	}
	
	/**
	 * @return number of left games
	 * @throws IOException stream problem
	 */
	public int loadLeft() throws IOException
	{
		InputStream input = new FileInputStream(PATH);
		// create a JsonReader from Json
		JsonReader reader = Json.createReader(input);	
		// get the JsonObject
		JsonObject json = reader.readObject();
		input.close() ;
		reader.close() ;
		JsonArray profiles = json.getJsonArray("profiles");
		for(int i = 0; i < profiles.size(); i++){
        	String uuid = profiles.getJsonObject(i).getString("uuid");
        	if(uuid.equals(this.uuid)) {
        		JsonArray rounds = profiles.getJsonObject(i).getJsonArray("rounds");
        		int left = rounds.getJsonObject(0).getInt("left");
                return left;
            }
        }
        return 0;
	}
	
	/**
	 * @return played time
	 * @throws IOException stream problem
	 */
	public int loadTime() throws IOException
	{
		InputStream input = new FileInputStream(PATH);
		// create a JsonReader from Json
		JsonReader reader = Json.createReader(input);	
		// get the JsonObject
		JsonObject json = reader.readObject();
		input.close() ;
		reader.close() ;
		JsonArray profiles = json.getJsonArray("profiles");
		for(int i = 0; i < profiles.size(); i++){
        	String uuid = profiles.getJsonObject(i).getString("uuid");
        	if(uuid.equals(this.uuid)) {
        		JsonArray rounds = profiles.getJsonObject(i).getJsonArray("rounds");
        		int time = rounds.getJsonObject(0).getInt("time");
                return time;
            }
        }
        return 0;
	}
	
	/**
	 * @return elo score
	 * @throws IOException stream problem
	 */
	public int loadElo() throws IOException
	{
		InputStream input = new FileInputStream(PATH);
		// create a JsonReader from Json
		JsonReader reader = Json.createReader(input);	
		// get the JsonObject
		JsonObject json = reader.readObject();
		input.close() ;
		reader.close() ;
		JsonArray profiles = json.getJsonArray("profiles");
		for(int i = 0; i < profiles.size(); i++){
        	String uuid = profiles.getJsonObject(i).getString("uuid");
        	if(uuid.equals(this.uuid)) {
        		JsonArray rounds = profiles.getJsonObject(i).getJsonArray("rounds");
        		int elo = rounds.getJsonObject(0).getInt("elo");
                return elo;
            }
        }
        return 0;
	}
	
	
	/**
	 * @return data for rounds
	 * @throws IOException stream problem
	 */
	public List<Round> loadRounds() throws IOException
	{
		List<Round> statGames = new ArrayList<>();
		InputStream input = new FileInputStream(PATH);
		// create a JsonReader from Json
		JsonReader reader = Json.createReader(input);	
		// get the JsonObject
		JsonObject json = reader.readObject();
		input.close() ;
		reader.close() ;
		JsonArray profiles = json.getJsonArray("profiles");
		for(int i = 0; i < profiles.size(); i++){
        	String uuid = profiles.getJsonObject(i).getString("uuid");
        	if(uuid.equals(this.uuid)) {
                JsonArray rounds = profiles.getJsonObject(i).getJsonArray("rounds");
                for(int j = 0; j < rounds.size(); j++) {
                	int score = rounds.getJsonObject(j).getInt("score");
                	int elo = rounds.getJsonObject(j).getInt("elo");                 
                	int won = rounds.getJsonObject(j).getInt("won");
                	int lost = rounds.getJsonObject(j).getInt("lost");
                	int left = rounds.getJsonObject(j).getInt("left");
                    int time = rounds.getJsonObject(j).getInt("time");
                    statGames.add(new Round(score,elo,won,lost,left,time));
                }
               return statGames;
            }
        }
		return null;
	}
	
	/**
	 * @return uuid
	 */
	public String getUUID()
	{
		return this.uuid;
	}
	
	/**
	 * @return played games
	 */
	public int getPlayedGames()
	{
		return this.played;
	}
	
	/**
	 * @return won game
	 */
	public int getWin()
	{
		return this.won;
	}
	
	/**
	 * @return loose game
	 */
	public int getLoose()
	{
		return this.lost;
	}
	
	/**
	 * @return left game
	 */
	public int getQuit()
	{
		return this.left;
	}
	
	/**
	 * @return total game time
	 */
	public int getTime()
	{
		return this.time;
	}
	
	/**
	 * @return elo score
	 */
	public int getElo()
	{
		return this.elo;
	}
	
	/**
	 * @return name of profile's stats
	 */
	public String getName()
	{
		return Profile.getName(getUUID());
	}
	
	/**
	 * @return list of rounds
	 */
	public List<Round> getRounds()
	{
		return this.rounds;
	}
	
	public int getTotalScore()
	{
		int total = 0;
		for(Round r : rounds)
		{
			total += r.getScore();
		}
		return total;
	}
	
	/**
	 * to transform time in a string with seconds and minutes
	 * @return string
	 */
	public String getStringTime()
	{
		int min = getTime()/60;
		int sec = getTime()%60;
		if(min != 0) return min+"min"+sec+"s";
		return sec+"s";
	}
	
	// HERE COMPARATORS TO SORT LISTS OF STATS
	public static class StatbyPlayedGames implements Comparator<Stat> {

        @Override
        public int compare(Stat s1, Stat s2) {
            return s2.played - s1.played;
        }
    }
	
	public static class StatbyWin implements Comparator<Stat> {

        @Override
        public int compare(Stat s1, Stat s2) {
            return s2.won - s1.won;
        }
    }
	
	public static class StatbyLoose implements Comparator<Stat> {

        @Override
        public int compare(Stat s1, Stat s2) {
            return s2.lost - s1.lost;
        }
    }
	
	public static class StatbyQuit implements Comparator<Stat> {

        @Override
        public int compare(Stat s1, Stat s2) {
            return s2.left - s1.left;
        }
    }
	
	public static class StatbyTime implements Comparator<Stat> {

        @Override
        public int compare(Stat s1, Stat s2) {
            return s2.time - s1.time;
        }
    }
	
	public static class StatbyElo implements Comparator<Stat> {

        @Override
        public int compare(Stat s1, Stat s2) {
            return s2.elo - s1.elo;
        }
    }
	
	@Override
	public int compareTo(Stat s) {
		return this.getName().compareTo(s.getName());
	}
}

