package com.pingu.party.stats;

/**
 * class Round<br>
 * represent data of rounds<br>
 * @author florian
 *
 */
public class Round {
	/**
	 * round score
	 */
	private int score;
	/**
	 * score elo
	 */
	private int elo;
	/**
	 * number of won game(s)
	 */
	private int won;
	/**
	 * number of lost game(s)
	 */
	private int lost;
	/**
	 * left game(s)
	 */
	private int left;
	/**
	 * played time
	 */
	private int time;
	
	
	/**
	 * @param score score for this round
	 * @param elo round's score elo
	 * @param won number of won rounds
	 * @param lost number of lost rounds
	 * @param left number of left games
	 * @param time round's time
	 */
	public Round(int score, int elo, int won, int lost, int left, int time)
	{
		this.score = score;
		this.elo = elo;
		this.won = won;
		this.lost = lost;
		this.left = left;
		this.time = time;
	}
	
	/**
	 * @return score of this round
	 */
	public int getScore()
	{
		return this.score;
	}
	
	/**
	 * @return elo score of this round
	 */
	public int getElo()
	{
		return this.elo;
	}
	
	/**
	 * @return won rounds
	 */
	public int getWon()
	{
		return this.won;
	}
	
	/**
	 * @return lost rounds
	 */
	public int getLost()
	{
		return this.lost;
	}
	
	/**
	 * @return left rounds
	 */
	public int getLeft()
	{
		return this.left;
	}
	
	/**
	 * @return round's time
	 */
	public int getTime()
	{
		return this.time;
	}	
}
