package com.pingu.party.stats;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.io.IOException;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 * class StatManager<br>
 * to manage all the stats
 * @author florian
 *
 */
public class StatManager {
	
	/**
	 * path of the stats database
	 */
	private static final String PATH = "src/com/pingu/party/engine/data/stats.json" ;
	/**
	 * array of stats for an user
	 */
	private List<Stat> stats;
	
	/**
	 * load all stats
	 */
	public StatManager()
	{
		try {
			this.stats = new ArrayList<Stat>();
			InputStream input = new FileInputStream(PATH);
			// create a JsonReader from Json
			JsonReader reader = Json.createReader(input);	
			// get the JsonObject
			JsonObject json = reader.readObject();
			input.close() ;
			reader.close() ;
			JsonArray profiles = json.getJsonArray("profiles");
			for(int i = 0; i < profiles.size(); i++){
	        	String uuid = profiles.getJsonObject(i).getString("uuid");
	        	this.stats.add(new Stat(uuid));
			}
		} catch(IOException e) {
			System.out.println("Stream Error");
		}
		
	}
	
	/**
	 * @return number of stats
	 */
	public int size()
	{
		return this.stats.size();
	}
	
	/**
	 * @return list of stats
	 */
	public List<Stat> getList()
	{
		return this.stats;
	}
	
	/**
	 * @return sorted list by name
	 */
	public List<Stat> sortName()
	{
		Collections.sort(this.stats);
		return this.stats;
	}
	
	/**
	 * @return sorted list by number of played games
	 */
	public List<Stat> sortPlayedGames()
	{
		Collections.sort(this.stats, new Stat.StatbyPlayedGames());
		return this.stats;
	}
	
	/**
	 * @return sorted list by number of games won
	 */
	public List<Stat> sortWin()
	{
		Collections.sort(this.stats, new Stat.StatbyWin());
		return this.stats;
	}
	
	/**
	 * @return sorted list by number of lost games
	 */
	public List<Stat> sortLoose()
	{
		Collections.sort(this.stats, new Stat.StatbyLoose());
		return this.stats;
	}
	
	/**
	 * @return sorted list by number of left games
	 */
	public List<Stat> sortQuit()
	{
		Collections.sort(this.stats, new Stat.StatbyQuit());
		return this.stats;
	}
	
	/**
	 * @return sorted list by number of time played
	 */
	public List<Stat> sortTime()
	{
		Collections.sort(this.stats, new Stat.StatbyTime());
		return this.stats;
	}
	
	/**
	 * @return sorted list by Elo score
	 */
	public List<Stat> sortElo()
	{
		Collections.sort(this.stats, new Stat.StatbyElo());
		return this.stats;
	}
}
