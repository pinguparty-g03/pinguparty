package com.pingu.party.gui.toaster;

import com.pingu.party.engine.logger.Logger;
import java.util.ArrayList;

import com.pingu.party.launcher.Utils.UIConsts;
import com.pingu.party.engine.core.GameCanvas;
import com.pingu.party.engine.loader.Module;

public class Toaster implements Module, Logger {
  private boolean visible;
  private String secret = "Toaster001";
  public String name;
  final double id;
  public int LO;

  public String desc;
  public double[] requiements;

  protected ArrayList<ToasterPanel> toasts;

  private int width;

  public static enum level { // TODO: move me to the logger
    DEBUG,
    INFO,
    WARNING,
    ERROR,
  }

  public Toaster() {
    this.toasts = new ArrayList<ToasterPanel>();
    this.id = 62161990; // Secret hashed
    this.name = "Toaster";

    this.visible  = false;
  }

  public void error(String message, long life) {
    ToasterPanel tp = new ToasterPanel(message, level.ERROR, (life>100)?life:500);
    addToast(tp);
  }
  public void warn(String message, long life) {
    ToasterPanel tp = new ToasterPanel(message, level.WARNING, (life>100)?life:500);
    addToast(tp);
  }
  public void info(String message, long life) {
    ToasterPanel tp = new ToasterPanel(message, level.INFO, (life>100)?life:500);
    addToast(tp);
  }
  public void debug(String message, long life) {
    ToasterPanel tp = new ToasterPanel(message, level.DEBUG, (life>100)?life:500);
    addToast(tp);
  }

  public void addToast(ToasterPanel tp) {
    synchronized(this.toasts) {
      this.toasts.add(tp);
    }
  }

  public void showAll() {
	  synchronized(this.toasts) {
	    for (int i = this.toasts.size()-1; i>=0 ;i-- ) {
	    	if(!this.toasts.get(i).isDead()) {
	    		this.toasts.get(i).setVisible(true);
	    	}
	    }
	  }
    this.visible = true;
  }

  public void load(GameCanvas gc) {
    //gc.log("Toaster loaded !", 0);

  }	// Used after init, for enabling tasks
  public void unload(GameCanvas gc) {
    System.out.print("Toaster unloaded . . .");

  } // Used when crashes and user-asked unload
  public String getName() {
    return this.name;
  } // Used for logs
  public int getID() {
    return 62161990;
  }
  public void setOrder(int o) {

  }
  public int getOrder() {
    return 0;
  }

  public void init(GameCanvas gc) {
    this.width = gc.getWidth();
    /*
    ToasterPanel tp = new ToasterPanel("Toaster created !");
    ToasterPanel tp2 = new ToasterPanel("Toaster created !", level.DEBUG);
    ToasterPanel tp3 = new ToasterPanel("Toaster created !", level.INFO);
    ToasterPanel tp4 = new ToasterPanel("Toaster created !", level.WARNING);
    ToasterPanel tp5 = new ToasterPanel("Toaster created !", level.ERROR);
    addToast(tp);
    addToast(tp2);
    addToast(tp3);
    addToast(tp4);
    addToast(tp5);*/
  }
  @Override
  public void render(GameCanvas gc ) {
    int top = 0;
    if(this.visible) {
      synchronized(this.toasts) {
        if(this.toasts.isEmpty()) return;
        for (int i = 0; i<this.toasts.size() ;i++ ) {
          //gc.getG2D().setColor(this.toasts.get(i).getBG());
          this.toasts.get(i).setPos(this.width, top+4);
          this.toasts.get(i).draw(gc.getG2D());
          top += 28;
        }
      }
    }
  }
  public void update(GameCanvas gc) {
    synchronized(this.toasts) {
      if(this.toasts.isEmpty()) return;
      for (int i = 0; i<this.toasts.size() ;i++ ) {
        if(this.toasts.get(i).isDead()) this.toasts.remove(i);
        if(i>=this.toasts.size()) return;
        this.toasts.get(i).consumeLife();
      }
    }
  }
  public void freeze(GameCanvas gc) {

  }
  public void resume(GameCanvas gc) {

  }
}
