package com.pingu.party.gui.toaster;

import java.awt.AlphaComposite;

import com.pingu.party.gui.toaster.Toaster.level;

import com.pingu.party.engine.config.Config;
import com.pingu.party.launcher.Utils.UIConsts;
import java.awt.Graphics2D;
import java.awt.Color;
import javax.swing.JPanel;

public class ToasterPanel extends JPanel {
  private String message;
  private Color bg;
  private Color fg;
  private int sw, width, height, y;

  private long life = 1000;

  private boolean visible = false;

  public ToasterPanel(String message, level l, long life) {
    this(message, l);
    this.life = life;
  }
  public ToasterPanel(String message, long life) {
    this(message);
    this.life = life;
  }
  
  public void setVisible(boolean b) {
	  this.visible = b;
  }
  public ToasterPanel(String message, level l) {
    this(message);
    switch (l) {
      case DEBUG : this.bg = new Color(131,152,167);
        this.fg = new Color(218,225,229);
        break;
      case INFO : this.bg = new Color(87,161,211);
        this.fg = new Color(205,227,242);
        break;
      case WARNING : this.bg = new Color(205,166,29);
        this.fg = new Color(240,229,188);
        break;
      case ERROR : this.bg = new Color(205,65,72);
        this.fg = new Color(240,198,201);
        break;
    }
  }

  public ToasterPanel(String message) {
    this.bg = UIConsts.COLOR_BLUE;
    this.fg = UIConsts.COLOR_WHITE;
    this.message = message;
    this.sw = Config.WIDTH;
    //this.width = (int)(sw/3);
    this.width = 36;
    this.height = 24;
    this.y = 4;
  }

  public void consumeLife() {
    this.life--;
  }

  public void setLife(long life /* to the king */) {
    this.life = life;
  }

  public boolean isDead() {
    return (this.life<=0)?true:false;
  }

  public void draw(Graphics2D g2d) {
    if (this.life>0) {
      super.paintComponent(g2d);

      //int wh = (this.sw-this.width)/2;
      //int wc = (12+this.message.length()*8);
      int wc = (24+g2d.getFontMetrics().stringWidth(this.message));
      if(wc>this.width) {
        this.width = wc;
      }
      int wh = (this.sw-this.width)/2;
      //wh = (wh>this.width)?(wh+12):((this.sw-this.width)/2);

      g2d.setColor(this.bg);
      g2d.fillRoundRect(wh, this.y, this.width, this.height, 8, 8);
      //g2d.fillRoundRect(0, 0, getWidth()/2, 32, 8, 8);

      g2d.setFont(UIConsts.ROBOTO_LIGHT);
      g2d.setColor(this.fg);
      g2d.drawString(this.message, wh+12, this.y+18);
    }
  }

  public void setPos(int w, int y) {
    this.y = y;
    this.sw = w;
    //setBounds((w-this.width)/2, y, this.width, this.height);
  }

  public String getMessage() {
    return this.message;
  }
  public Color getBG() {
    return this.bg;
  }
  public Color getFG() {
    return this.fg;
  }
}
