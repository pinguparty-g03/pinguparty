package com.pingu.party.gui.test;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

/**
 * @author Pho3
 *
 * Some Gradient panel 
 *
 */
public class Graphics00Panel extends JPanel {
	@Override
	protected void paintComponent(Graphics gr) {
		super.paintComponents(gr);
		Graphics2D g2d = (Graphics2D) gr;
		g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		final Color StartColor = new Color(27,83,178);
		final Color StopColor = new Color(114,204,240);
		GradientPaint grpnt = new GradientPaint(0, 0, StartColor, 0, getHeight(), StopColor);
    // TODO: Gradient to 75% Height ?
		g2d.setPaint(grpnt);
		g2d.fillRect(0, 0, getWidth(), getHeight());
	}
}
