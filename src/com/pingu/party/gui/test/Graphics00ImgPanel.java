package com.pingu.party.gui.test;

import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Graphics00ImgPanel extends JPanel {

	Image dlgImg;

	private BufferedImage BdlgImg;

	@Override
	protected void paintComponent(Graphics gr) {
		super.paintComponents(gr);
		Graphics2D g2d = (Graphics2D) gr;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f));
		g2d.fillRect(0, 0, getWidth(), getHeight());
	}

	/*public void paint(Graphics gr) {
		ImageIcon Iimg = new ImageIcon("res/dlg.png");
		Image img = Iimg.getImage();
		BdlgImg = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		Graphics2D b2d = BdlgImg.createGraphics();
		b2d.drawImage(img, 0, 0, null);
		b2d.dispose();

		//super.paintComponent(gr);
		Graphics2D g2d = (Graphics2D) gr;
		g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);

		//AffineTransform aft = AffineTransform.getTranslateInstance(this.getWidth() - BdlgImg.getWidth(), this.getHeight() - BdlgImg.getHeight());
		//aft.scale(0.5f, 0.5f);
		//g2d.drawRenderedImage(BdlgImg, aft);

		gr.drawImage(dlgImg, 0, 0, null);
		g.setColor(Color.BLACK);
		//gr.drawRect(0, 0, 672, 941);
	}*/
}
