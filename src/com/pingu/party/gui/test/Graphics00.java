package com.pingu.party.gui.test;

import com.pingu.party.gui.test.Graphics00Panel;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Font;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import java.awt.FlowLayout;
import javax.swing.JSplitPane;
import javax.swing.border.Border;

/**
 * @author Pho3
 *
 * Graphics00 test
 *
 */
public class Graphics00 extends JFrame {

	/**
	 * Panel declaration
	 */
	private Graphics00Panel contentPane;

	Image nepImg;
	ImageIcon icoImg = new ImageIcon("res/noot30.png");

	/**
	 * Launch the application.
	 *
	 * @param args run
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					Graphics00 frame = new Graphics00();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/*public void paint(Graphics gr) {
		ImageIcon img = new ImageIcon("res/nepp.png");
		nepImg = img.getImage();
		gr.drawImage(nepImg, 0, 0, null);
		g.setColor(Color.BLACK);
		//gr.drawRect(0, 0, 672, 941);
	}*/

	/**
	 * Create the frame.
	 */
	public Graphics00() {
		this.setBounds(100, 100, 920, 540);
		this.setTitle("MegaNootMention Pingu");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setIconImage(this.icoImg.getImage());
		this.contentPane = new Graphics00Panel();
		this.contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panel = new Graphics00ImgPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		this.contentPane.add(panel, BorderLayout.SOUTH);

		JTextPane txtDialog = new JTextPane();
		txtDialog.setForeground(new Color(242, 243, 241));
		txtDialog.setEditable(false);
		txtDialog.setText("*EPIC BGM PLAYS*");
		txtDialog.setOpaque(false);
		panel.add(txtDialog);

		JPanel panel_1 = new Graphics00PictPanel();
		this.contentPane.add(panel_1, BorderLayout.CENTER);
	}

}
