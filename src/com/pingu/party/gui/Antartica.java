package com.pingu.party.gui;

import javax.swing.plaf.metal.DefaultMetalTheme;
import javax.swing.plaf.ColorUIResource;

public class Antartica extends DefaultMetalTheme {
	public String getName() { return "Antartica"; }

  /** Borders */
   private final ColorUIResource primary1 = new ColorUIResource(72, 87, 92);

   /** Sub title bar */
   private final ColorUIResource primary2 = new ColorUIResource(72, 87, 92);

   /** The primary3. */
   private final ColorUIResource primary3 = new ColorUIResource(72, 87, 92);

   /** The secondary1. */
   private final ColorUIResource secondary1 = new ColorUIResource(36, 54, 61);

   /** The secondary2. */
   private final ColorUIResource secondary2 = new ColorUIResource(36, 54, 61);

   /** The secondary3. */
   private final ColorUIResource secondary3 = new ColorUIResource(36, 54, 61);

   /** The black. */
   private final ColorUIResource black = new ColorUIResource(193, 198, 200);

   /** The white. */
   private final ColorUIResource white = new ColorUIResource(72, 87, 92);

   /* (non-Javadoc)
    * @see javax.swing.plaf.metal.DefaultMetalTheme#getPrimary1()
    */
   protected ColorUIResource getPrimary1() { return primary1; }

   /* (non-Javadoc)
    * @see javax.swing.plaf.metal.DefaultMetalTheme#getPrimary2()
    */
   protected ColorUIResource getPrimary2() { return primary2; }

   /* (non-Javadoc)
    * @see javax.swing.plaf.metal.DefaultMetalTheme#getPrimary3()
    */
   protected ColorUIResource getPrimary3() { return primary3; }

   /* (non-Javadoc)
    * @see javax.swing.plaf.metal.DefaultMetalTheme#getSecondary1()
    */
   protected ColorUIResource getSecondary1() { return secondary1; }

   /* (non-Javadoc)
    * @see javax.swing.plaf.metal.DefaultMetalTheme#getSecondary2()
    */
   protected ColorUIResource getSecondary2() { return secondary2; }

   /* (non-Javadoc)
    * @see javax.swing.plaf.metal.DefaultMetalTheme#getSecondary3()
    */
   protected ColorUIResource getSecondary3() { return secondary3; }

   /* (non-Javadoc)
    * @see javax.swing.plaf.metal.MetalTheme#getBlack()
    */
   protected ColorUIResource getBlack() { return black; }

   /* (non-Javadoc)
    * @see javax.swing.plaf.metal.MetalTheme#getWhite()
    */
   protected ColorUIResource getWhite() { return white; }
}
