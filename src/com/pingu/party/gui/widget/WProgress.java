package com.pingu.party.gui.widget;

import java.awt.Color;
import com.pingu.party.launcher.Utils.UIConsts;
import java.awt.Graphics2D;

public class WProgress implements WComponent {
  int x;
  int y;
  int w;
  int h;
  private int dx, dy;
  private int progress;
  private Color bg;
  private Color fg;

  public WProgress(int x, int y, int w, int h, Color fg, Color bg) {
    this.dx = x;
    this.dy = y;
    this.w = w;
    this.h = h;
    this.progress = 0;
    this.bg = bg;
    this.fg = fg;
    this.x = 0;
    this.y = 0;
  }
  public WProgress(int x, int y, int w, int h) {
    this(x, y, w, h, UIConsts.COLOR_BUTTON, UIConsts.COLOR_BUTTON_HOVER);
  }

  public WProgress(int x, int y, int w, int h, Color fg) {
    this(x, y, w, h, UIConsts.COLOR_BUTTON, fg);
  }

  public void setProgress(int v) {
    this.progress = (v+this.progress>100)?100:v+this.progress;
  }
  public void step() {
    this.progress = (this.progress<100)?this.progress+1:100;
  }
  public void step(int v) {
    this.progress = (this.progress<100-v)?this.progress+v:100;
  }

  public int getProgress() {
    return this.progress;
  }

  public boolean isCompleted() {
    return this.progress==100;
  }

  public void recalc() {

  }
  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public void setX(int x) {
    this.x = x;
  }
  public void setY(int y) {
    this.y = y;
  }
  public void setDX(int x) {
    this.dx = x;
  }
  public void setDY(int y) {
    this.dy = y;
  }

  public void draw(Graphics2D g2d) {
    g2d.setColor(this.bg);
    g2d.fillRoundRect(this.dx+this.x, this.dy+this.y, this.w, this.h, this.h, this.h); // x y w h br br
    g2d.setColor(this.fg);
    g2d.fillRoundRect(this.dx+this.x, this.dy+this.y, (int)((this.w/100f)*this.progress), this.h, this.h, this.h); // x y w h br br
  }
}
