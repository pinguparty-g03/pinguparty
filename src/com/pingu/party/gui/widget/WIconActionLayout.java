package com.pingu.party.gui.widget;

import com.pingu.party.engine.config.Config;

import java.awt.Color;
import com.pingu.party.launcher.Utils.UIConsts;
import java.awt.Graphics2D;

public class WIconActionLayout implements WComponent {
  int x;
  int y;
  private int dx, dy;
  int w;
  int h;
  private int border;
  private Color bg;

  private boolean visible;

  private boolean drawBG = true;

  private int iconWidth = 64;

  private WComponent inner;
  private WComponent icon;
  private WComponent action;

  public WIconActionLayout(int x, int y, int w, int h, int iconWidth, Color c, WComponent icon, WComponent inner) {
    this.dx = x;
    this.dy = y;
    this.w = w;
    this.h = h;
    this.iconWidth = iconWidth;
    this.bg = c;
    this.x = 0;
    this.y = 0;
    this.setIcon(icon);
    this.setInner(inner);
    this.visible = true;
  }
  public WIconActionLayout(int x, int y, WComponent icon, WComponent inner) {
    this(x, y,new Color(0,0,0,42), icon, inner);
    this.drawBG = false;
  }
  public WIconActionLayout(int x, int y, Color c, WComponent icon, WComponent inner) {
    if(icon == null) icon = new WStatusIcon(64, 64, "res/UI/menu/serv.png");
    if(inner == null) inner = new WBox(0, 0, 404, 64);
    this.dx = x;
    this.dy = y;
    this.w = icon.getWidth()+inner.getWidth();
    this.h = (icon.getHeight()>inner.getHeight())?(icon.getHeight()):(inner.getHeight());
    this.iconWidth = icon.getWidth();
    this.bg = c;
    this.x = 0;
    this.y = 0;
    this.setIcon(icon);
    this.setInner(inner);
    this.visible = true;
  }
  public WIconActionLayout(int x, int y, int w, int h, int iconWidth, Color c) {
    this.dx = x;
    this.dy = y;
    this.w = w;
    this.h = h;
    this.iconWidth = iconWidth;
    this.bg = c;
    this.x = 0;
    this.y = 0;
    this.visible = true;
  }
  public WIconActionLayout(int x, int y, int w, int iconWidth, int h) {
    this(x, y, w, h, iconWidth, new Color(0,0,0,42));
    this.drawBG = false;
  }
  public WIconActionLayout(int x, int y, int w, int h) {
    this(x, y, w, h, 64, new Color(0,0,0,42));
    this.drawBG = false;
  }

  public void setInner(WComponent w) {
    w.setDX(this.dx+this.iconWidth+w.getDX());
    w.setDY(w.getDY()+this.dy);
    w.setXY(this.x, this.y);
    this.inner = w;
    if(this.h<w.getHeight()) this.h = w.getHeight();
  }

  public WComponent getInner() {
    return this.inner;
  }

  public void setIcon(WComponent w) {
    w.setDX(w.getDX()+this.dx);
    w.setDY(w.getDY()+this.dy);
    w.setXY(this.x, this.y);
    this.icon = w;
    if(this.w<w.getWidth()+w.getDX()) this.w = w.getWidth()+w.getDX();
    if(this.h<w.getHeight()+w.getDY()*2) this.h = w.getHeight()+w.getDY()*2;
    if(this.iconWidth<w.getWidth()+w.getDX()) this.iconWidth = w.getWidth()+w.getDX();
  }
  public void setIconWidth(int w) {
    this.iconWidth = w;
  }

  public void setAction(WComponent w) {
    w.setDX(this.dx+this.iconWidth+(this.w-w.getDX()));
    w.setDY(w.getDY()+this.dy);
    w.setXY(this.x, this.y);
    this.action = w;
    if(this.h<w.getHeight()) this.h = w.getHeight();
  }

  public void recalc() {
    this.inner.recalc();
    this.icon.recalc();
    if(this.action != null) this.action.recalc();
  }
  public void setX(int x) {
    this.x = x;
    if(this.inner != null) this.inner.setX(x);
    if(this.icon != null) this.icon.setX(x);
    if(this.action != null) this.action.setX(x);
  }
  public void setDX(int x) {
    this.dx = x;
  }
  public void setY(int y) {
    this.y = y;
    if(this.inner != null) this.inner.setY(y);
    if(this.icon != null) this.icon.setY(y);
    if(this.action != null) this.action.setY(y);
  }
  public void setDY(int y) {
    this.dy = y;
    if(this.inner != null) this.inner.setDY(this.inner.getDY()+y);
    if(this.icon != null) this.icon.setDY(this.icon.getDY()+y);
    if(this.action != null) this.action.setDY(this.action.getDY()+y);
  }
  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
    if(this.inner != null) this.inner.setXY(x, y);
    if(this.icon != null) this.icon.setXY(x, y);
    if(this.action != null) this.action.setXY(x, y);
  }

  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }

  public int getX() {
    return this.x;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public int getY() {
    return this.y;
  }

  public boolean isVisible() {
    return visible;
  }

  public void show() {
    visible = true;
  }

  public void hide() {
    visible = false;
  }


  public void draw(Graphics2D g2d) {
    if (visible) {      
      g2d.setColor(this.bg);
      if(Config.GUI_TRACE || this.drawBG) {
        g2d.fillRect(this.dx+this.x, this.dy+this.y, this.w, this.h); // x y w h br br
      }
      if(this.inner != null) {
        synchronized (this.inner) {
          this.inner.draw(g2d);
        }
      }
      if(this.icon != null) {
        synchronized (this.icon) {
          this.icon.draw(g2d);
        }
      }
      if(this.action != null) {
        synchronized (this.action) {
          this.action.draw(g2d);
        }
      }
    }
  }
}
