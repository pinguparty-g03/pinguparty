package com.pingu.party.gui.widget;

import com.pingu.party.gui.widget.Anchor.alignType;

import java.util.ArrayList;
import java.awt.Graphics2D;
import com.pingu.party.engine.config.Config;

public class Widget {
  private int x, y, w, h;
  private ArrayList<WComponent> elems;
  private boolean fixed = true;
  private Anchor align = new Anchor();
  private int dx, dy;
  private final int _x, _y, _w, _h;

  public Widget(int x, int y, int w, int h, boolean f) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this._x = x;
    this._y = y;
    this._w = w;
    this._h = h;
    this.fixed = f;
    this.dx = 0;
    this.dy = 0;
    this.elems = new ArrayList<WComponent>();
  }
  /**
   * Wiget
   * @param x x pos >
   * @param y y pos \/
   * @param w widget width =
   * @param h widget height ||
   */
  public Widget(int x, int y, int w, int h) {
    this(x, y, w, h, true);
  }
  public Widget() {
    this(0,0,Config.WIDTH,Config.HEIGHT);
  }

  /*
  Getters and setters
  */

  public void setMargin(int mx, int my) {
    this.dx = mx;
    this.dy = my;
  }

  public void setVerticalMargin(int m) {
    this.dy = m;
  }

  public void setHorizontalMargin(int m) {
    this.dx = m;
  }

  public boolean isFixed() {
   return this.fixed;
  }

  public Anchor getAlign() {
    return this.align;
  }

  public void setAlign(alignType v, alignType h) {
    this.align.setVerticalAlign(v);
    this.align.setHorizontalAlign(h);
  }

  public void setAlign(int v, int h) {
    this.align.setVerticalAlign(v);
    this.align.setHorizontalAlign(h);
  }

  public void setVerticalAlign(int v) {
    this.align.setVerticalAlign(v);
  }

  /**
   * Set Horizontal Alignzmznt
   * @param h number between 0 and 3 :
   * <p>
   * 0: ORIGIN
   * 1: CENTERED
   * 2: REVERSED
   * 3: UNSET (default)
   * </p>
   */
  public void setHorizontalAlign(int h) {
    this.align.setHorizontalAlign(h);
  }

  public alignType getHorizontalAlign() {
    return this.align.getHorizontalAlign();
  }

 public void setFixed(boolean b) {
   this.fixed = b;
 }

  public int getX() {
    return this.x;
  }

  public int getY() {
    return this.y;
  }

  public int getW() {
    return this.w;
  }

  public int getH() {
    return this.h;
  }

  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
    synchronized(this.elems) {
      for (WComponent w : this.elems) {
        w.setXY(x, y);
      }
    }
  }
  /**
  * Add an element to this widget -- Just a test, should be removed
  */
  public void _add() {
    WBox wb = new WBox(16,16,32,32);
    synchronized(this.elems) {
      this.elems.add(wb);
    }
  }
  public void add(WComponent w) {
    synchronized(this.elems) {
      w.setXY(this.x, this.y);
      w.setDX(this.dx+w.getDX());
      w.setDY(this.dy+w.getDY());
      //System.out.println("P<Widget>"+w.getX()+":"+w.getY());
      this.elems.add(w);
    }
  }

  public void remove(WComponent w) {
    synchronized(this.elems) {
      this.elems.remove(w);
    }
  }

  public void removeAll() {
    synchronized (this.elems) {
      this.elems.clear();
    }
  }

  private void doAlign() {
    switch (this.align.getHorizontalAlign()) {
      case ORIGIN: this.x = _x; break;
      case CENTERED: this.x = (Config.WIDTH-this._w)/2; break;
      case REVERSED: this.x = Config.WIDTH-this._w-this.dx; break;
      case UNSET: this.x = 0; break;
    }
    this.setXY(this.x, this.y);
    //System.out.println(""+this.x);
  }


  public void scale() {
    if(!this.fixed) {
      doAlign();
    }
    synchronized(this.elems) {
      for (WComponent wc : this.elems) {
        wc.recalc();
      }
    }
  }

  public synchronized void draw(Graphics2D g2d) {
    synchronized(this.elems) {
      for (WComponent e : this.elems) {
        synchronized (g2d) {
          e.draw(g2d);
        }
      }
    }
    // Override me !
  }
}
