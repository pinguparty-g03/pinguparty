package com.pingu.party.gui.widget;

import com.pingu.party.engine.input.ListenZone;
import com.pingu.party.network.structures.IP;

import com.pingu.party.engine.config.Config;

import com.pingu.party.launcher.Utils.Font.Roboto;
import java.awt.Font;
import java.awt.Color;
import com.pingu.party.launcher.Utils.UIConsts;
import java.awt.Graphics2D;

public class WInput implements WComponent {
  int x;
  int y;
  private int dx, dy;
  int h;
  int w = 256;
  private String caption; // will be converted to E
  protected Font font;
  private Color color;
  private Color debug_color = new Color(0,0,0,42);
  private Color border = UIConsts.COLOR_BLUE;
  private Color select_color = UIConsts.COLOR_BUTTON;
  private Color bg;
  private boolean focus = false;

  private ListenZone clicZone;
  private ListenZone hoverZone;

  public WInput(String s, int x, int y, Font f, Color c, int w, int h) {
    this.dx = x;
    this.dy = y;
    this.h = h;
    this.caption = s;
    this.font = f;
    this.color = c;
    this.x = 0;
    this.y = 0;
    this.bg = UIConsts.COLOR_FOREGROUND;
    this.clicZone = new ListenZone(this.dx, this.dy, this.w, this.h, null);
    this.hoverZone = new ListenZone(this.dx, this.dy, this.w, this.h, null);
    initButtons();
  }
  public WInput(String s, int x, int y) {
    this(s, x, y, new Roboto().get_medium(16), UIConsts.COLOR_MIDGREY, 256, 32);
  }
  public WInput(String s, int x, int y, int fsize) {
    this(s, x, y, new Roboto().get_medium(fsize), UIConsts.COLOR_MIDGREY, 256, fsize+16);
  }
  public WInput(String s, int x, int y, Color c) {
    this(s, x, y, new Roboto().get_medium(16), c, 256, 24);
  }
  public WInput(String s, int x, int y, Color c, int fsize) {
    this(s, x, y, new Roboto().get_medium(fsize), c, 256, fsize+16);
  }
  public WInput(String s, int x, int y, int h, Color c, int fsize) {
    this(s, x, y, new Roboto().get_medium(fsize), c, 256, h);
  }

  public String getString() {
    return this.caption;
  }

  public int getInteger() {
    return Integer.parseInt(this.caption);
  }

  public IP getIP() {
    return IP.parseIP(this.caption);
  }

  public void recalc() {

  }

  public boolean isFocused() {
    return this.focus;
  }

  public void toggleFocus() {
    this.focus = !this.focus;
  }

  private void initButtons() {
    this.clicZone.setAction( () -> {toggleFocus();});
  }


  public void setAction(Runnable action) {
    this.clicZone.setAction(action);
    this.clicZone.setDX(this.dx);
    this.clicZone.setDY(this.dy);
  }
  public void setHoverAction(Runnable action) {
    if(this.hoverZone != null){
      this.hoverZone.setAction(action);
    } else {
      this.hoverZone = new ListenZone(this.dx, this.dy, this.w, this.h, action);
    }
    this.hoverZone.setDX(this.dx);
    this.hoverZone.setDY(this.dy);
  }

  public ListenZone getActionZone() {
    return this.clicZone;
  }

  public ListenZone getHoverZone() {
    return this.hoverZone;
  }

  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
    if(this.clicZone!=null) this.clicZone.setXY(x, y);
    if(this.hoverZone!=null) this.hoverZone.setXY(x, y);
  }

  public void listen() {
    if(!this.focus) return;
    //TODO : this =>
    // Have a listen to key bus
    // parse infos and get them as string (maybe a subclass ? bus.getString() )
    // and then put them in the caption
    System.out.print("o");
  }

  public void setCaption(String s) {
    this.caption = s;
  }

  public String getCaption() {
    return this.caption;
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public void setX(int x) {
    this.x = x;
    if(this.clicZone!=null) this.clicZone.setX(x);
    if(this.hoverZone!=null) this.hoverZone.setX(x);
  }
  public void setY(int y) {
    this.y = y;
    if(this.clicZone!=null) this.clicZone.setY(y);
    if(this.hoverZone!=null) this.hoverZone.setY(y);
  }
  public void setDX(int x) {
    this.dx = x;
    if(this.clicZone!=null) this.clicZone.setDX(x);
    if(this.hoverZone!=null) this.hoverZone.setDX(x);
  }
  public void setDY(int y) {
    //System.out.println("sDY:"+y);
    /*if(y>40) {
      for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
        System.out.println("   "+ste);
      }
    }*/
    this.dy = y;
    if(this.clicZone!=null) this.clicZone.setDY(y);
    if(this.hoverZone!=null) this.hoverZone.setDY(y);
  }

  public void draw(Graphics2D g2d) {
    if(Config.GUI_TRACE) {
      g2d.setColor(this.debug_color);
      if(this.w == 0) this.w = g2d.getFontMetrics(this.font).stringWidth(this.caption);
    }
    g2d.setColor(this.bg);
    g2d.fillRect(this.dx+this.x, this.dy+this.y, this.w, this.h);
    g2d.setFont(this.font);
    if(this.focus) {
      g2d.setColor(this.select_color);
      g2d.drawString(this.caption+"|", this.dx+this.x+8, this.dy+this.y+this.h-10);
      g2d.setColor(this.border);
      g2d.drawRect(this.dx+this.x, this.dy+this.y, this.w-1, this.h-1);
      g2d.drawRect(this.dx+this.x+1, this.dy+this.y+1, this.w-3, this.h-3); //cheat
    } else {
      g2d.setColor(this.color);
      g2d.drawString(this.caption, this.dx+this.x+8, this.dy+this.y+this.h-10);
    }
  }
}
