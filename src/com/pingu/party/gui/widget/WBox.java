package com.pingu.party.gui.widget;

import java.awt.Color;
import com.pingu.party.launcher.Utils.UIConsts;
import java.awt.Graphics2D;

public class WBox implements WComponent {
  int x;
  int y;
  int w;
  int h;
  private int dx;
  private int dy;

  private int border;
  private Color bg;

  public WBox(int x, int y, int w, int h, int border, Color c) {
    this.dx = x;
    this.dy = y;
    this.w = w;
    this.h = h;
    this.x = 0;
    this.y = 0;
    this.border = border;
    this.bg = c;
  }
  public WBox(int x, int y, int w, int h) {
    this(x, y, w, 4, 4, UIConsts.COLOR_BUTTON);
  }

  public WBox(int x, int y, int w, int h, Color c) {
    this(x, y, w, h, 4, c);
  }

  public void recalc() {

  }
  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public void setX(int x) {
    this.x = x;
  }
  public void setY(int y) {
    this.y = y;
  }
  public void setDX(int x) {
    this.dx = x;
  }
  public void setDY(int y) {
    this.dy = y;
  }

  public void draw(Graphics2D g2d) {
    g2d.setColor(this.bg);
    g2d.fillRoundRect(this.x+this.dx, this.y+this.dy, this.w, this.h, this.border, this.border); // x y w h br br
  }
}
