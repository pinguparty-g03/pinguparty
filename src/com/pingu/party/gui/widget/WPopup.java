package com.pingu.party.gui.widget;

import com.pingu.party.launcher.Utils.UIConsts;

import java.awt.Color;
import java.awt.Component;
import com.pingu.party.gui.widget.Widget;
import javax.swing.SwingUtilities;

import com.pingu.party.gui.widget.Anchor.alignType;

import java.util.ArrayList;
import java.awt.Graphics2D;
import com.pingu.party.engine.config.Config;

public class WPopup extends Widget implements WComponent {
  private int x, y, w, h;
  private ArrayList<WComponent> elems;
  private boolean centered = true;
  private Anchor align = new Anchor();

  private Color bg;
  private Color border;
  private boolean hasBorders;

  private int dx, dy;
  private final int _x, _y, _w, _h;

  private boolean visible = false;

  public WPopup(int x, int y, int w, int h, boolean center, Color c, boolean hasBorder, Color border) {
    this.dx = x;
    this.dy = y;
    this.w = w;
    this.h = h;
    this._x = x;
    this._y = y;
    this._w = w;
    this._h = h;
    this.centered = center;
    this.bg = c;
    this.border = border;
    this.hasBorders = hasBorder;
    this.x = 0;
    this.y = 0;
    this.elems = new ArrayList<WComponent>();
    if(center) this.setCentered(true);
  }

  public WPopup(int x, int y, int w, int h, boolean center, Color c, boolean hasBorder) {
    this(x, y, w, h, center, c, hasBorder, (hasBorder)?(UIConsts.COLOR_BUTTON_BORDER):(new Color(0,0,0,0)));
  }
  public WPopup(int x, int y, int w, int h, boolean center, boolean hasBorder) {
    this(x, y, w, h, center, UIConsts.COLOR_BUTTON, hasBorder, (hasBorder)?(UIConsts.COLOR_BUTTON_BORDER):(new Color(0,0,0,0)));
  }
  public WPopup(int x, int y, int w, int h, boolean center) {
    this(x, y, w, h, center, UIConsts.COLOR_BUTTON, false, new Color(0,0,0,0));
  }
  public WPopup(int x, int y, int w, int h) {
    this(x, y, w, h, true);
  }
  public WPopup(int w, int h) {
    this(0, 0, w, h, true, UIConsts.COLOR_BUTTON, true, UIConsts.COLOR_BUTTON_BORDER);
  }
  public WPopup() {
    this((Config.WIDTH-576)/2,(Config.HEIGHT-360)/2,576,360, true);
    this.setCentered(true);
  }

  public void recalc() {
    synchronized(this.elems) {
      for (WComponent wc : this.elems) {
        wc.recalc();
      }
    }
  }

  /*
  Getters and setters
  */

  public void setVisible() {
    this.visible = true;
  }

  public void setVisible(boolean b) {
    this.visible = b;
  }

  public void setHiden() {
    this.visible = false;
  }

  public boolean isVisible() {
    return this.visible;
  }

  private void setChildDX(int x) {
    synchronized(this.elems) {
      for (WComponent w : this.elems) {
        w.setDX(w.getDX()+x);
      }
    }
  }
  private void setChildDY(int y) {
    synchronized(this.elems) {
      for (WComponent w : this.elems) {
        w.setDY(w.getDY()+y);
      }
    }
  }

  public void setDX(int x) {
    this.dx = x;
    setChildDX(x);
  }
  public void setDY(int y) {
    this.dy = y;
    setChildDY(y);
  }

  public void setMargin(int mx, int my) {
    this.dx = mx;
    this.dy = my;
  }

  public void setVerticalMargin(int m) {
    this.dy = m;
  }

  public void setHorizontalMargin(int m) {
    this.dx = m;
  }

  public boolean isCentered() {
   return this.centered;
  }

  public Anchor getAlign() {
    return this.align;
  }

  public void setAlign(alignType v, alignType h) {
    this.align.setVerticalAlign(v);
    this.align.setHorizontalAlign(h);
  }

  public void setAlign(int v, int h) {
    this.align.setVerticalAlign(v);
    this.align.setHorizontalAlign(h);
  }

  public void setVerticalAlign(int v) {
    this.align.setVerticalAlign(v);
  }

  public void setHorizontalAlign(int h) {
    this.align.setHorizontalAlign(h);
  }

  public alignType getHorizontalAlign() {
    return this.align.getHorizontalAlign();
  }

 public void setCentered(boolean b) {
   this.centered = b;
   this.x = (Config.WIDTH-this.w)/2;
   this.y = (Config.HEIGHT-this.h)/2;
 }

  public int getX() {
    return this.x;
  }

  public int getY() {
    return this.y;
  }

  public int getDX() {
    return this.dx;
  }

  public int getDY() {
    return this.dy;
  }

  public int getWidth() {
    return this.w;
  }

  public int getHeight() {
    return this.h;
  }


  public void setX(int x) {
    if(!this.centered) {
      this.x = x;
      synchronized(this.elems) {
        for (WComponent w : this.elems) {
          w.setX(x);
        }
      }
    }
  }
  public void setY(int y) {
    if(!this.centered) {
      this.y = y;
      synchronized(this.elems) {
        for (WComponent w : this.elems) {
          w.setY(y);
        }
      }
    }
  }

  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
    synchronized(this.elems) {
      for (WComponent w : this.elems) {
        w.setXY(x, y);
      }
    }
  }
  /**
  * Add an element to this widget -- Just a test, should be removed
  */
  public void _add() {
    WBox wb = new WBox(16,16,32,32);
    synchronized(this.elems) {
      this.elems.add(wb);
    }
  }
  public void add(WComponent w) {
    synchronized(this.elems) {
      w.setXY(w.getX()+this.x, w.getY()+this.y);
      //System.out.println("P<WPopup>"+w.getX()+":"+w.getY());
      this.elems.add(w);
    }
  }

  private void doAlign() {
    switch (this.align.getHorizontalAlign()) {
      case ORIGIN: this.x = _x; break;
      case CENTERED: this.x = (Config.WIDTH-this._w)/2; break;
      case REVERSED: this.x = Config.WIDTH-this._w-this.dx; break;
      case UNSET: this.x = 0; break;
    }
    this.setXY(this.x, this.y);
    //System.out.println(""+this.x);
  }


  public void scale() {
    if(!this.centered) {
      doAlign();
    } else {
      setXY((Config.WIDTH-this.w)/2, (Config.HEIGHT-this.h)/2);
    }
    synchronized(this.elems) {
      for (WComponent wc : this.elems) {
        wc.recalc();
      }
    }
  }

  public synchronized void draw(Graphics2D g2d) {
    if(this.visible) {
      g2d.setColor(this.bg);
      g2d.fillRect(this.x, this.y, this.w, this.h);
      if(this.hasBorders) {
        g2d.setColor(this.border);
        g2d.drawRect(this.x, this.y, this.w, this.h);
      }
      synchronized(this.elems) {
        for (WComponent e : this.elems) {
          synchronized (g2d) {
            e.draw(g2d);
          }
        }
      }
    }
  }
}
