package com.pingu.party.gui.widget;

import com.pingu.party.engine.core.AssetManager;

import com.pingu.party.launcher.Utils.UIConsts;
import java.awt.Graphics2D;

import com.pingu.party.engine.input.ListenZone;

public class WCheckbox implements WComponent {
  int x;
  int y;
  int w;
  int h;

  private int dx, dy;
  private WImage background;

  private boolean check = false;
  private WImage Icheck;
  private WImage Ichecked;

  private ListenZone clicZone;
  private ListenZone hoverZone;

  public WCheckbox(int x, int y, int w, int h, boolean st) {
    this.dx = x;
    this.dy = y;
    this.x = 0;
    this.y = 0;
    this.w = (w>0)?w:32;
    this.h = (h>0)?h:16;
    this.clicZone = new ListenZone(this.dx, this.dy, this.w, this.h, null);
    this.hoverZone = null;
    this.check = st;
    postCheck();
  }

  private void postCheck() {
    if (this.Icheck == null) {
      this.Icheck = new WImage(AssetManager.getImage("check"));
    }
    if (this.Ichecked == null) {
      this.Ichecked = new WImage(AssetManager.getImage("checked"));
    }
    if(this.background == null) {
      this.background = (this.check)?this.Ichecked:this.Icheck;
    }
  }

  public WCheckbox(int x, int y, int w, int h) {
    this(x, y, w, h, false);
  }

  public WCheckbox(int x, int y, int w, int h, WImage bg) {
    this(x, y, w, h, false);
    this.setBackground(bg);
  }

  public boolean isChecked() {
    return this.check;
  }

  public void toggle() {
    this.check = !this.check;
    int dx = this.background.getDX();
    int dy = this.background.getDY();

    this.background = (this.check)?this.Ichecked:this.Icheck;
    this.background.setXY(this.x, this.y);
    this.background.setDX(dx);
    this.background.setDY(dy);
  }

  public void check() {
    this.check = true;
    this.background = this.Ichecked;
  }

  public void setBackground(WImage bg) {
    if(bg==null) return;
    bg.setXY(this.x, this.y);
    bg.setDX(this.dx);
    bg.setDY(this.dy);
    this.background = bg;
    this.check = false;
  }

  public void setAction(Runnable action) {
    this.clicZone.setAction(action);
    this.clicZone.setDX(this.dx);
    this.clicZone.setDY(this.dy);
  }
  public void setHoverAction(Runnable action) {
    if(this.hoverZone != null){
      this.hoverZone.setAction(action);
    } else {
      this.hoverZone = new ListenZone(this.dx, this.dy, this.w, this.h, action);
    }
    this.hoverZone.setDX(this.dx);
    this.hoverZone.setDY(this.dy);
  }

  public ListenZone getActionZone() {
    return this.clicZone;
  }

  public ListenZone getHoverZone() {
    return this.hoverZone;
  }

  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
    if(this.clicZone!=null) this.clicZone.setXY(x, y);
    if(this.hoverZone!=null) this.hoverZone.setXY(x, y);
    if(this.background!=null) this.background.setXY(x, y);
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public void setX(int x) {
    this.x = x;
    if(this.clicZone!=null) this.clicZone.setX(x);
    if(this.hoverZone!=null) this.hoverZone.setX(x);
    if(this.background != null) this.background.setX(x);
  }
  public void setY(int y) {
    this.y = y;
    if(this.clicZone!=null) this.clicZone.setY(y);
    if(this.hoverZone!=null) this.hoverZone.setY(y);
    if(this.background != null) this.background.setY(y);
  }
  public void setDX(int x) {
    this.dx = x;
    if(this.clicZone!=null) this.clicZone.setDX(x);
    if(this.hoverZone!=null) this.hoverZone.setDX(x);
    if(this.background != null) this.background.setDX(x);
  }
  public void setDY(int y) {
    this.dy = y;
    if(this.clicZone!=null) this.clicZone.setDY(y);
    if(this.hoverZone!=null) this.hoverZone.setDY(y);
    if(this.background != null) this.background.setDY(y);
  }


  public void recalc() {

  }

  public void draw(Graphics2D g2d) {
    g2d.setColor(UIConsts.COLOR_BUTTON);
    if(this.background == null)g2d.fillRoundRect(this.dx+this.x, this.dy+this.y, this.w, this.h, 0, 0); // x y w h br br
    else this.background.draw(g2d);
  }
}
