package com.pingu.party.gui.widget;

import com.pingu.party.engine.config.Config;
import com.pingu.party.engine.input.ListenZone;
import com.pingu.party.launcher.Utils.UIConsts;
import com.pingu.party.engine.core.AssetManager;
import java.awt.Graphics2D;
import java.awt.Image;

public class WTile implements WComponent {
  int x;
  int y;
  int w;
  int h;
  private int dx, dy;
  private Image tileImage;
  public static enum tileType {
    UNSET,
    RED,
    VIOLET,
    BLUE,
    YELLOW,
    GREEN,
    GREY,
  }

  private ListenZone clicZone = null;
  private ListenZone hoverZone = null;

  private tileType type = tileType.UNSET;

  public WTile(int x, int y, int w, int h, tileType t) {
    this.dx = x;
    this.dy = y;
    this.x = 0;
    this.y = 0;
    this.w = w;
    this.h = h;
    type = t;
    genTileImage();
    this.clicZone = new ListenZone(this.dx, this.dy, this.w, this.h, null);
    this.hoverZone = null;
  }
  public WTile(int x, int y, int w, int h, int t) {
    this(x, y, w, h, tileType.values()[(t<=tileType.values().length-1&&t>=0)?t:tileType.values().length-1]);
  }
  public WTile(int x, int y, int t) {
    this(x, y, 100, 120, t);
  }
  public WTile(int t) {
    this(0, 0, t);
  }

  public void setTile(tileType t) {
    this.type = t;
    genTileImage();
  }

  public void setTileIndex(int i) {
    if(i<=tileType.values().length-1&&i>=0) this.type =tileType.values()[i];
    else this.type = tileType.UNSET;
    genTileImage();
  }

  public tileType getTile() {
    return this.type;
  }

  public int getTileIndex() {
    return tileType.valueOf(this.type.name()).ordinal();
    /*switch (this.type) {
      case UNSET: return 0;
      case RED: return 1;
      case VIOLET: return 2;
      case BLUE: return 3;
      case YELLOW: return 4;
      case GREEN: return 5;
      case GREY: return 6;
      default: return 0;
    }*/
  }

  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public void setX(int x) {
    this.x = x;
  }
  public void setY(int y) {
    this.y = y;
  }
  public void setDX(int x) {
    this.dx = x;
  }
  public void setDY(int y) {
    this.dy = y;
  }

  private void genTileImage() {
    switch (this.type) {
      case YELLOW: this.tileImage = AssetManager.getImage("tile-y"); break;
      case BLUE: this.tileImage = AssetManager.getImage("tile-b"); break;
      case GREEN: this.tileImage = AssetManager.getImage("tile-g"); break;
      case VIOLET: this.tileImage = AssetManager.getImage("tile-v"); break;
      case RED: this.tileImage = AssetManager.getImage("tile-r"); break;
      case GREY: this.tileImage = AssetManager.getImage("tile-1"); break;
      case UNSET: this.tileImage = AssetManager.getImage("tile-0"); break;
    }
  }

  public void setAction(Runnable action) {
    this.clicZone.setAction(action);
    this.clicZone.setDX(this.dx);
    this.clicZone.setDY(this.dy);
  }

  public void setHoverAction(Runnable action) {
    if(this.hoverZone != null){
      this.hoverZone.setAction(action);
    } else {
      this.hoverZone = new ListenZone(this.dx, this.dy, this.w, this.h, action);
    }
    this.hoverZone.setDX(this.dx);
    this.hoverZone.setDY(this.dy);
  }

  public boolean isActive() {
    return this.clicZone.isActive();
  }
  public void enable() {this.clicZone.enable();}
  public void disable() {this.clicZone.disable();}

  public ListenZone getActionZone() {
    return this.clicZone;
  }
  public ListenZone getHoverZone() {
    return this.hoverZone;
  }


  public void recalc() {

  }

  public void draw(Graphics2D g2d) {
    if(this.type != tileType.UNSET) g2d.drawImage(this.tileImage, this.x+this.dx, this.y+this.dy, null);
    if(Config.GUI_TRACE) {
      g2d.setColor(UIConsts.COLOR_DEBUG_GREEN);
      g2d.fillRoundRect(this.x+this.dx+2, this.y+this.dy+2, 100-2, 120-2, 4, 4);
    }
  }
}
