package com.pingu.party.gui.widget;

import com.pingu.party.engine.config.Config;

import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import com.pingu.party.launcher.Utils.UIConsts;
import com.pingu.party.engine.utils.ImageUtils;

public class WBackground implements WComponent {
  int x;
  int y;
  int w;
  int h;
  private int dx, dy;
  private Image img = null;
  private BufferedImage rimg = null;
  private String baseName;
  private String ext = "png";
  private boolean fill = false; // Will recalc on resize if true

  private boolean locateImage(final File path) {
    for(final File f : path.listFiles()) {
      //System.out.print(f.getName());
      if(!f.isDirectory()){
        if(f.getName().equals(this.baseName+"-"+this.w+"-"+this.h+"."+this.ext)) {
          try {
            this.img = ImageIO.read(new File(path.toString().replace("\\", "/")+"/"+f.getName())); // Windows quick fix
            return true;
          } catch (Exception e) {
            System.out.println(e);
          }
        }
      }
    }
    return false;
  }

  public WBackground(int x, int y, int w, int h, Image i, BufferedImage b) {
    this.dx = x;
    this.dy = y;
    this.x = 0;
    this.y = 0;
    this.w = (w>x)?w:x+1;
    this.h = (h>y)?h:y+1;
    this.rimg = b;
  }
  public WBackground(int x, int y, int w, int h, Image i) {
    this(x, y, w, h, i, ImageUtils.bufferize(i));
  }
  public WBackground(int x, int y, int w, int h) {
    this(x, y, w, h, null, null);
  }
  public WBackground(int x, int y, Image i) {
    this(x, y, i.getWidth(null), i.getHeight(null), i);
  }
  public WBackground(Image i) {
    this(0, 0, i);
  }

  public WBackground(int w, int h, String baseName) {
    this(0, 0, w, h);
    this.baseName = baseName;
    locateImage(new File("res/UI/menu/Backgrounds/"));
    if(this.img != null) {
      this.rimg = ImageUtils.bufferize(this.img);
    }
  }

  public void recalc(int w, int h) {
    this.w = w;
    this.h = h;
    fitBackground();
  }

  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }

  public void setX(int x) {
    this.x = x;
  }
  public void setY(int y) {
    this.y = y;
  }
  public void setDX(int x) {
    this.dx = x;
  }
  public void setDY(int y) {
    this.dy = y;
  }


  public void recalc() {
    this.w = Config.WIDTH;
    this.h = Config.HEIGHT;
    //System.out.println("RECALC "+this.w+" "+this.h);
    if(!locateImage(new File("res/UI/menu/Backgrounds/"))) {
      fitBackground();
    } else {
      this.dx = 0;
      this.dy = 0;
    }
  }

  private void fitBackground() {
    try {
      this.img = ImageIO.read(new File("res/UI/menu/Backgrounds/bg-1920-1080.png"));
    } catch (Exception e) {
      System.out.println(e);
    }
    if(this.img != null) {
      this.dx = -(this.img.getWidth(null)-this.w);
      this.dy = -(this.img.getHeight(null)-this.h);
      this.rimg = ImageUtils.bufferize(this.img);
    } else {
      this.dx = 0;
      this.dy = 0;
      //System.out.println("File not found");
    }
    this.x = 0;
    this.y = 0;
    //System.out.println("File not found");
  }

  public WBackground(String baseName) {
    this.baseName = baseName;
    this.w = Config.WIDTH;
    this.h = Config.HEIGHT;
    locateImage(new File("res/UI/menu/Backgrounds/"));

    if(this.img != null) {
      this.rimg = ImageUtils.bufferize(this.img);
      this.dx = 0;
      this.dy = 0;
      this.x = 0;
      this.y = 0;
    } else {
      fitBackground();
    }
  }


  public void draw(Graphics2D g2d) {
    g2d.drawImage(this.rimg, this.x+this.dx, this.y+this.dy, null);
  }
}
