package com.pingu.party.gui.widget;

import com.pingu.party.engine.config.Config;

import com.pingu.party.launcher.Utils.Font.Roboto;
import java.awt.Font;
import java.awt.Color;
import com.pingu.party.launcher.Utils.UIConsts;
import java.awt.Graphics2D;

public class WLabel implements WComponent {
  int x;
  int y;
  private int dx, dy;
  int h;
  int w = 0;
  private String caption;
  protected Font font;
  private Color color;

  private boolean visible;

  public WLabel(String s, int x, int y, Font f, Color c, int h) {
    this.dx = x;
    this.dy = y;
    this.h = h;
    this.caption = s;
    this.font = f;
    this.color = c;
    this.x = 0;
    this.y = 0;
    this.visible = true;
  }
  public WLabel(String s, int x, int y) {
    this(s, x, y, new Roboto().get_medium(16), UIConsts.COLOR_WHITE, 16);
  }
  public WLabel(String s, int x, int y, int fsize) {
    this(s, x, y, new Roboto().get_medium(fsize), UIConsts.COLOR_WHITE, fsize);
  }
  public WLabel(String s, int x, int y, Color c) {
    this(s, x, y, new Roboto().get_medium(16), c, 16);
  }
  public WLabel(String s, int x, int y, Color c, int fsize) {
    this(s, x, y, new Roboto().get_medium(fsize), c, fsize);
  }
  public WLabel(String s, int x, int y, int h, Color c, int fsize) {
    this(s, x, y, new Roboto().get_medium(fsize), c, h);
  }

  public WLabel(String s, int x, int y, int h, Font f, Color c, int fsize) {
    this(s, x, y, f, c, h);
  }

  public void recalc() {

  }

  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public void setCaption(String s) {
    this.caption = s;
  }

  public String getCaption() {
    return this.caption;
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public void setX(int x) {
    this.x = x;
  }
  public void setY(int y) {
    this.y = y;
  }
  public void setDX(int x) {
    //System.out.println("sDX:"+x);
    /*if(x>40) {
      for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
        System.out.println(ste);
      }

    }*/
    this.dx = x;
  }
  public void setDY(int y) {
    //System.out.println("sDY:"+y);
    /*if(y>40) {
      for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
        System.out.println("   "+ste);
      }

    }*/
    this.dy = y;
  }

  public boolean isVisible() {
    return this.visible;
  }

  public void show() {
    this.visible = true;
  }

  public void hide() {
    this.visible = false;
  }

  public void draw(Graphics2D g2d) {
    if(this.visible) {
      if(Config.GUI_TRACE) {
        g2d.setColor(new Color(0,0,0,42));
        if(this.w == 0) this.w = g2d.getFontMetrics(this.font).stringWidth(this.caption);
        g2d.fillRect(this.dx+this.x, this.dy+this.y, this.w, this.h);
      }
      g2d.setColor(this.color);
      g2d.setFont(this.font);
      g2d.drawString(this.caption, this.dx+this.x, this.dy+this.y+this.h);
    }
    }
}
