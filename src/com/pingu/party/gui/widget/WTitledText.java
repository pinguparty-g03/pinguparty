package com.pingu.party.gui.widget;

import com.pingu.party.launcher.Utils.UIConsts;

import java.awt.Color;
import java.util.ArrayList;
import java.awt.Graphics2D;
import com.pingu.party.engine.config.Config;

public class WTitledText implements WComponent  {
  private int dx, dy;
  private int x, y, w, h, margin;
  private int ItemSize;

  private String title;
  private int titleHeight;
  private WLabel text;
  private ArrayList<WListItem> li;

  private Color bg;

  private boolean drawBG = true;

  public WTitledText(int x, int y, int w, int h, int m, Color c, String s, Color tc, int fsize) {
    this.dx = x;
    this.dy = y;
    this.w = w;
    this.h = h;
    this.margin = m;
    this.li = new ArrayList<WListItem>();
    this.x = 0;
    this.y = 0;
    this.bg = c;
    this.ItemSize = 0; //list item size
    this.title = s;
    this.text = new WLabel(s,x, y, fsize-(fsize/6),  tc, fsize);
    this.titleHeight = fsize;
  }

  public WTitledText(int x, int y, int w, int h, int m, String s) {
    this(x,y,w, h, m, new Color(0,0,0,42), s, UIConsts.COLOR_FOREGROUND, 24);
    this.drawBG = false;
  }
  public WTitledText(int x, int y, int w, int m, String s) {
    this(x,y,w, 0, m, new Color(0,0,0,42), s, UIConsts.COLOR_FOREGROUND, 24);
    this.drawBG = false;
  }
  public WTitledText(String s) {
    this(0,0,256, 0, 8, new Color(0,0,0,42), s, UIConsts.COLOR_FOREGROUND, 24);
    this.drawBG = false;
  }

  /*
  Getters and setters
  */

  public int getItemsCount() {
    return this.li.size();
  }

  public WListItem getItem(int i) {
    //try {
      return this.li.get(i);
    /*} catch (Exception e) {
      return null;
    }*/
  }

  public int getX() {
    return this.x;
  }

  public int getY() {
    return this.y;
  }

  public int getDX() {
    return this.dx;
  }

  public int getDY() {
    return this.dy;
  }

  public int getWidth() {
    return this.w;
  }

  public int getHeight() {
    return this.h;
  }

  public void setX(int x) {
    this.x = x;
    setChildXY(x, this.y);
  }
  public void setY(int y) {
    this.y = y;
    setChildXY(this.x, y);
  }

  public void setDX(int x) {
    this.dx = x;
    setChildDX(x);
  }

  public void setDY(int y) {
    this.dy = y;
    setChildDY(y);
  }

  private void setChildXY(int x, int y) {
    //System.out.println("sCXY"+x+":"+y);
    this.text.setXY(x, y);
    synchronized(this.li) {
      for (WListItem w : this.li) {
        w.setXY(x, y);
      }
    }
  }

  private void setChildDX(int x) {
    this.text.setDX(x);
    synchronized(this.li) {
      for (WListItem w : this.li) {
        w.setDX(x); //TODO change me
      }
    }
  }
  private void setChildDY(int y) {
    this.text.setDY(y);
    synchronized(this.li) {
      for (WListItem w : this.li) {
        w.setDY(w.getDY()+y);
      }
    }
  }

  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
    this.text.setXY(x, y);
    setChildXY(x, y);
  }

  public void setItem(int index, WListItem w) {
    synchronized(this.li) {
      if(index >= this.li.size() || index <0) return;
      this.li.set(index, w);
    }
  }

  public void setTitle(String t) {
    this.title = t;
  }

  public void setText(String t) {
    this.text.setCaption(t);
  }

  public void add(WListItem w) {
    synchronized(this.li) {
      w.setXY(0, 0);
      w.setDY(0);
      w.setPDY(this.ItemSize+16);
      w.setInnerXY(0,0);
      w.setInnerDXY(0, this.ItemSize);
      this.li.add(w);
      this.ItemSize += 24;
      this.h = this.titleHeight+16+this.ItemSize + this.margin;
      //System.out.println(""+this.h+":"+this.ItemSize+"+"+this.titleHeight+"+"+this.margin);
    }
  }

  public void recalc() {
    synchronized(this.li) {
      for (WListItem wc : this.li) {
        wc.recalc();
      }
    }
  }

  public synchronized void draw(Graphics2D g2d) {
    if(Config.GUI_TRACE || this.drawBG) {
      g2d.setColor(this.bg);
      g2d.fillRect(this.dx+this.x, this.dy+this.y, this.w, this.ItemSize+this.margin);
    }
    this.text.draw(g2d);
    synchronized(this.li) {
      for (WListItem e : this.li) {
        synchronized (g2d) {
          e.draw(g2d);
        }
      }
    }
    // Override me !
  }
}
