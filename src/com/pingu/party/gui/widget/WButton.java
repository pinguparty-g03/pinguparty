package com.pingu.party.gui.widget;

import com.pingu.party.launcher.Utils.UIConsts;
import java.awt.Graphics2D;

import com.pingu.party.engine.input.ListenZone;

public class WButton implements WComponent {
  int x;
  int y;
  int w;
  int h;

  private int dx, dy;
  private int border;
  private WImage background;

  private boolean visible = true;

  private ListenZone clicZone;
  private ListenZone hoverZone;

  public WButton(int x, int y, int w, int h, int border) {
    this.dx = x;
    this.dy = y;
    this.x = 0;
    this.y = 0;
    this.w = (w>0)?w:32;
    this.h = (h>0)?h:16;
    this.border = border;
    this.clicZone = new ListenZone(this.dx, this.dy, this.w, this.h, null);
    this.hoverZone = null;
  }
  public WButton(int x, int y, int w, int h) {
    this(x, y, w, h, 4);
  }

  public WButton(int x, int y, int w, int h, WImage bg) {
    this(x, y, w, h, 4);
    this.setBackground(bg);
  }

  public void setBackground(WImage bg) {
    if(bg==null) return;
    bg.setXY(this.x, this.y);
    bg.setDX(this.dx);
    bg.setDY(this.dy);
    this.background = bg;
  }

  public void setAction(Runnable action) {
    this.clicZone.setAction(action);
    this.clicZone.setDX(this.dx);
    this.clicZone.setDY(this.dy);
  }
  public void setHoverAction(Runnable action) {
    if(this.hoverZone != null){
      this.hoverZone.setAction(action);
    } else {
      this.hoverZone = new ListenZone(this.dx, this.dy, this.w, this.h, action);
    }
    this.hoverZone.setDX(this.dx);
    this.hoverZone.setDY(this.dy);
  }

  public void hide() {
    this.visible = false;
  }

  public void show() {
    this.visible = true;
  }

  public boolean isVisible() {
    return this.visible;
  }

  public ListenZone getActionZone() {
    return this.clicZone;
  }

  public ListenZone getHoverZone() {
    return this.hoverZone;
  }

  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
    if(this.clicZone!=null) this.clicZone.setXY(x, y);
    if(this.hoverZone!=null) this.hoverZone.setXY(x, y);
    if(this.background!=null) this.background.setXY(x, y);
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public void setX(int x) {
    this.x = x;
    if(this.clicZone!=null) this.clicZone.setX(x);
    if(this.hoverZone!=null) this.hoverZone.setX(x);
    if(this.background != null) this.background.setX(x);
  }
  public void setY(int y) {
    this.y = y;
    if(this.clicZone!=null) this.clicZone.setY(y);
    if(this.hoverZone!=null) this.hoverZone.setY(y);
    if(this.background != null) this.background.setY(y);
  }
  public void setDX(int x) {
    this.dx = x;
    if(this.clicZone!=null) this.clicZone.setDX(x);
    if(this.hoverZone!=null) this.hoverZone.setDX(x);
    if(this.background != null) this.background.setDX(x);
  }
  public void setDY(int y) {
    this.dy = y;
    if(this.clicZone!=null) this.clicZone.setDY(y);
    if(this.hoverZone!=null) this.hoverZone.setDY(y);
    if(this.background != null) this.background.setDY(y);
  }


  public void recalc() {

  }

  public void draw(Graphics2D g2d) {
    if(this.visible) {
      g2d.setColor(UIConsts.COLOR_BUTTON);
      if(this.background == null)g2d.fillRoundRect(this.dx+this.x, this.dy+this.y, this.w, this.h, this.border, this.border); // x y w h br br
      else this.background.draw(g2d);
    }
  }
}
