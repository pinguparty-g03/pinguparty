package com.pingu.party.gui.widget;

import java.util.ArrayList;
import java.awt.Font;

import java.awt.Color;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;


import com.pingu.party.launcher.Utils.Font.Roboto;
import com.pingu.party.launcher.Utils.UIConsts;
import com.pingu.party.engine.utils.ImageUtils;

public class WCardTitle implements WComponent {
  int x;
  int y;
  int w;
  int h;
  private int dx, dy;
  private final int titleHeight = 24;
  private WImage ico;
  private WBox bg;

  private Widget content;

  private String title =  "Title";

  private Color cbg;
  private Color almostGrey = new Color(72, 87, 92, 204);
  private Color cTitle = new Color(74, 111, 125, 255);
  private Color cText = new Color(118, 158, 174, 255);

  private Font fTitle = new Roboto().get_bold(13);

  private WCardTitle(int x, int y, int w, int h, WImage i, WBox b, String title) {
    this.dx = x;
    this.dy = y;
    this.w = w;
    this.h = (h<this.titleHeight)?this.titleHeight*4:h;
    this.ico = i;
    this.cbg = almostGrey;
    this.title = title;
    //this.bg = new WBox(x, y, w,24, this.cTitle); //int x, int y, int w, int h
    this.bg = b; //int x, int y, int w, int h
    this.x = 0;
    this.y = 0;

    this.content = new Widget(x,y+this.titleHeight, w, h-this.titleHeight);
  }
  public WCardTitle(int x, int y, int w, int h, Image i) {
    this(x, y, w, h, new WImage(x+4, y+4, 16, 16, i), new WBox(x, y, w, 24, 0, new Color(74, 111, 125, 255)), "Title");
  }
  public WCardTitle(int x, int y, int w, int h, Image i, String s) {
    this(x, y, w, h, new WImage(x+4, y+4, 16, 16, i), new WBox(x, y, w, 24, 0, new Color(74, 111, 125, 255)), s);
  }

  private WCardTitle(int x, int y, int w, int h) {
    this(x, y, w, h, new WImage(x+4, y+4, 16, 16, null), new WBox(x, y, w, 24, 0, new Color(74, 111, 125, 255)), "Title");
  }

  public void setXY(int x, int y) {
    this.x = x;
    this.ico.setXY(x, y);
    this.bg.setXY(x, y);
    this.content.setXY(x, y+this.titleHeight);

    this.y = y;
  }

  public void setTitle(String s) {
    this.title = s;
  }

  public void cleanContainer() {
    synchronized(this.content) {
      this.content.removeAll();
    }
  }

  public void addToContainer(WComponent w) {
    synchronized(this.content) {
      if(this.h<w.getHeight()) this.h = w.getHeight()+this.titleHeight;
      w.setXY(this.x, this.y);
      w.setDX(w.getDX()+this.x);
      w.setDY(w.getDY()+this.y);
      //System.out.println("P<WCardTitle>"+w.getX()+":"+w.getY());
      this.content.add(w);
    }
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public void setX(int x) {
    this.x = x;
  }
  public void setY(int y) {
    this.y = y;
  }

  public void setW(int w) {
    this.w = w;
  }

  public void setDX(int x) {
    this.dx = x;
  }
  public void setDY(int y) {
    this.dy = y;
  }

  public void recalc() {

  }

  public void draw(Graphics2D g2d) {
    g2d.setColor(this.cbg);
    g2d.fillRect(this.x, this.y,this.w, this.h);
    g2d.setColor(this.cTitle);
    this.bg.draw(g2d);
    this.ico.draw(g2d);
    g2d.setColor(this.cText);
    g2d.setFont(this.fTitle);
    g2d.drawString(this.title, this.x+this.titleHeight, this.y+17); // 17 = round(this.titleHeight-13+(this.titleHeight-13)/2)


    synchronized (this.content) {
      this.content.draw(g2d);
    }
    //g2d.drawImage();
    //g2d.setColor(UIConsts.COLOR_BUTTON);
  }
}
