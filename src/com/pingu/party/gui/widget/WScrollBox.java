package com.pingu.party.gui.widget;

import java.awt.Color;
import java.util.ArrayList;
import java.awt.Graphics2D;
import com.pingu.party.engine.config.Config;

public class WScrollBox implements WComponent  {
  private int dx, dy;
  private int x, y, w, h, margin;
  private int ItemSize;
  private int scrollSize = 12;
  private ArrayList<WListItem> li;

  private Color bg;

  public WScrollBox(int x, int y, int w, int h, int m, Color c) {
    this.dx = x;
    this.dy = y;
    this.w = w;
    this.h = h;
    this.margin = m;
    this.li = new ArrayList<WListItem>();
    this.x = 0;
    this.y = 0;
    this.bg = c;
    this.ItemSize = m; //list item size
  }

  public WScrollBox(int x, int y, int w, int m) {
    this(x,y,w, 0, m, new Color(0,0,0,42));
  }
  public WScrollBox() {
    this(0,0,480, 0, 8, new Color(0,0,0,42));
  }

  /*
  Getters and setters
  */

  public int getItemsCount() {
    return this.li.size();
  }

  public WListItem getItem(int i) {
    //try {
      return this.li.get(i);
    /*} catch (Exception e) {
      return null;
    }*/
  }

  public int getX() {
    return this.x;
  }

  public int getY() {
    return this.y;
  }

  public int getDX() {
    return this.dx;
  }

  public int getDY() {
    return this.dy;
  }

  public int getWidth() {
    return this.w;
  }

  public int getHeight() {
    return this.h;
  }

  public void setX(int x) {
    this.x = x;
    setChildXY(x, this.y);
  }
  public void setY(int y) {
    this.y = y;
    setChildXY(this.x, y);
  }
  public void setDX(int x) {
    this.dx = x;
    setChildDX(x);
  }
  public void setDY(int y) {
    this.dy = y;
    setChildDY(y);
  }

  private void setChildXY(int x, int y) {
    synchronized(this.li) {
      for (WListItem w : this.li) {
        w.setXY(x, y);
      }
    }
  }
  private void setChildDX(int x) {
    synchronized(this.li) {
      for (WListItem w : this.li) {
        w.setDX(w.getDX()+x);
      }
    }
  }
  private void setChildDY(int y) {
    synchronized(this.li) {
      for (WListItem w : this.li) {
        w.setDY(w.getDY()+y);
      }
    }
  }

  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
    setChildXY(x, y);
  }

  public void add(WListItem w) {
    synchronized(this.li) {
      w.setXY(this.x, this.y);
      this.ItemSize += w.getHeight() + w.getDY();
      this.li.add(w);
      w.setDY(w.getDY()+this.dy+this.ItemSize);
      w.setDX(w.getDX()+this.dx);
      this.h = this.ItemSize + this.margin;
    }
  }

  public void recalc() {
    synchronized(this.li) {
      for (WListItem wc : this.li) {
        wc.recalc();
      }
    }
  }

  public synchronized void draw(Graphics2D g2d) {
    g2d.setColor(this.bg);
    g2d.fillRect(this.dx+this.x, this.dy+this.y, this.w, this.ItemSize+this.margin);
    synchronized(this.li) {
      for (WListItem e : this.li) {
        synchronized (g2d) {
          e.draw(g2d);
        }
      }
    }
    // Override me !
  }
}
