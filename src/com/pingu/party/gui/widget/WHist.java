package com.pingu.party.gui.widget;

import com.pingu.party.launcher.Utils.Font.Roboto;
import com.pingu.party.launcher.Utils.UIConsts;

import java.awt.*;
import java.util.ArrayList;

public class WHist implements WComponent {
  int x;
  int y;
  int w;
  int h;
  private int dx;
  private int dy;

  private int border;
  private Color bg;

  private ArrayList<ArrayList<Integer>> axis;
  private ArrayList<Integer> axisValues;
  private ArrayList<String> axisName;
  private int size;

  //Constants
  private Color lineColor = UIConsts.COLOR_MIDGREY;
  private int margin = 2;
  private int textSize = 12;
  private Font textFont = new Roboto().get_medium(textSize);

  private static ArrayList<Color> axisColor = new ArrayList<>();

  //scale vars : list max
  private int _max;
  private double _scale;

  public WHist(int x, int y, int w, int h, int border, Color c) {
    this.dx = x;
    this.dy = y;
    this.w = w;
    this.h = h;
    this.x = 0;
    this.y = 0;
    this.border = border;
    this.bg = c;
    init();
  }
  public WHist(int x, int y, int w, int h) {
    this(x, y, w, h, 4, UIConsts.COLOR_FOREGROUND);
  }

  public WHist(int x, int y, int w, int h, Color c) {
    this(x, y, w, h, 4, c);
  }

  public void recalc() {
  }

  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
  }

  private void init() {
    axis = new ArrayList<>();
    axisName = new ArrayList<>();
    axisValues = new ArrayList<>();
    size = 0;
    if(axisColor.isEmpty()) {
      axisColor.add(UIConsts.COLOR_AXIS_1);
      axisColor.add(UIConsts.COLOR_AXIS_2);
      axisColor.add(UIConsts.COLOR_AXIS_3);
      axisColor.add(UIConsts.COLOR_AXIS_4);
    }
  }

  /**
   * Add axis
   * @param a
   * @return false if cannot add
   */
  public boolean addAxis(ArrayList<Integer> a, String name) {
    if(a.isEmpty()) return false;
    if(axis.isEmpty()) {
      size = a.size();
      _max = 0;
    } else {
      //if(a.size() != axis.get(0).size()) return false;
    }
    axis.add(a);
    axisName.add(name);
    //processAxis(a);
    processAll();
    return true;
  }

  public boolean setAxisName(int i, String name) {
    if(i >= axisName.size()) return false;
    axisName.set(i, name);
    return true;
  }

  public boolean setAxis(int i, ArrayList<Integer> a) {
    if(a.isEmpty() || i >= axis.size()) return false;
    //if(a.isEmpty() || i >= axis.size() || a.size() != axis.get(i).size()) return false;
    axis.set(i, a);
    processAll();
    return true;
  }

  private void processAll() {
    _max = 0;
    axisValues.clear();
    for (ArrayList<Integer> axe : axis ) {
      System.out.print("[");
      for (Integer i : axe) {
        if(!axisValues.contains(i)) axisValues.add(i);
        System.out.print(" " +i);
        if(i>_max) {
          _max = i;
        }
      }
      System.out.print("] - ");
      System.out.println("Max : "+_max);
    }
    _scale = ((double)(h-margin*4)/_max);

    processAxisValueClear();
  }

  /**
   * Avoid overlay text
   */
  private void processAxisValueClear() {
    int range = (int)(24.0/_scale);
    int id = 0;
    int sz = axisValues.size();
    while(id < sz) {
        Integer i = axisValues.get(id);
        for (Integer j = i+1; j <= i+range; j++) {
            if(axisValues.contains(j)) {
                axisValues.remove(j);
                sz--;
            }
        }
        id++;
    }
  }

  private void processAxis(ArrayList<Integer> a) {
    System.out.print("[");
    for (Integer i : a) {
      System.out.print(" " +i);
      if(i>_max) {
        _max = i;
      }
    }
    System.out.print("] - ");
    System.out.println("Max : "+_max);
    _scale = ((double)(h-margin*4)/_max);
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public void setX(int x) {
    this.x = x;
  }
  public void setY(int y) {
    this.y = y;
  }
  public void setDX(int x) {
    this.dx = x;
  }
  public void setDY(int y) {
    this.dy = y;
  }

  private void placeValues(Graphics2D g2d) {
    for (Integer i : axisValues) {
      g2d.setFont(textFont);
      g2d.drawString(String.format("%d", i), this.dx+this.x+margin, this.y+this.dy+margin+(this.h-margin*2)-(int)(_scale*i)+textSize-4);
    }
  }

  public void draw(Graphics2D g2d) {
    g2d.setColor(this.bg);
    g2d.fillRoundRect(this.x+this.dx, this.y+this.dy, this.w, this.h, this.border, this.border); // x y w h br br

    //draw lines
    g2d.setColor(this.lineColor);
    g2d.fillRect(this.x+this.dx+margin, this.y+this.dy+this.h-margin*2, w-margin*2, margin);

    placeValues(g2d);

    if(size != 0) {
      int c = 0;
      int x1, y1, x, y;
      for (ArrayList<Integer> a : axis) {
        g2d.setColor(axisColor.get(c));
        if(axisName.size() >= c) {
          g2d.setFont(textFont);
          g2d.drawString(axisName.get(c), this.dx+this.x+margin+textSize*2, this.dy+this.y+margin+textSize*2+textSize*c+1);
        }
        int d = (this.w-margin*2) / a.size();
        int c2 = 0;
        x = y = 0;
        for (Integer i : a) {
          x1 = this.x+this.dx+margin+c2*d+d/2;
          y1 = this.y+this.dy+margin+ (this.h-margin*2)-(int)(_scale*i);
          g2d.drawRoundRect(x1-2, y1-2, 4, 4, 4, 4);
          if(x != 0) {
            g2d.drawLine(x, y, x1, y1);
          }
          x = x1;
          y = y1;
          c2++;
        }
        c++;
      }
    }

  }
}
