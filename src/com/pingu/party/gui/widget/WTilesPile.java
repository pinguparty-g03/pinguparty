package com.pingu.party.gui.widget;

import com.pingu.party.engine.input.ListenZone;
import com.pingu.party.launcher.Utils.UIConsts;
import com.pingu.party.engine.config.Config;

import java.awt.Graphics2D;
import java.util.ArrayList;

public class WTilesPile implements WComponent {
  int x;
  int y;
  int w;
  int h;
  private final int tileWidth = 100;
  private final int tileHeight = 120;
  private final int tileBot = 30;

  private int dx, dy;
  private int pos = 0;
  private ArrayList<WTile> tiles = new ArrayList<>(37);

  public WTilesPile(int x, int y, int w, int h) {
    this.dx = x;
    this.dy = y;
    this.x = 0;
    this.y = 0;
    this.w = w;
    this.h = h;
  }

  public void setTileIndex(int index, int val) {
    synchronized(this.tiles) {
      if(index<this.tiles.size()) {
        this.tiles.get(index).setTileIndex(val);
      }
    }
  }

  private void setChildXY(int x, int y) {
    synchronized(this.tiles) {
      for (WTile w : this.tiles) {
        w.setXY(x, y);
      }
    }
  }

  private void _setChildDXY(WTile w, int p) {
    if(p<8) { //downer line
      w.setDY((this.tileHeight-this.tileBot)+this.tileBot*7);
      w.setDX(p*this.tileWidth);
    } else if (p<15) {
      w.setDY((this.tileHeight-this.tileBot)+this.tileBot*6);
      w.setDX((p-8)*this.tileWidth+(int)(this.tileWidth/2));
    } else if (p<21) {
      w.setDY((this.tileHeight-this.tileBot)+this.tileBot*5);
      w.setDX((p-14)*this.tileWidth);
    } else if (p<26) {
      w.setDY((this.tileHeight-this.tileBot)+this.tileBot*4);
      w.setDX((p-20)*this.tileWidth+(int)(this.tileWidth/2));
    } else if (p<30) {
      w.setDY((this.tileHeight-this.tileBot)+this.tileBot*3);
      w.setDX((p-24)*this.tileWidth);
    }else if (p<33) {
      w.setDY((this.tileHeight-this.tileBot)+this.tileBot*2);
      w.setDX((p-28)*this.tileWidth+(int)(this.tileWidth/2));
    } else if (p<35) {
      w.setDY((this.tileHeight-this.tileBot)+this.tileBot);
      w.setDX((p-30)*this.tileWidth);
    } else if (p==35) {
      w.setDY((this.tileHeight-this.tileBot));
      w.setDX((p-32)*this.tileWidth+(int)(this.tileWidth/2));
    }
  }

  public void add(WTile w) {
    synchronized(this.tiles) {
      w.setXY(this.x, this.y);
      _setChildDXY(w, this.pos);
      //System.out.println("P<WTitePile> "+w.getTileIndex());
      this.pos++;
      this.tiles.add(w);
      //System.out.println(this.pos);
      //System.out.println("PLOP"+this.li.size()+":"+this.ItemSize);
    }
  }

  public void setTileAction(int pos, Runnable action) { if(pos>=0 && pos<this.tiles.size()) this.tiles.get(pos).setAction(action); }
  public void disableTileAction(int pos) {if(pos>=0 && pos<this.tiles.size()) this.tiles.get(pos).disable();}
  public void enableTileAction(int pos) {if(pos>=0 && pos<this.tiles.size()) this.tiles.get(pos).enable();}

  public WTile get(int i) {
    if(i>=0 && i<this.tiles.size())  return this.tiles.get(i);
    return null;
  }

  public void setChildDXY(int x, int y) {
    int c = 0;
    synchronized(this.tiles) {
      for (WTile w : this.tiles) {
         _setChildDXY(w, c);
      }
    }
  }

  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
    setChildXY(x, y);
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public void setX(int x) {
    this.x = x;
    setChildXY(x, this.y);
  }
  public void setY(int y) {
    this.y = y;
    setChildXY(this.x, y);
  }
  public void setDX(int x) {
    this.dx = x;
  }
  public void setDY(int y) {
    this.dy = y;
  }

  public void recalc() {
    synchronized(this.tiles) {
      for (WTile wt : this.tiles) {
        wt.recalc();
      }
    }
  }

  private void debugNumber(Graphics2D g2d) {
    g2d.setColor(UIConsts.COLOR_SEMI_BUTTON);
    g2d.setFont(UIConsts.ROBOTO_BOLD);
    for (int p=0; p<this.tiles.size(); p++) {
      if(p<8) { //downer line
        g2d.drawString(""+p, this.x+p*this.tileWidth+((int)this.tileHeight/2)-16, this.y+(this.tileHeight-this.tileBot)+this.tileBot*7+this.tileHeight-12);
      } else if (p<15) {
        g2d.drawString(""+p, this.x+(p-8)*this.tileWidth+this.tileWidth-8, this.y+(this.tileHeight-this.tileBot)+this.tileBot*6+this.tileHeight-12);
        /*w.setDY((this.tileHeight-this.tileBot)+this.tileBot*6);
        w.setDX((p-8)*this.tileWidth+(int)(this.tileWidth/2));*/
      } else if (p<21) {
        g2d.drawString(""+p, this.x+(p-14)*this.tileWidth+((int)this.tileHeight/2)-16, this.y+(this.tileHeight-this.tileBot)+this.tileBot*5+this.tileHeight-12);
        /*w.setDY((this.tileHeight-this.tileBot)+this.tileBot*5);
        w.setDX((p-14)*this.tileWidth);*/
      } else if (p<26) {
        g2d.drawString(""+p, this.x+(p-20)*this.tileWidth+this.tileWidth-8, this.y+(this.tileHeight-this.tileBot)+this.tileBot*4+this.tileHeight-12);
        /*w.setDY((this.tileHeight-this.tileBot)+this.tileBot*4);
        w.setDX((p-20)*this.tileWidth+(int)(this.tileWidth/2));*/
      } else if (p<30) {
        g2d.drawString(""+p, this.x+(p-24)*this.tileWidth+((int)this.tileHeight/2)-16, this.y+(this.tileHeight-this.tileBot)+this.tileBot*3+this.tileHeight-12);
        /*w.setDY((this.tileHeight-this.tileBot)+this.tileBot*3);
        w.setDX((p-24)*this.tileWidth);*/
      }else if (p<33) {
        g2d.drawString(""+p, this.x+(p-28)*this.tileWidth+this.tileWidth-8, this.y+(this.tileHeight-this.tileBot)+this.tileBot*2+this.tileHeight-12);
        /*w.setDY((this.tileHeight-this.tileBot)+this.tileBot*2);
        w.setDX((p-28)*this.tileWidth+(int)(this.tileWidth/2));*/
      } else if (p<35) {
        g2d.drawString(""+p, this.x+(p-30)*this.tileWidth+((int)this.tileHeight/2)-16, this.y+(this.tileHeight-this.tileBot)+this.tileBot+this.tileHeight-12);
        /*w.setDY((this.tileHeight-this.tileBot)+this.tileBot);
        w.setDX((p-30)*this.tileWidth);*/
      } else if (p==35) {
        g2d.drawString(""+p, this.x+(p-32)*this.tileWidth+this.tileWidth-8, this.y+(this.tileHeight-this.tileBot)+this.tileHeight-12);
        /*w.setDY((this.tileHeight-this.tileBot));
        w.setDX((p-32)*this.tileWidth+(int)(this.tileWidth/2));*/
      }
    }
    //g2d.drawString("0", this.dx+this.x, this.dy+this.y+this.h);
  }

  public void draw(Graphics2D g2d) {
    synchronized(this.tiles) {
      for (WTile wt : this.tiles) {
        synchronized (g2d) {
          wt.draw(g2d);
        }
      }
    }
    if(Config.GUI_TRACE) {
      g2d.setColor(UIConsts.COLOR_DEBUG_GREEN);
      g2d.fillRoundRect(this.x+this.dx+2, this.y+this.dy+2, 100-2, 120-2, 4, 4);
      debugNumber(g2d);
    }
  }

}
