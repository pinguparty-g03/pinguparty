package com.pingu.party.gui.widget;

import java.awt.Color;
import com.pingu.party.launcher.Utils.UIConsts;
import java.awt.Graphics2D;

public class WSeparator implements WComponent {
  int x;
  int y;
  int w;
  int h;
  private int dx, dy;
  private int border;
  private Color color;

  public WSeparator(int x, int y, int w, int h, int border, Color color) {
    this.dx = x;
    this.dy = y;
    this.w = w;
    this.h = h;
    this.x = 0;
    this.y = 0;
    this.border = border;
  }
  public WSeparator(int x, int y, int h) {
    this(x, y, 2, h, 0, UIConsts.COLOR_BUTTON_HOVER);
  }

  public void recalc() {

  }
  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public void setX(int x) {
    this.x = x;
  }
  public void setY(int y) {
    this.y = y;
  }
  public void setDX(int x) {
    this.dx = x;
  }
  public void setDY(int y) {
    this.dy = y;
  }

  public void draw(Graphics2D g2d) {
    g2d.setColor(this.color);
    g2d.fillRoundRect(x, y, w, h, this.border, this.border); // x y w h br br
  }
}
