package com.pingu.party.gui.widget;

import java.awt.Color;
import com.pingu.party.network.structures.Server.Status;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import com.pingu.party.launcher.Utils.UIConsts;
import com.pingu.party.engine.utils.ImageUtils;

public class WStatusIcon implements WComponent {
  int x;
  int y;
  private int dx, dy;
  int w;
  int h;
  private Image img = null;
  private BufferedImage rimg = null;

  Status state;

  public WStatusIcon(int x, int y, int w, int h, Image i, BufferedImage b, Status s) {
    this.dx = x;
    this.dy = y;
    this.w = w;
    this.h = h;
    this.rimg = b;
    this.state = s;
    this.x = 0;
    this.y = 0;
  }
  public WStatusIcon(int x, int y, int w, int h, Image i) {
    this(x, y, w, h, i, ImageUtils.bufferize(i), Status.OFFLINE);
  }
  public WStatusIcon(int x, int y, Image i) {
    this(x, y, i.getWidth(null), i.getHeight(null), i);
  }
  public WStatusIcon(Image i) {
    this(0, 0, i);
  }
  public WStatusIcon(String path) {
    this(path, Status.OFFLINE);
  }
  public WStatusIcon(String path, Status s) {
    this(0, 0, path, s);
  }
  public WStatusIcon(int x, int y, String path) {
    this(x, y, path, Status.OFFLINE);
  }
  public WStatusIcon(int x, int y, String path, Status s) {
    try {
        this.img = ImageIO.read(new File(path));
    } catch (Exception e) {
      //System.out.println("FAILED");
      System.out.println(e);
    }
    //System.out.println("FAILED");
    if(this.img == null) return;
    //System.out.println("ADDED");
    this.rimg = ImageUtils.bufferize(this.img);
    this.w = this.img.getWidth(null);
    this.h = this.img.getHeight(null);
    this.dx = x;
    this.dy = y;
    this.x = 0;
    this.y = 0;
    this.state = s;
  }

  public void setStatus(Status s) {
    this.state = s;
  }

  public void setStatusIcon(Image i) {
      if(i==null) return;
      this.img = i;
      this.rimg = ImageUtils.bufferize(i);
  }

  private Color getColor() {
    switch (this.state) {
      case OFFLINE:
        return new Color(132, 142, 146);
      case OPEN:
        return new Color(87, 161, 211);
      case IN_GAME:
        return new Color(198, 97, 78);
      default:
        return new Color(132, 142, 146);


    }
  }

  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public void setX(int x) {
    this.x = x;
  }
  public void setY(int y) {
    this.y = y;
  }
  public void setDX(int x) {
    this.dx = x;
  }
  public void setDY(int y) {
    this.dy = y;
  }

  public void recalc() {

  }

  public void draw(Graphics2D g2d) {
    g2d.drawImage(this.rimg, this.dx+this.x, this.dy+this.y, null);
    g2d.setColor(getColor());
    g2d.fillOval(this.dx+this.x+28, this.dy+this.y+28, 12, 12);
    //g2d.draWStatusIcon();
    //g2d.setColor(UIConsts.COLOR_BUTTON);
  }
}
