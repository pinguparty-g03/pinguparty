package com.pingu.party.gui.widget;

import java.util.ArrayList;
import com.pingu.party.engine.config.Config;

public class Anchor {
  public static enum alignType {
    ORIGIN,
    CENTERED,
    REVERSED,
    UNSET
  }
  private alignType verticalAlign;
  private alignType horizontalAlign;

  public Anchor() {
    this.verticalAlign = alignType.ORIGIN;
    this.horizontalAlign = alignType.ORIGIN;
  }
  public Anchor(int h) {
    this.verticalAlign = alignType.ORIGIN;
    switch (h) {
      case 0: this.horizontalAlign = alignType.ORIGIN; break;
      case 1: this.horizontalAlign = alignType.CENTERED; break;
      case 2: this.horizontalAlign = alignType.REVERSED; break;
      case 3: this.horizontalAlign = alignType.UNSET; break;
      default: this.horizontalAlign = alignType.ORIGIN;
    }
  }
  public Anchor(alignType v, alignType h) {
    this.verticalAlign = v;
    this.horizontalAlign = h;
  }
  public Anchor(int v, int h) {
    switch (v) {
      case 0: this.verticalAlign = alignType.ORIGIN; break;
      case 1: this.verticalAlign = alignType.CENTERED; break;
      case 2: this.verticalAlign = alignType.REVERSED; break;
      case 3: this.verticalAlign = alignType.UNSET; break;
      default: this.verticalAlign = alignType.ORIGIN;
    }
    switch (h) {
      case 0: this.horizontalAlign = alignType.ORIGIN; break;
      case 1: this.horizontalAlign = alignType.CENTERED; break;
      case 2: this.horizontalAlign = alignType.REVERSED; break;
      case 3: this.horizontalAlign = alignType.UNSET; break;
      default: this.horizontalAlign = alignType.ORIGIN;
    }
  }

  public void setVerticalAlign(alignType v) {
    this.verticalAlign = v;
  }

  public void setVerticalAlign(int v) {
    switch (v) {
      case 0: this.verticalAlign = alignType.ORIGIN; break;
      case 1: this.verticalAlign = alignType.CENTERED; break;
      case 2: this.verticalAlign = alignType.REVERSED; break;
      case 3: this.verticalAlign = alignType.UNSET; break;
      default: this.verticalAlign = alignType.ORIGIN;
    }
  }

  public void setHorizontalAlign(alignType h) {
    this.horizontalAlign = h;
  }

  public void setHorizontalAlign(int h) {
    switch (h) {
      case 0: this.horizontalAlign = alignType.ORIGIN; break;
      case 1: this.horizontalAlign = alignType.CENTERED; break;
      case 2: this.horizontalAlign = alignType.REVERSED; break;
      case 3: this.horizontalAlign = alignType.UNSET; break;
      default: this.horizontalAlign = alignType.ORIGIN;
    }
  }

  public alignType getHorizontalAlign() {
    return this.horizontalAlign;
  }
  public alignType getVerticalAlign() {
    return this.verticalAlign;
  }

}
