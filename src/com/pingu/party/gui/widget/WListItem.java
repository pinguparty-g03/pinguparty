package com.pingu.party.gui.widget;

import com.pingu.party.engine.config.Config;

import java.awt.Color;
import com.pingu.party.launcher.Utils.UIConsts;
import java.awt.Graphics2D;

public class WListItem implements WComponent {
  int x;
  int y;
  private int dx, dy;
  int w;
  int h;
  private int border;
  private Color bg;
  private boolean visible;

  private boolean drawBG = true;

  private WComponent inner;

  public WListItem(int x, int y, int w, int h, Color c) {
    this.dx = x;
    this.dy = y;
    this.w = w;
    this.h = h;
    this.bg = c;
    this.x = 0;
    this.y = 0;
    this.drawBG = true;
    this.visible = true;
  }
  public WListItem(int x, int y, int w, int h) {
    this(x, y, w, h, new Color(0,0,0,42));
    this.drawBG = false;
  }

  public void setInner(WComponent w) {
    if(w.getHeight()+w.getDY()>this.h) this.h = w.getHeight()+w.getDY();
    w.setXY(this.x, this.y);
    w.setDY(this.dy);
    w.setDX(this.dx);
    this.inner = w;
  }

  public WComponent getInner() {
    return this.inner;
  }

  public void recalc() {
    this.inner.recalc();
  }
  public void setX(int x) {
    this.x = x;
    if(this.inner != null) {
      this.inner.setX(x);
    }
  }
  public void setDX(int x) {
    this.dx = x;
    if(this.inner != null) {
      this.inner.setDX(x);
    }
  }
  public void setY(int y) {
    this.y = y;
    if(this.inner != null) {
      this.inner.setY(y);
    }
  }
  public void setDY(int y) {
    this.dy = y;
    if(this.inner != null) {
      //System.out.print(" Wli:"+this.inner.getDY());
      this.inner.setDY(y);
    }
  }
  public void setXY(int x, int y) {
    //System.out.println("aa:"+x+":"+y);
    this.x = x;
    this.y = y;
    if(this.inner != null) {
      this.inner.setXY(this.x, this.y);
    }
  }

  public void setInnerXY(int x, int y) {
    if(this.inner != null) {
      this.inner.setXY(this.x, this.y);
    }
  }
  public void setInnerDXY(int x, int y) {
    if(this.inner != null) {
      this.inner.setDX(this.x);
      this.inner.setDY(this.y);
    }
  }

  public void setPX(int x) {
    this.x =x ;
  }
  public void setPY(int y) {
    this.y =y ;
  }
  public void setPDX(int x) {
    this.dx =x ;
  }
  public void setPDY(int y) {
    this.dy =y ;
  }

  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }


  public int getX() {
    return this.x;
  }

  public int getW() {
    return this.w;
  }
  public int getH() {
    return this.h;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public int getY() {
    return this.y;
  }

  public boolean isVisible() {
    return this.visible;
  }

  public void show() {
    this.visible = true;
  }

  public void hide() {
    this.visible = false;
  }

  public void draw(Graphics2D g2d) {
    if(visible) {
      if(Config.GUI_TRACE || this.drawBG) {
        g2d.setColor(this.bg);
        g2d.fillRect(this.dx+this.x, this.dy+this.y, this.w, this.h); // x y w h br br
      }
      if(this.inner != null) {
        synchronized (this.inner) {
          this.inner.draw(g2d);
        }
      }
    }
  }
}
