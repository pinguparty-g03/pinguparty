package com.pingu.party.gui.widget;

import java.awt.Image;
import java.awt.Color;
import com.pingu.party.engine.config.Config;
import java.util.ArrayList;
import com.pingu.party.launcher.Utils.UIConsts;
import java.awt.Graphics2D;

public class WTitle extends WImage implements WComponent {
  int x;
  int y;
  final int w;
  final int h;
  private Color bg;
  Image logo;
  private int dx, dy;

  public WTitle(int x, int y, int w, int h, Color bg, Image i) {
    super(x, y, w, h, i);
    this.dx = x;
    this.dy = y;
    this.w = (w>x)?w:x+1;
    this.h = (h>y)?h:y+1;
    this.bg = bg;
    this.logo = i;
    this.x = 0;
    this.y = 0;
  }
  public WTitle(int x, int y, int w, int h, Image i) {
    this(x, y, w, h, UIConsts.COLOR_BUTTON, i);
  }
  public WTitle(int x, int y, Image i) {
    this(x, y, i.getWidth(null), i.getHeight(null), UIConsts.COLOR_BUTTON, i);
  }

  public WTitle(Image i) {
    this(0, 0, i);
  }
  public WTitle(Color bg, Image i) {
    this(0, 0, i.getWidth(null), i.getHeight(null), bg, i);
  }

  public void recalc() {

  }
  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public void setX(int x) {
    this.x = x;
  }
  public void setY(int y) {
    this.y = y;
  }
  public void setDX(int x) {
    this.dx = x;
  }
  public void setDY(int y) {
    this.dy = y;
  }

  public void draw(Graphics2D g2d) {
    g2d.setColor(this.bg);
    g2d.fillRect(this.x+this.dx, this.y+this.dy, this.w, this.h);
    g2d.drawImage(this.logo, this.x+this.dx, this.y+this.dy, null);
    //g2d.fillRoundRect(x, y, w, h, this.border, this.border); // x y w h br br
  }
}
