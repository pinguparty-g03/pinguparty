package com.pingu.party.gui.widget;

import java.awt.geom.GeneralPath;
import java.awt.Shape;
import java.awt.Color;
import com.pingu.party.launcher.Utils.UIConsts;
import java.awt.Graphics2D;

public class WPoly implements WComponent {
  int x;
  int y;
  private int dx, dy;
  int w;
  int h;
  int d;
  private boolean red = false;
  private Color bg;

  private Shape s;

  public WPoly(int x, int y, int w, int h, int d, Color c) {
    this.dx = x;
    this.dy = y;
    this.w = w;
    this.h = h;
    this.d = d;
    this.bg = c;
    this.s = createShape();
    this.x = 0;
    this.y = 0;
  }
  public WPoly(int x, int y, int w, int h) {
    this(x, y, w, h, 0, UIConsts.COLOR_BUTTON);
  }

  public void setReduced(boolean b) {
    this.red = b;
  }
  public boolean isReduced() {
    return this.red;
  }

  public void recalc() {
    this.s = createShape();
  }
  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
    recalc();
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public void setX(int x) {
    this.x = x;
  }
  public void setY(int y) {
    this.y = y;
  }
  public void setDX(int x) {
    this.dx = x;
  }
  public void setDY(int y) {
    this.dy = y;
  }


  private Shape createShape() {
    GeneralPath path = new GeneralPath(GeneralPath.WIND_NON_ZERO);
    path.moveTo(this.dx+this.x+this.d, this.dy+this.y);
    path.lineTo(this.dx+this.x+this.w, this.dy+this.y);
    path.lineTo(this.dx+this.x+this.w, this.dy+this.y+this.h);
    path.lineTo(this.dx+this.x, this.dy+this.y+this.h);
    path.closePath();
    return path;
  }

  public void draw(Graphics2D g2d) {
    g2d.setColor(this.bg);
    if(this.red) {
      g2d.fillRect(this.x, this.y, this.w, this.h);
    } else {
      g2d.setPaint(this.bg);
      g2d.fill(this.s);
    }
  }
}
