package com.pingu.party.gui.widget;

import java.awt.Color;
import java.util.ArrayList;
import java.awt.Graphics2D;
import com.pingu.party.engine.config.Config;

public class WList implements WComponent  {
  private int dx, dy;
  private int x, y, w, h, margin;
  private int ItemSize;
  private ArrayList<WListItem> li;

  private Color bg;

  private boolean drawBG = true;

  public WList(int x, int y, int w, int h, int m, Color c) {
    this.dx = x;
    this.dy = y;
    this.x = 0;
    this.y = 0;
    this.w = w;
    this.h = h;
    this.margin = m;
    this.li = new ArrayList<WListItem>();
    this.dx = 0;
    this.dy = 0;
    this.bg = c;
    this.ItemSize = m; //list item size
  }

  public WList(int x, int y, int w, int h, int m) {
    this(x,y,w, h, m, new Color(0,0,0,42));
    this.drawBG = false;
  }
  public WList(int x, int y, int w, int m) {
    this(x,y,w, 0, m, new Color(0,0,0,42));
    this.drawBG = false;
  }
  public WList() {
    this(0,0,480, 0, 8, new Color(0,0,0,42));
    this.drawBG = false;
  }

  /*
  Getters and setters
  */

  public int getItemsCount() {
    return this.li.size();
  }

  public boolean setItem(int i, WListItem w) {
    if(i>= li.size() ||i<0) return false;
    this.li.set(i, w);
    return true;
  }

  public WListItem getItem(int i) {
    //try {
      return this.li.get(i);
    /*} catch (Exception e) {
      return null;
    }*/
  }

  public int getItemSize() {
    return this.li.size();
  }

  public int getX() {
    return this.x;
  }

  public int getY() {
    return this.y;
  }

  public int getDX() {
    return this.dx;
  }

  public int getDY() {
    return this.dy;
  }

  public int getWidth() {
    return this.w;
  }

  public int getHeight() {
    return this.h;
  }

  public void setX(int x) {
    this.x = x;
    setChildXY(x, this.y);
  }
  public void setY(int y) {
    this.y = y;
    setChildXY(this.x, y);
  }
  public void setDX(int x) {
    this.dx = x;
    setChildDX(x);
  }
  public void setDY(int y) {
    this.dy = y;
    setChildDY(y);
  }

  private void setChildXY(int x, int y) {
    synchronized(this.li) {
      for (WListItem w : this.li) {
        w.setXY(x, y);
      }
    }
  }

  private void setChildDX(int x) {
    synchronized(this.li) {
      for (WListItem w : this.li) {
        //w.setDX(w.getDX()+x);
      }
    }
  }
  private void setChildDY(int y) {
    synchronized(this.li) {
      for (WListItem w : this.li) {
        //System.out.print(" Wl:"+w.getDY());
        //w.setDY(w.getDY()+y);
      }
    }
  }

  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
    setChildXY(x, y);
  }

  public void add(WListItem w) {
    synchronized(this.li) {
      w.setXY(this.x, this.y);
      w.setDY(this.ItemSize);
      this.ItemSize += w.getHeight();
      w.setDX(this.dx);
      this.li.add(w);
      this.h = this.ItemSize + this.margin;
      //System.out.println("PLOP"+this.li.size()+":"+this.ItemSize);
    }
  }

  public void remove(int index) {
    synchronized(this.li) {
      if(index >=0 && index <this.li.size()) {
        this.ItemSize -= this.li.get(index).getHeight();
        this.h = this.ItemSize + this.margin;
        this.li.remove(index);
      }
    }
    recalc();
  }

  public void recalc() {
    synchronized(this.li) {
      for (WListItem wc : this.li) {
        wc.recalc();
      }
    }
  }

  public synchronized void draw(Graphics2D g2d) {
    if(Config.GUI_TRACE || this.drawBG) {
      g2d.setColor(this.bg);
      g2d.fillRect(this.dx+this.x, this.dy+this.y, this.w, this.ItemSize+this.margin);
    }
    synchronized(this.li) {
      for (WListItem e : this.li) {
        synchronized (g2d) {
          e.draw(g2d);
        }
      }
    }
    // Override me !
  }
}
