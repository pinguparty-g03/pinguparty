package com.pingu.party.gui.widget;

import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import com.pingu.party.engine.utils.ImageUtils;

public class WImage implements WComponent {
  int x;
  int y;
  int w;
  int h;
  private int dx, dy;
  private Image img = null;
  private BufferedImage rimg = null;
  private boolean visible = true;

  public boolean isVisible() {
    return visible;
  }

  public void setVisible(boolean visible) {
    this.visible = visible;
  }

  public void show() {
    this.visible = true;
  }

  public void hide() {
    this.visible = false;
  }

  public WImage(int x, int y, int w, int h, Image i, BufferedImage b) {
    this.dx = x;
    this.dy = y;
    this.x = 0;
    this.y = 0;
    this.w = w;
    this.h = h;
    this.rimg = b;
  }
  public WImage(int x, int y, int w, int h, Image i) {
    this(x, y, w, h, i, ImageUtils.bufferize(i));
  }
  public WImage(int x, int y, Image i) {
    this(x, y, i.getWidth(null), i.getHeight(null), i);
  }
  public WImage(Image i) {
    this(0, 0, i);
  }

  public void setImg(Image i) {
    this.img = i;
    this.rimg =  ImageUtils.bufferize(i);
  }

  public WImage(String path) {
    try {
        this.img = ImageIO.read(new File(path));
    } catch (Exception e) {
      //System.out.println("FAILED");
      System.out.println(e);
    }
    //System.out.println("FAILED");
    if(this.img == null) return;
    //System.out.println("ADDED");
    this.rimg = ImageUtils.bufferize(this.img);
    this.w = this.img.getWidth(null);
    this.h = this.img.getHeight(null);
    this.x = 0;
    this.y = 0;
    this.dx = 0;
    this.dy = 0;
  }

  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public void setX(int x) {
    this.x = x;
  }
  public void setY(int y) {
    this.y = y;
  }
  public void setDX(int x) {
    this.dx = x;
  }
  public void setDY(int y) {
    this.dy = y;
  }


  public void recalc() {

  }

  public void draw(Graphics2D g2d) {
    if(visible) {
      g2d.drawImage(this.rimg, this.x+this.dx, this.y+this.dy, null);
    }
  }
}
