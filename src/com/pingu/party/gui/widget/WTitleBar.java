package com.pingu.party.gui.widget;

import java.awt.Point;
import java.awt.Color;
import com.pingu.party.engine.config.Config;
import java.util.ArrayList;
import com.pingu.party.launcher.Utils.UIConsts;
import java.awt.Graphics2D;

public class WTitleBar extends WBox implements WComponent {
  int x;
  int y;
  int w;
  int h;
  private int dx, dy;
  private int border;
  private Color bg;
  private ArrayList<WComponent> rightElems;
  private WComponent title = null;
  private boolean fill = false; // Will recalc on resize if true
  private WComponent sep = null;

  public WTitleBar(int x, int y, int w, int h, int border, Color bg, Color fg) {
    super(x, y, w, h, 0, fg);
    this.dx = x;
    this.dy = y;
    this.x = 0;
    this.y = 0;
    this.w = (w>x)?w:x+1;
    this.h = (h>y)?h:y+1;
    this.border = border;
    this.bg = bg;
    this.rightElems = new ArrayList<WComponent>();
  }
  public WTitleBar(int x, int y, int w, int h, int border, Color bg) {
    this(x, y, w, h, 0, bg, UIConsts.COLOR_BUTTON_HOVER);

}
  public WTitleBar(int x, int y, int w, int h) {
    this(x, y, w, h, 0, UIConsts.COLOR_BUTTON);
  }

  public WTitleBar(int h) {
    this(0, 0, Config.WIDTH, h, 0, UIConsts.COLOR_BUTTON);
    this.fill = true;
  }
  public WTitleBar(int h, Color bg) {
    this(0, 0, Config.WIDTH, h, 0, bg);
    this.fill = true;
  }

  public void addButton() { //TODO: change this !
    synchronized (this.rightElems) {
      WImage ico = new WImage("res/UI/menu/buttons/setting-bg.png");

      if(Config.FULLSCREEN) {
        ico.setXY(this.w-152,8);

        WImage ico2 = new WImage("res/UI/menu/buttons/close-bg.png");
        ico2.setXY(this.w-80,8);
        this.rightElems.add(ico2);

        this.sep = new WSeparator(this.w-168-2, 0, 2, 80, 0, UIConsts.COLOR_BUTTON);
      } else {
        ico.setXY(this.w-80,8);
        this.sep = new WSeparator(this.w-96-2, 0, 2, 80, 0, UIConsts.COLOR_BUTTON);

      }
      this.rightElems.add(ico);
      //(int x, int y, int w, int h, int border, Color color)
    }

  }
  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public void setX(int x) {
    this.x = x;
  }
  public void setY(int y) {
    this.y = y;
  }
  public void setDX(int x) {
    this.dx = x;
  }
  public void setDY(int y) {
    this.dy = y;
  }

  public void recalc() {
    if(this.fill) {
      this.w = Config.WIDTH-x;
      Point p = new Point(this.w-72-8, 8);
      synchronized (this.rightElems) {
        //System.out.println(this.rightElems.size());
        for (WComponent wc : this.rightElems) {
          wc.setXY(p.x, p.y);
          p.x -= 72;
        }
      }
      if (this.sep != null) {
        p.x += 54;
        this.sep.setXY(p.x, 0);
      }
    }
  }

  public void addTitle(WComponent title) {
    this.title = title;
  }

  public synchronized void draw(Graphics2D g2d) {
    g2d.setColor(this.bg);
    /*if(this.fill) {
      g2d.fillRoundRect(x, y, Config.WIDTH-x, h, this.border, this.border); // x y w h br br
    } else {
      g2d.fillRoundRect(x, y, w, h, this.border, this.border); // x y w h br br
    }*/
    g2d.fillRoundRect(this.x+this.dx, this.y+this.dy, this.w, this.h, this.border, this.border); // x y w h br br

    if(this.rightElems.size()>0) {
      synchronized (this.rightElems) {
        for (WComponent wc : this.rightElems) {
          wc.draw(g2d);
        }
      }
      this.sep.draw(g2d);
    }

    if(this.title != null) this.title.draw(g2d);
  }
}
