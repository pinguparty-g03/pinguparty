package com.pingu.party.gui.widget;

import java.awt.Graphics2D;
import java.util.ArrayList;

public class WTilesHand implements WComponent {
  int x;
  int y;
  int w;
  int h;
  private final int tileWidth = 100;
  private final int tileHeight = 120;
  private final int tileBot = 30;

  private int dx, dy;
  private int pos = 0;
  private ArrayList<WTile> hand = new ArrayList<>(14);

  public WTilesHand(int x, int y, int w, int h) {
    this.dx = x;
    this.dy = y;
    this.x = 0;
    this.y = 0;
    this.w = w;
    this.h = h;
  }

  public void setTileIndex(int index, int val) {
    synchronized(this.hand) {
      if(index<this.hand.size()) {
        this.hand.get(index).setTileIndex(val);
      }
    }
  }

  public WTile get(int i) {
    if(i>=0 && i<this.hand.size()) return this.hand.remove(i);
    else return null;
  }

  private void _setChildDXY(WTile w, int p) {
    w.setDX(this.dx+p*this.tileWidth);
  }

  private void setChildDXY() {
    int p = 0;
    synchronized(this.hand) {
      for (WTile w : this.hand ) {
        _setChildDXY(w, p);
        p++;
      }
    }
  }

  private void setChildXY(int x, int y) {
    synchronized(this.hand) {
      for (WTile w : this.hand ) {
        w.setXY(x, y);
      }
    }
  }

  public void add(WTile w) {
    synchronized(this.hand) {
      w.setXY(this.x, this.y);
      w.setDX(this.dx+this.pos*this.tileWidth);
      w.setDY(this.dy);
      this.pos++;
      this.hand.add(w);
      //System.out.println(this.pos);
      //System.out.println("PLOP"+this.li.size()+":"+this.ItemSize);
    }
  }


  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
    setChildXY(x, y);
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public void setX(int x) {
    this.x = x;
    setChildXY(x, this.y);
  }
  public void setY(int y) {
    this.y = y;
    setChildXY(this.x, y);
  }
  public void setDX(int x) {
    this.dx = x;
    setChildDXY();
  }
  public void setDY(int y) {
    this.dy = y;
  }

  public void recalc() {
    synchronized(this.hand) {
      for (WTile wt : this.hand) {
        wt.recalc();
      }
    }
  }

  public void draw(Graphics2D g2d) {
    synchronized(this.hand) {
      for (WTile wt : this.hand) {
        synchronized (g2d) {
          wt.draw(g2d);
        }
      }
    }
  }

}
