package com.pingu.party.gui.widget;

import java.awt.Graphics2D;

public interface WComponent {
  int x = 0;
  int y = 0;
  int w = 64;
  int h = 16;
  int dx = 0;
  int dy = 0;
  /*
            x     dx         w
  0 +------------+---+------------+---
    |            |   |            |
  y |            |   |            |
    +------------+---+            |
 dy |            |   |            |
    +------------+---+============+
    |                [            ]
  h |                [ WComponent ]
    |                [            ]
    +------------+---+============+
    |
    |
   */

  public void draw(Graphics2D g2d);
  public void recalc();
  public void setXY(int x, int y);
  public int getX(); // root x
  public int getY(); // root y
  public int getDX(); // align x
  public int getDY();  // align y
  public int getWidth();
  public int getHeight();
  public void setX(int x);
  public void setY(int y);
  public void setDX(int x);
  public void setDY(int y);

}
