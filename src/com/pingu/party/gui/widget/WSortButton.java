package com.pingu.party.gui.widget;

import java.awt.Image;
import com.pingu.party.engine.core.AssetManager;
import com.pingu.party.launcher.Utils.UIConsts;

public class WSortButton extends WButton {

  private int status;
  private String mute;

  public WSortButton(int x, int y, int w, int h, int border) {
    super(x, y, w, h, border);
    this.status = 0;
    this.mute = "";
    applystatus();
  }
  public WSortButton(int x, int y, int w, int h) {
    super(x, y, w, h);
    this.status = 0;
    this.mute = "";
    applystatus();
  }

  public WSortButton(int x, int y, int w, int h, WImage bg) {
    super(x, y, w, h, bg);
    this.status = 0;
    this.mute = "";
    applystatus();
  }

  public void setMuteSize(String s) {
    this.mute = "-"+s;
    applystatus();
  }

  /**
   * Set status
   * between 0 (normal) 1 (up) and 2 (down)
   * @param s status <3
   */
  public void setState(int s) {
    if(s<3 && s>=0) this.status = s;
    applystatus();
  }

  public int getState() {
    return this.status;
  }

  private void applystatus() {
    WImage bg;
    Image sortImg;
    switch (this.status) {
      case 1:
        sortImg = AssetManager.getImage("sort"+this.mute);
        bg = new WImage(this.x, this.y, this.w, this.h, sortImg);
        break;
      case 2:
        sortImg = AssetManager.getImage("sortR"+this.mute);
        bg = new WImage(this.x, this.y, this.w, this.h, sortImg);
        break;
      default:
        sortImg = AssetManager.getImage("sortD"+this.mute);
        bg = new WImage(this.x, this.y, this.w, this.h, sortImg);
    }
    setBackground(bg);
  }


}
