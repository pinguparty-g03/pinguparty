package com.pingu.party.gui.widget;

import com.pingu.party.engine.core.AssetManager;
import com.pingu.party.engine.structs.SSelectList;
import com.pingu.party.launcher.Utils.UIConsts;
import java.awt.Graphics2D;

import com.pingu.party.engine.input.ListenZone;

public class WSelector<E> implements WComponent {
  int x;
  int y;
  int w;
  int h;

  private boolean needRescale = true;

  private int dx, dy;
  private boolean big;
  private boolean circular = true;
  private WImage background;

  private SSelectList<E> sel;

  private ListenZone clicZone;
  private ListenZone hoverZone;

  private WButton nextB;
  private boolean nextBD = false;
  private WButton prevB;
  private boolean prevBD = false;
  private WLabel caption;

  private boolean visible = true;

  /**
   * Selector widget
   * @param x   pos >
   * @param y   pos v
   * @param big is large (64px) or small (48px)
   * @param cir if last+1 => first
   */
  public WSelector(int x, int y, boolean big, boolean cir) {
    this.dx = x;
    this.dy = y;
    this.x = 0;
    this.y = 0;
    this.big = big;
    this.circular = cir;
    this.sel = new SSelectList<E>(cir);
    initButtons();
  }

  public WSelector(int x, int y, boolean big) {
    this(x, y, big, true);
  }

  public WSelector(int x, int y)  {
    this(x, y, false);
  }

  private void btnUpdate() {
    if(this.sel.getPos()==this.sel.size()-1 && !this.circular && !this.nextBD) {
      this.nextB.setBackground(new WImage(AssetManager.getImage((this.big)?"BnextHD":"nextHD")));
      this.nextBD = true;
    }
    if(this.sel.getPos()==0 && !this.circular && !this.prevBD) {
      this.prevB.setBackground(new WImage(AssetManager.getImage((this.big)?"BprevHD":"prevHD")));
      this.prevBD = true;
    }
  }

  public void step(int i) {
    if(this.sel != null) {
      if(i>0) {
        this.sel.next();
        if(this.prevBD) {
          this.prevB.setBackground(new WImage(AssetManager.getImage((this.big)?"BprevH":"prevH")));
          this.prevBD = false;
        }
        btnUpdate();
      } else {
        this.sel.prev();
        if(this.nextBD) {
          this.nextB.setBackground(new WImage(AssetManager.getImage((this.big)?"BnextH":"nextH")));
          this.nextBD = false;
        }
        btnUpdate();
      }
      this.needRescale = true;
      this.caption.setCaption(this.sel.getCurrent().toString());
    }
  }

  public ListenZone getNextZone() {
    return this.nextB.getActionZone();
  }
  public ListenZone getPrevZone() {
    return this.prevB.getActionZone();
  }

  public void setNextAction(Runnable action) {
    this.nextB.setAction(action);
  }
  public void setPrevAction(Runnable action) {
    this.prevB.setAction(action);
  }

  public void add(E elem) {
    this.sel.add(elem);
    this.caption.setCaption(this.sel.getCurrent().toString());
  }

  public E getCurrent() {
    return this.sel.getCurrent();
  }

  public void hackCaption(String s) {
    this.caption.setCaption(s);
  }

  public E getIndex(int index) {
    return this.sel.get(index);
  }

  public int getPos() {
    return this.sel.getPos();
  }

  public int getSize() {
    return this.sel.size();
  }


  private void initButtons() {
    if(this.big) {
      //int x, int y, int w, int h, WImage bg
      this.w = 350;
      this.h = 64;
      this.nextB = new WButton(this.dx, this.dy, 64, 64, new WImage(AssetManager.getImage("BnextH")));
      this.prevB = new WButton(this.dx+this.w-64, this.dy, 64, 64, new WImage(AssetManager.getImage("BprevH")));
      this.caption = new WLabel("<>", 96, 12, UIConsts.COLOR_FOREGROUND, 32); //(String s, int x, int y, int fsize)

    } else {
      this.w = 338;
      this.h = 48;
      this.nextB = new WButton(this.dx, this.dy, 48, 48, new WImage(AssetManager.getImage("nextH")));
      this.prevB = new WButton(this.dx+this.w-48, this.dy, 48, 48, new WImage(AssetManager.getImage("prevH")));
      this.caption = new WLabel("<>", 80, 12, UIConsts.COLOR_FOREGROUND, 24);

    }
    this.nextB.setAction( () -> {step(1);});
    this.prevB.setAction( () -> {step(-1);});
    btnUpdate();
  }

  public WSelector(int x, int y, int w, int h, int border) {
    this.dx = x;
    this.dy = y;
    this.x = 0;
    this.y = 0;
    this.w = (w>0)?w:32;
    this.h = (h>0)?h:16;
    this.clicZone = new ListenZone(this.dx, this.dy, this.w, this.h, null);
    this.hoverZone = null;
  }
  public WSelector(int x, int y, int w, int h) {
    this(x, y, w, h, 4);
  }

  public WSelector(int x, int y, int w, int h, WImage bg) {
    this(x, y, w, h, 4);
    this.setBackground(bg);
  }

  public void setBackground(WImage bg) {
    if(bg==null) return;
    bg.setXY(this.x, this.y);
    bg.setDX(this.dx);
    bg.setDY(this.dy);
    this.background = bg;
  }

  public void setAction(Runnable action) {
    this.clicZone.setAction(action);
    this.clicZone.setDX(this.dx);
    this.clicZone.setDY(this.dy);
  }
  public void setHoverAction(Runnable action) {
    if(this.hoverZone != null){
      this.hoverZone.setAction(action);
    } else {
      this.hoverZone = new ListenZone(this.dx, this.dy, this.w, this.h, action);
    }
    this.hoverZone.setDX(this.dx);
    this.hoverZone.setDY(this.dy);
  }

  public ListenZone getActionZone() {
    return this.clicZone;
  }

  public ListenZone getHoverZone() {
    return this.hoverZone;
  }

  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
    if(this.clicZone!=null) this.clicZone.setXY(x, y);
    if(this.hoverZone!=null) this.hoverZone.setXY(x, y);
    if(this.background!=null) this.background.setXY(x, y);
    if(this.nextB != null) this.nextB.setXY(x, y);
    if(this.prevB != null) this.prevB.setXY(x, y);
    if(this.caption != null) this.caption.setXY(x, y);
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }
  public void setX(int x) {
    this.x = x;
    if(this.clicZone!=null) this.clicZone.setX(x);
    if(this.hoverZone!=null) this.hoverZone.setX(x);
    if(this.background != null) this.background.setX(x);
    if(this.nextB != null) this.nextB.setX(x);
    if(this.prevB != null) this.prevB.setX(x);
    if(this.caption != null) this.caption.setX(x);
  }
  public void setY(int y) {
    this.y = y;
    if(this.clicZone!=null) this.clicZone.setY(y);
    if(this.hoverZone!=null) this.hoverZone.setY(y);
    if(this.background != null) this.background.setY(y);
    if(this.nextB != null) this.nextB.setY(y);
    if(this.prevB != null) this.prevB.setY(y);
    if(this.caption != null) this.caption.setY(y);
  }
  public void setDX(int x) {
    this.dx = x;
    if(this.clicZone!=null) this.clicZone.setDX(x);
    if(this.hoverZone!=null) this.hoverZone.setDX(x);
    if(this.background != null) this.background.setDX(x);
    if(this.prevB != null) this.prevB.setDX(x);
    if(this.nextB != null) this.nextB.setDX((this.big)?x+this.w-64:x+this.w-48);
    if(this.caption != null) this.caption.setDX((this.big)?x+96:x+80);
  }
  public void setDY(int y) {
    this.dy = y;
    if(this.clicZone!=null) this.clicZone.setDY(y);
    if(this.hoverZone!=null) this.hoverZone.setDY(y);
    if(this.background != null) this.background.setDY(y);
    if(this.nextB != null) this.nextB.setDY(y);
    if(this.prevB != null) this.prevB.setDY(y);
      if(this.caption != null) this.caption.setDY((this.big)?y+12:y+8);
  }

  public boolean isVisible() {
    return visible;
  }

  public void hide() {
    this.visible = false;
  }

  public void show() {
    this.visible = true;
  }

  public void recalc() {

  }

  private void doRescale(Graphics2D g2d) {
    int sw =  g2d.getFontMetrics(this.caption.font).stringWidth(this.caption.getCaption());
    int dsx = (int)((this.w-sw)/2);
    this.caption.setDX(this.dx+dsx);
  }

  public void draw(Graphics2D g2d) {
    if(this.needRescale) {
      this.needRescale = false;
      doRescale(g2d);
    }
    if(this.visible) {
      this.nextB.draw(g2d);
      this.prevB.draw(g2d);
      this.caption.draw(g2d);

      if(!this.big) {
        g2d.setColor(UIConsts.COLOR_GREY);
        g2d.fillRect(this.dx+this.x, this.dy+this.y, this.w, 4);
        g2d.fillRect(this.dx+this.x, this.dy+this.y+this.h-4, this.w, 4);
      }
    }
    //g2d.setColor(UIConsts.COLOR_BUTTON);
    //if(this.background == null)g2d.fillRoundRect(this.dx+this.x, this.dy+this.y, this.w, this.h, 0, 0); // x y w h br br
    //else this.background.draw(g2d);
  }
}
