package com.pingu.party.engine.config;

import java.util.List;


import com.pingu.party.engine.game.Profile;
import com.pingu.party.engine.game.ProfileManager;
import com.pingu.party.stats.Stat;
/**
 * to give info to the interfaces
 * @author florian
 *
 */
public class LoadInfo {
	/**
	 * selected profile
	 */
	public static Profile ACTUALPROFILE;
	/**
	 * base to create a new profile (not AI or Distant)
	 */
	public static final Profile NEWPROFILE = new Profile("uuid","name","image.png");
	/**
	 * a list of profiles
	 */
	public static List<Profile> PROFILES;
	/**
	 * list of all profiles in the data base
	 */
	public static ProfileManager PROFILEMANAGER  = new ProfileManager();
	
	/**
	 * a stat with all data for a profile stats (score, elo, etc) 
	 */
	public static Stat STAT;
	/**
	 * list of stats
	 */
	public static List<Stat> STATS;
	
	/**
	 * @return actual profile
	 */
	public Profile getProfile() {
		 return ACTUALPROFILE;
	}
	
	/**
	 * @return list of profiles
	 */
	public List<Profile> getProfiles() {
		 return PROFILES;
	}
	
	
	/**
	 * @return stat of a profile
	 */
	public Stat getStat() {
		 return STAT;
	}
	
	/**
	 * @return list of stats
	 */
	public List<Stat> getStats() {
		 return STATS;
	}
	
	/**
	 * to initialize
	 */
	public static void launch()
	{
		ProfileManager manage = new ProfileManager();
		PROFILES = manage.getList();
	}
	
	/**
	 * PROFILES = list of all profiles (locals and not AI)
	 */
	public static void localNoneAI()
	{
		ProfileManager manage = new ProfileManager();
		List<Profile> profiles = manage.getLocal();
		PROFILES = profiles;
	}
	
}
