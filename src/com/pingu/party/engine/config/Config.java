package com.pingu.party.engine.config ;

import java.io.FileOutputStream;
import java.util.Map;

import java.io.File;
import java.io.FileInputStream;
import com.pingu.party.engine.config.ini.Ini;

//TODO : generated config from ini file ;)

public class Config {
	// Screen
	public static int WIDTH = 1440;
	public static int HEIGHT = 960;


	// Window
	public static boolean FULLSCREEN = false;
	public static boolean BORDERED = false;

	// Render
	public static int FPS = 60;

	// Debug
	public static boolean ALLOW_CONSOLE = true;
	public static boolean GUI_TRACE = true;

	// User
	public static String LANG = null; //de, en, fr, jp

	/* Final */
	public static int BASE_WIDTH = 1440;
	public static int BASE_HEIGHT = 960;

	/* Added */
	public static boolean DO_REPLAY = false;
	public static int REPLAY_COUNT = 4;

	private static String outFile = "config/Engine.ini";
	private static Ini f = new Ini(outFile);

	public static void getFromFile() {
		//Ini f = new Ini();
		try {
			f.setFname(outFile);
			f.read(new FileInputStream(new File(outFile)));
			for (Ini.Section s : f.values()) {
				for (Map.Entry<String,String> e : s.entrySet()) {
                    init(e.getKey(), e.getValue());
				}
			}
		} catch (Exception e) {}
	}

	public static void commit() {
		String tmpFile = "config/Engine.in_";
		//Ini f = new Ini();
		try {
			File fl = new File(tmpFile);
			f.write(new FileOutputStream(fl));
			if(f.check(new FileInputStream(fl))) {
				f.backup(new FileInputStream(new File(outFile)), new FileOutputStream(outFile+".bak"));
				f.write(new FileOutputStream(outFile));
			}
		} catch (Exception e) {}
	}

	public static void set(String key, String value) {
		_set(key, value);
		commit();
	}

	private static void init(String key, String value) {
        int i = 0;
        switch (key) {
            case "WIDTH":
                i=Integer.parseInt(value);
                if(i>0) WIDTH = i;
                break;
            case "HEIGHT":
                i=Integer.parseInt(value);
                if(i>0) HEIGHT = i;
                break;
            case "BORDERED":
                BORDERED= value.equals("1");
                break;
            case "FULLSCREEN":
                FULLSCREEN= value.equals("1");
                break;
            case "FPS":
                i=Integer.parseInt(value);
                if(i>0) FPS = i;
                break;
            case "ALLOW_CONSOLE":
                i=Integer.parseInt(value);
                //System.out.println(" > "+value+" "+((i==1)?"Yes":"No"));
                //ALLOW_CONSOLE = true;
                ALLOW_CONSOLE= i == 1;
                break;
            case "GUI_TRACE":
                i=Integer.parseInt(value);
                GUI_TRACE= i == 1;
                break;
            case "DO_REPLAY":
                DO_REPLAY= value.equals("1");
                break;
            case "REPLAY_COUNT":
                i=Integer.parseInt(value);
                REPLAY_COUNT= i;
                break;
            case "LANG":
                switch (value.toLowerCase()) {
                    case "de":
                        LANG = "de";
                        break;
                    case "en":
                        LANG = "en";
                        break;
                    case "fr":
                        LANG = "fr";
                        break;
                    case "jp":
                        LANG = "jp";
                        break;
                    default:
                        LANG = "en";
                        break;
                }
                break;
        }
    }

	private static void _set(String key, String value) {
		int i = 0;
		switch (key) {
			case "WIDTH":
				i=Integer.parseInt(value);
				if(i>0) WIDTH = i;
				f.replaceIn("Screen", "WIDTH", value);
				break;
			case "HEIGHT":
				i=Integer.parseInt(value);
				if(i>0) HEIGHT = i;
				f.replaceIn("Screen", "HEIGHT", value);
				break;
			case "BORDERED":
				//Integer.parseInt(((Boolean) true).toString());
				BORDERED= value.equals("1");
				f.replaceIn("Window", "BORDERED", value);
				break;
			case "FULLSCREEN":
				i=Integer.parseInt(value);
				FULLSCREEN= i == 1;
				f.replaceIn("Window", "FULLSCREEN", value);
				break;
			case "FPS":
				i=Integer.parseInt(value);
				if(i>0) FPS = i;
				f.replaceIn("Render", "FPS", value);
				break;
			case "ALLOW_CONSOLE":
                i=Integer.parseInt(value);
			    //System.out.println("ALLOW_CONSOLE  = "+value);
				ALLOW_CONSOLE= i == 1;
                f.replaceIn("Debug", "ALLOW_CONSOLE", value);
                //System.out.println("ALLOW_CONSOLE  = "+(ALLOW_CONSOLE?"True":"False")+" "+((i == 1?"True":"False")));
                break;
			case "GUI_TRACE":
				i=Integer.parseInt(value);
				GUI_TRACE= i == 1;
				f.replaceIn("Debug", "GUI_TRACE", value);
				break;
			case "DO_REPLAY":
				i=Integer.parseInt(value);
				DO_REPLAY= i == 1;
				f.replaceIn("Game", "DO_REPLAY", value);
				break;
			case "REPLAY_COUNT":
				i=Integer.parseInt(value);
                REPLAY_COUNT= i;
				f.replaceIn("Game", "REPLAY_COUNT", value);
				break;
			case "LANG":
				switch (value.toLowerCase()) {
					case "de":
						LANG = "de";
						f.replaceIn("Locale", "LANG", value);
						break;
					case "en":
						LANG = "en";
						f.replaceIn("Locale", "LANG", value);
						break;
					case "fr":
						LANG = "fr";
						f.replaceIn("Locale", "LANG", value);
						break;
					case "jp":
						LANG = "jp";
						f.replaceIn("Locale", "LANG", value);
						break;
					default:
						LANG = "en";
						f.replaceIn("Locale", "LANG", value);
						break;
				}
				break;
		}
		commit();
	}

	public static int get(String key) {
		switch (key) {
			case "WIDTH":
				return WIDTH;
			case "HEIGHT":
				return HEIGHT;
			case "BORDERED":
				return (BORDERED)?(1):0;
			case "FULLSCREEN":
				return (FULLSCREEN)?(1):0;
			case "FPS":
				return FPS;
			case "ALLOW_CONSOLE":
			    return (ALLOW_CONSOLE)?(1):0;
			case "GUI_TRACE":
				return (GUI_TRACE)?(1):0;
			case "LANG":
				switch (LANG) {
					case "de":
						return 0;
					case "en":
						return 1;
					case "fr":
						return 2;
					case "jp":
						return 3;
				}
				break;
			case "DO_REPLAY":
				return (DO_REPLAY)?(1):(0);
			case "REPLAY_COUNT":
				return REPLAY_COUNT;
			default:
				return 0;
		}
		return 0;
	}

}
