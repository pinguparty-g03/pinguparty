package com.pingu.party.engine.config ;

import java.util.Map;
import com.pingu.party.engine.config.ini.Ini;
import java.io.FileInputStream;
import java.io.File;

//TODO : generated config from ini file ;)

public class Keys {
	 public static int KEY_CONSOLE = 178;

	 public static void getFromFile() {
		 Ini f = new Ini();
		 try {
				f.read(new FileInputStream(new File("config/Input.ini")));
			 for (Ini.Section s : f.values()) {
				 for (Map.Entry<String,String> e : s.entrySet()) {
					 set(e.getKey(), e.getValue());
				 }
			 }
			} catch (Exception e) {}
	 }

	 private static void set(String key, String value) {
		 int i = 0;
		 switch (key) {
			 case "KEY_CONSOLE":
				 i=Integer.parseInt(value);
				 if(i>0) KEY_CONSOLE = i;
				 break;
		 }
	 }

	 public static int get(String key) {
		 switch (key) {
		 	case "KEY_CONSOLE":
			return KEY_CONSOLE;
		 	default:
		 		return 0;

		 }
	 }
}
