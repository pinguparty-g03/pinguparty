package com.pingu.party.engine.config.ini;

import java.io.LineNumberReader;
import java.io.IOException;
import java.io.Reader;

public class Parser {
  static final String COMMENT_CHARS = ";#";
  static final char ASSIGNMENT = '=';
  static final char SECTION_OPEN = '[';
  static final char SECTION_CLOSE = ']';

  /**
   * Parser
   * @param  in          Reader stream
   * @param  f           Ini to write into (must be initialized)
   * @throws IOException Read failed
   */
  public static void parse(Reader in, Ini f) throws IOException, InvalidIniFileException {

    LineNumberReader lnr = new LineNumberReader(in);
    boolean inSection = false;
    String sectionName = null;
    for (String line = lnr.readLine(); line!=null; line = lnr.readLine()) {
      line = line.trim();
      if(line.length() == 0 || COMMENT_CHARS.indexOf(line.charAt(0))>=0) {
        continue;
      }
      if(line.charAt(0) == SECTION_OPEN) { // Line begin with "["
        if(line.length()<=2) throwError(f.getFname(), lnr.getLineNumber(), line, "Invalid section name"); // "[?" line
        if(!(Character.isLetter(line.charAt(1)) || Character.isDigit(line.charAt(1)))) throwError(f.getFname(), lnr.getLineNumber(), line, "Invalid character in section name"); // "[*" line with special char
        if(line.charAt(line.length()-1) == SECTION_CLOSE) {
          sectionName = line.substring(1, line.length()-1).trim();
          if(sectionName != null) f.add(sectionName);
          inSection = true;
        } else {
          throwError(f.getFname(), lnr.getLineNumber(), line, "Unclosed bracket");
        }
      }
      if(!inSection) throwError(f.getFname(), lnr.getLineNumber(), line, "Untracked property"); // Untracked properties
      int aindex = line.indexOf(ASSIGNMENT);
      if(aindex>0) {
        if(line.length()<=2) throwError(f.getFname(), lnr.getLineNumber(), line, "Missing statement"); // "=a" or "a=" line
        if(!(Character.isLetter(line.charAt(1)) || Character.isDigit(line.charAt(1)))) throwError(f.getFname(), lnr.getLineNumber(), line, "Special character are not supported"); // "*=" line with special char
        if(line.length()-1<= aindex || !(Character.isLetter(line.charAt(aindex+1)) || Character.isDigit(line.charAt(aindex+1)))) throwError(f.getFname(), lnr.getLineNumber(), line, "Missing value at key "+line.split(String.format("%s", ASSIGNMENT))[0]); // "stuff=" line
        String key = line.substring(0, aindex).trim();
        String val = line.substring(aindex+1).trim();

        if(sectionName != null) f.putIn(sectionName, key, val);
      }

      //System.out.println(""+lnr.getLineNumber() + "\t"+ sectionName +"\t"+line);
    }
  }

  private static void throwError(String file, int line, String content, String message) throws InvalidIniFileException {
    throw new InvalidIniFileException("Parse error at "+file+":"+line+" error : "+message+"\n "+content);
  }

  private static void throwError(String file, int line, String content) throws InvalidIniFileException {
    throw new InvalidIniFileException("Parse error at "+file+":"+line+"\n "+content);
  }
}
