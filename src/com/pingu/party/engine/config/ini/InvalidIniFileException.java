package com.pingu.party.engine.config.ini;

import java.io.IOException;

public class InvalidIniFileException extends IOException {
    public InvalidIniFileException(String m) {
        super(m);
        printStackTrace();
    }

    public InvalidIniFileException(String m, Throwable e) {
        super(m, e);
        initCause(e);
    }

    public InvalidIniFileException(Throwable e) {
        super(e);
        initCause(e);
    }
}
