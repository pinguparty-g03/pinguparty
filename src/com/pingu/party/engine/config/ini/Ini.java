package com.pingu.party.engine.config.ini;

import java.io.*;
import java.util.Map;
import java.util.LinkedHashMap;

public class Ini extends LinkedHashMap<String,Ini.Section> {

  private String fname;

  public static class Section extends LinkedHashMap<String,String> {
    private String name;

    public Section(String name) {
      super();
      this.name = name;
    }

    public String getName() {
      return name;
    }
  }

  public String getFname() {
      return fname;
  }

  public Ini() {
    super();
    fname = "@"+this.getClass().getName();
  }

  public Ini(String fname) {
      this();
      this.fname = fname;
  }

  public void setFname(String f) {
      this.fname = f;
  }

  public Ini(InputStream in) throws IOException {
    this();
    this.read(in);
  }

  public Section add(String key) {
    Section val = new Section(key);
    this.put(key, val);
    return val;
  }

  public void remove(Section key) {
    remove(key.getName());
  }

  public void push(Section key) {
    this.put(key.getName(), key);
  }

  public Section pop(String key) {
    return remove(key);
  }

  public boolean check(InputStream in) {
      return check(new InputStreamReader(in));
  }

  public boolean check(Reader in) {
      Ini tmp = new Ini();
      try {
          Parser.parse(in, tmp);
      } catch (InvalidIniFileException e) {
          return false;
      } catch (IOException e) {
          return false;
      }
      return true;
  }

  public void read(InputStream in) throws IOException {
    this.read(new InputStreamReader(in));
  }

  public void read(Reader in) throws IOException  {
    Parser.parse(in, this);
  }

  public void backup(InputStream in, OutputStream out) throws IOException {
      Ini tmp = new Ini();
      tmp.read(in);
      tmp.write(out);
  }

  public void write(OutputStream out) throws IOException {
    write(new OutputStreamWriter(out));
  }

  public void write(Writer out) throws IOException {
    PrintWriter pw = new PrintWriter(out);
    for(Ini.Section s : values()) {
      pw.print(Parser.SECTION_OPEN);
      pw.print(encode(s.getName()));
      pw.println(Parser.SECTION_CLOSE);

      for(Map.Entry<String, String> e : s.entrySet()) {
        pw.print(encode(e.getKey()));
        pw.print(Parser.ASSIGNMENT);
        pw.println(encode(e.getValue()));
      }
      pw.println();
    }
    pw.flush();
  }

  private String encode(String raw) {
    StringBuilder buf = new StringBuilder();
    for(Character c : raw.toCharArray()) {
      if((c < 0b100000) || (c > 0b1111110)) {
        buf.append("\\u");
        buf.append(Integer.toHexString(c));
      } else {
        buf.append(c);
      }

      //IDEA: replace ?
      /*if(sp == 0) {
      } else {
        buf.append("\\");
        buf.append(c);
      }*/
      //int sp = "\\\n\t\r\f\b".indexOf(c);
    }
    //return raw;
    return buf.toString();
  }

  public void putIn(Section section, Map.Entry<String,String> e) {
    this.get(section.getName()).put(e.getKey(), e.getValue());
  }
  public void putIn(String section, String key, String value) {
    this.get(section).put(key, value);
  }

  public void replaceIn(Section section, Map.Entry<String,String> e) {
    if(this.get(section) != null) {
      if(this.get(section).get(e.getKey()) != null) this.get(section).replace(e.getKey(), e.getValue());
      else putIn(section.getName(), e.getKey(), e.getValue());
    } else {
      this.add(section.getName());
      putIn(section.getName(), e.getKey(), e.getValue());
    }
  }
  public void replaceIn(String section, String key, String value) {
    if(this.get(section) != null) {
      if(this.get(section).get(key) != null) this.get(section).replace(key, value);
      else putIn(section, key, value);
    } else {
      this.add(section);
      putIn(section, key, value);
    }
  }

  public String getIn(Section section, String key) {
    Section se = this.get(section.getName());
    if(se != null) return se.get(key);
    return null;
  }
  public String getIn(String section, String key) {
    Section se = this.get(section);
    if(se != null) return se.get(key);
    return null;
  }

  /* TEST */
	public void print() {
    for (Ini.Section s : values()) {
      System.out.println(Parser.SECTION_OPEN + s.getName() + Parser.SECTION_CLOSE);
      for (Map.Entry<String,String> e : s.entrySet()) {
        System.out.println(e.getKey() + " " + Parser.ASSIGNMENT + " " + e.getValue());
      }
    }
	}
}
