package com.pingu.party.engine.core;

import java.awt.Color;
import java.util.Map;
import java.io.File;
import javax.imageio.ImageIO;
import com.pingu.party.gui.widget.Widget;
import java.awt.Image;
import java.util.HashMap;

public class AssetManager extends Thread {
  private static final HashMap<String, Image> iMap = new HashMap<String, Image>();
  private static final HashMap<String, Widget> wMap = new HashMap<String, Widget>();
  private static final HashMap<String, Color> cMap = new HashMap<String, Color>();

  private static final HashMap<String, String> imageQueue = new HashMap<String, String>();

  public AssetManager() {
    super();
    AssetManager.queueImage("missingno","missingno.png");
    Image img = null;
    try {
      img = ImageIO.read(new File("res/"+"missingno.png"));
      iMap.put("missingno", img);
    } catch (Exception e) {
      System.out.println("Fatal error");
    }
  }

  public final void run() {
    synchronized(imageQueue) {
      for (Map.Entry<String, String> i : imageQueue.entrySet()) {
        String name = i.getKey();
        String path = i.getValue();
        Image img = null;
        try {
          img = ImageIO.read(new File("res/"+path));
          iMap.put(name, img);
        } catch (Exception e) {
          System.out.println("error loading "+i);
        }
      }
    }
    imageQueue.clear();
    System.out.println("Assets Done !");
  }

  public static final void queueImage(String name, String path) {
    imageQueue.put(name, path);
  }

  public static final void queueWidget(String name, Widget w) {
    wMap.putIfAbsent(name, w);
  }

  public static final void queueColor(String name, Color c) {
    cMap.putIfAbsent(name, c);
  }

  public static final void loadAll() {
    new AssetManager().start();
  }

  public static final Image getImage(String name) {
    return(iMap.containsKey(name))?iMap.get(name):iMap.get("missingno");
  }

  public static final Color getColor(String name) {
    return(cMap.containsKey(name))?cMap.get(name):(new Color(138,96,164));
  }

  public static final Widget getWidget(String name) {
    return wMap.get(name);
  }

  public static final void clear() {
    imageQueue.clear();
  }

}
