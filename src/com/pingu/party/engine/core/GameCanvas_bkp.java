package com.pingu.party.engine.core ;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import com.pingu.party.launcher.Utils.UIConsts;

import javax.swing.JFrame; // REMOVE ME after tests

public class GameCanvas_bkp extends Canvas implements Runnable {

  private JFrame frame;

  private Color backColor = UIConsts.COLOR_BACKGROUND;
  private Color foreColor = UIConsts.COLOR_FOREGROUND;

  private boolean frozen = true;

  private int x, y;
  private int w, h; // canvas sizes & positions

  public GameCanvas_bkp() {
    setBackground(UIConsts.COLOR_BACKGROUND);
    setForeground(UIConsts.COLOR_FOREGROUND);
    this.w = 800;
    this.h = 600;

    start();
  }

  public void start() {
    if(this.frozen) {
      this.frozen = false;
      new Thread(this).start();
    }
  }

  public void stop() {
    this.frozen = true;
  }

  private void prepare() {
    //BufferStrategy bs = new BufferStrategy();
    //BufferStrategy bs  = this.getBufferStrategy();
    //if(bs == null) {
      //createBufferStrategy(2);
      requestFocus();
      return;
    //}

    /*Graphics gr = bs.getDrawGraphics();
    gr.setColor(this.backColor);
    gr.fillRect(0, 0, this.getWidth(), this.getHeight()); //TODO : use w & h + onresize
    gr.setColor(this.foreColor);

    this.draw(gr); // DRAW method

    gr.dispose();
    bs.show();*/

  }

  @Override
  public void run() {
    int fps = 60;
    int tick = 0;

    boolean render = true;

    while(!this.frozen) {
      if (render) {
        tick++;
        prepare();
      }
      try { Thread.sleep(10); } catch(Exception e) { e.printStackTrace(); }
    }
  }

  private void draw(Graphics gr) {
    PaintTest(gr);

    // INJECT DRAWINGS HERE

  }

  private void PaintTest(Graphics gr) {
    gr.setColor(UIConsts.COLOR_FOREGROUND);
    gr.drawLine(0, 0, this.getWidth(), this.getHeight());
    gr.drawLine(this.getWidth(), 0, 0, this.getHeight());

    gr.setColor(UIConsts.COLOR_BUTTON);
    gr.fillRect(this.getWidth()-64-4, 4, 64, 24);
    gr.setColor(UIConsts.COLOR_BUTTON_HOVER);
    gr.drawRect(this.getWidth()-64-4, 4, 64, 24);

    gr.setColor(UIConsts.COLOR_FOREGROUND);
    gr.drawString(this.getWidth()+":"+this.getHeight(), this.getWidth()-64, 20);
  }

  public void paint(Graphics gr) {
    //PaintTest(gr);
  }

  /*
  * Testing Canvas only. REMOVE ME
  */
	public static void test() {
		GameCanvas gc = new GameCanvas();
    JFrame fr = new JFrame("Canvas Test");
    fr.setSize(600,600);
    //fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    fr.getContentPane().add(gc);
    fr.setVisible(true);
	}
}
