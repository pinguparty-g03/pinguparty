package com.pingu.party.engine.core ;

import com.pingu.party.engine.input.ListenZone;
import com.pingu.party.launcher.Utils.Font.Roboto;
import com.pingu.party.gui.widget.Widget;
import com.pingu.party.engine.logger.Logger;
import com.pingu.party.launcher.Component.ErrorDialog;

import java.awt.Cursor;
import java.util.ArrayList;
import javax.swing.RepaintManager;

import java.awt.Font;

import com.pingu.party.engine.config.Config;

import java.awt.Point;

import java.awt.image.BufferStrategy;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Canvas;
import java.awt.Graphics;

import com.pingu.party.launcher.Utils.UIConsts;

import com.pingu.party.engine.input.Input;
import com.pingu.party.engine.input.Binding;

import com.pingu.party.engine.loader.Loader;
import com.pingu.party.engine.logger.SystemLogger;

public class GameCanvas extends Canvas implements Runnable {

	private static final long serialVersionUID = 9042792632778888656L;

//private Image bgImg; // Back default image
  private Graphics2D bg2D; // Back Graphics render

  private BufferStrategy bf = null;
  private boolean frozen = true; //Frozing render ?

  private static boolean isInited = false;
  private boolean debug = false;
  private boolean debugDiag = false;

  private boolean isHover = false;

  private static Loader LO;

  private static ArrayList<Binding> KBinds; // Keys binds
  private static ArrayList<ListenZone> CBinds; // Clics binds
  private static ArrayList<ListenZone> HBinds; // Clics binds
  private static ArrayList<ListenZone> SBinds; // Scroll binds
  private static ArrayList<Widget> widgets;

	private static int watchdog = 0;
	private static final int wClean = 1800;

	private Font dfont = new Roboto().get_regular(14);

  private Font FPSFont = new Roboto().get_thin();

  Input IHandler = null;
  private boolean lockInput = false;
  private boolean paused = false;
  private boolean locked = false;

  protected static Logger Logger;

  private boolean redraw = false;
  private boolean debugListenZones = true;
  boolean isEventDriven = false;

  int px, py, w, h; // canvas sizes & positions
  int fps, tps;

  public GameCanvas() {
    setBackground(UIConsts.COLOR_BACKGROUND);
    setIgnoreRepaint(true);
    SystemLogger sl = new SystemLogger();
    Logger = sl;

    widgets = new ArrayList<Widget>();

    initIO();
  }

  public final void rescale() {
    OnRescale();
  }

  public void OnRescale() {
    //Override me !
  }

  public static void addToViewport(Widget w) {
    synchronized(widgets) {
      widgets.add(w);
    }
  }

  public static boolean removeFromViewport(Widget w) {
    synchronized(widgets) {
      return widgets.remove(w);
    }
  }

  public static void scaleViewport() {
    synchronized(widgets) {
      for (Widget wi : widgets) {
        wi.scale();
      }
    }
  }

  public Graphics2D getG2D() {
    synchronized (this.bg2D) {
      return this.bg2D;
    }
  }

  public void setLogger(Logger l) {
	  Logger = l;
  }
  public void log(String message, int level) {
	  switch (level) {
	  	case 0: Logger.debug(message, 500);
	  			break;
	  	case 1: Logger.info(message, 500);
			break;
	  	case 2: Logger.warn(message, 500);
			break;
	  	case 3: Logger.error(message, 1000);
			break;
		default: Logger.info(message, 500);
		break;
	  }
  }
  public Logger getLogger() {
	  return Logger;
  }

  public Loader getLoader() {
    return LO;
  }

  public Input getInputHandler() {
    return this.IHandler;
  }

  private void initIO() {
    this.IHandler = Input.initInput(this);
    this.IHandler.setCursorVisible(true); //change me ?

    KBinds = new ArrayList<Binding>();
    CBinds = new ArrayList<ListenZone>();
    HBinds = new ArrayList<ListenZone>();
    SBinds = new ArrayList<ListenZone>();
  }

  /** Test */

  public static void addBinding(Binding b) {
    if(!isInited) {
      Logger.error("IO Not inited", 500);
      return;
    }
    synchronized(KBinds) {
      if (KBinds.indexOf(b) == -1) {
        KBinds.add(b);
        //Logger.debug("Bind success on "+b.getCode(), 500);
      } else {
				System.out.println("Key "+b.toString()+" already binded !");
    	 //Logger.debug("Key "+b.getCode()+" already binded !", 500);
      }
    }
  }

	public static boolean hasClickBinding(ListenZone b) {
		if(!isInited) {
      Logger.error("IO Not inited", 500);
      return false;
    }
    synchronized(CBinds) {
      if (CBinds.indexOf(b) == -1) return false;
		}
		return true;

	}

  public static void addClickBinding(ListenZone b) {
    if(!isInited) {
      Logger.error("IO Not inited", 500);
      return;
    }
    synchronized(CBinds) {
      if (CBinds.indexOf(b) == -1) {
        CBinds.add(b);
        //Logger.debug("Bind success on "+b.toString(), 500);
      } else {
			System.out.println("Zone "+b.toString()+" already binded !");
    	 //Logger.debug("Zone "+b.toString()+" already binded !", 500);
      }
    }
  }

  public static void addHoverBinding(ListenZone b) {
    if(!isInited) {
      Logger.error("IO Not inited", 500);
      return;
    }
    synchronized(HBinds) {
      if (HBinds.indexOf(b) == -1) {
        HBinds.add(b);
        //Logger.debug("Bind success on "+b.toString(), 500);
      } else {
				System.out.println("HZone "+b.toString()+" already binded !");
    	 //Logger.debug("Zone "+b.toString()+" already binded !", 500);
      }
    }
  }

  public static void addScrollBinding(ListenZone b) {
    if(!isInited) {
      Logger.error("IO Not inited", 500);
      return;
    }
    synchronized(SBinds) {
      if (SBinds.indexOf(b) == -1) {
        SBinds.add(b);
        //Logger.debug("Bind success on "+b.toString(), 500);
      } else {
				System.out.println("SZone "+b.toString()+" already binded !");
    	 //Logger.debug("Zone "+b.toString()+" already binded !", 500);
      }
    }
  }

  public static void removeBinding(Binding b) {
    synchronized(KBinds) {
      if (KBinds.indexOf(b) == -1) {
        // Logger.info("Control Unbinded !", 100);
      } else {
        KBinds.remove(b);
      }
    }
  }

  public static void removeClickBinding(ListenZone b) {
    synchronized(CBinds) {
      if (CBinds.indexOf(b) == -1) {
        // Logger.info("Control Unbinded !", 100);
      } else {
        CBinds.remove(b);
      }
    }
  }

  public static void removeHoverBinding(ListenZone b) {
    synchronized(HBinds) {
      if (HBinds.indexOf(b) == -1) {
        // Logger.info("Control Unbinded !", 100);
      } else {
        HBinds.remove(b);
      }
    }
  }

  public static void removeScrollBinding(ListenZone b) {
    synchronized(SBinds) {
      if (SBinds.indexOf(b) == -1) {
        // Logger.info("Control Unbinded !", 100);
      } else {
        SBinds.remove(b);
      }
    }
  }

  public static void removeBinding(int keyCode) {
    Binding bind = null;
    synchronized(KBinds) {
      for (Binding b : KBinds) {
        if (b.getCode() == keyCode){
        	bind = b;
        	break; //remove the first
        }
      }
      if (bind != null) KBinds.remove(bind);
    }
  }

  public static void removeClickBinding(int x, int y) {
    ListenZone bind = null;
    synchronized(CBinds) {
      for (ListenZone b : CBinds) {
        if (b.isIn(x, y)){
        	bind = b;
        	break; //remove the first
        }
      }
      if (bind != null) CBinds.remove(bind);
    }
  }

  public static void removeHoverBinding(int x, int y) {
    ListenZone bind = null;
    synchronized(HBinds) {
      for (ListenZone b : HBinds) {
        if (b.isIn(x, y)){
        	bind = b;
        	break; //remove the first
        }
      }
      if (bind != null) HBinds.remove(bind);
    }
  }

  public static void removeScrollBinding(int x, int y) {
    ListenZone bind = null;
    synchronized(SBinds) {
      for (ListenZone b : SBinds) {
        if (b.isIn(x, y)){
        	bind = b;
        	break; //remove the first
        }
      }
      if (bind != null) SBinds.remove(bind);
    }
  }

  public void pause() {
    this.paused = true;
  }
  public void Unpause() {
    this.paused = false;
  }
  public boolean isPaused() {
    return this.paused;
  }

  public boolean isEventDriven() {
    return this.isEventDriven;
  }
  public void setEventDriven(boolean b) {
    this.isEventDriven = b;
  }


  public boolean isDebugged() {
    return this.debug;
  }

  public void setDebug(boolean b) {
    Config.GUI_TRACE = b;
    this.debug = b;
  }
  public void setDebugDiag(boolean b) {
    this.debugDiag = b;
  }
  public boolean isDiagDebugged() {
    return this.debugDiag;
  }

  public void UnlockInput() {
    this.lockInput = false;
    this.IHandler.UnlockMouse();
  }

  public boolean isInputLocked() {
    return this.lockInput;
  }

  public void lockInput() {
    this.lockInput = true;
    this.IHandler.lockMouse();
  }

  public boolean isKeyPressed(int code) {
    return this.IHandler.hasKeyPressed(code);
  }

  /** end test */

  @Override
  public final void run() {
    int frames = 0; // count frames for fps
    int ticks = 0; // count ticks for tps
		watchdog = 0;

    //Config.getFromFile();

    System.setProperty("sun.awt.noerasebackground", "true");
    int FPS = Config.FPS;
    final int RENDER_DELAY = 1000 / FPS;

    double TPS = (double)FPS/2; // Choice : for 60 fps having 30 tps. Increase for better physics
    double UPDATE_DELAY = 1000 / (double)TPS;

    final int UPDATE_RENDER = 5; // Ticks before rendering

    double lastUpdateTime = System.currentTimeMillis();
    double lastRenderTime;

    int lastSecondTime = (int) (lastUpdateTime / 1000);

    while(!this.frozen) {
      if(FPS != Config.FPS) {
        FPS = Config.FPS;
        TPS = (double)FPS/2;
        UPDATE_DELAY = 1000 / (double)TPS;
      }
      double current = System.currentTimeMillis();
      int updateCount = UPDATE_RENDER;

      double lim = current-UPDATE_DELAY;

      while(lastUpdateTime < lim && updateCount > 0) {
        ticks++;
        tick(updateCount);
        lastUpdateTime += UPDATE_DELAY;
        updateCount--;
      }
      if(lastUpdateTime < lim) {
        lastUpdateTime = lim;
      }
      if (this.redraw) {
        frames++;
        display();
      }
      lastRenderTime = current;

      int thisSecond = (int) (lastUpdateTime / 1000);
      if(thisSecond > lastSecondTime) {
        this.fps = frames;
        this.tps = ticks;
        frames = 0;
        ticks = 0;
        lastSecondTime = thisSecond;
        FPSCount();
      }
      while ( current - lastRenderTime < RENDER_DELAY && lastUpdateTime > lim) {
        Thread.yield();
        try {Thread.sleep(1);} catch(Exception e) { Logger.debug("sleep failed", 500);}
        current = System.currentTimeMillis();

      }
    }
  }

  //@Override
  public final void Old_run() {
    int frames = 0; // count frames for fps
    int ticks = 0; // count ticks for tps

    long ct = System.currentTimeMillis();
    long uptime = ct + 1000;

    double TSkip = 1000 / (double)Config.FPS; // ms per tick
    double accumulator = 0;

    boolean rerender = false;

    while(!frozen && !this.locked) {
      long current = System.currentTimeMillis();
      long diff = current - ct;
      ct = current;

      accumulator += (diff/TSkip); // diff/TSkip

      while(accumulator >= 1) {
        ticks++;
        tick(accumulator);
        accumulator--;
        rerender = true;
      }

      if (rerender) {
        frames++;
        display();
      }

      if(uptime < System.currentTimeMillis()) {
        this.fps = frames;
        this.tps = ticks;
        frames = 0;
        ticks = 0;
        FPSCount();
        uptime = System.currentTimeMillis() + 1000;
      }
    }
    pre_stop();
  }

  private void hoverCursor(Point p) {
    boolean h = false;
    for (ListenZone b : CBinds) {
      if(b.isActive() && b.isIn(p.x, p.y)) {
        h = true;
        break; // Only First event / IS THAT Z-INDEX ??
      }
    }
    if(h == this.isHover) return;
    if(h == false) {
      this.isHover = false;
      setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    } else {
      this.isHover = true;
      setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }
  }

  private void tick(double cons) {
    this.redraw = !this.isEventDriven;
		watchdog++;
		//if(watchdog>=wClean) System.gc();
    if (isInited) {
      if(this.IHandler.IOAction()) {
        this.redraw = true;
        /** Test action */
        for (Binding b : KBinds) {
          if(this.IHandler.hasKeyPressed(b.getCode())) {
            b.run();
          }
        }
        if(this.IHandler.hasClicked()) {
          /** Test action */
          Point p = this.IHandler.getLastClic();
          if (p!=null) {
            for (ListenZone b : CBinds) {
              if(b.isActive() && b.isIn(p.x, p.y)) {
                b.run();
                break; // Only First event / IS THAT Z-INDEX ??
              }
            }
            OnClick();
          }
          this.IHandler.consumeClicked();
        }
        if(this.IHandler.hasScrolled()) {
          /** Test action */
          Point p = this.IHandler.getMouseLocation();
          if (p!=null) {
            for (ListenZone b : SBinds) {
              if(b.isActive() && b.isIn(p.x, p.y)) {
                b.run();
                break; // Only First event / IS THAT Z-INDEX ??
              }
            }
            OnScroll();
          }
          this.IHandler.consumeScrolled();
        }
        if(this.IHandler.hasMoved()) {
					//System.gc();
          /** Test action */
          Point p = this.IHandler.getMove();
          if (p!=null) {
            hoverCursor(p);
            for (ListenZone b : HBinds) {
              if(b.isActive() && b.isIn(p.x, p.y)) {
                b.run();
                break; // Only First event / IS THAT Z-INDEX ??
              }
            }
            OnHover();
          }
          this.IHandler.consumeMoved();
        }
        //System.out.println(this.IHandler.getIOBusLog());
        this.IHandler.IOTick();
      }
      try {
        LO.update(this);
      } catch (Exception e) {
        ErrorDialog.handle("Module has crashed",e, true, false);
      }
    }
    OnTick();
  }

  public void OnInit() {
    // Override me !
  }

  public void OnTick() {
    // Override me !
  }

  public void OnClick() {
    Point p = this.IHandler.getLastClic();
    if (p==null) return;

    //System.out.println("Clic event @"+p.x+":"+p.y);
    // Override me !
  }
  public void OnHover() {
    Point p = this.IHandler.getMove();
    if (p==null) return;

    //System.out.println("Move event @"+p.x+":"+p.y);
    // Override me !
  }

  public void OnScroll() {
    int i = this.IHandler.getLastScroll();
    if (i==0) return;

    //System.out.println("Scroll event @"+i);
    // Override me !
  }

  public synchronized void render(Graphics2D g2d) {
    PaintTest(g2d);
    debug(g2d);
  };

  private synchronized void FPSCount() {
    if(! isInited) return;
    if(this.bf != null) {
      synchronized (this.bf) {
        if (this.bg2D == null) {
          try {
            this.bg2D = UIConsts.l_G2D(this.bf.getDrawGraphics());
          } catch (Exception e) {
            return;
            //this.bg2D = UIConsts.l_G2D(null);
          }
        }
        synchronized (this.bg2D) {
          this.bg2D.setColor(UIConsts.COLOR_WHITE);
          this.bg2D.setFont(this.FPSFont);
          this.bg2D.drawString(this.fps+"", 2, 14);
          this.bg2D.dispose();
          this.bf.show();
        }
      }
    }
  }

  private synchronized void renderViewport(Graphics2D g2d) {
    synchronized (widgets) {
      for (Widget w : widgets) {
        w.draw(g2d);
      }
    }
  }

  private void display() {
    RepaintManager rm = RepaintManager.currentManager(this);
    boolean b = rm.isDoubleBufferingEnabled();
    rm.setDoubleBufferingEnabled(false);
    if(this.frozen) return;
    this.locked = true;
    try {
	     if (this.bg2D == null) return;
      synchronized(this.bg2D) {
        Graphics2D g2d = UIConsts.l_G2D(bf.getDrawGraphics());
        this.bg2D = UIConsts.l_G2D(bf.getDrawGraphics()); //CHANGE THIS FOR IMG BG
        this.bg2D.setColor(UIConsts.COLOR_BACKGROUND);
        this.bg2D.fillRect(0, 0, getWidth(), getHeight());
        this.bg2D.setColor(UIConsts.COLOR_FOREGROUND);
        this.render(this.bg2D);
        renderViewport(this.bg2D);
        LO.render(this);
        if(this.debug) {
          this.debug(this.bg2D);
        }
        FPSCount();

				if (this.redraw) {
					bg2D.dispose();
					g2d.dispose();
				}
        bf.show();
      }

    } catch (Exception e) {
      //ErrorDialog.handle("Display error : ",e, true, false);
    }
    this.locked = false;
    rm.setDoubleBufferingEnabled(b);
  }

  public void reIgnite() {
    isInited = false;
  }

  public final void start() {
    if (!isInited) {
      createBufferStrategy(2);
      bf = getBufferStrategy();

      LO = new Loader();

      OnInit();
      isInited = true;

      LO.init(this);

    } else {
      onRemuse();
      LO.resume(this);
      this.IHandler.resume(this);
    }

    System.gc();
    System.runFinalization();

    if (frozen) {
      //Logger.debug("Render is active", 100);
      frozen = false;
      new Thread(this).start();
    }
  }

  public void onStop() {
    //Override Me !
  }
  public void onRemuse() {
    //Override Me !
  }

  protected static boolean setActivity(GameCanvas gc) {
    //WindowMaker w = (WindowMaker)SwingUtilities.getAncestorOfClass(WindowMaker.class, gc);
    try {
      if(WindowMaker.gc == null) {
        WindowMaker.make(gc);
      } else {
        WindowMaker.set(gc);
      }
      return true;
    } catch (Exception e) {
			ErrorDialog.handle("Activity error : ",e, true, false);
      return false;
    }
  }

  private void pre_stop() {
    synchronized (this.bg2D) {
      Font f = this.bg2D.getFont();
      Color o = this.bg2D.getColor();
      this.bg2D.setFont(UIConsts.MATERIALICONS);
      this.bg2D.setColor(UIConsts.COLOR_WHITE);
      this.bg2D.drawString(""+'\uE034', this.getWidth()-24, 24);
      this.bg2D.setFont(f);
      this.bg2D.setColor(o);
      this.bg2D.dispose();
      this.bf.show();
    }
  }

  public final void stop() {
		//TODO : find unneccessary refreshes
		/*StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		StringBuilder sb = new StringBuilder();

		for (StackTraceElement st : ste) {
		    sb.append(st.toString() + System.lineSeparator());
		}
		System.out.println(sb);*/
		onStop();
    /*while(this.locked) {
      try {Thread.sleep(1);} catch(Exception e) { Logger.debug("sleep failed", 500);}
    }*/
    pre_stop();
    LO.freeze(this);
    this.IHandler.freeze(this);
    frozen = true;
    pre_stop();

  }

  public final boolean isFrozen() {
    return frozen;
  }

  private void debug(Graphics gr) {
    if(!this.paused) {
      PaintTest(gr);
			gr.setFont(this.dfont);
      gr.setColor(UIConsts.COLOR_BUTTON);
      gr.fillRect(this.getWidth()-64-4, 32, 64, 36);
      gr.setColor(UIConsts.COLOR_FOREGROUND);
      gr.drawString("f:"+Config.FULLSCREEN, this.getWidth()-64, 42);
      gr.drawString("b:"+Config.BORDERED, this.getWidth()-64, 53);
      gr.drawString(":", this.getWidth()-64, 64);
    }
  }

  private void PaintTest(Graphics gr) {
		gr.setFont(this.dfont);
    if (this.debugDiag) {
      gr.setColor(UIConsts.COLOR_FOREGROUND);
      gr.drawLine(0, 0, this.getWidth(), this.getHeight());
      gr.drawLine(this.getWidth(), 0, 0, this.getHeight());
    }

    gr.setColor(UIConsts.COLOR_BUTTON);
    gr.fillRect(this.getWidth()-64-4, 4, 64, 24);
    gr.setColor(UIConsts.COLOR_BUTTON_HOVER);
    gr.drawRect(this.getWidth()-64-4, 4, 64, 24);

    Point p = this.IHandler.getMouseLocation();
    gr.setColor(UIConsts.COLOR_BUTTON);
    gr.drawLine(p.x, 0, p.x, this.getHeight());
    gr.drawLine(0, p.y, this.getWidth(), p.y);

    Point c = this.IHandler.getLastClic();
    if (c != null) {
      gr.setColor(UIConsts.COLOR_BLUE);
      gr.drawOval(c.x-2, c.y-2, 4, 4);
    }

    if (p!=null) {
      gr.setColor(UIConsts.COLOR_FOREGROUND);
      gr.drawString(p.x+":"+p.y, this.getWidth()-64, 20);
    }

    if(this.debugListenZones) {
      synchronized(CBinds) {
        for (ListenZone b : CBinds) {
          if(b.isActive()) {
            gr.setColor(UIConsts.COLOR_DEBUG_GREEN);
            gr.fillRect(b.getX()+b.getDX(), b.getY()+b.getDY(), b.getWidth(), b.getHeight());
          }
        }
      }
    }
    if(this.debugListenZones) {
      synchronized(SBinds) {
        for (ListenZone b : SBinds) {
          if(b.isActive()) {
            gr.setColor(UIConsts.COLOR_DEBUG_RED);
            gr.fillRect(b.getX()+b.getDX(), b.getY()+b.getDY(), b.getWidth(), b.getHeight());
          }
        }
      }
    }
  }

  public void draw(Graphics gr) { // Paint ?
    PaintTest(gr);
    debug(gr);
  }
}
