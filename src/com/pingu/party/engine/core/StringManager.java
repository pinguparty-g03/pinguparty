package com.pingu.party.engine.core;

import java.awt.Color;
import java.util.Map;
import java.io.File;
import javax.imageio.ImageIO;
import com.pingu.party.gui.widget.Widget;
import java.awt.Image;
import java.util.HashMap;

public class StringManager extends Thread {
  private static final HashMap<String, String> sMap = new HashMap<String, String>();

  public StringManager() {
    super();
    StringManager.add("missingno","<...>");
  }

  public final void run() {
    System.out.println("Strings Done !");
  }

  public static final void add(String key, String value) {
    sMap.put(key, value);
  }

  public static final void loadAll() {
    new AssetManager().start();
  }

  public static final String get(String name) {
    return(sMap.containsKey(name))?sMap.get(name):sMap.get("missingno");
  }

  public static final void clear() {
    sMap.clear();
  }

}
