package com.pingu.party.engine.core;

import com.pingu.party.activities.base.Activity;

import com.pingu.party.activities.base.GameWindow;
import com.pingu.party.engine.config.Config;
import javax.swing.AbstractAction;
import java.awt.event.InputEvent;
import javax.swing.JLabel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.KeyStroke;
import java.awt.GraphicsEnvironment;
import java.awt.Window;
import java.awt.DisplayMode;

import com.pingu.party.launcher.Utils.UIConsts;

import javax.swing.plaf.metal.MetalLookAndFeel;
import com.pingu.party.gui.Antartica;
import javax.swing.plaf.metal.MetalTheme;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import java.awt.AWTEvent;

import javax.swing.JRootPane;
import java.awt.Toolkit;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import javax.swing.JFrame;


public class WindowMaker extends JFrame implements ActionListener {
	private static final long serialVersionUID = -4342769584584240125L;
static GameCanvas gc = null;
  GameWindow gw = null;

  static WindowMaker window = null;

  public static int X = Config.WIDTH;
  public static int Y = Config.HEIGHT;

  MetalTheme theme;

  private static final Object defaultLookAndFeelDecoratedKey =
            new StringBuffer("JFrame.defaultLookAndFeelDecorated");

  GraphicsDevice screen = null;
  private final DisplayMode sdm;
  boolean isFull = false;

  public static void doRefresh() {
    X = Config.WIDTH;
    Y = Config.HEIGHT;
    if(instance != null) {
      instance.rescale();
      if (Config.FULLSCREEN) {
        instance.winTheme(false);
        instance.setFullscreen(true);
      } else {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        instance.setLocation((screenSize.width-instance.getWidth())/2, (screenSize.height-instance.getHeight())/2);
      }
    }
    gc.rescale();
    gc.repaint();
    /*
    gc.stop();

    gc.start();*/
  }

  private void rescale() {
    setSize(X+10, Y+32);
  }

  private static WindowMaker instance = null;

  private void winTheme(boolean show) {
    enableEvents(AWTEvent.KEY_EVENT_MASK | AWTEvent.WINDOW_EVENT_MASK);
    setLocale( JComponent.getDefaultLocale() );
    setRootPane(createRootPane());
    setBackground(UIManager.getColor("control"));
    setRootPaneCheckingEnabled(true);

    this.theme = new Antartica();
    MetalLookAndFeel.setCurrentTheme(this.theme);

    try {
      UIManager.setLookAndFeel(new MetalLookAndFeel());
      UIManager.getLookAndFeelDefaults().put("Panel.background", UIConsts.COLOR_BACKGROUND);
  	} catch (UnsupportedLookAndFeelException e) {
  		e.printStackTrace();
  	}
    if (show) {
      setUndecorated(true);
      getRootPane().setWindowDecorationStyle(JRootPane.PLAIN_DIALOG);
      getRootPane().setOpaque(true);
      setSize(getWidth()+10, getHeight()+32); // Size fix (borders)
    }
  }

  public WindowMaker(GameWindow gw, GameCanvas gac, String title, int w, int h) {
    super(title);
    instance = this;
    setUndecorated(true);
    setResizable(false);
    setSize(w, h);

    this.screen = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
    this.sdm = this.screen.getDisplayMode();

    if (Config.FULLSCREEN) {
      winTheme(false);
      setFullscreen(true);
    } else {
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      setLocation((screenSize.width-getWidth())/2, (screenSize.height-getHeight())/2);

      if (Config.BORDERED) {
        winTheme(true);
      } else {
        winTheme(false);
      }
    }

    JLabel debugT = new JLabel("");

    debugT.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_DOWN_MASK), "stopclose");
    debugT.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F11, 0), "fullscr");
    debugT.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0), "cursor");
    debugT.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0), "pause");
    debugT.getActionMap().put("stopclose", new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
            HandleClose();
        }
    });
    debugT.getActionMap().put("fullscr", new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
            ToggleEsc();
        }
    });
    debugT.getActionMap().put("cursor", new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
            //gc.IHandler.setCursorVisible(!gc.IHandler.isCursorVisible());
        }
    });
    debugT.getActionMap().put("pause", new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
          if (gc.isFrozen()) {
            gc.start();
          } else {
            gc.stop();
          }
        }
    });

    this.add(debugT);

    gc = gac;

    this.gw = gw;

    this.gw.setFocusable(true);
    setVisible(true);

    this.gw.register(new Activity(gc, 2));

    gc = this.gw.Init().getCanvas();
    //set(gc);
    add(gc);
    gc.setFocusable(true);

    gc.start(); //Finally start the game :D/**/

  }
  private WindowMaker(GameCanvas gac, String title, int w, int h) {
    super(title);
    instance = this;
    setUndecorated(true);
    setResizable(false);
    setSize(w, h);
    //System.out.print(getWidth()+":"+getHeight());

    this.screen = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();

    this.sdm = this.screen.getDisplayMode();

    //setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    if (Config.FULLSCREEN) {
      winTheme(false);
      setFullscreen(true);
    } else {
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      setLocation((screenSize.width-getWidth())/2, (screenSize.height-getHeight())/2);

      if (Config.BORDERED) {
        winTheme(true);
      } else {
        winTheme(false);
      }
    }

    JLabel debugT = new JLabel("");

    debugT.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_DOWN_MASK), "stopclose");
    debugT.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F11, 0), "fullscr");
    debugT.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0), "cursor");
    debugT.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0), "pause");
    debugT.getActionMap().put("stopclose", new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
            HandleClose();
        }
    });
    debugT.getActionMap().put("fullscr", new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
            ToggleEsc();
        }
    });
    debugT.getActionMap().put("cursor", new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
            gc.IHandler.setCursorVisible(!gc.IHandler.isCursorVisible());
        }
    });
    debugT.getActionMap().put("pause", new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
          if (gc.isFrozen()) {
            gc.start();
          } else {
            gc.stop();

          }
        }
    });

    this.add(debugT);

    gc = gac;
    add(gc);
    gc.setFocusable(true);
    setVisible(true);

    gc.start(); //Finally start the game :D
  }

  private void HandleClose() {
    gc.stop();
    // TODO: Close confirmation ?
    if(this.isFull) {
      setFullscreen(false);
    }
    this.dispose();
  }

  private void ToggleEsc() {
    if(this.isFull) {
      setFullscreen(false);
    } else {
      setFullscreen(true);
    }
  }

  private final void setFullscreen(boolean set) {
    if(set && !this.isFull) { // Enable Fullscreen
      getRootPane().setWindowDecorationStyle(JRootPane.NONE);
      this.screen.setFullScreenWindow((Window) this);
      if(this.screen.isDisplayChangeSupported()) {
        DisplayMode[] dm = this.screen.getDisplayModes();
        for (DisplayMode d : dm) {
          if (d.getWidth() == Config.WIDTH && d.getHeight() == Config.HEIGHT) {
            this.screen.setDisplayMode(d);
          }
        }
      }
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      Config.BASE_WIDTH = Config.WIDTH;
      Config.BASE_HEIGHT = Config.HEIGHT;
      Config.WIDTH = screenSize.width;
      Config.HEIGHT = screenSize.height;
      isFull = true;
      Config.FULLSCREEN = true;
      if(gc != null) {
        gc.rescale();
      }

    } else if (!set && this.isFull) { //Disable Fullscreen
      if (Config.BORDERED || Config.FULLSCREEN) {
        getRootPane().setWindowDecorationStyle(JRootPane.PLAIN_DIALOG);
      } else {
        getRootPane().setWindowDecorationStyle(JRootPane.NONE);
      }
      this.screen.setFullScreenWindow(null);
      if(this.screen.isDisplayChangeSupported()) {
        this.screen.setDisplayMode(this.sdm);
      }
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      setLocation((screenSize.width-getWidth())/2, (screenSize.height-getHeight())/2);
      isFull = false;
      Config.FULLSCREEN = false;
      Config.WIDTH = Config.BASE_WIDTH;
      Config.HEIGHT = Config.BASE_HEIGHT;
      gc.rescale();
    }
   }


  public static final WindowMaker make(GameCanvas gc) {
    window = new WindowMaker(gc, UIConsts.LPANEL_TITLE, Config.WIDTH, Config.HEIGHT);
    return window;
  }

  public static final WindowMaker make(GameWindow gw, GameCanvas gc) {
    window = new WindowMaker(gw, gc, UIConsts.LPANEL_TITLE, Config.WIDTH, Config.HEIGHT);
    return window;
  }

  public static final void set(GameCanvas gac) {
    gc.pause();
    try {
      gc.stop();
    } catch (Exception e) {

    }
    window.remove(gc);
		gc = null;
    gc = gac;
		gac = null;
    window.add(gc);
    gc.setFocusable(true);
    gc.reIgnite();
    window.setVisible(true);
    gc.requestFocusInWindow();

    gc.start(); //Finally start the game :D
  }

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}
}
