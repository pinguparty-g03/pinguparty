package com.pingu.party.engine.math;

public final class Vector2D {
  public double x, y;

  public Vector2D(double x, double y) {
    this.x = x;
    this.y = y;
  }
  public Vector2D() {
    this.x = 0;
    this.y = 0;
  }

	/**
	* Returns value of x
	* @return double
	*/
	public double getX() {
		return this.x;
	}

  /**
  * Returns value of y
  * @return double
  */
  public double getY() {
    return this.y;
  }

  public double getAngle() {
    return Math.atan2(this.y, this.x);
  }

	/**
	* Sets new value of x
	* @param x double
	*/
	public void setX(double x) {
		this.x = x;
	}

	/**
	* Sets new value of y
	* @param y double
	*/
	public void setY(double y) {
		this.y = y;
	}

  public boolean isNull() {
    return (this.x==0&&this.y==0);
  }

  public boolean isDiag() {
    return (this.x==this.y);
  }

  public boolean is(Vector2D v){
    return(this.x==v.x&&this.y==v.y);
  }
  public boolean is(double x, double y){
    return(this.x==x&&this.y==y);
  }

  public Vector2D negate() {
    return new Vector2D(-this.x, -this.y);
  }

  public void Faff(Vector2D v, double n) {
    this.x += (n*v.x);
    this.y += (n*v.y);
  }
  public Vector2D FaddN(Vector2D v, double n) {
    return new Vector2D(this.x+n*v.x,this.y+n*v.y);
  }

  public void add(Vector2D v) {
    this.x += v.x;
    this.y += v.y;
  }
  public void add(Vector2D v, Vector2D v2) {
    this.x = v.x+v2.x;
    this.y = v.y+v2.y;
  }
  public void add(double x, double y) {
    this.x += x;
    this.y += y;
  }
  public Vector2D addN(Vector2D v) {
    return new Vector2D(this.x+v.x,this.y+v.y);
  }
  public Vector2D addN(double x, double y) {
    return new Vector2D(this.x+x,this.y+y);
  }

  public void sub(Vector2D v) {
    this.x -= v.x;
    this.y -= v.y;
  }
  public void sub(Vector2D v, Vector2D v2) {
    this.x = v.x-v2.x;
    this.y = v.y-v2.y;
  }
  public void sub(double x, double y) {
    this.x -= x;
    this.y -= y;
  }
  public Vector2D subN(Vector2D v) {
    return new Vector2D(this.x-v.x,this.y-v.y);
  }
  public Vector2D subN(double x, double y) {
    return new Vector2D(this.x-x,this.y-y);
  }

  public void mult(double m) {
    this.x *= m;
    this.y *= m;
  }
  public Vector2D multN(double m) {
    return new Vector2D(this.x*m,this.y*m);
  }

  public double determinant(Vector2D v) {
    return (this.x*v.y-this.y*v.x);
  }

  public double dot(Vector2D v) {
    return (this.x*v.x+this.y*v.y);
  }
  public Vector2D dotN(Vector2D v) {
    return new Vector2D(this.x*v.x,this.y*v.y);
  }

  public double signedAngle(Vector2D v) {
    return (Math.atan2(v.y,v.x) - Math.atan2(this.y,this.x));
  }

  public double length() {
    return Math.sqrt((this.x*2)+(this.y*this.y));
  }

  public void rotate(double beta) {
    //x=cosβ*x−sinβ*y
    //y=sinβ*x+cosβ*y
    double s = Math.sin(beta);
    double c = Math.cos(beta);
    this.x = c*this.x - s*this.y;
    this.y = s*this.x + c*this.y;
  }

  public Vector2D interpolate(Vector2D v, double n) {
    return new Vector2D((1-n)*this.x+n*v.x,(1-n)*this.y+n*v.y);
  }

  /* Converters */
  public double[] toArray() {
    double[] rtrn = new double[2];
    rtrn[0] = this.x;
    rtrn[1] = this.y;
    return rtrn;
  }
  public String toString() {
    return "("+this.x+","+this.y+")";
  }
}
