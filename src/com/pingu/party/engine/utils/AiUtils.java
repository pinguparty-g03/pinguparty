package com.pingu.party.engine.utils;

import com.pingu.party.game.*;

import java.util.*;

import static java.util.Map.Entry.*;

public class AiUtils {
    public static AiUtils instance = new AiUtils();

    public enum colors {
        UNSET,
        RED,
        VIOLET,
        BLUE,
        YELLOW,
        GREEN,
        GREY
    }
    /*
     * PYTHON :
import random
colors = ["r","p","b","y","g"]
cards = colors * 7 + ['g']
random.shuffle(cards)
dist = [cards[i:i+6] for i in [i*6 for i in range(0,6)]]

counts = {}
for i in range(0,6):
  counts[i] = {}
  for c in colors:
    counts[i][c] =  dist[i].count(c)
  print(' '.join(["{}:{} ".format(j,counts[i][j]) for j in counts[i]]))

ocnt = {}
for i in range(0,8):
  ocnt[i] = 0

gcnt = {}
for i in range(0,9):
  gcnt[i] = 0

ccnt = {}
for i in range(0,9):
  ccnt[i] = 0


for i in range(0, 100000):
  random.shuffle(cards)
  dist = [cards[i:i+6] for i in [i*6 for i in range(0,6)]]
  for i in range(0,6):
    for c in colors:
      ocnt[dist[i].count(c)] += 1
    gcnt[dist[i].count('g')] += 1
    ccnt[dist[i].count('p')] += 1

import matplotlib.pyplot as plt
plt.plot([gcnt[i] for i in gcnt], 'go')
plt.plot([ccnt[i] for i in ccnt], 'bo')
plt.show()
     */

    public colors getColor(int i) {
        switch (i) {
            case 0:
                return colors.UNSET;
            case 1:
                return colors.RED;
            case 2:
                return colors.VIOLET;
            case 3:
                return colors.BLUE;
            case 4:
                return colors.YELLOW;
            case 5:
                return colors.GREEN;
            case 6:
                return colors.GREY;
            default:
                return colors.UNSET;
        }
    }

    public int getRemy(colors c) {
        switch (c) {
            case UNSET:
                return 0;
            case RED:
                return 1;
            case VIOLET:
                return 2;
            case BLUE:
                return 3;
            case YELLOW:
                return 4;
            case GREEN:
                return 5;
            case GREY:
                return 6;
            default:
                return 1;
        }
    }

    public Class getAI(String name) {
        switch (name) {
            case "AITwo":
                return AITwo.class;
            case "AIOne":
                return AIOne.class;
            default:
                try {
                    return Class.forName("com.pingu.party.game."+name);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    return AITwo.class;
                }

        }
    }

    /* board stuff */

    /**
     * is board empty ?
     * @param T Board
     * @return true if empty else false
     */
    public boolean isBoardEmpty(Board T) {
        //System.out.println(T.length()+" : "+T.length(0)+" - "+T.length(1));
        return isRankEmpty(T, 0);
        /*for (int i = 0; i < T.length(0); i++) {
            if(T.getColorBoard(0, i) != 0) return false;
        }
        return true;*/
    }

    public Boolean isRankEmpty(Board T, int rank) {
        if(rank<0 || rank >= T.length()) return null;
        //System.out.println(T.length()+" : "+T.length(0)+" - "+T.length(1));
        for (int i = 0; i < T.length(rank); i++) {
            if(T.getColorBoard(rank, i) != 0) return false;
        }
        return true;
    }

    /**
     * Search the highest rank in board
     * @param T Board
     * @return rank index
     */
    public int getHighestRank(Board T) {
        int i;
        for (i = 0; i < T.length(); i++) {
            if(isRankEmpty(T, i)) return i-1;
        }
        return i;
    }

    /**
     * Search the highest pair in board
     * @param T Board
     * @return rank of any else -1
     */
    public int getHighestRankPair(Board T) {
        for (int i = getHighestRank(T); i >= 0; i--) {
            if(hasAnyPair(T, i)) return i;
        }
        return -1;
    }

    /**
     * Search board for pairs
     * @param T Board
     * @return true if any
     */
    public boolean hasAnyRankAnyPair(Board T) {
        boolean any = false;
        for (int i = 0; i < T.length(); i++) {
            if(isRankEmpty(T, i)) return any;
            return hasAnyPair(T, i);
        }
        return false;
    }

    /**
     * Return if any pair
     * @param T Board
     * @param rank rank index
     * @return true or false, null if invalid
     */
    public Boolean hasAnyPair(Board T, int rank) {
        if(rank<0 || rank >= T.length()) return null;
        for (int i = 0; i < T.length(rank)-1; i++) {
            if(T.getColorBoard(rank, i) != 0 && T.getColorBoard(rank, i+1) != 0) return true;
        }
        return false;
    }

    /**
     * Hand contains color
     * @param h Card[]
     * @param color color integer value
     * @return null if false else index
     */
    public Integer handContains(Card[] h, int color) {
        for (int i = 0; i < h.length; i++) {
            if(h[i].getColor() == color) return i;
        }
        return null;
    }

    /**
     * pair of cards match A xor B in hand
     * @param T Board
     * @param hand Card[]
     * @param rank rank index
     * @return array of possibilities
     */
    public ArrayList<Possibility> pairXorHand(Board T, Card[] hand, int rank) {
        ArrayList<Possibility> possibilities = new ArrayList<>();
        if(rank<0 || rank >= T.length()) return possibilities;

        Map<colors, Integer> counts = sortMappedColorInBoard(T);

        int A, B;
        Integer posA, posB;

        //Possibility(int x, int y, AiUtils.colors color, Integer weight)
        for (int i = 0; i < T.length(rank)-1; i++) {
            A = T.getColorBoard(rank, i);
            B = T.getColorBoard(rank, i+1);
            if( A != 0 && B != 0) {
                posA = handContains(hand, A);
                posB = handContains(hand, B);
                if((posA != null) ^ (posB != null)) {
                    //can the card be played in corner ?
                    if(i == 0 || i == T.length(rank)-2) {
                        // In the corner
                        if(rank>0) {
                            int color = T.getColorBoard(rank-1, (i==0)?0:T.length(rank-1)-1);
                            Integer pos = handContains(hand, color);
                            if(T.getColorBoard(rank+1, i) == 0) {
                                if (pos != null) {
                                    possibilities.add(new Possibility(rank + 1, i, getColor(((posA == null)) ? B : A), AITree.DECREASE_CORNER_WEIGHT));
                                } else {
                                    possibilities.add(new Possibility(rank + 1, i, getColor(((posA == null)) ? B : A), AITree.PLAY_THIS_CHOICE_WEIGHT));
                                }
                            }
                        }
                    } else {
                        // Increase playing possibility
                        if(T.getColorBoard(rank+1, i) == 0) {
                            possibilities.add(new Possibility(rank + 1, i, getColor(((posA == null)) ? B : A), AITree.SAME_RANK_A_XOR_B_NOT_CORNER_WEIGHT));
                        }
                    }
                } else {
                    // Decrease possibility
                    if(T.getColorBoard(rank+1, i) == 0) {
                        if((i > 0 && i < T.length(rank+1)-1) && rank > 0) {
                            int preColor = T.getColorBoard(rank, i-1);
                            int nextColor = T.getColorBoard(rank, i+1);
                            if(preColor != 0 || nextColor != 0) {

                                ArrayList<Possibility> accessibleColorsA = getAccessibleColors(T, getColor(A));
                                ArrayList<Possibility> accessibleColorsB = getAccessibleColors(T, getColor(B));

                                // if accessible somewhere else
                                if(accessibleColorsA.size() > 1) {
                                    possibilities.addAll(accessibleColorsA);
                                }
                                if(accessibleColorsB.size() > 1) {
                                    possibilities.addAll(accessibleColorsB);
                                }
                                // else :

                                if (preColor != 0 && preColor != A) {
                                    possibilities.add(new Possibility(rank + 1, i, getColor(A), AITree.BLOCKING_SELF_WEIGHT));
                                }
                                if (nextColor != 0 && nextColor != A) {
                                    possibilities.add(new Possibility(rank + 1, i, getColor(A), AITree.BLOCKING_SELF_WEIGHT));
                                }
                                if (preColor != 0 && preColor != B) {
                                    possibilities.add(new Possibility(rank + 1, i, getColor(B), AITree.BLOCKING_SELF_WEIGHT));
                                }
                                if (nextColor != 0 && nextColor != B) {
                                    possibilities.add(new Possibility(rank + 1, i, getColor(B), AITree.BLOCKING_SELF_WEIGHT));
                                }
                                return possibilities;
                            }
                        }
                        int AWeight = AITree.SAME_RANK_A_AND_B_IN_HAND_WEIGHT;
                        int BWeight = AITree.SAME_RANK_A_AND_B_IN_HAND_WEIGHT;
                        if(counts.containsKey(getColor(A)) && counts.containsKey(getColor(B))) {
                            if(counts.get(getColor(A)) > counts.get(getColor(B))) {
                                BWeight = BWeight + BWeight;
                            } else if(counts.get(getColor(A)) < counts.get(getColor(B))) {
                                AWeight = AWeight + AWeight;
                            }
                        }
                        possibilities.add(new Possibility(rank+1, i, getColor(A), AWeight));
                        possibilities.add(new Possibility(rank+1, i, getColor(B), BWeight));

                    }
                }
            }
        }

        return possibilities;
    }

    /**
     * Is the color accessible?
     * @param T Board
     * @param color color
     * @return empty array if none else positions
     */
    public ArrayList<Possibility> getAccessibleColors(Board T, colors color) {
        ArrayList<Possibility> possibilities = new ArrayList<>();
        for (int x = 0; x < T.length(); x++) {
            for (int y = 0; y < T.length(x); y++) {
                int c = T.getColorBoard(x, y);
                if(c != 0) {
                    colors col = getColor(c);
                    if(col == color) {
                        if(x < T.length()-1 && y < T.length(x)-1) {
                            if(y>0) {
                                // can check both parent
                                if(T.getColorBoard(x+1, y) == 0) {
                                    possibilities.add(new Possibility(x+1, y, color, AITree.EMPTY_CASE_WEIGHT));
                                    possibilities.add(new Possibility(x+1, y-1, color, AITree.EMPTY_CASE_WEIGHT));
                                }
                            } else {
                                if(T.getColorBoard(x+1, y) == 0) {
                                    possibilities.add(new Possibility(x+1, y, color, AITree.EMPTY_CASE_WEIGHT));
                                }
                            }
                        }
                    }
                }
            }
        }
        return possibilities;
    }

    public Map<colors, Integer> sortMappedColorInBoard(Board T) {
        Map<colors, Integer> count = new HashMap<colors, Integer>();
        for ( colors c : colors.values()) {
            count.put(c, 0);
        }

        for (int i = 0; i < T.length(); i++) {
            for (int j = 0; j < T.length(i); j++) {
                int c = T.getColorBoard(i, j);
                if(c != 0) {
                    colors color = getColor(c);
                    count.put(color, count.get(color) + 1);
                }
            }
        }

        return count;
    }

    public ArrayList<colors> sortColorInBoard(Board T) {
        ArrayList<colors> col = new ArrayList<>();
        Map<colors, Integer> count = new HashMap<colors, Integer>();
        for ( colors c : colors.values()) {
            count.put(c, 0);
        }

        for (int i = 0; i < T.length(); i++) {
            for (int j = 0; j < T.length(i); j++) {
                int c = T.getColorBoard(i, j);
                if(c != 0) {
                    colors color = getColor(c);
                    count.put(color, count.get(color) + 1);
                }
            }
        }

        count.entrySet()
                .stream()
                .parallel()
                .sorted(Map.Entry.comparingByValue())
                .forEachOrdered(x -> col.add(x.getKey()));

        return col;
    }

    /* cards stuff */

    private HashMap<colors, Integer> shortCards(Card[] hand) {
        HashMap<colors, Integer> count = new HashMap<>();
        for (colors col : colors.values()) {
            if(col != colors.UNSET && col != colors.GREY) {
                count.put(col, 0);
            }
        }
        for(Card crd : hand) {
            colors c = getColor(crd.getColor());
            if(c != colors.UNSET && c != colors.GREY) {
                count.replace(c, count.get(c) + 1);
            }
        }
        return count;
    }

    @Deprecated
    public colors _lessPresent(Card[] hand) {
        HashMap<colors, Integer> count = shortCards(hand);
        colors c = colors.UNSET;
        int min = 0;

        for (colors cl : colors.values()) {
            if(cl != colors.UNSET && cl != colors.GREY) {
                if(min == 0 && count.get(cl)> 0) {
                    min = count.get(cl);
                    c = cl;
                }
                if(count.get(cl) < min && count.get(cl)> 0) {
                    c = cl;
                    min = count.get(cl);
                }
            }
        }

        return c;
    }

    public colors lessPresent(Card[] hand) {
        return shortCards(hand)
                .entrySet()
                .stream()
                .sorted(comparingByValue())
                .filter(x -> x.getValue() > 0 && x.getKey() != colors.UNSET)
                .iterator()
                .next()
                .getKey();
    }

    public ArrayList<colors> uniq(Card[] hand) {
        HashMap<colors, Integer> count = shortCards(hand);
        ArrayList<colors> uniq = new ArrayList<>();

        for (colors cl : colors.values()) {
            if(cl != colors.UNSET && cl != colors.GREY) {
                if(count.get(cl) == 1) {
                    uniq.add(cl);
                }
            }
        }

        return uniq;
    }

    public ArrayList<colors> shortByPresence(Card[] hand) {
        int sz = colors.values().length-2;
        ArrayList<colors> c = new ArrayList<>(sz);
        Map<colors, Integer> count = shortCards(hand);

        count.entrySet()
                .stream()
                .sorted(comparingByValue())
                .forEach(colorsIntegerEntry -> {
                    colors cl = colorsIntegerEntry.getKey();
                    if (cl != colors.UNSET && cl != colors.GREY && colorsIntegerEntry.getValue() > 0) {
                        //System.out.print("" + cl.name() + " " + count.get(cl)+" ");
                        c.add(cl);
                    }
                });

        return c;
    }

    public Possibility mostInterestPossibility(ArrayList<Possibility> possibilities) {
        if(possibilities.size() == 0) return null;
        Possibility max = possibilities.get(0);
        for (int i = 0; i < possibilities.size(); i++) {
           if(possibilities.get(i).getWeight() > max.getWeight()) max = possibilities.get(i);
        }
        return max;
    }

    public colors mostPresent(Card[] hand) {
        Iterator<Map.Entry<colors, Integer>> it = shortCards(hand)
                .entrySet()
                .stream()
                .sorted(comparingByValue())
                .filter(x -> x.getValue() > 0 && x.getKey() != colors.UNSET)
                .iterator();
        Map.Entry<colors, Integer> c= it.next();
        while (it.hasNext()) c = it.next();
        return c.getKey();
    }
    @Deprecated
    public colors _mostPresent(Card[] hand) {
        HashMap<colors, Integer> count = shortCards(hand);
        /*
        count.entrySet()
                .stream()
                .max((colors e1, Integer e2) -> e1.getValue()
                        .compareTo(e2.getValue())
                );


        count.entrySet()
                .stream()
                .sorted(comparingByValue())
                .iterator()
                .next()
                .getKey();
                .max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1).get().getKey();
*/
        colors c = colors.RED;
        int max = count.get(c);

        for (colors cl : colors.values()) {
            if(cl != colors.UNSET && cl != colors.GREY) {
                if(count.get(cl) > max) {
                    c = cl;
                    max = count.get(cl);
                }
            }
        }

        return c;
    }

    private AiUtils() {
        //Stuff
    }

}
