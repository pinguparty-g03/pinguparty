package com.pingu.party.engine.utils;

import java.util.Objects;

/**
 * λ
 */
public interface Lambda<T> {
  void run(T t);
  default T setT(T t) {
    return t;
  }
  default Lambda<T> yield(T t) {
    run(t);
    return this;
  };
  default T[] runW(T... t) {
    T[] args;
    run(t[0]);
    return t;
  };
  default Lambda<T> and(Lambda<? super T> r) {
        Objects.requireNonNull(r);
        return (T t) -> { run(t); r.run(t); };
    }
}
