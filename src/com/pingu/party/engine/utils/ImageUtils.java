package com.pingu.party.engine.utils;

import java.awt.Toolkit;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

public abstract class ImageUtils {
  public ImageUtils() {}

  public static BufferedImage bufferize(Image img) {
    BufferedImage rtrn = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
    Graphics2D g2d = rtrn.createGraphics();
    g2d.drawImage(img, 0, 0, null);
    g2d.dispose();
    return rtrn;
  }

  public static Image debufferize(BufferedImage bfi) {
    return Toolkit.getDefaultToolkit().createImage(bfi.getSource());
  }

}
