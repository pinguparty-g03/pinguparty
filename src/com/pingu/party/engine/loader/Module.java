package com.pingu.party.engine.loader ;

import com.pingu.party.engine.core.GameCanvas;

public interface Module {
  /** Mod fancy name */
	//public String name;
  /** Mod unique id */
  //final double id;
  /** Load order value */
//  public int LO;

  /** Mod Description */
  //public String desc;
  /** Mod requiements (id list) */
  //public double[] requiements;

	public void load(GameCanvas gc);	// Used after init, for enabling tasks
	public void unload(GameCanvas gc); // Used when crashes and user-asked unload
	public String getName(); // Used for logs
	public int getID();
	public void setOrder(int o);
	public int getOrder();

	public void init(GameCanvas gc);
  public void render(GameCanvas gc);
	public void update(GameCanvas gc);
	public void freeze(GameCanvas gc);
	public void resume(GameCanvas gc);
}
