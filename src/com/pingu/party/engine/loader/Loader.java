package com.pingu.party.engine.loader ;

import com.pingu.party.launcher.Component.ErrorDialog;

import com.pingu.party.engine.core.GameCanvas;
import com.pingu.party.engine.config.Load;
import com.pingu.party.engine.loader.Module;

import com.pingu.party.engine.console.Console;

import java.util.ArrayList;
import java.util.Map;

public class Loader {
  protected ArrayList<Module> ModsLoaded;
  //protected Map<int, Module> ModsLoad;

  private boolean unloadOnCrash = true;

  public Loader() {
    ModsLoaded = new ArrayList<Module>();
    System.out.println("New console");
    ModsLoaded.add(new Console());
	}

  public String getLoaded() {
    StringBuilder rtrn = new StringBuilder();
    for (Module m : ModsLoaded) {
      rtrn.append(m.getName()+";");
    }
    return rtrn.toString();
  }

  public void reload(GameCanvas gc) {
    synchronized(this.ModsLoaded) {
      for (Module m : ModsLoaded) {
        m.freeze(gc);
        m.resume(gc);
      }
    }
  }

  public boolean load(Module m) {
    try {
      ModsLoaded.add(m);
      // TODO : Advanced load mechanism
      System.out.print("Loaded : "+m.getName());
      return true;

    } catch (Exception e) {
      ErrorDialog.handle("Cannot add "+m.getName(),e, true, false);
      return false;
    }
  }
  public final void render(GameCanvas gc) {
    if(gc.isFrozen()) return;
    synchronized(this.ModsLoaded) {
      for (Module m : ModsLoaded) {
        m.render(gc);
      }
    }
  }
	public void init(GameCanvas gc) {
    for (Module m : ModsLoaded) {
      try {
        m.init(gc);
        m.load(gc);
      } catch (Exception e) {
        m.unload(gc);
        ErrorDialog.handle("Cannot load "+m.getName(),e, true, false);
        //System.out.println("Cannot load "+m.getName()+" ("+e+")");
      }
    }
  }
	public void freeze(GameCanvas gc) {
    for (Module m : ModsLoaded) {
      try {
        m.freeze(gc);
      } catch (Exception e) {
        if(this.unloadOnCrash) m.unload(gc);
        ErrorDialog.handle("Module "+m.getName()+" has crashed",e, true, false);
      }
    }
  }

  public void resume(GameCanvas gc) {
    for (Module m : ModsLoaded) {
      try {
        m.resume(gc);
      } catch (Exception e) {
        if(this.unloadOnCrash) m.unload(gc);
        ErrorDialog.handle("Module "+m.getName()+" has crashed",e, true, false);
      }
    }
  }

  public void update(GameCanvas gc) {
    synchronized(this.ModsLoaded) {
      for (Module m : ModsLoaded) {
        m.update(gc);
      }
    }
  }
}
