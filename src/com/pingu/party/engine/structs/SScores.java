package com.pingu.party.engine.structs;

import java.util.ArrayList;

public class SScores {
  private ArrayList<SScore> scores;
  private static final int UMAX_SIZE = 6;
  private int pos = 0;

  public SScores() {
    this(UMAX_SIZE);
  }
  public SScores(int max) {
    this.scores = new ArrayList<>((max<6)?6:max);
  }
  public void addScore(SScore s) {
    this.scores.add(s);
    pos++;
  }
  public void addScore(String name, int score, int wins, int greenCards, int ps) {
    this.scores.add(new SScore(name,score,wins,greenCards,ps));
    pos++;
  }
  public int getPos() {
    return this.pos;
  }
  public SScore getScore(int index) {
    return this.scores.get(index);
  }

}
