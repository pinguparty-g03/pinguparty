package com.pingu.party.engine.structs;

import java.util.Collection;

public interface SList<E> {
  int size();
  boolean isEmpty();
  E get(int index);
  boolean add(E elem);
  boolean add(int index, E elem);
  boolean addAll(Collection<? extends E> col);
  boolean remove(Object o);
  E remove(int index);
  int indexOf(Object o);
  void clear();
  Object[] toArray();
}
