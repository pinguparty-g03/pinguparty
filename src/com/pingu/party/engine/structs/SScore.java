package com.pingu.party.engine.structs;

public class SScore {
  private String name;
  private int score;
  private int wins;
  private int greenCards;
  private int pos;


  public int getPos() {
    return this.pos;
  }

  public void setPos(int p) {
    this.pos = p;
  }

	/**
	* Returns value of name
	* @return
	*/
	public String getName() {
		return this.name;
	}

	/**
	* Sets new value of name
	* @param
	*/
	public void setName(String name) {
		this.name = name;
	}

	/**
	* Returns value of score
	* @return
	*/
	public int getScore() {
		return this.score;
	}

	/**
	* Sets new value of score
	* @param
	*/
	public void setScore(int score) {
		this.score = score;
	}

	/**
	* Returns value of wins
	* @return
	*/
	public int getWins() {
		return this.wins;
	}

	/**
	* Sets new value of wins
	* @param
	*/
	public void setWins(int wins) {
		this.wins = wins;
	}

	/**
	* Returns value of greenCards
	* @return
	*/
	public int getGreenCards() {
		return this.greenCards;
	}

	/**
	* Sets new value of greenCards
	* @param
	*/
	public void setGreenCards(int greenCards) {
		this.greenCards = greenCards;
	}

	public SScore() {
		this("Unknown", 0, 0, 0, 0);
	}

  public SScore(String name) {
    this(name, 0, 0, 0, 0);
  }

	/**
	* Default SScore constructor
	*/
	public SScore(String name, int score, int wins, int greenCards, int pos) {
		super();
		this.name = name;
		this.score = score;
		this.wins = wins;
		this.greenCards = greenCards;
    this.pos = pos;
	}

	/**
	* Create string representation of SScore for printing
	* @return
	*/
	@Override
	public String toString() {
		return "SScore [pos="+ pos + ", name=" + name + ", score=" + score + ", wins=" + wins + ", greenCards=" + greenCards + "]";
	}
}
