package com.pingu.party.engine.structs;

import com.pingu.party.network.structures.IP;

public class SUser {
  private String name;
  private IP ip;
  private int port;
  private boolean isIA;
  private String agent;



	/**
	* Returns value of name
	* @return
	*/
	public String getName() {
		return this.name;
	}

	/**
	* Sets new value of name
	* @param
	*/
	public void setName(String name) {
		this.name = name;
	}

	/**
	* Returns value of ip
	* @return
	*/
	public IP getIp() {
		return this.ip;
	}

	/**
	* Sets new value of ip
	* @param
	*/
	public void setIp(IP ip) {
		this.ip = ip;
	}

	/**
	* Returns value of port
	* @return
	*/
	public int getPort() {
		return this.port;
	}

	/**
	* Sets new value of port
	* @param
	*/
	public void setPort(int port) {
		this.port = port;
	}

	/**
	* Returns value of isIA
	* @return
	*/
	public boolean isIsIA() {
		return this.isIA;
	}

	/**
	* Sets new value of isIA
	* @param
	*/
	public void setIsIA(boolean isIA) {
		this.isIA = isIA;
	}

	/**
	* Returns value of agent
	* @return
	*/
	public String getAgent() {
		return this.agent;
	}

	/**
	* Sets new value of agent
	* @param
	*/
	public void setAgent(String agent) {
		this.agent = agent;
	}

	/**
	* Default empty SUser constructor
	*/
	public SUser() {
		super();
	}

	/**
	* Default SUser constructor
	*/
	public SUser(String name, IP ip, int port, boolean isIA, String agent) {
		super();
		this.name = name;
		this.ip = ip;
		this.port = port;
		this.isIA = isIA;
		this.agent = agent;
	}

  public String getDetails() {
    return ""+ip+":"+port+" ["+agent+"]"+((isIA)?" (IA)":"");
  }

	/**
	* Create string representation of SUser for printing
	* @return
	*/
	@Override
	public String toString() {
		return "SUser [name=" + name + ", ip=" + ip + ", port=" + port + ", isIA=" + isIA + ", agent=" + agent + "]";
	}
}
