package com.pingu.party.engine.structs;

import java.util.Arrays;
import java.util.Collection;

/**
 * @param <E> Circular List
 */
public class SSelectList<E> implements SList<E> {
  private static final int ULIST_MAX = 8;

  private boolean circular = true;
  private int pos = 0;

  private int size = 0;
  protected Object[] list;
  public SSelectList(int max) {
    if(max>0) {
      this.list = new Object[max];
    } else {
      this.list = new Object[ULIST_MAX];
    }
  }

  public SSelectList(boolean cir) {
    this();
    this.circular = cir;
  }

  public SSelectList() {
    this.list = new Object[ULIST_MAX];
  }

  public SSelectList(Collection<? extends E> col) {
    this.list = col.toArray();
    this.size = this.list.length;
  }

  private boolean isIndexValid(int index) {
    return (index<0 || index>=this.size);
  }

  @SuppressWarnings("unchecked")
  private E _get(int index) {
    return (E) this.list[index];
  }

  private boolean _extend(int added) {
    this.list = Arrays.copyOf(this.list, this.size+added);
    //TODO if extends failed return false
    return false;
  }

  private boolean _checkSize(int size) {
    if(this.size + size >= ULIST_MAX) return _extend(ULIST_MAX);
    else return true;
  }

  private void _remove(int index) {
    int l = this.size-index-1;
    if(index>=0 && l>0) System.arraycopy(this.list, index+1, this.list, index, l);
    this.list[--this.size] = null;
  }

  public int size() {
    return this.size;
  }

  public boolean isEmpty() {
    return (this.size==0);
  }

  public E get(int index) {
    return (isIndexValid(index))?_get(index):null;
  }

  public E getCurrent() {
    return _get(pos);
  }

  public int getPos() {
    return pos;
  }

  public void next() {
    if(this.circular) this.pos = (this.pos+1)%this.size;
    else if (this.pos+1<this.size()) this.pos++;
  }
  public void prev() {
    if(this.circular) this.pos = (this.pos-1>=0)?this.pos-1:this.size-1;
    else if (this.pos-1>=0) this.pos--;
  }

  public boolean add(E elem) {
    _checkSize(1);
    this.list[this.size++] = elem;
    return true;
  }

  public boolean add(int index, E elem) {
    if(!isIndexValid(index) || !_checkSize(1)) return false;
    System.arraycopy(this.list, index, this.list, index+1,this.size-index);
    this.list[this.size++] = elem;
    return true;
  }

  public boolean setValue(int index, E elem) {
    if(!isIndexValid(index) || !_checkSize(1)) return false;
    this.list[index] = elem;
    return true;
  }

  public boolean setCurrentValue(E elem) {
    return setValue(this.pos, elem);
  }

  public boolean addAll(Collection<? extends E> col) {
    Object[] o = col.toArray();
    _checkSize(o.length);
    if(o.length == 0) return false;
    System.arraycopy(o, 0, this.list, this.size, o.length);
    this.size += o.length;
    return true;
  }

  public boolean remove(Object o) {
    if(o==null) return false;
    for (int i = 0;i<this.size ;i++ ) {
      if(o.equals(this.list[i])) {
        _remove(i);
        return true;
      }
    }
    return false;
  }

  public E remove(int index) {
    if(!isIndexValid(index) || !_checkSize(1)) return null;
    E rtrn = _get(index);
    _remove(index);
    return rtrn;
  }

  public int indexOf(Object o) {
    if(o==null) return -1;
    for (int i = 0; i < size; i++) if (o.equals(this.list[i])) return i;
    return -1;
  }

  public void clear() {
    for (;this.size>0;this.size-- )  this.list[this.size-1] = null;
    this.size = 0;
  }

  public Object[] toArray() {
    return Arrays.copyOf(this.list, this.size);
  }

}
