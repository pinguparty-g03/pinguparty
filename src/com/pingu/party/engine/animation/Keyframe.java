package com.pingu.party.engine.animation;

import java.awt.Image;
import java.awt.image.BufferedImage;

public class Keyframe {
  private int duration = 1;
  private Image img = null;
  private BufferedImage rimg = null;

  public Keyframe(int d, Image i, BufferedImage b) {
    this.duration = d;
    this.img = i;
    this.rimg = b;
  }

  public Image getImage() {
    return this.img;
  }
  public BufferedImage getBufferedImage() {
    return this.rimg;
  }
  public int getDuration() {
    return this.duration;
  }

}
