package com.pingu.party.engine.animation;

import java.awt.image.BufferedImage;

import java.awt.Image;
import java.util.ArrayList;
import com.pingu.party.engine.utils.ImageUtils;
import com.pingu.party.engine.animation.Keyframe;

//TODO: check for bad keyframes + empty Keyframe

public class Animation {
  private boolean paused = false;
  private boolean loop = true;
  private boolean ret = false; //only for ALTERNATE

  private ArrayList<Keyframe> Keyframes = null;

  private Keyframe current = null;

  private int duration;
  private int index = 0;
  private int timer = 0;
  private int toNext = 0;
  public static enum playType {
    NORMAL,
    REVERSED,
    ALTERNATE,
    ALTERNATE_REVERSED,
  }
  private playType direction = playType.NORMAL;

  public Animation() {
    this.Keyframes = new ArrayList<Keyframe>();
    this.duration = 1;
    initIndex();
  }
  public Animation(int duration) {
    this.Keyframes = new ArrayList<Keyframe>();
    this.duration = duration;
    initIndex();
  }
  public Animation(int duration, boolean loop) {
    this.Keyframes = new ArrayList<Keyframe>();
    this.duration = duration;
    this.loop = loop;
  }
  public Animation(int duration, boolean loop, playType direction) {
    this.Keyframes = new ArrayList<Keyframe>();
    this.duration = duration;
    this.loop = loop;
    this.direction = direction;
    initIndex();
  }

  public void addKey(Keyframe k) {
    synchronized (this.Keyframes) {
      this.Keyframes.add(k);
      this.duration+=k.getDuration();
    }
    if (this.direction == playType.REVERSED || this.direction == playType.ALTERNATE_REVERSED) this.index = this.Keyframes.size()-1;
  }
  public void addKey(int d, Image i) {
    synchronized (this.Keyframes) {
      this.Keyframes.add(new Keyframe(d, i, ImageUtils.bufferize(i)));
      this.duration+=d;
    }
    if (this.direction == playType.REVERSED || this.direction == playType.ALTERNATE_REVERSED) this.index = this.Keyframes.size()-1;
  }

  public void initIndex() {
    this.index = 0;
    //(this.direction == playType.NORMAL || this.direction == playType.ALTERNATE)?0:this.Keyframes.size()-1;
  }

  public int getIndex() {
    return this.index;
  }

  public void setLooping(boolean l) {
    this.loop = l;
  }
  public boolean isLooping() {
    return this.loop;
  }

  public void setplayType(playType d) {
    this.direction = d;
  }
  public playType getPlayType() {
    return this.direction;
  }

  public synchronized Image getCurrentImage() {
    return this.Keyframes.get(this.index).getImage();
  }
  public synchronized BufferedImage getCurrentBufferedImage() {
    return this.Keyframes.get(this.index).getBufferedImage();
  }
  public synchronized int getFrameDuration() {
    return this.Keyframes.get(this.index).getDuration();
  }
  public synchronized Keyframe getCurrentFrame() {
    return this.Keyframes.get(this.index);
  }

  public synchronized void step(int c) {
    this.index = (this.index + c)%this.duration+1;
  }

  public synchronized int getTimer() {
    return this.timer;
  }

  public synchronized int getDuration() {
    return this.duration;
  }

  public synchronized void next() {
    this.timer++;
    switch (this.direction) {
      case NORMAL:
        if(this.index+1>=this.Keyframes.size() && this.timer >= this.current.getDuration()) {
          if (this.loop) {
            this.index = 0;
            this.timer = 0;
            this.current = this.Keyframes.get(this.index);
            //System.out.println(this.index);
          }else{
            break;
          }
        }
        if(this.timer>=this.Keyframes.get(this.index).getDuration()) {
          this.index++;
          this.timer = 0;
          this.current = this.Keyframes.get(this.index);
          //System.out.println(this.index);
        }
        break;
      case REVERSED:
      if(this.index-1<0 && this.timer >= this.current.getDuration()) {
        if (this.loop) {
          this.index = this.Keyframes.size()-1;
          this.timer = 0;
          this.current = this.Keyframes.get(this.index);
          //System.out.println(this.index);
        }else{
          break;
        }
      }
      if(this.timer>=this.Keyframes.get(this.index).getDuration()) {
        this.index--;
        this.timer = 0;
        this.current = this.Keyframes.get(this.index);
        //System.out.println(this.index);
      }
      break;
      case ALTERNATE:
        if(this.index==0 && this.ret) {
          if(!this.loop) {
            return;
          }
          this.ret = false;
          this.timer = 0;
          this.current = this.Keyframes.get(this.index);
        }
        if(this.index+1>=this.Keyframes.size() && this.timer >= this.current.getDuration()) {
          this.ret = true;
          this.index--;
          this.timer = 0;
          this.current = this.Keyframes.get(this.index);
          //System.out.println(this.index);
        }
        if(this.timer>=this.Keyframes.get(this.index).getDuration()) {
          if(this.ret) {
            this.index--;
          } else {
            this.index++;
          }
          this.timer = 0;
          this.current = this.Keyframes.get(this.index);
          //System.out.println(this.index);
        }
        break;
      case ALTERNATE_REVERSED:
        if(this.index==this.Keyframes.size()-1 && this.ret) {
          if(!this.loop) {
            return;
          }
          this.ret = false;
          this.timer = 0;
          this.current = this.Keyframes.get(this.index);
        }
        if(this.index-1<0 && this.timer >= this.current.getDuration()) {
          this.ret = true;
          this.index++;
          this.timer = 0;
          this.current = this.Keyframes.get(this.index);
          //System.out.println(this.index);
        }
        if(this.timer>=this.Keyframes.get(this.index).getDuration()) {
          if(this.ret) {
            this.index++;
          } else {
            this.index--;
          }
          this.timer = 0;
          this.current = this.Keyframes.get(this.index);
          //System.out.println(this.index);
        }
    }
    //this.index = (this.index + 1)%this.duration+1;
  }
  public synchronized void prev() {
    this.timer--;
    switch (this.direction) {
      case NORMAL:
        if(this.index-1<0 && this.timer <=0) {
          if (this.loop) {
            this.index = this.Keyframes.size()-1;
            this.timer = this.Keyframes.get(this.index).getDuration();
            this.current = this.Keyframes.get(this.index);
            //System.out.println(this.index);
          }else{
            break;
          }
        }
        if(this.timer<=0) {
          this.index--;
          this.timer = this.Keyframes.get(this.index).getDuration();
          this.current = this.Keyframes.get(this.index);
          //System.out.println(this.index);
        }
        break;
      case REVERSED:
        if(this.index+1>=this.Keyframes.size() && this.timer <=0) {
          if (this.loop) {
            this.index = 0;
            this.timer = this.Keyframes.get(this.index).getDuration();
            this.current = this.Keyframes.get(this.index);
            //System.out.println(this.index);
          }else{
            break;
          }
        }
        if(this.timer<=0) {
          this.index++;
          this.timer = this.Keyframes.get(this.index).getDuration();
          this.current = this.Keyframes.get(this.index);
          //System.out.println(this.index);
        }
        break;
      case ALTERNATE:
        if(this.index==0 && this.ret) {
          if(!this.loop) {
            return;
          }
          this.ret = false;
          this.timer = 0;
          this.current = this.Keyframes.get(this.index);
        }
        if(this.index+1>=this.Keyframes.size() && this.timer >= this.current.getDuration()) {
          this.ret = true;
          this.index--;
          this.timer = 0;
          this.current = this.Keyframes.get(this.index);
          //System.out.println(this.index);
        }
        if(this.timer>=this.Keyframes.get(this.index).getDuration()) {
          if(this.ret) {
            this.index--;
          } else {
            this.index++;
          }
          this.timer = 0;
          this.current = this.Keyframes.get(this.index);
          //System.out.println(this.index);
        }
        break;
      case ALTERNATE_REVERSED:
        if(this.index==this.Keyframes.size()-1 && this.ret) {
          if(!this.loop) {
            return;
          }
          this.ret = false;
          this.timer = 0;
          this.current = this.Keyframes.get(this.index);
        }
        if(this.index-1<0 && this.timer >= this.current.getDuration()) {
          this.ret = true;
          this.index++;
          this.timer = 0;
          this.current = this.Keyframes.get(this.index);
          //System.out.println(this.index);
        }
        if(this.timer>=this.Keyframes.get(this.index).getDuration()) {
          if(this.ret) {
            this.index++;
          } else {
            this.index--;
          }
          this.timer = 0;
          this.current = this.Keyframes.get(this.index);
          //System.out.println(this.index);
        }
    }
    //this.index = (this.index - 1)%this.duration+1;
  }

  public synchronized void play() {
    this.paused = false;
    this.current = Keyframes.get(this.index);
  }
  public synchronized void pause() {
    this.paused = true;
  }
  public synchronized void stop() {
    this.paused = true;
    this.index = (this.direction == playType.NORMAL || this.direction == playType.ALTERNATE)?0:this.duration-1;
    this.timer = 0;
    this.current = Keyframes.get(this.index);
  }
  public boolean isPaused() {
    return this.paused;
  }
}
