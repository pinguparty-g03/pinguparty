package com.pingu.party.engine.game;

import java.io.IOException;

/**
 * class AIProfile<br>
 * for AI profiles
 * @author florian
 *
 */
public class AIProfile extends Profile {
	/**
	 * @param name distant user's name
	 * @param uuid distant user's uuid
	 * @param image path of the distant user's image
	 * @param level AI level
	 */
	public AIProfile(String uuid, boolean distant, String name, String image, int level, String type)
	{
		super(uuid,name,image);
		this.distant = distant;
		this.imAI = true;
		this.level = level;
		this.AIType = type;
	}
}
