package com.pingu.party.engine.game.Test;

import com.pingu.party.gui.widget.Widget;

import com.pingu.party.gui.toaster.Toaster;
import java.awt.Toolkit;

import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Image;

import com.pingu.party.engine.animation.Animation;
import com.pingu.party.launcher.Utils.UIConsts;
import java.util.Arrays;
import java.awt.Point;

import java.awt.image.SampleModel;
import java.awt.image.DataBufferInt;
import java.awt.image.DirectColorModel;

import com.pingu.party.engine.core.GameCanvas;
import com.pingu.party.engine.config.Config;
import java.util.Random;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.IndexColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.awt.Graphics;

import com.pingu.party.engine.loader.Loader;

public class Pixels extends GameCanvas {
  long framesDrawed;
  int col=0;

  int w, h;
  int[] raster;
  ColorModel cm;
  DataBuffer buffer;
  SampleModel sm;
  WritableRaster wrRaster;
  BufferedImage backBuffer;
  Loader l;
  Toaster t;

  int cur = 0;

  Animation anU = new Animation(0, true, Animation.playType.NORMAL);
  Animation an = new Animation(0, true, Animation.playType.NORMAL);
  Animation an1 = new Animation(0, true, Animation.playType.REVERSED);
  Animation an2 = new Animation(0, true, Animation.playType.ALTERNATE);
  Animation an3 = new Animation(0, true, Animation.playType.ALTERNATE_REVERSED);

  // https://stackoverflow.com/questions/33460365/what-the-fastest-way-to-draw-pixels-buffer-in-java

  public void init() {
    an.play();
    an1.play();
    an2.play();
    an3.play();
  }


public void initViewport() {
  Widget w = new Widget();
  w._add();
  this.addToViewport(w);
}

//36, 54, 61 - 193, 198, 200 - 72, 87, 92
  public Pixels() {
    super();
    Image im = null;
    for (int i = 0; i <11; i++) {
      try {
        im = ImageIO.read(new File("src/com/pingu/party/engine/game/Test/kanna/"+i+".png"));
        an.addKey(Config.FPS/6, im);
        an1.addKey(Config.FPS/6, im);
        an2.addKey(Config.FPS/6, im);
        an3.addKey(Config.FPS/6, im);
      } catch (Exception e) {
        System.out.println(e);
      }
    }

    Image im2 = null;
    for (int i = 0; i <51; i++) {
      try {
        im = ImageIO.read(new File("src/com/pingu/party/engine/game/Test/uni/frame_"+((i<10)?("0"+i):i)+"_delay-0.04s.jpg"));
        anU.addKey(Config.FPS/15, im);
      } catch (Exception e) {
        System.out.println(e);
      }
    }

    init();

    initViewport();

  }

  @Override
  public void OnInit() {
    this.l = getLoader();
    this.t = new Toaster();
    this.Logger = this.t;
    this.l.load(this.t);
    //this.t.info("We're updating our privacy policy", 1500);
    //this.t.warn("Your windows version is outdated. Please refer to update notice", 500);
    //this.t.error("X", 1000);
    this.t.showAll();
  }

  @Override
  public void onStop() {
    this.an.pause();
    this.an1.pause();
    this.an2.pause();
    this.an3.pause();
    this.anU.pause();
  }

  @Override
  public void onRemuse() {
    this.an.play();
    this.an1.play();
    this.an2.play();
    this.an3.play();
    this.anU.play();
  }


  public void render(Graphics2D g2d) {
    if(!this.an.isPaused() && g2d != null) {
      an.next();
      an1.next();
      an2.next();
      an3.next();
      anU.next();
      //Image img1 = Toolkit.getDefaultToolkit().getImage("src/com/pingu/party/engine/game/Test/kanna/0.png");
      g2d.drawImage(an.getCurrentImage(), 8,8, this);
      g2d.drawString(":"+an.getIndex(),16,144);
      g2d.drawImage(an1.getCurrentImage(), 140,8, this);
      g2d.drawString(":"+an1.getIndex(),148,144);
      g2d.drawImage(an2.getCurrentImage(), 272,8, this);
      g2d.drawString(":"+an2.getIndex(),280,144);
      g2d.drawImage(an3.getCurrentImage(), 404,8, this);
      g2d.drawString(":"+an3.getIndex(),412,144);
      g2d.drawImage(anU.getCurrentBufferedImage(), 8,150, this);

      g2d.drawString(""+anU.getIndex(),8,150+270+17);

      this.cur = (cur+1)%(anU.getDuration()+1);
      g2d.drawString(anU.getDuration()+":"+this.cur,34+(anU.getDuration()+1)*2,150+270+17);
      //g2d.finalize();
      g2d.setColor(UIConsts.COLOR_BUTTON);
      g2d.drawRect(28,150+270+4, (anU.getDuration()+1)*2, 16);
      g2d.setColor(UIConsts.COLOR_BUTTON_HOVER);
      g2d.setBackground(UIConsts.COLOR_BUTTON_HOVER);
      g2d.fillRect(28+this.cur*2,150+270+4,2,16);

    }

  };

}
