package com.pingu.party.engine.game.Test;

import com.pingu.party.launcher.Utils.UIConsts;

import com.pingu.party.engine.core.GameCanvas;
import com.pingu.party.engine.config.Config;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.GeneralPath;

public class Bouncy extends GameCanvas {
  private int[] coordinates;
  private int[] deltas;
  private Paint paint;

  // http://www.java2s.com/Code/Java/2D-Graphics-GUI/Hypnosisanimation.htm

  public void init() {
    int numberOfCoordinates = 512*4+2;
    coordinates = new int[numberOfCoordinates];
    deltas = new int[numberOfCoordinates];
    for (int i = 0; i < numberOfCoordinates; i++) {
      coordinates[i] = (int) (Math.random() * 300);
      deltas[i] = (int) (Math.random() * 4 + 3);
      if (deltas[i] > 4)
        deltas[i] = -(deltas[i] - 3);
    }
    paint = new GradientPaint(0, 0, UIConsts.COLOR_BUTTON_HOVER, 200, 100, UIConsts.COLOR_BUTTON, true);
  }

  @Override
  public void OnTick() {
    Dimension d = getSize();
    if (d.width == 0 || d.height == 0)
      return;
    for (int i = 0; i < coordinates.length; i++) {
      coordinates[i] += deltas[i];
      int limit = (i % 2 == 0) ? d.width : d.height;
      if (coordinates[i] < 0) {
        coordinates[i] = 0;
        deltas[i] = -deltas[i];
      } else if (coordinates[i] > limit) {
        coordinates[i] = limit - 1;
        deltas[i] = -deltas[i];
      }
    }
  }

  private Shape createShape() {
    GeneralPath path = new GeneralPath();
    path.moveTo(coordinates[0], coordinates[1]);
    for (int i = 2; i < coordinates.length; i += 4)
      path.quadTo(coordinates[i], coordinates[i + 1], coordinates[i + 2], coordinates[i + 3]);
    path.closePath();
    return path;
  }

//36, 54, 61 - 193, 198, 200 - 72, 87, 92
  public Bouncy() {
    super();
    init();
  }

  public void render(Graphics2D g2d) {
    Shape s = createShape();
    g2d.setPaint(paint);
    g2d.fill(s);
    g2d.setPaint(UIConsts.COLOR_BUTTON_HOVER);
    g2d.draw(s);

  };

}
