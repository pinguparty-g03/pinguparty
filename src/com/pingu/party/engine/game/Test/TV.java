package com.pingu.party.engine.game.Test;

import com.pingu.party.gui.toaster.Toaster;
import com.pingu.party.engine.loader.Loader;
import com.pingu.party.engine.core.GameCanvas;
import com.pingu.party.engine.config.Config;
import java.util.Random;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.IndexColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.awt.Graphics;

public class TV extends GameCanvas {
  byte[] data;
  Random random;
  BufferedImage image;

  Loader l;
  Toaster t;

  // http://www.java2s.com/Code/Java/2D-Graphics-GUI/NoiseImage.htm

  @Override
  public void OnInit() {
    this.l = getLoader();
    this.t = new Toaster();
    this.Logger = this.t;
    this.l.load(this.t);
    this.t.showAll();
  }

  public void init() {
    int l = ((Config.WIDTH + 8)*Config.HEIGHT) / 8;
    data = new byte[l];
    DataBuffer db = new DataBufferByte(data, l);
    WritableRaster wr = Raster.createPackedRaster(db, Config.WIDTH, Config.HEIGHT, 1, null);
    ColorModel cm = new IndexColorModel(1, 2, new byte[] { (byte) 72, (byte) 193 }, new byte[] {
        (byte) 87, (byte) 198 }, new byte[] { (byte) 92, (byte) 200 });
    image = new BufferedImage(cm, wr, false, null);
    random = new Random();
  }


//36, 54, 61 - 193, 198, 200 - 72, 87, 92
  public TV() {
    super();
    init();
  }

  public void render(Graphics2D g2d) {
    random.nextBytes(data);
    if (image == null) init();
    g2d.drawImage(image, 0, 0, this);

  };

}
