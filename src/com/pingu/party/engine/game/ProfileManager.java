package com.pingu.party.engine.game;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.io.IOException;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 * class ProfileManager<br>
 * to manage profiles
 * @author florian
 *
 */
public class ProfileManager {
	
	/**
	 * path of the profiles database
	 */
	private static final String PATH = "src/com/pingu/party/engine/data/profiles.json" ;
	/**
	 * list of profiles
	 */
	private List<Profile> profiles;
	
	/**
	 * load all profiles
	 */
	public ProfileManager()
	{
		try {
			this.profiles = new ArrayList<Profile>();
			InputStream input = new FileInputStream(PATH);
			// create a JsonReader from Json
			JsonReader reader = Json.createReader(input);	
			// get the JsonObject
			JsonObject json = reader.readObject();
			input.close() ;
			reader.close() ;
			JsonArray profiles = json.getJsonArray("profiles");
			for(int i = 0; i < profiles.size(); i++){
	        	String uuid = profiles.getJsonObject(i).getString("uuid");
	        	this.profiles.add(new Profile(uuid));
			}
		} catch(IOException e) {
			System.out.println("Stream Error");
		}
	}
		
	/**
	 * @param name user's name
	 * @param image path of the user's image
	 */
	public void createProfile(String name,String image)
	{
		String uuid = UUID.randomUUID().toString();
		while(!(Profile.verifyUUID(uuid))) // loop to have an unique uuid
		{
			uuid = UUID.randomUUID().toString();
		}
		Profile newProfile = new Profile(uuid,name,image);
		newProfile.createUser();
		this.profiles.add(newProfile);
	}
	
	/**
	 * @param name user's name
	 * @param image path of the user's image
	 */
	public void createDistantProfile(String name,String image)
	{
		String uuid = UUID.randomUUID().toString();
		while(!(Profile.verifyUUID(uuid))) // loop to have an unique uuid
		{
			uuid = UUID.randomUUID().toString();
		}
		Profile newProfile = new DistantProfile(uuid,name,image);
		newProfile.createUser();
		this.profiles.add(newProfile);
	}
	
	/**
	 * @param name user's name
	 * @param image path of the user's image
	 * @param level level of AI
	 * @param type AI's type
	 */
	public void createAIProfile(String name,boolean distant,String image,int level, String type)
	{
		String uuid = UUID.randomUUID().toString();
		while(!(Profile.verifyUUID(uuid))) // loop to have an unique uuid
		{
			uuid = UUID.randomUUID().toString();
		}
		Profile newProfile = new AIProfile(uuid,distant,name,image,level,type);
		newProfile.createUser();
		this.profiles.add(newProfile);
	}
	
	
	/**
	 * test display console
	 */
	public void display()
	{
		if(this.profiles.size() != 0)
		{
			System.out.println("Profiles :\n") ;
			for(Profile p : this.profiles)
			{
				System.out.println(p.toString());
			}
		}
	}

	
	/**
	 * @return list of profiles
	 */
	public List<Profile> getList()
	{
		return this.profiles;
	}


	public List<Profile> getAiList()
	{
		return this.profiles.stream().filter(p -> p.imAI).collect(Collectors.toList());
	}

	/**
	 * delete none local profiles and AI
	 */
	public void deleteNoneLocal()
	{
		for (Iterator<Profile> iter = this.profiles.listIterator(); iter.hasNext(); )
		{
		    Profile p = iter.next();
		    if(p.getDistant()) iter.remove();
			if(p.getAI()) iter.remove();
		}
	}
	
	/**
	 * @return local an none AI profile
	 */
	public List<Profile> getLocal()
	{
		deleteNoneLocal();
		return this.profiles;
	}
	
	/**
	 * @return random profile
	 */
	public Profile getRandomProfile()
	{
		Random rand = new Random(); 
		int nb = rand.nextInt(profiles.size());
		while(nb >= profiles.size())
		{
			nb = rand.nextInt(profiles.size());
		}
		return profiles.get(nb);
	}
	
}
