package com.pingu.party.engine.game;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonWriter;

import com.pingu.party.stats.Stat ; // to use the class Stat
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.IOException;



/**
 * class Profil
 * @author florian RIBOU
 *
 */
public class Profile {
	/**
	 * path of the profiles database
	 */
	private static final String PATH = "src/com/pingu/party/engine/data/profiles.json" ;
	/**
	 * user's name
	 */
	private String name ;
	/**
	 * user's uuid
	 */
	private String uuid ;
	/**
	 * path of user's profile image
	 */
	private String image;
	/**
	 * global stats of this user
	 */
	private Stat stats;
	/**
	 * user is distant or no
	 */
	protected boolean distant;
	/**
	 * user is an AI or no
	 */
	protected boolean imAI;
	/**
	 * AI's level or 0 if is not an AI
	 */
	protected int level;
	/**
	 * AI's type
	 */
	protected String AIType;


	/**
	 * to create a new profile
	 * @param name (String) user's name
	 * @param uuid (String) user's uuid
	 * @param image (String) path of the user's profile image
	 */
	public Profile(String uuid,String name,String image)
	{	
			this.uuid = uuid;
			this.name = name;
			this.image = image;
			this.distant = false;
			this.imAI = false;
			this.level = 0;
			this.AIType = "none";
			//this.stats = new Stat(uuid);
	}

	/**
	 * load existing profile
	 * @param uuid (String) user's uuid
	 */
	public Profile(String uuid)
	{
		try {
			this.uuid = uuid;
			this.name = loadName();
			this.image = loadImage();
			this.distant = loadDistant();
			this.imAI = loadAI();
			this.level = loadLevel();
			this.AIType = loadAIType();
			// create a Stat instance
			this.stats = new Stat(uuid);
		} catch(IOException e) {
			System.out.println("Stream Error");
		}

	}


	/**
	 * create an user in the profiles DB
	 */
	public void createUser()
	{
		try {
			InputStream input = new FileInputStream(PATH) ;
			// create a JsonReader from Json
			JsonReader reader = Json.createReader(input) ;
			// get the JsonObject
			JsonObject json = reader.readObject() ;
			input.close() ;
			reader.close() ;
			JsonArray profiles = json.getJsonArray("profiles");
			JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
			JsonArrayBuilder newProfiles = Json.createArrayBuilder();
			for(int i = 0; i < profiles.size(); i++){
				JsonObjectBuilder newProfile = Json.createObjectBuilder();
	        	String name = profiles.getJsonObject(i).getString("name");
	        	newProfile.add("name",name);
	        	String uuid = profiles.getJsonObject(i).getString("uuid");
	        	newProfile.add("uuid",uuid);
	        	String image = profiles.getJsonObject(i).getString("image");
	        	newProfile.add("image",image);
	        	boolean distant = profiles.getJsonObject(i).getBoolean("distant");
	        	newProfile.add("distant",distant);
	        	boolean imAI = profiles.getJsonObject(i).getBoolean("imAI");
	        	newProfile.add("imAI",imAI);
	        	int level = profiles.getJsonObject(i).getInt("level");
	        	newProfile.add("level",level);
	        	String AIType = profiles.getJsonObject(i).getString("type");
	        	newProfile.add("type",AIType);
	        	newProfiles.add(newProfile);
	        }
			JsonObjectBuilder newProfile = Json.createObjectBuilder();
			newProfile.add("uuid",this.uuid) ;
			newProfile.add("name",this.name) ;
			newProfile.add("image",this.image);
			newProfile.add("distant",this.distant);
			newProfile.add("imAI",this.imAI);
			newProfile.add("level",this.level);
			newProfile.add("type",this.AIType);
			newProfiles.add(newProfile);
			jsonBuilder.add("profiles", newProfiles);
			JsonObject root = jsonBuilder.build();
			OutputStream output = new FileOutputStream(PATH);
			JsonWriter writer = Json.createWriter(output);
			writer.writeObject(root);
			writer.close();
			output.close();
			// create new stat for this user
			Stat.createStatsUser(this.uuid);
		} catch(IOException e) {
			System.out.println("Stream Error");
		}
	}
	
	/**
	 * create an user in the profiles DB with stats
	 */
	public void createUser(Stat stat)
	{
		try {
			InputStream input = new FileInputStream(PATH) ;
			// create a JsonReader from Json
			JsonReader reader = Json.createReader(input) ;
			// get the JsonObject
			JsonObject json = reader.readObject() ;
			input.close() ;
			reader.close() ;
			JsonArray profiles = json.getJsonArray("profiles");
			JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
			JsonArrayBuilder newProfiles = Json.createArrayBuilder();
			for(int i = 0; i < profiles.size(); i++){
				JsonObjectBuilder newProfile = Json.createObjectBuilder();
	        	String name = profiles.getJsonObject(i).getString("name");
	        	newProfile.add("name",name);
	        	String uuid = profiles.getJsonObject(i).getString("uuid");
	        	newProfile.add("uuid",uuid);
	        	String image = profiles.getJsonObject(i).getString("image");
	        	newProfile.add("image",image);
	        	boolean distant = profiles.getJsonObject(i).getBoolean("distant");
	        	newProfile.add("distant",distant);
	        	boolean imAI = profiles.getJsonObject(i).getBoolean("imAI");
	        	newProfile.add("imAI",imAI);
	        	int level = profiles.getJsonObject(i).getInt("level");
	        	newProfile.add("level",level);
	        	String AIType = profiles.getJsonObject(i).getString("type");
	        	newProfile.add("type",AIType);
	        	newProfiles.add(newProfile);
	        }
			JsonObjectBuilder newProfile = Json.createObjectBuilder();
			newProfile.add("uuid",this.uuid) ;
			newProfile.add("name",this.name) ;
			newProfile.add("image",this.image);
			newProfile.add("distant",this.distant);
			newProfile.add("imAI",this.imAI);
			newProfile.add("level",this.level);
			newProfile.add("type",this.AIType);
			newProfiles.add(newProfile);
			jsonBuilder.add("profiles", newProfiles);
			JsonObject root = jsonBuilder.build();
			OutputStream output = new FileOutputStream(PATH);
			JsonWriter writer = Json.createWriter(output);
			writer.writeObject(root);
			writer.close();
			output.close();
			// create new stat for this user
			Stat.createStatsUser(stat);
		} catch(IOException e) {
			System.out.println("Stream Error");
		}
	}

	/**
	 * del an user in the profiles DB
	 */
	public static void deleteUser(String verif)
	{
		try {
			InputStream input = new FileInputStream(PATH) ;
			// create a JsonReader from Json
			JsonReader reader = Json.createReader(input) ;
			// get the JsonObject
			JsonObject json = reader.readObject() ;
			input.close() ;
			reader.close() ;
			JsonArray profiles = json.getJsonArray("profiles");
			JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
			JsonArrayBuilder newProfiles = Json.createArrayBuilder();
			for(int i = 0; i < profiles.size(); i++){
				String uuid = profiles.getJsonObject(i).getString("uuid");
	        	if(!(uuid.equals(verif))) {
	        		JsonObjectBuilder newProfile = Json.createObjectBuilder();
	        		newProfile.add("uuid",uuid);
	            	String name = profiles.getJsonObject(i).getString("name");
	            	newProfile.add("name",name);
	            	String image = profiles.getJsonObject(i).getString("image");
	            	newProfile.add("image",image);
	            	boolean distant = profiles.getJsonObject(i).getBoolean("distant");
	            	newProfile.add("distant",distant);
	            	boolean imAI = profiles.getJsonObject(i).getBoolean("imAI");
	            	newProfile.add("imAI",imAI);
	            	int level = profiles.getJsonObject(i).getInt("level");
	            	newProfile.add("level",level);
	            	String AIType = profiles.getJsonObject(i).getString("type");
		        	newProfile.add("type",AIType);
	            	newProfiles.add(newProfile);
	        	}
	        }
			jsonBuilder.add("profiles", newProfiles);
			JsonObject root = jsonBuilder.build();
			OutputStream output = new FileOutputStream(PATH);
			JsonWriter writer = Json.createWriter(output);
			writer.writeObject(root);
			writer.close();
			output.close();
		} catch(IOException e) {
			System.out.println("Stream Error");
		}
	}

	/**
	 * @return uuid of this user
	 */
	public String getUUID()
	{
		return this.uuid;
	}

	/**
	 * @return name of this user
	 * @throws IOException stream problem
	 */
	public String loadName() throws IOException
	{
		InputStream input = new FileInputStream(PATH) ;
		// create a JsonReader from Json
		JsonReader reader = Json.createReader(input) ;
		// get the JsonObject
		JsonObject json = reader.readObject() ;
		input.close() ;
		reader.close() ;
		JsonArray profiles = json.getJsonArray("profiles");
		for(int i = 0; i < profiles.size(); i++){
			String uuid = profiles.getJsonObject(i).getString("uuid");
        	if(uuid.equals(this.uuid)) {
            	String name = profiles.getJsonObject(i).getString("name");
            	return name;
        	}
        }
		return "";
	}

	/**
	 * @return image's path of this user
	 * @throws IOException stream problem
	 */
	public String loadImage() throws IOException
	{
		InputStream input = new FileInputStream(PATH) ;
		// create a JsonReader from Json
		JsonReader reader = Json.createReader(input) ;
		// get the JsonObject
		JsonObject json = reader.readObject() ;
		input.close() ;
		reader.close() ;
		JsonArray profiles = json.getJsonArray("profiles");
		for(int i = 0; i < profiles.size(); i++){
			String uuid = profiles.getJsonObject(i).getString("uuid");
        	if(uuid.equals(this.uuid)) {
            	String image = profiles.getJsonObject(i).getString("image");
            	return image;
        	}
        }
		return "";
	}

	/**
	 * @return boolean true if user is distant<br>
	 * false if is not
	 * @throws IOException stream problem
	 */
	public boolean loadDistant() throws IOException
	{
		InputStream input = new FileInputStream(PATH) ;
		// create a JsonReader from Json
		JsonReader reader = Json.createReader(input) ;
		// get the JsonObject
		JsonObject json = reader.readObject() ;
		input.close() ;
		reader.close() ;
		JsonArray profiles = json.getJsonArray("profiles");
		for(int i = 0; i < profiles.size(); i++){
			String uuid = profiles.getJsonObject(i).getString("uuid");
        	if(uuid.equals(this.uuid)) {
            	boolean distant = profiles.getJsonObject(i).getBoolean("distant");
            	return distant;
        	}
        }
		return false;
	}

	/**
	 * @return boolean true if user is an AI<br>
	 * false if is not
	 * @throws IOException stream problem
	 */
	public boolean loadAI() throws IOException
	{
		InputStream input = new FileInputStream(PATH) ;
		// create a JsonReader from Json
		JsonReader reader = Json.createReader(input) ;
		// get the JsonObject
		JsonObject json = reader.readObject() ;
		input.close() ;
		reader.close() ;
		JsonArray profiles = json.getJsonArray("profiles");
		for(int i = 0; i < profiles.size(); i++){
			String uuid = profiles.getJsonObject(i).getString("uuid");
        	if(uuid.equals(this.uuid)) {
            	boolean imAI = profiles.getJsonObject(i).getBoolean("imAI");
            	return imAI;
        	}
        }
		return false;
	}

	/**
	 * @return int level of AI, 0 if user is not an AI
	 * @throws IOException stream problem
	 */
	public int loadLevel() throws IOException
	{
		InputStream input = new FileInputStream(PATH) ;
		// create a JsonReader from Json
		JsonReader reader = Json.createReader(input) ;
		// get the JsonObject
		JsonObject json = reader.readObject() ;
		input.close() ;
		reader.close() ;
		JsonArray profiles = json.getJsonArray("profiles");
		for(int i = 0; i < profiles.size(); i++){
			String uuid = profiles.getJsonObject(i).getString("uuid");
        	if(uuid.equals(this.uuid)) {
            	int level = profiles.getJsonObject(i).getInt("level");
            	return level;
        	}
        }
		return -1;
	}
	
	/**
	 * @return String of AI's type
	 * @throws IOException stream problem
	 */
	public String loadAIType() throws IOException
	{
		InputStream input = new FileInputStream(PATH) ;
		// create a JsonReader from Json
		JsonReader reader = Json.createReader(input) ;
		// get the JsonObject
		JsonObject json = reader.readObject() ;
		input.close() ;
		reader.close() ;
		JsonArray profiles = json.getJsonArray("profiles");
		for(int i = 0; i < profiles.size(); i++){
			String uuid = profiles.getJsonObject(i).getString("uuid");
        	if(uuid.equals(this.uuid)) {
            	String AIType = profiles.getJsonObject(i).getString("type");
            	return AIType;
        	}
        }
		return "AIOne";
	}

	/**
	 * @param verif UUID for vérification
	 * @return true if not exist, false if is already exist
	 */
	public static boolean verifyUUID(String verif)
	{
		try {
			InputStream input = new FileInputStream(PATH) ;
			// create a JsonReader from Json
			JsonReader reader = Json.createReader(input) ;
			// get the JsonObject
			JsonObject json = reader.readObject() ;
			input.close() ;
			reader.close() ;
			JsonArray profiles = json.getJsonArray("profiles");
			for(int i = 0; i < profiles.size(); i++){
				String uuid = profiles.getJsonObject(i).getString("uuid");
	        	if(uuid.equals(verif)) {
	            	return false;
	        	}
	        }
			return true;
		} catch(IOException e) {
			System.out.println("Stream Error");
			return false;
		}

	}


	/**
	 * @return profile's name
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * @return stats of this profile
	 */
	public Stat getStats()
	{
		return this.stats;
	}

	/**
	 * @return stats of this profile
	 */
	public String getImage()
	{
		return this.image;
	}
	
	
	/**
	 * @return distant profile or no
	 */
	public boolean getDistant()
	{
		return this.distant;
	}
	
	
	/**
	 * @return AI or no
	 */
	public boolean getAI()
	{
		return this.imAI;
	}
	
	
	/**
	 * @return level if AI
	 */
	public int getLevel()
	{
		return this.level;
	}
	
	/**
	 * @return level if AI
	 */
	public String getAIType()
	{
		return this.AIType;
	}
	
	/**
	 * get the name with the uuid
	 * @param uuid user's uuid
	 * @return profile's name
	 */
	public static String getName(String uuid)
	{
		try {
			InputStream input = new FileInputStream(PATH) ;
			// create a JsonReader from Json
			JsonReader reader = Json.createReader(input) ;
			// get the JsonObject
			JsonObject json = reader.readObject() ;
			input.close() ;
			reader.close() ;
			JsonArray profiles = json.getJsonArray("profiles");
			for(int i = 0; i < profiles.size(); i++){
				String uuidSearch = profiles.getJsonObject(i).getString("uuid");
	        	if(uuidSearch.equals(uuid)) {
	            	String name = profiles.getJsonObject(i).getString("name");
	            	return name;
	        	}
	        }
			return "Error";
		} catch(IOException e) {
			System.out.println("Stream Error");
			return "Error";
		}

	}
	
	/**
	 * to set the name of the selected profile
	 * @param newName 
	 */
	public void setName(String newName)
	{
		try {
			InputStream input = new FileInputStream(PATH) ;
			// create a JsonReader from Json
			JsonReader reader = Json.createReader(input) ;
			// get the JsonObject
			JsonObject json = reader.readObject() ;
			input.close() ;
			reader.close() ;
			JsonArray profiles = json.getJsonArray("profiles");
			JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
			JsonArrayBuilder newProfiles = Json.createArrayBuilder();
			for(int i = 0; i < profiles.size(); i++){
				JsonObjectBuilder newProfile = Json.createObjectBuilder();
				String name;
				if(this.uuid.equals(uuid)) name = newName;
				else name = profiles.getJsonObject(i).getString("name");
	        	newProfile.add("name",name);
	        	String uuid = profiles.getJsonObject(i).getString("uuid");
	        	newProfile.add("uuid",uuid);
	        	String image = profiles.getJsonObject(i).getString("image");
	        	newProfile.add("image",image);
	        	boolean distant = profiles.getJsonObject(i).getBoolean("distant");
	        	newProfile.add("distant",distant);
	        	boolean imAI = profiles.getJsonObject(i).getBoolean("imAI");
	        	newProfile.add("imAI",imAI);
	        	int level = profiles.getJsonObject(i).getInt("level");
	        	newProfile.add("level",level);
	        	String AIType = profiles.getJsonObject(i).getString("type");
	        	newProfile.add("type",AIType);
	        	newProfiles.add(newProfile);
	        }
			JsonObject root = jsonBuilder.build();
			OutputStream output = new FileOutputStream(PATH);
			JsonWriter writer = Json.createWriter(output);
			writer.writeObject(root);
			writer.close();
			output.close();
		} catch(IOException e) {
			System.out.println("Stream Error");
		}
	}
	
	/**
	 * to set an image for the selected profile
	 * @param newImage
	 */
	public void setImage(String newImage)
	{
		try {
			InputStream input = new FileInputStream(PATH) ;
			// create a JsonReader from Json
			JsonReader reader = Json.createReader(input) ;
			// get the JsonObject
			JsonObject json = reader.readObject() ;
			input.close() ;
			reader.close() ;
			JsonArray profiles = json.getJsonArray("profiles");
			JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
			JsonArrayBuilder newProfiles = Json.createArrayBuilder();
			for(int i = 0; i < profiles.size(); i++){
				JsonObjectBuilder newProfile = Json.createObjectBuilder();
	        	String name = profiles.getJsonObject(i).getString("name");
	        	newProfile.add("name",name);
	        	String uuid = profiles.getJsonObject(i).getString("uuid");
	        	newProfile.add("uuid",uuid);
	        	String image;
	        	if(this.uuid.equals(uuid)) image = newImage;
	        	else image = profiles.getJsonObject(i).getString("image");
	        	newProfile.add("image",image);
	        	boolean distant = profiles.getJsonObject(i).getBoolean("distant");
	        	newProfile.add("distant",distant);
	        	boolean imAI = profiles.getJsonObject(i).getBoolean("imAI");
	        	newProfile.add("imAI",imAI);
	        	int level = profiles.getJsonObject(i).getInt("level");
	        	newProfile.add("level",level);
	        	String AIType = profiles.getJsonObject(i).getString("type");
	        	newProfile.add("type",AIType);
	        	newProfiles.add(newProfile);
	        }
			JsonObject root = jsonBuilder.build();
			OutputStream output = new FileOutputStream(PATH);
			JsonWriter writer = Json.createWriter(output);
			writer.writeObject(root);
			writer.close();
			output.close();
		} catch(IOException e) {
			System.out.println("Stream Error");
		}
	}
	
	/**
	 * to set the level of the selected profile (just for AI)
	 * @param newLevel
	 */
	public void setLevel(int newLevel)
	{
		try {
			InputStream input = new FileInputStream(PATH) ;
			// create a JsonReader from Json
			JsonReader reader = Json.createReader(input) ;
			// get the JsonObject
			JsonObject json = reader.readObject() ;
			input.close() ;
			reader.close() ;
			JsonArray profiles = json.getJsonArray("profiles");
			JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
			JsonArrayBuilder newProfiles = Json.createArrayBuilder();
			for(int i = 0; i < profiles.size(); i++){
				JsonObjectBuilder newProfile = Json.createObjectBuilder();
	        	String name = profiles.getJsonObject(i).getString("name");
	        	newProfile.add("name",name);
	        	String uuid = profiles.getJsonObject(i).getString("uuid");
	        	newProfile.add("uuid",uuid);
	        	String image = profiles.getJsonObject(i).getString("image");
	        	newProfile.add("image",image);
	        	boolean distant = profiles.getJsonObject(i).getBoolean("distant");
	        	newProfile.add("distant",distant);
	        	boolean imAI = profiles.getJsonObject(i).getBoolean("imAI");
	        	newProfile.add("imAI",imAI);
	        	int level;
	        	if(this.uuid.equals(uuid)) level = newLevel;
	        	else level = profiles.getJsonObject(i).getInt("level");
	        	newProfile.add("level",level);
	        	String AIType = profiles.getJsonObject(i).getString("type");
	        	newProfile.add("type",AIType);
	        	newProfiles.add(newProfile);
	        }
			JsonObject root = jsonBuilder.build();
			OutputStream output = new FileOutputStream(PATH);
			JsonWriter writer = Json.createWriter(output);
			writer.writeObject(root);
			writer.close();
			output.close();
		} catch(IOException e) {
			System.out.println("Stream Error");
		}
	}
	
	/**
	 * to set the type of the AI profile
	 * @param newType
	 */
	public void setAIType(String newType)
	{
		try {
			InputStream input = new FileInputStream(PATH) ;
			// create a JsonReader from Json
			JsonReader reader = Json.createReader(input) ;
			// get the JsonObject
			JsonObject json = reader.readObject() ;
			input.close() ;
			reader.close() ;
			JsonArray profiles = json.getJsonArray("profiles");
			JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
			JsonArrayBuilder newProfiles = Json.createArrayBuilder();
			for(int i = 0; i < profiles.size(); i++){
				JsonObjectBuilder newProfile = Json.createObjectBuilder();
	        	String name = profiles.getJsonObject(i).getString("name");
	        	newProfile.add("name",name);
	        	String uuid = profiles.getJsonObject(i).getString("uuid");
	        	newProfile.add("uuid",uuid);
	        	String image = profiles.getJsonObject(i).getString("image");
	        	newProfile.add("image",image);
	        	boolean distant = profiles.getJsonObject(i).getBoolean("distant");
	        	newProfile.add("distant",distant);
	        	boolean imAI = profiles.getJsonObject(i).getBoolean("imAI");
	        	newProfile.add("imAI",imAI);
	        	int level = profiles.getJsonObject(i).getInt("level");
	        	newProfile.add("level",level);
	        	String AIType;
	        	if(this.uuid.equals(uuid)) AIType = newType;
	        	else AIType = profiles.getJsonObject(i).getString("type");
	        	newProfile.add("type",AIType);
	        	newProfiles.add(newProfile);
	        }
			JsonObject root = jsonBuilder.build();
			OutputStream output = new FileOutputStream(PATH);
			JsonWriter writer = Json.createWriter(output);
			writer.writeObject(root);
			writer.close();
			output.close();
		} catch(IOException e) {
			System.out.println("Stream Error");
		}
	}
	

	
	@Override
	public String toString()
	{
		String display = ""+this.name;
		return display;
	}
}
