package com.pingu.party.engine.game;

import java.util.ArrayList;

class Score {
	private String uuid;
	private int score;
	
	public Score(String uuid, int score)
	{
		this.uuid = uuid;
		this.score = score;
	}
	
	public String getUUID()
	{
		return this.uuid;
	}
	
	public int getScore()
	{
		return this.score;
	}
}

/**
 * @author florian
 * Elo class to calculate the Elo score
 */
public class Elo {
	
	/**
	 * to calculate the new elo score
	 * @param prof selected profile
	 * @param listeP ArrayList of all profiles for this game
	 * @param listeS ArrayList of scores
	 * @return score to add to the ELO for the selected profile
	 */
	public static int calcul(Profile prof, ArrayList<Profile> listeP, ArrayList<Score> listeS)
	{
		int eloMe = prof.getStats().getElo();
		double elo = 0;
		for(Profile p : listeP)
		{
			if(!prof.getUUID().equals(p.getUUID()))
			{
				double esti = estimation(eloMe,p.getStats().getElo());
				int myScore = 0; 
				int hisScore = 0;
				for(Score s : listeS)
				{
					if(s.getUUID().equals(prof.getUUID())) myScore = s.getScore();
					if(s.getUUID().equals(p.getUUID())) hisScore = s.getScore();
				}
				double result = resultat(myScore,hisScore);
				elo += duel(eloMe,result,esti);
			}
		}
		return (int) Math.round(elo);
	}
	
	/**
	 * to calculate an estimation between the elo score of two opponents
	 * @param eloMe
	 * @param eloAdv
	 * @return estimation
	 */
	public static double estimation(int eloMe, int eloAdv)
	{
		double exp = (eloAdv - eloMe) / 400.0;
		double formule = 1 / 1 + Math.pow(10, exp);
		return formule;
	}
	
	/**
	 * to calculate a duel between two opponents
	 * @param eloMe
	 * @param resultat
	 * @param estimation
	 * @return
	 */
	public static double duel(int eloMe,double resultat,double estimation)
	{
		int K = k(eloMe);
		double formule = K*(resultat - estimation);
		return formule;
	}
	
	/**
	 * variable K
	 * @param elo
	 * @return k
	 */
	public static int k(int elo)
	{
		if(elo < 700)
		{
			return 20;
		}
		else
		{
			return 10;
		}
	}
	
	/**
	 * who win this game between the two opponents
	 * @param myScore
	 * @param hisScore
	 * @return 0 or 1
	 */
	public static double resultat(int myScore, int hisScore)
	{
		if(myScore < hisScore)
		{
			return 1.0;
		}
		else
		{
			return 0.0;
		}
	}
}
