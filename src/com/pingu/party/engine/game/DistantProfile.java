package com.pingu.party.engine.game;

import java.io.IOException;

/**
 * class DistantProfile<br>
 * for distant profiles
 * @author florian
 *
 */
public class DistantProfile extends Profile {
	/**
	 * @param name distant user's name
	 * @param uuid distant user's uuid
	 * @param image path of the distant user's image
	 */
	public DistantProfile(String uuid,String name,String image)
	{
		super(uuid,name,image);
		this.distant = true;
	}
	
}
