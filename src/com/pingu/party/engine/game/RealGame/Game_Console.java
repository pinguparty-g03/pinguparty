package com.pingu.party.engine.game.RealGame;

import java.util.Scanner;
import java.util.Random;
import java.util.Vector;

/**
 * Main class which create the party
 * @author Remy BARRIOL
 */
public class Game_Console {

  public static void main(String[] args) {
    System.out.println("Welcome to Pingu Party !");
	/*Game G=new Game();
	G.initialization();
	G.distribution();
	G.mixPlayer();
	G.mainGame();*/

	/*G.initialization();
	G.distributionTest(0);
	G.mainGameTest();*/

	//G.testRanking();
	
	Init I=new Init();
	I.initialization();
  }
}
class Init{
	int nbPlayer =-1;
	int nbAI =-1;
	public void initialization()
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Number of players : ");
		System.out.println("1- Local ; 2- Multiplayer");
		int nint = sc.nextInt();
		while((nint!=1)&&(nint!=2))
		{
			System.out.println("There are only two possibility !");
			nint = sc.nextInt();
		}
		if(nint==1)
		{
			Game G=new Game();
			G.initialization();
			G.distribution();
			G.mixPlayer();
			G.mainGame();
		}
		else if(nint==2)
		{
			System.out.println("1- Server ; 2- Client");
			nint = sc.nextInt();
			while((nint!=1)&&(nint!=2))
			{
				System.out.println("There are only two possibility !");
				nint = sc.nextInt();
			}
			if(nint==1)
			{
				System.out.println("Numbers of Players :");
				nbPlayer = sc.nextInt();
				while((6<nbPlayer)||(nbPlayer<2))
				{
					System.out.println("The number of players cannot be more than 6 or negative : ");
					nbPlayer = sc.nextInt();
				}
			  String[] str = new String[1];
			  str[0]=Integer.toString(nbPlayer);
			  new MultiThreadServer().main(str);
			}
			else if(nint==2)
			{
				/*System.out.println("Enter the Server ip adress : ");*/
				String str;//= sc.next();
				str="127.0.0.1";
				String[] str1=new String[1];
				str1[0] = str;
				if(str!=null)
				{	
					new GameClient().main(str1);
				}
			}
		}
		else
		{
				System.err.println("ERROR");
		}
	}
	
}
/**
 * Class of the game, which manage players, rounds and turns.
 * @author Remy BARRIOL
 */
class Game
{
	protected Card discarded;
	protected int nbPlayer = -1;
	protected int nbAI = -1;
	protected Board B=new Board();
	protected Player[] allPlayer;
	protected int nbSlain=0;
	protected int turn=0;
	protected int lastFight=0;
	
	
	
	
	/**
	*  * Mix players of the game to randomise the order of players
	* @author Remy BARRIOL
	*/
	@SuppressWarnings("unchecked")
	public void mixPlayer()
	{
		Vector V=new Vector();
		Random rand = new Random();
		Player[] allPlayer2=new Player[nbPlayer];
		for(int i=0;i<nbPlayer;i++)
		{
			V.add(allPlayer[i]);
			allPlayer2[i]=allPlayer[i];
		}
		int mx=nbPlayer-1;
		int mn=0;
		int x;
		for(int i=0;i<nbPlayer;i++)
		{
			x=rand.nextInt(mx - mn + 1) + mn;
			allPlayer2[i]=(Player)V.get(x);
			V.remove(x);
			mx--;
		}
		for(int i=0;i<nbPlayer;i++)
		{
			allPlayer[i]=allPlayer2[i];
		}
	}
		/**
	*  * Function wich can test the Ranking in case of bug
	* @author Remy BARRIOL
	*/
	public void testRanking()
	{
		nbPlayer=6;
		allPlayer= new Player[nbPlayer];
		for(int i=0;i<nbPlayer;i++)
		{
			allPlayer[i]=new Player(nbPlayer,i+1);
		}
		B.createTable(nbPlayer);
		for(int i=0;i<nbPlayer;i++)
		{
			allPlayer[i].incScore(nbPlayer-i);
			//allPlayer[i].incScoreRound();
		}
		allPlayer[5].incScoreRound();
		allPlayer[1].incScore(1);
		allPlayer[5].incScore(15);
		allPlayer[4].incScore(2);
		allPlayer[4].increaseGreen();
		allPlayer[1].increaseGreen();
		allPlayer[0].increaseGreen();
		End();
	}
	public void distributionTest(int o)
	{
		if(o==0)
		{
			allPlayer[0].addCard(new Card(1));
			allPlayer[0].addCard(new Card(1));
			allPlayer[0].addCard(new Card(2));
			allPlayer[0].addCard(new Card(2));
			allPlayer[0].addCard(new Card(3));
			allPlayer[0].addCard(new Card(3));
			allPlayer[0].addCard(new Card(3));
			allPlayer[0].addCard(new Card(4));
			allPlayer[0].addCard(new Card(4));
			allPlayer[0].addCard(new Card(4));
			allPlayer[0].addCard(new Card(5));
			allPlayer[0].addCard(new Card(5));
			allPlayer[0].addCard(new Card(5));
			allPlayer[0].addCard(new Card(5));

			allPlayer[1].addCard(new Card(1));
			allPlayer[1].addCard(new Card(1));
			allPlayer[1].addCard(new Card(1));
			allPlayer[1].addCard(new Card(1));
			allPlayer[1].addCard(new Card(2));
			allPlayer[1].addCard(new Card(2));
			allPlayer[1].addCard(new Card(3));
			allPlayer[1].addCard(new Card(3));
			allPlayer[1].addCard(new Card(3));
			allPlayer[1].addCard(new Card(3));
			allPlayer[1].addCard(new Card(4));
			allPlayer[1].addCard(new Card(4));
			allPlayer[1].addCard(new Card(5));
			allPlayer[1].addCard(new Card(5));
		}
		else
		{
			allPlayer[0].addCard(new Card(1));
			allPlayer[0].addCard(new Card(1));
			allPlayer[0].addCard(new Card(1));
			allPlayer[0].addCard(new Card(1));
			allPlayer[0].addCard(new Card(2));
			allPlayer[0].addCard(new Card(2));
			allPlayer[0].addCard(new Card(2));
			allPlayer[0].addCard(new Card(2));
			allPlayer[0].addCard(new Card(3));
			allPlayer[0].addCard(new Card(3));
			allPlayer[0].addCard(new Card(5));
			allPlayer[0].addCard(new Card(5));
			allPlayer[0].addCard(new Card(5));
			allPlayer[0].addCard(new Card(5));

			allPlayer[1].addCard(new Card(1));
			allPlayer[1].addCard(new Card(1));
			allPlayer[1].addCard(new Card(2));
			allPlayer[1].addCard(new Card(2));
			allPlayer[1].addCard(new Card(3));
			allPlayer[1].addCard(new Card(3));
			allPlayer[1].addCard(new Card(3));
			allPlayer[1].addCard(new Card(4));
			allPlayer[1].addCard(new Card(4));
			allPlayer[1].addCard(new Card(4));
			allPlayer[1].addCard(new Card(4));
			allPlayer[1].addCard(new Card(4));
			allPlayer[1].addCard(new Card(5));
			allPlayer[1].addCard(new Card(5));
		}
		for(int i=0;i<2;i++)
		{
			allPlayer[i].sortingHand();
		}
	}
	public void mainGameTest()
	{
		int o=0;
		int min;
		while(o!=nbPlayer)
		{
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.println("Round "+o);
			System.out.println("");
			System.out.println("");
			min=999999;
			Round();
			for(int i=0;i<nbPlayer;i++)
			{
				if(min>allPlayer[i].getTurnDeath())
				{
					min=allPlayer[i].getTurnDeath();
				}
			}
			for(int i=0;i<nbPlayer;i++)
			{
				if(min==allPlayer[i].getTurnDeath())
				{
					lastFight=i;
				}
			}
			reset();
			o++;
			distributionTest(o);
		}
		End();
	}
	/**
	*  * Initialize data of the game with the information given by the user
	* @author Remy BARRIOL
	*/
	public void initialization()
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Number of players : ");
   		nbPlayer = sc.nextInt();
		while((6<nbPlayer)||(nbPlayer<2))
		{
			System.out.println("The number of players cannot be more than 6 or negative : ");
			nbPlayer = sc.nextInt();
		}
		System.out.println("Number of AI : ");
		nbAI = sc.nextInt();
		while((nbAI>nbPlayer)||(nbAI<0))
		{
			System.out.println("The number of AI cannot be more than the number of players or negative : ");
			nbAI = sc.nextInt();
		}

		allPlayer= new Player[nbPlayer];
		int u=0;
		for(int i=0;i<nbAI;i++)
		{
			allPlayer[i]=new AI(nbPlayer,i+1);
			u=i;
		}
		if(nbAI==0)
		{
			u=-1;
		}
		for(int i=u+1;i<nbPlayer;i++)
		{
			allPlayer[i]=new Player(nbPlayer,i+1);
		}
		B.createTable(nbPlayer);
		mixPlayer();
	}
		/**
	*  * It distribute cards between players
	* @author Remy BARRIOL
	*/
	public void distribution()
	{
		Random rand = new Random();
		int max=35;
		int min=0;
		int[] Super_Ultimate_Drawer={1,1,1,1,1,1,1,2,2,2,2,2,2,2,3,3,3,3,3,3,3,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5};
		int o=0;
		int x;
		while(o!=allPlayer[0].getMaxCard())
		{
			for(int k=0;k<nbPlayer;k++)
			{
				x=rand.nextInt(max - min + 1) + min;
				max--;
				allPlayer[k].addCard(new Card(Super_Ultimate_Drawer[x]));
				Super_Ultimate_Drawer=numberAdvance(Super_Ultimate_Drawer, x);
			}
			o++;
		}
		if(nbPlayer==5)
		{
			B.changeDis(new Card(Super_Ultimate_Drawer[0]));
		}
		for(int i=0;i<allPlayer.length;i++)
		{
			allPlayer[i].sortingHand();
		}
	}
	/**
	*  * Used in distribution(), takes the chosen integer at the end of the table
	* @param C The table which will be modified
	* @param i The position of the integer wich has been take in distribution()
	* @return the modified table
	* @author Remy BARRIOL
	*/
	public int[] numberAdvance(int []C,int i)
	{
		int t;
		C[i]=0;
		for(int j=i;j<C.length-1;j++)
		{
			t=C[j];
			C[j]=C[j+1];
			C[j+1]=t;
		}
		return C;
	}
		/**
	*  * reset information that have to be reset beatween turns
	* @author Remy BARRIOL
	*/
	public void reset()
	{
		for(int i=0;i<allPlayer.length;i++)
		{
			allPlayer[i].changeNbCard(0);
			allPlayer[i].changeTurnDeath(0);
			allPlayer[i].resetDefeat();
		}
		nbSlain=0;
		turn=0;
		B.createTable(nbPlayer);
	}
	/**
	*  * Manages the turn of a player
	* @param j It's the position of the player who have to play in allPlayer
	* @author Remy BARRIOL
	*/
	public void Turn(int j)
	{
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("It's your turn "+allPlayer[j].getName());
		Vector K=new Vector();
		K=allPlayer[j].possibilityPlayer(B);
		int a=(int)K.get(0);
		int b=(int)K.get(1);
		int c=(int)K.get(2);
		if(c==5)
		{
			allPlayer[j].increaseGreen();
		}
		if(c==0)
		{
			allPlayer[j].doDefeat();
			nbSlain++;
			allPlayer[j].changeTurnDeath(turn);
		}
		else
		{
			allPlayer[j].removeCard(c);
			B.changeColorBoard(a,b,c);
			B.removeX();
		}
	}
	/**
	*  * Manages the round,launches turns
	* @author Remy BARRIOL
	*/
	public void Round()
	{
		//int t=0;
		int r=0;
		while(nbPlayer!=nbSlain)
		{
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.println("Turn "+turn);
			for(int i=lastFight;i<nbPlayer;i++)
			{
				if(!allPlayer[i].getDefeat())
				{
					r++;
					Turn(i);
					B.toDisplay();
				}
			}
			turn++;
			if(lastFight!=0)
			{
				turn--;
			}
			lastFight=0;
		}
		for(int y=0;y<nbPlayer;y++)
		{
			allPlayer[y].incScore(allPlayer[y].getNbCard());
		}
		lastFighter();
	}
	/**
	*  * Manages the party,launches the rounds
	* @author Remy BARRIOL
	*/
	public void mainGame()
	{
		int o=0;
		int min;
		while(o!=nbPlayer)
		{
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.println("Round "+o);
			System.out.println("");
			System.out.println("");
			min=999999;
			Round();
			for(int i=0;i<nbPlayer;i++)
			{
				if(min>allPlayer[i].getTurnDeath())
				{
					min=allPlayer[i].getTurnDeath();
				}
			}
			for(int i=0;i<nbPlayer;i++)
			{
				if(min==allPlayer[i].getTurnDeath())
				{
					lastFight=i;
				}
			}
			reset();
			distribution();
			o++;
		}
		End();
	}
	/**
	*  * Make the ranking of the party with the information of players
	* @author Remy BARRIOL
	*/
	@SuppressWarnings("unchecked")
	public void End()
	{
		int PlayerManage=0;
		Vector Ranking=new Vector();
		int minScore;
		int minGeneralScore=-1;
		while(PlayerManage<nbPlayer)
		{
			minScore=9999;
			for(int i=0;i<nbPlayer;i++)
			{
				if((minScore>allPlayer[i].getScore())&&(minGeneralScore<allPlayer[i].getScore()))
				{
					minScore=allPlayer[i].getScore();
				}
			}
			minGeneralScore=minScore;
			Vector Transition1=new Vector();
			for(int i=0;i<nbPlayer;i++)
			{
				if(minScore==allPlayer[i].getScore())
				{
					Transition1.add(allPlayer[i]);
				}
			}
			if(Transition1.size()==1)
			{
				Player First[]=new Player[1];
				First[0]=(Player)Transition1.get(0);
				Transition1.removeAllElements();
				Ranking.add(First);
				PlayerManage++;
			}
			else
			{
				while(Transition1.size()>0)
				{
					Player TransitionRound[]=new Player[Transition1.size()];
					for(int i=0;i<Transition1.size();i++)
					{
						TransitionRound[i]=(Player)Transition1.get(i);
					}
					int max=-1;
					for(int i=0;i<TransitionRound.length;i++)
					{
						if(max<TransitionRound[i].getScoreRound())
						{
							max=TransitionRound[i].getScoreRound();
						}
					}
					Vector Transition2=new Vector();
					for(int i=TransitionRound.length-1;i>=0;i--)
					{
						if(max==TransitionRound[i].getScoreRound())
						{
							Transition1.remove(i);
							Transition2.add(TransitionRound[i]);
						}
					}
					if(Transition2.size()==1)
					{
						Player First[]=new Player[1];
						First[0]=(Player)Transition2.get(0);
						Transition2.removeAllElements();
						Ranking.add(First);
						PlayerManage++;
					}
					else
					{
						while(Transition2.size()>0)
						{
							Player TransitionGreen[]=new Player[Transition2.size()];
							for(int i=0;i<Transition2.size();i++)
							{
								TransitionGreen[i]=(Player)Transition2.get(i);
							}
							int minG=9999;
							for(int i=0;i<TransitionGreen.length;i++)
							{
								if(minG>TransitionGreen[i].getGreen())
								{
									minG=TransitionGreen[i].getGreen();
								}
							}
							Vector Transition3=new Vector();
							for(int i=TransitionGreen.length-1;i>=0;i--)
							{
								if(minG==TransitionGreen[i].getGreen())
								{
									Transition3.add(TransitionGreen[i]);
									Transition2.remove(i);
								}
							}
							if(Transition3.size()==1)
							{
								Player First[]=new Player[1];
								First[0]=(Player)Transition3.get(0);
								Transition3.removeAllElements();
								Ranking.add(First);
								PlayerManage++;
							}
							else
							{
								Player First[]=new Player[Transition3.size()];
								for(int i=0;i<First.length;i++)
								{
									First[i]=(Player)Transition3.get(i);
								}
								PlayerManage=PlayerManage+Transition3.size();
								Transition3.removeAllElements();
								Transition2.removeAllElements();
								Transition1.removeAllElements();
								Ranking.add(First);
							}
						}
					}
				}
			}
		}
		if(nbPlayer==PlayerManage)
		{
			Player[][] P=new Player[Ranking.size()][];
			for(int i=0;i<Ranking.size();i++)
			{
				P[i]=(Player[])Ranking.get(i);
			}
			System.out.println("");
			System.out.println("");
			System.out.println("Ranking :");
			System.out.println("");
			for(int i=0;i<P.length;i++)
			{
				System.out.print("	" + (i+1) + "-	");
				for(int j=0;j<P[i].length;j++)
				{
					if((P[i].length>1)&&(j>0))
					{
						System.out.print("		");
					}
					System.out.println("	   "+P[i][j].getName());
					System.out.println("	 	  score : "+P[i][j].getScore());
					System.out.println("	 	  Nb of rounds win : "+P[i][j].getScoreRound());
					System.out.println("	 	  Nb of green cards used : "+P[i][j].getGreen());
					System.out.println("");
				}
			}
			
		}
		else
		{
			System.err.println("Some players aren't in the Ranking !");
		}
	}
		/**
	*  * Set players, who have be defeated a the last turns, winners of the rounds
	* @author Remy BARRIOL
	*/
	public void lastFighter()
	{
		int max=0;
		for(int i=0;i<nbPlayer;i++)
		{
			if(max<allPlayer[i].getTurnDeath())
			{
				max=allPlayer[i].getTurnDeath();
			}
		}
		for(int i=0;i<nbPlayer;i++)
		{
			if(max==allPlayer[i].getTurnDeath())
			{
				allPlayer[i].incScoreRound();
			}
		}
	}
}
/**
 * Class of the player,which give the possibility of play in a turn 
 * @author Remy BARRIOL
 */
class Player
{
	protected String name;
	protected Card[] hand;
	protected boolean defeated=false;
	protected int turnOfDefeat=0;
	protected int nbCard=0;
	protected int nbMaxCard=0;
	protected int nbGreenCards = 0;
	protected int score = 0;
	protected int scoreRound = 0;	
	/**
	*  * Constructor of a Player
	* @param nbPlayer The number of player to determine the number of card in his hand
	* @param number The number is to determine his name and make a difference between him and the others
	* @author Remy BARRIOL
	*/
	Player(int nbPlayer,int number)
	{
		int hhh=0;
		if(nbPlayer==2)
		{
			hhh=14;
		}
		else if(nbPlayer==3)
		{
			hhh=12;
		}
		else if(nbPlayer==4)
		{
			hhh=9;
		}
		else if(nbPlayer==5)
		{
			hhh=7;
		}
		else if(nbPlayer==6)
		{
			hhh=6;
		}
		else
		{
			System.err.println("The number of players is incorrect");
		}
		nbMaxCard=hhh;
		hand=new Card[hhh];
		for(int i=0;i<hhh;i++)
		{
			hand[i]=new Card();
		}
		name="Player "+number;
	}
	/**
	*  * Display the hand of the player
	* @author Remy BARRIOL
	*/
	public void displayHand()
	{
		System.out.println("Your hand, master : ");
		System.out.println();
		System.out.print("[ ");
		for(int i=0;i<nbCard-1;i++)
		{
			hand[i].displayCard();
			System.out.print("| ");
		}
		hand[nbCard-1].displayCard();
		System.out.println(" ]");
	}
	/**
	*  * Add a card in the hand of the player
	* @param NCard The card that we want to add
	* @author Remy BARRIOL
	*/
	public void addCard(Card NCard)
	{
		hand[nbCard]=NCard;
		nbCard++;
	}
	/**
	*  * Return the number of card the player have
	* @author Remy BARRIOL
	*/
	public int getNbCard()
	{
		return nbCard;
	}
	/**
	*  * Remove a card of a certain color in the hand of the player
	* @param C The color of the card to remove
	* @author Remy BARRIOL
	*/
	public void removeCard(int C)
	{
		int b=0;
		
		for(int i=0;i<nbCard;i++)
		{
			if(hand[i].getColor()==C)
			{
				b=i;
				i=nbCard+1;
			}
		}
		hand[b].changeColor(0);
		hand=cardAdvance(hand,b);
		nbCard--;
	}
	/**
	*  * Used in removeCard(), takes the chosen cards at the end of the table
	* @param C The table which will be modified
	* @param i The position of the Card wich has been take in removeCard()
	* @return the modified table
	* @author Remy BARRIOL
	*/
	public Card[] cardAdvance(Card []C,int i)
	{
		int t;
		for(int j=i;j<C.length-1;j++)
		{
			t=C[j].getColor();
			C[j].changeColor(C[j+1].getColor());
			C[j+1].changeColor(t);
		}
		return C;
	}
	/**
	*  * Returns the maximum number of cards the Player can have
	* @author Remy BARRIOL
	*/
	public int getMaxCard()
	{
		return nbMaxCard;
	}
	/**
	*  * Sorts the hand of the player (groups the cards by color)
	* @author Remy BARRIOL
	*/
	public void sortingHand()
	{
		Card x;
		int j;
		for(int i=1;i<hand.length;i++)
		{
			x=hand[i];
			j=i;
			while((j>0)&&(hand[j-1].getColor()>x.getColor()))
			{
				hand[j]=hand[j-1];
				j--;
			}
			hand[j]=x;
		}
	}
	/**
	*  * Change the number of cards the player have
	* @param i The number of cards that we want
	* @author Remy BARRIOL
	*/
	public void changeNbCard(int i)
	{
		nbCard=i;
	}
	/**
	*  * Shows all possibility of actions the player can do at his turn and let him the choice
	* @param T The actual Board
	* @author Remy BARRIOL
	*/
	@SuppressWarnings("unchecked")
	public Vector possibilityPlayer(Board T)
	{
		Vector Poss=new Vector<int[]>();
		Vector Poss_Color=new Vector<int[]>();
		if(nbCard!=0)
		{
			boolean bColor[]=new boolean[5];
			bColor=booleanColor();
			for(int y=0;y<T.length(0);y++)
			{
				if(T.getColorBoard(0,y)==0)
				{
					T.changeColorBoard(0,y,6);
					int[] g1={0,y};
					Poss.add(g1);
					for(int o=0;o<5;o++)
					{
						if(bColor[o])
						{
							int[] g2={Poss.size()-1,o+1};
							Poss_Color.add(g2);
						}
					}
				}
			}
			for(int x=1;x<T.length();x++)
			{
				for(int y=0;y<T.length(x);y++)
				{
					if((T.getColorBoard(x,y)==0)&&((T.getColorBoard(x-1,y)!=0)&&(T.getColorBoard(x-1,y)!=6))&&((T.getColorBoard(x-1,y+1)!=0)&&(T.getColorBoard(x-1,y+1)!=6)))
					{
						if((bColor[T.getColorBoard(x-1,y)-1])||(bColor[T.getColorBoard(x-1,y+1)-1]))
						{
							if(T.getColorBoard(x-1,y)==T.getColorBoard(x-1,y+1))
							{
								int[] G1={x,y};
								Poss.add(G1);
								int[] G2={Poss.size()-1,T.getColorBoard(x-1,y)};
								Poss_Color.add(G2);
								T.changeColorBoard(x,y,6);
							}
							else
							{
								int[] G1={x,y};
								Poss.add(G1);
								if(bColor[T.getColorBoard(x-1,y)-1])
								{
									int[] G2={Poss.size()-1,T.getColorBoard(x-1,y)};
									Poss_Color.add(G2);
								}
								if(bColor[T.getColorBoard(x-1,y+1)-1])
								{
									int[] G3={Poss.size()-1,T.getColorBoard(x-1,y+1)};
									Poss_Color.add(G3);
								}
								T.changeColorBoard(x,y,6);
							}
						}
					}
				}
			}
			if(Poss.size()==0)
			{
				Poss.add(0);
				Poss.add(0);
				Poss.add(0);
				return Poss;
			}
			T.toDisplay();
			System.out.println("");
			displayHand();
			displayPoss(Poss);
			int choice=theChosenOne(Poss.size());
			int[] hhh=(int[])Poss.get(choice);
			int x1=hhh[0];
			int y1=hhh[1];
			System.out.println("");
			Poss.removeAllElements();
			for(int i=0;i<Poss_Color.size();i++)
			{
				hhh=(int[])Poss_Color.get(i);
				if(hhh[0]==choice)
				{
					Poss.add(hhh[1]);
				}
			}
			displayPossColor(Poss);
			choice=theChosenOne(Poss.size());
			int col=(int)Poss.get(choice);
			Poss.removeAllElements();
			Poss.add(x1);
			Poss.add(y1);
			Poss.add(col);
		}
		else
		{
			Poss.add(0);
			Poss.add(0);
			Poss.add(0);
		}
		return Poss;
	}
	/**
	*  * Return a boolean table wich indicate the color that the player own in his hand
	* @author Remy BARRIOL
	*/
	public boolean[] booleanColor()
	{
		boolean[] bColor={false,false,false,false,false};
		for(int i=0;i<hand.length;i++)
		{
			if((hand[i].getColor()>=1)&&(hand[i].getColor()<6))
			{
				bColor[hand[i].getColor()-1]=true;
			}
		}
		return bColor;
	}
	/**
	*  * Display all possibility of action in the board
	* @param V Vector wich contain all possibility of color
	* @author Remy BARRIOL
	*/
	public void displayPoss(Vector<int[]> V)
	{
		System.out.println("");
		System.out.println("Possibility :");
		for(int i=0;i<V.size();i++)
		{
			int[] h=V.get(i);
			if(i%2==0)
			{
				System.out.print("	" + i + " - [" + h[0] + "," + h[1] + "]");
			}
			else
			{
				System.out.println("	" + i + " - [" + h[0] + "," + h[1] + "]");
			}
		}
		System.out.println("");
	}
	/**
	*  * Display all possibility of color of cards 
	* @param V Vector wich contain all possibility of color
	* @author Remy BARRIOL
	*/
	public void displayPossColor(Vector<Integer> V)
	{
		System.out.println("");
		System.out.println("Possibility :");
		for(int i=0;i<V.size();i++)
		{
			int h=V.get(i);
			if(i%2==0)
			{
				System.out.print("	" + i + " - [ ");
				displayColor(h);
				System.out.print("]");
			}
			else
			{
				System.out.print("	" + i + " - [ ");
				displayColor(h);
				System.out.println("]");
			}
		}
		System.out.println("");
	}
	/**
	*  * Get by input the index of a answer
	* @param i The maximum integer of an answer
	* @author Remy BARRIOL
	*/
	public int theChosenOne(int i)
	{
		int poss;
		Scanner sc = new Scanner(System.in);
		System.out.println("Choose a possibility : ");
   		poss = sc.nextInt();
		while((0>poss)||(poss>=i))
		{
			System.out.println("This possibility doesn't exist : ");
			poss = sc.nextInt();
			
		}
		return poss;
	}
	/**
	*  * Display the color of a cards
	* @param i The integer associated to the card that we want to display
	* @author Remy BARRIOL
	*/
	public void displayColor(int i)
	{
			if(i==1)
			{
				System.out.print("\u001B[31mR\u001B[0m ");
			}
			else if(i==2)
			{
				System.out.print("\u001B[35mP\u001B[0m ");
			}
			else if(i==3)
			{
				System.out.print("\u001B[34mB\u001B[0m ");
			}
			else if(i==4)
			{
				System.out.print("\u001B[33mY\u001B[0m ");
			}
			else if(i==5)
			{
				System.out.print("\u001B[32mG\u001B[0m ");
			}
	}
	/**
	*  * Returns the name of the player
	* @return The name of the player
	* @author Remy BARRIOL
	*/
	public String getName()
	{
		return name;
	}
	/**
	*  * Increases by one the number of green card played
	* @author Remy BARRIOL
	*/
	public void increaseGreen()
	{
		nbGreenCards++;
	}
	/**
	*  * Returns the number of green card the player have played
	* @return Returns the number of green card played
	* @author Remy BARRIOL
	*/
	public int getGreen()
	{
		return nbGreenCards;
	}
	/**
	*  * Increases the player score
	* @param i The integer that we want to sum with the score
	* @author Remy BARRIOL
	*/
	public void incScore(int i)
	{
		score=score+i;
	}
	/**
	*  * Increase the number of winned rounds
	* @author Remy BARRIOL
	*/
	public void incScoreRound()
	{
		scoreRound++;
	}
	/**
	*  * Set the the turn of the defeat of the player
	* @param i The turn of his defeat
	* @author Remy BARRIOL
	*/
	public void changeTurnDeath(int i)
	{
		turnOfDefeat=i;
	}
	/**
	*  * Return the turn when the player was defeated
	* @return Return the turn of his defeat
	* @author Remy BARRIOL
	*/
	public int getTurnDeath()
	{
		return turnOfDefeat;
	}
	/**
	*  * Return the state of the player (defeated or not)
	* @return Return true if he is defeated, false if not
	* @author Remy BARRIOL
	*/
	public boolean getDefeat()
	{
		return defeated;
	}
	/**
	*  * Set the player like a not defeated player
	* @author Remy BARRIOL
	*/
	public void resetDefeat()
	{
		defeated=false;
	}
	/**
	*  * Set the player like a defeated player
	* @author Remy BARRIOL
	*/
	public void doDefeat()
	{
		System.out.println("You have be slain, sorry "+getName());
		defeated=true;
	}
	/**
	*  * Returns the score of the player
	* @author Remy BARRIOL
	*/
	public int getScore()
	{
		return score;
	}
	/**
	*  * Returns the number of winned rounds
	* @author Remy BARRIOL
	*/
	public int getScoreRound()
	{
		return scoreRound;
	}

}
/**
 * Class of the AI 
 * @author Remy BARRIOL
 */
class AI extends Player
{
	protected int difficulty;
	/**
	*  * Constructor of the AI
	* @author Remy BARRIOL
	*/
	AI(int nbPlayer,int number)
	{
		super(nbPlayer,number);
		name="AI "+number;
	}
	/**
	*  * Set the action of the AI (It will choose every time the first choice it can do)
	* @param T The actual Board
	* @author Remy BARRIOL
	*/
	@SuppressWarnings("unchecked")
	public Vector possibilityPlayer(Board T)
	{
		Vector Poss=new Vector<int[]>();
		Vector Poss_Color=new Vector<int[]>();
		if(nbCard!=0)
		{
			boolean bColor[]=new boolean[5];
			bColor=booleanColor();
			for(int y=0;y<T.length(0);y++)
			{
				if(T.getColorBoard(0,y)==0)
				{
					T.changeColorBoard(0,y,6);
					int[] g1={0,y};
					Poss.add(g1);
					for(int o=0;o<5;o++)
					{
						if(bColor[o])
						{
							int[] g2={Poss.size()-1,o+1};
							Poss_Color.add(g2);
						}
					}
				}
			}
			for(int x=1;x<T.length();x++)
			{
				for(int y=0;y<T.length(x);y++)
				{
					if((T.getColorBoard(x,y)==0)&&((T.getColorBoard(x-1,y)!=0)&&(T.getColorBoard(x-1,y)!=6))&&((T.getColorBoard(x-1,y+1)!=0)&&(T.getColorBoard(x-1,y+1)!=6)))
					{
						if((bColor[T.getColorBoard(x-1,y)-1])||(bColor[T.getColorBoard(x-1,y+1)-1]))
						{
							if(T.getColorBoard(x-1,y)==T.getColorBoard(x-1,y+1))
							{
								int[] G1={x,y};
								Poss.add(G1);
								int[] G2={Poss.size()-1,T.getColorBoard(x-1,y)};
								Poss_Color.add(G2);
								T.changeColorBoard(x,y,6);
							}
							else
							{
								int[] G1={x,y};
								Poss.add(G1);
								if(bColor[T.getColorBoard(x-1,y)-1])
								{
									int[] G2={Poss.size()-1,T.getColorBoard(x-1,y)};
									Poss_Color.add(G2);
								}
								if(bColor[T.getColorBoard(x-1,y+1)-1])
								{
									int[] G3={Poss.size()-1,T.getColorBoard(x-1,y+1)};
									Poss_Color.add(G3);
								}
								T.changeColorBoard(x,y,6);
							}
						}
					}
				}
			}
			if(Poss.size()==0)
			{
				Poss.add(0);
				Poss.add(0);
				Poss.add(0);
				return Poss;
			}
			//displayHand();
			int choice=0;
			int[] hhh=(int[])Poss.get(choice);
			int x1=hhh[0];
			int y1=hhh[1];
			Poss.removeAllElements();
			for(int i=0;i<Poss_Color.size();i++)
			{
				hhh=(int[])Poss_Color.get(i);
				if(hhh[0]==choice)
				{
					Poss.add(hhh[1]);
				}
			} 
			choice=0;
			int col=(int)Poss.get(choice);
			Poss.removeAllElements();
			Poss.add(x1);
			Poss.add(y1);
			Poss.add(col);
		}
		else
		{
			Poss.add(0);
			Poss.add(0);
			Poss.add(0);
		}
		return Poss;
	}		
}
/**
 * Class of the Board of the game, which manage all the modification on its
 * @author Remy BARRIOL
 */
class Board {
	private Card[][] Board_T;
	private Card discarded;

	public int getDis()
	{
		if(discarded != null)
		{
			return discarded.getColor();
		}
		else
		{
			return 9;
		}

	}
	/**
	*  * Change the discarded card(It is the card unused in the case of five players)
	* @param Dis The future discarded card
	* @author Remy BARRIOL
	*/
	public void changeDis(Card Dis)
	{
		discarded=Dis;
	}
	/**
	*  * Create the Board in function of the number of players
	* @param nbPlayer The number of players
	* @author Remy BARRIOL
	*/
	public void createTable(int nbPlayer)
	{
		if(nbPlayer==2)
		{
			Board_T=new Card[7][];
			int g=7;
			for(int i=0;i<7;i++,g--)
			{
				Board_T[i]=new Card[g];
				for(int j=g-1;j!=-1;j--)
				{
					Board_T[i][j]=new Card();
				}
			}
		}
		else if((nbPlayer>=3)&&(nbPlayer<=6))
		{
			Board_T=new Card[8][];
			int g=8;
			for(int i=0;i<8;i++,g--)
			{
				Board_T[i]=new Card[g];
				for(int j=g-1;j!=-1;j--)
				{
					Board_T[i][j]=new Card();
				}
			}
		}
		else
		{
			System.err.println("The number of players is incorrect");
		}
	}
	/**
	*  * Display the Board
	* @author Remy BARRIOL
	*/
	public void toDisplay()
	{
		if(discarded!=null)
		{
			if(discarded.getColor()!=9)
			{
				System.out.println("");
				System.out.print("discarded : [ ");
				discarded.displayCard(); 
				System.out.println("]");
				System.out.println("");
			}
		}
		System.out.println("");
		int c=0;
		for(int i=Board_T.length-1;i!=-1;i--)
		{
			System.out.print(i + " ");
			for(int k=0;k<i;k++)
			{
				System.out.print(" ");
			}
			for(int j=0;j<Board_T[i].length;j++)
			{
				Board_T[i][j].displayCard();
			}
			System.out.println("");
		}
	}
	/**
	*  * Change the color of a card int the board (an unused card place is in fact an uncolored card )
	* @param x One of the coordinate of the targeted card (left to right)
	* @param y One of the coordinate of the targeted card (Bottom to Top)
	* @param i The new color of the targeted card
	* @author Remy BARRIOL
	*/
	public void changeColorBoard(int x,int y,int i)
	{
		Board_T[x][y].changeColor(i);
	}
	/**
	*  * Return the length of a line of the Board
	* @param i The index of the targeted line 
	* @return The length of the targeted line
	* @author Remy BARRIOL
	*/
	public int length(int i)
	{
		return Board_T[i].length;
	}
	/**
	*  * Return the number of line in the Board
	* @return The number line
	* @author Remy BARRIOL
	*/
	public int length()
	{
		return Board_T.length;
	}
	/**
	*  * Return the color of a card in the board
	* @param x One of the coordinate of the targeted card (left to right)
	* @param y One of the coordinate of the targeted card (Bottom to Top)
	* @return The color of the targeted card
	* @author Remy BARRIOL
	*/
	public int getColorBoard(int x,int y)
	{
		return Board_T[x][y].getColor();
	}
	/**
	*  * Remove all of the "possibility card", and set them to "uncolored cards"
	* @author Remy BARRIOL
	*/
	public void removeX()
	{
		for(int x=0;x<Board_T.length;x++)
		{
			for(int y=0;y<Board_T[x].length;y++)
			{
				if(Board_T[x][y].getColor()==6)
				{
					Board_T[x][y].changeColor(0);
				}
			}
		}
	}
}
/**
 * Class of Cards of the game
 * @author Remy BARRIOL
 */
class Card {
	private int Color;
	/**
	*  * Constructor of the Card class with a determined color
	* @param mColor An integer which affect the Card at a color 
	* @author Remy BARRIOL
	*/
	Card(int mColor)
	{
		Color=mColor;
	}
	/**
	*  * Constructor of the Card class without a determined color(set to 0, "the uncolored card")
	* @author Remy BARRIOL
	*/
	Card()
	{
		Color=0;
	}
	/**
	*  * Return the color of the card
	* @return The integer which represents of the card color
	* @author Remy BARRIOL
	*/
	public int getColor()
	{
		return Color;
	}
	/**
	*  * Change the color of the card
	* @param i The integer which represent the future color of the card
	* @author Remy BARRIOL
	*/
	public void changeColor(int i)
	{
		Color=i;
	}
	/**
	*  * Display the color of the card
	*/
	public void displayCard()
	{
				if(Color==0)
				{
					System.out.print("- ");
				}
				else if(Color==1)
				{
					System.out.print("\u001B[31mR\u001B[0m ");
				}
				else if(Color==2)
				{
					System.out.print("\u001B[35mP\u001B[0m ");
				}
				else if(Color==3)
				{
					System.out.print("\u001B[34mB\u001B[0m ");
				}
				else if(Color==4)
				{
					System.out.print("\u001B[33mY\u001B[0m ");
				}
				else if(Color==5)
				{
					System.out.print("\u001B[32mG\u001B[0m ");
				}
				else if(Color==6)
				{
					System.out.print("X ");
				}
	}
}
