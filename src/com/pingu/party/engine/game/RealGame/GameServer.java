package com.pingu.party.engine.game.RealGame;
import java.util.Scanner;
import java.util.Random;
import java.util.Vector;
/*import com.pingu.party.game.Game_Console;
import com.pingu.party.game.Card;
import com.pingu.party.game.Player;
import com.pingu.party.game.Game;
import com.pingu.party.game.AI;
import com.pingu.party.game.Board;
import com.pingu.party.game.Init;*/

public class GameServer extends Game
{
	public static PlayerServer[] allPlayer;
	GameServer(int n)
	{
		allPlayer= new PlayerServer[n];
		nbPlayer=n;
		for(int i=0;i<nbPlayer;i++)
		{
			allPlayer[i]=new PlayerServer(n,i+1);
		}
		nbPlayer=n;
		
		System.out.println(allPlayer.length);
		B.createTable(n);
	}
	public PlayerServer[] getAllPlayer()
	{
		return allPlayer;
	}
	public int getDis()
	{
		return B.getDis();
	}
	public void distribution(clientThread[] t)
	{
		Random rand = new Random();
		int max=35;
		int min=0;
		int[] Super_Ultimate_Drawer={1,1,1,1,1,1,1,2,2,2,2,2,2,2,3,3,3,3,3,3,3,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5};
		int o=0;
		int x;
		String[] s=new String[allPlayer[0].getMaxCard()];
		Card P;
		while(o!=allPlayer[0].getMaxCard())
		{
			for(int k=0;k<nbPlayer;k++)
			{
				x=rand.nextInt(max - min + 1) + min;
				max--;
				P=new Card(Super_Ultimate_Drawer[x]);
				allPlayer[k].addCard(P);
				Super_Ultimate_Drawer=numberAdvance(Super_Ultimate_Drawer, x);
				if(o==0)
				{
					s[k]=Integer.toString(P.getColor());
				}
				else
				{	
					s[k]=s[k]+P.getColor();
				}
			}
			o++;
		}
		if(nbPlayer==5)
		{
			B.changeDis(new Card(Super_Ultimate_Drawer[0]));
		}
		for(int i=0;i<allPlayer.length;i++)
		{
			allPlayer[i].sortingHand();
		}
		for(int i=0;i<t.length;i++)
		{	
			if(t[i]!=null)
				t[i].send("h"+s[i]);
		}
	}
	public void reset(clientThread[] t)
	{
		for(int i=0;i<allPlayer.length;i++)
		{
			allPlayer[i].changeNbCard(0);
			allPlayer[i].changeTurnDeath(0);
			allPlayer[i].resetDefeat();
		}
		for(int i=0;i<t.length;i++)
		{
			if(t[i]!=null)
				t[i].send("r");
		}
		nbSlain=0;
		turn=0;
		B.createTable(allPlayer.length);
	}
	public void Turn(clientThread[] t,int j,String s)
	{
		Vector K=new Vector();
		int c=allPlayer[j].doAction(j,t,B,s);
		if(c==5)
		{
			allPlayer[j].increaseGreen();
		}
		if(c==0)
		{
			allPlayer[j].doDefeatS(t[j]);
			nbSlain++;
			allPlayer[j].changeTurnDeath(turn);
		}
		else
		{
			allPlayer[j].removeCard(c);
		}
	}
	public void Round(clientThread[] t)
	{
		int r=0;
		while(allPlayer.length!=nbSlain)
		{
			System.out.println("Turn "+turn);
			System.out.println("Turn "+nbSlain);
			//System.out.println(allPlayer.length);
			for(int i=lastFight;i<allPlayer.length;i++)
			{
				if((!allPlayer[i].getDefeat())&&(!allPlayer[i].getKicked()))
				{
					//Pa.toDisplayTempString("Your turn");
					r++;
					String s=t[i].listener();
					if(!(s.equals("kkk")))
						Turn(t,i,s);
					else
					{
						nbSlain++;
						allPlayer[i].kicked();
						t[i].send("k");
						t[i]=null;
					}
				}
			}
			turn++;
			if(lastFight!=0)
			{
				turn--;
			}
			lastFight=0;
		}
		for(int y=0;y<allPlayer.length;y++)
		{
			allPlayer[y].incScore(allPlayer[y].getNbCard());
		}
		lastFighter();
	}
	public void lastFighter()
	{
		int max=0;
		for(int i=0;i<allPlayer.length;i++)
		{
			if(max<allPlayer[i].getTurnDeath())
			{
				max=allPlayer[i].getTurnDeath();
			}
		}
		for(int i=0;i<allPlayer.length;i++)
		{
			if(max==allPlayer[i].getTurnDeath())
			{
				allPlayer[i].incScoreRound();
			}
		}
	}
	public void mainGame(clientThread[] t)
	{
		int o=0;
		int min;
		while(o!=nbPlayer)
		{
			min=999999;
			for(int i=0;i<allPlayer.length;i++)
			{
				if(allPlayer[i].getKicked())
				{
					nbSlain++;
				}
			}
			Round(t);
			for(int i=0;i<nbPlayer;i++)
			{
				if(min>allPlayer[i].getTurnDeath())
				{
					min=allPlayer[i].getTurnDeath();
				}
			}
			for(int i=0;i<nbPlayer;i++)
			{
				if(min==allPlayer[i].getTurnDeath())
				{
					lastFight=i;
				}
			}
			reset(t);
			distribution(t);
			o++;
		}
		End(t);
	}
	@SuppressWarnings("unchecked")
	public void End(clientThread[] t)
	{
		int PlayerManage=0;
		Vector Ranking=new Vector();
		int minScore;
		int minGeneralScore=-1;
		while(PlayerManage<allPlayer.length)
		{
			minScore=9999;
			for(int i=0;i<allPlayer.length;i++)
			{
				if((minScore>allPlayer[i].getScore())&&(minGeneralScore<allPlayer[i].getScore()))
				{
					minScore=allPlayer[i].getScore();
				}
			}
			minGeneralScore=minScore;
			Vector Transition1=new Vector();
			for(int i=0;i<nbPlayer;i++)
			{
				if(minScore==allPlayer[i].getScore())
				{
					Transition1.add(allPlayer[i]);
				}
			}
			if(Transition1.size()==1)
			{
				PlayerServer First[]=new PlayerServer[1];
				First[0]=(PlayerServer)Transition1.get(0);
				Transition1.removeAllElements();
				Ranking.add(First);
				PlayerManage++;
			}
			else
			{
				while(Transition1.size()>0)
				{
					PlayerServer TransitionRound[]=new PlayerServer[Transition1.size()];
					for(int i=0;i<Transition1.size();i++)
					{
						TransitionRound[i]=(PlayerServer)Transition1.get(i);
					}
					int max=-1;
					for(int i=0;i<TransitionRound.length;i++)
					{
						if(max<TransitionRound[i].getScoreRound())
						{
							max=TransitionRound[i].getScoreRound();
						}
					}
					Vector Transition2=new Vector();
					for(int i=TransitionRound.length-1;i>=0;i--)
					{
						if(max==TransitionRound[i].getScoreRound())
						{
							Transition1.remove(i);
							Transition2.add(TransitionRound[i]);
						}
					}
					if(Transition2.size()==1)
					{
						PlayerServer First[]=new PlayerServer[1];
						First[0]=(PlayerServer)Transition2.get(0);
						Transition2.removeAllElements();
						Ranking.add(First);
						PlayerManage++;
					}
					else
					{
						while(Transition2.size()>0)
						{
							PlayerServer TransitionGreen[]=new PlayerServer[Transition2.size()];
							for(int i=0;i<Transition2.size();i++)
							{
								TransitionGreen[i]=(PlayerServer)Transition2.get(i);
							}
							int minG=9999;
							for(int i=0;i<TransitionGreen.length;i++)
							{
								if(minG>TransitionGreen[i].getGreen())
								{
									minG=TransitionGreen[i].getGreen();
								}
							}
							Vector Transition3=new Vector();
							for(int i=TransitionGreen.length-1;i>=0;i--)
							{
								if(minG==TransitionGreen[i].getGreen())
								{
									Transition3.add(TransitionGreen[i]);
									Transition2.remove(i);
								}
							}
							if(Transition3.size()==1)
							{
								PlayerServer First[]=new PlayerServer[1];
								First[0]=(PlayerServer)Transition3.get(0);
								Transition3.removeAllElements();
								Ranking.add(First);
								PlayerManage++;
							}
							else
							{
								PlayerServer First[]=new PlayerServer[Transition3.size()];
								for(int i=0;i<First.length;i++)
								{
									First[i]=(PlayerServer)Transition3.get(i);
								}
								PlayerManage=PlayerManage+Transition3.size();
								Transition3.removeAllElements();
								Transition2.removeAllElements();
								Transition1.removeAllElements();
								Ranking.add(First);
							}
						}
					}
				}
			}
		}
		if(nbPlayer==PlayerManage)
		{
			PlayerServer[][] P=new PlayerServer[Ranking.size()][];
			for(int i=0;i<Ranking.size();i++)
			{
				P[i]=(PlayerServer[])Ranking.get(i);
			}
			System.out.println("");
			System.out.println("");
			System.out.println("Ranking :");
			System.out.println("");
			/*for(int i=0;i<t.length;i++)
			{
				if(t[i]!=null)
				{
					t[i].send("c");
					t[i].send("c");
					t[i].send("cRanking :");
					t[i].send("c");
				}
			}*/
			int no=0;
			String SuperEnd;
			for(int i=0;i<P.length;i++)
			{
				if((i!=0)&&(P[i].length>1))
					no+=P[i].length;
				/*for(int o=0;o<t.length;o++)
					{
						if(t[o]!=null)
						{
							t[o].send("c	" + (i+1+no) + "-	");
						}
					}*/
				//System.out.print("	" + (i+1+no) + "-	");
				SuperEnd="m";
				SuperEnd+=(i+1+no);
				for(int j=0;j<P[i].length;j++)
				{
					/*if((P[i].length>1)&&(j>0))
					{
						System.out.print("		");
					}*/
					//System.out.println("	   "+P[i][j].getName());
					//System.out.println("	 	  score : "+P[i][j].getScore());
					//System.out.println("	 	  Nb of rounds win : "+P[i][j].getScoreRound());
					//System.out.println("	 	  Nb of green cards used : "+P[i][j].getGreen());
				//	System.out.println("");
					
					
					
					SuperEnd+="m";
					SuperEnd+=P[i][j].getScore();
					SuperEnd+="m";
					SuperEnd+=P[i][j].getScoreRound();
					SuperEnd+="m";
					SuperEnd+=P[i][j].getGreen();
					//SuperEnd+="m";
					SuperEnd+="m"+P[i][j].getName();
					for(int o=0;o<t.length;o++)
					{
						if(t[o]!=null)
						{
							t[o].send(SuperEnd);
							try { Thread.sleep(60); } catch (Exception e) { }
							/*if((P[i].length>1)&&(j>0))
							{
								System.out.print("		");
							}
							t[o].send("c		   "+P[i][j].getName());
							t[o].send("c	 	  score : "+P[i][j].getScore());
							t[o].send("c	 	  Nb of rounds win : "+P[i][j].getScoreRound());
							t[o].send("c	 	  Nb of green cards used : "+P[i][j].getGreen());
							t[o].send("c");*/
						}
					}
				}
			}
			for(int o=0;o<t.length;o++)
					{
						if(t[o]!=null)
						{
							t[o].send("y");
						}
					}
			
		}
		else
		{
			System.err.println("Some players aren't in the Ranking !");
		}
	}
}
class PlayerServer extends Player
{
	private boolean kicked=false;
	private int turnKicked=0;
	PlayerServer(int nbPlayer,int number)
	{
		super(nbPlayer,number);
	}
	public void doDefeatS(clientThread t)
	{
		if(t!=null)
		{
			t.send("c");
			t.send("cYou have be slain, sorry "+getName());
		}
		defeated=true;
	}
	public boolean getKicked()
	{
		return kicked;
	}
	public void kicked()
	{
		kicked=true;
		//turnKicked=t;
	}
	public String addCardS(Card NCard)
	{
		if(!kicked)
		{
			hand[nbCard]=NCard;
			nbCard++;
			return "h"+NCard.getColor();
		}
		return null;
	}
	public int doAction(int i,clientThread[] t,Board T,String line)
	{
		System.out.println("WOW");
		System.out.println("WOW"+line);
		int x=Character.getNumericValue(line.charAt(1));
		int y=Character.getNumericValue(line.charAt(2));
		int color=Character.getNumericValue(line.charAt(3));
		if(color!=0)
		{
			T.changeColorBoard(x,y,color);
			for(int o=0;o<t.length;o++)
			{
				if(o!=i)
				{
					if(t[o]!=null)
						t[o].send("j"+x+y+color);
				}
			}
		}
		return color;
	}
}
/*
class AIServer extends AI
{
	AIServer(int nbPlayer,int number)
	{
		super(nbPlayer,number);
	}
	public void doDefeatS(clientThread t)
	{
		t.send("c");
		t.send("cYou have be slain, sorry "+getName());
		defeated=true;
	}
	public String addCardS(Card NCard)
	{
		hand[nbCard]=NCard;
		nbCard++;
		return "h"+NCard.getColor();
	}
	public int doAction(int i,clientThread[] t,Board T,String line)
	{
		int x=Character.getNumericValue(line.charAt(1));
		int y=Character.getNumericValue(line.charAt(2));
		int color=Character.getNumericValue(line.charAt(3));
		if(color!=0)
		{
			T.changeColorBoard(x,y,color);
			for(int o=0;o<t.length;o++)
			{
				if(o!=i)
				{
					t[o].send("j"+x+y+color);
				}
			}
		}
		return color;
	}
}*/