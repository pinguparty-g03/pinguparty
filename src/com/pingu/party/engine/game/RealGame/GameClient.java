package com.pingu.party.engine.game.RealGame;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.Vector;


public class GameClient {

    static class GameAccess
    {
        private Socket socket;
        private OutputStream outputStream;
        private int number=0;
        private Board B;
        private Player allPlayer;
        private int nbPlayer;
		private boolean AI=false;
		//pivate int difficulty=0;
        public void InitSocket(String server, int port) throws IOException 
        {
			
			Scanner sc = new Scanner(System.in);
			System.out.println("1- Player ; 2- AI :");
			int op = sc.nextInt();
			while((1!=op)&&(op!=2))
			{
				System.out.println("You have only two choice : ");
				op = sc.nextInt();
			}
			if(op==2)
				AI=true;
            socket = new Socket(server, port);
            outputStream = socket.getOutputStream();

            Thread receivingThread = new Thread() 
            {
                @Override
                public void run() 
                {
                    try 
                    {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        String line;
                        while ((line = reader.readLine()) != null)
                        {
							//System.out.println(line);
							if(line.startsWith("h"))
							{
								if(line.length()==2)
								{
									System.out.println("Ajout Carte");
									allPlayer.addCard(new Card(Character.getNumericValue(line.charAt(1))));
									
								}
								else
								{
									System.out.println("Initialisation main");
									for(int i=1;i<line.length();i++)
									{
										allPlayer.addCard(new Card(Character.getNumericValue(line.charAt(i))));
									}
								}
							}
							if(line.startsWith("j"))
							{
								if(line.length()==2)
								{
									System.out.println("Your turn");
									Turn();
								}
								else
								{
									System.out.println("Mise à jour");
									B.changeColorBoard(Character.getNumericValue(line.charAt(1)),Character.getNumericValue(line.charAt(2)),Character.getNumericValue(line.charAt(3)));
									B.toDisplay();
								}
							}
							else if(line.startsWith("k"))
							{
								//System.out.println(line.charAt(1));
								/*if(Integer.parseInt(Character.toString(line.charAt(1)))==number)
								{*/
									System.out.println("Try to close");
									close();
								//}
							}
							else if(line.startsWith("c"))
							{
								String c=new String();
								for(int i=1;i<line.length();i++)
								{
									c=c+line.charAt(i);
								}
								System.out.println(c);
							}
							else if(line.startsWith("b"))
							{	
								number=Character.getNumericValue(line.charAt(1));
								nbPlayer=Character.getNumericValue(line.charAt(2));
								if(AI)
									allPlayer=new AI(nbPlayer,number);
								else
									allPlayer=new Player(nbPlayer,number);
								B=new Board();
								B.createTable(nbPlayer);
								B.changeDis(new Card(Character.getNumericValue(line.charAt(3))));
							}
							else if(line.startsWith("r"))
							{
								reset();
							}
						}
                            
                    } 
                    catch (IOException ex) 
                    {
                    }
                }
            };
            receivingThread.start();
        }
		public void reset()
		{
			allPlayer.changeNbCard(0);
			allPlayer.changeTurnDeath(0);
			allPlayer.resetDefeat();
			B.createTable(nbPlayer);
		}
		public void Turn()
				{
					System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("It's your turn "+allPlayer.getName());
					Vector K=new Vector();
					K=allPlayer.possibilityPlayer(B);
					int a=(int)K.get(0);
					int b=(int)K.get(1);
					int c=(int)K.get(2);
					if(c==5)
					{
						allPlayer.increaseGreen();
					}
					if(c==0)
					{
						send("j990");
					}
					else
					{
						allPlayer.removeCard(c);
						B.changeColorBoard(a,b,c);
						send("j"+a+b+c);
						B.removeX();
					}
					B.toDisplay();
		}
        private static final String CRLF = "\r\n"; 
        public void send(String text)
         {
            try 
            {
                outputStream.write((text + CRLF).getBytes());
                outputStream.flush();
            } 
            catch (IOException ex) 
            {
            }
        }
        public void close() 
        {
            try 
            {
				System.out.println("Closing Thread");
                socket.close();
            } 
            catch (IOException ex)
            {
            }
        }
    }
    public static void main(String[] args) 
    {
        String server = args[0];
        int port =2222;
        GameAccess access = new GameAccess();
        try 
        {
            access.InitSocket(server,port);
        } 
        catch (IOException ex)
        {
            System.out.println("Cannot connect to " + server + ":" + port);
            ex.printStackTrace();
            System.exit(0);
        }
    }
}
