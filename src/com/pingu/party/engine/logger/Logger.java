package com.pingu.party.engine.logger;
import com.pingu.party.engine.core.GameCanvas;

public interface Logger {

  public static enum level { // TODO: move me to the logger
    DEBUG,
    INFO,
    WARNING,
    ERROR,
  }

  public void error(String message, long life);
  public void warn(String message, long life);
  public void info(String message, long life);
  public void debug(String message, long life);
  public void showAll();
}
