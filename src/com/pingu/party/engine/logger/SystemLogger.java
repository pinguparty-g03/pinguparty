package com.pingu.party.engine.logger;
import com.pingu.party.engine.core.GameCanvas;

public class SystemLogger implements Logger {
  public void error(String message, long life) {
    System.err.println(message);
  }
  public void warn(String message, long life) {
    System.out.println("[!] "+message);
  }
  public void info(String message, long life) {
    System.out.println("[i] "+message);
  }
  public void debug(String message, long life) {
    System.out.println("[>] "+message);
  }
  public void showAll() {
  }

}
