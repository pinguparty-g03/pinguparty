package com.pingu.party.engine.console;

import com.pingu.party.engine.console.base.Window;

import com.pingu.party.engine.console.base.Modules;
import com.pingu.party.engine.console.base.Toast;
import java.util.Arrays;

import com.pingu.party.engine.loader.Loader;
import com.pingu.party.engine.core.GameCanvas;
import java.util.List;
import java.util.ArrayList;

import com.pingu.party.engine.console.Buffer;

public class ConsoleHandler {
  private Loader l = null;
  private GameCanvas context = null;

  private ArrayList<Command> cmds;

  public ConsoleHandler(GameCanvas context) {
    this.cmds = new ArrayList<Command>();
    this.context = context;
    this.l = context.getLoader();
    init(context);
  }

  public void init(GameCanvas context) {
    Modules m = new Modules("MODULES", context);
    m.init(context);
    Echo e = new Echo();
    e.init(context);
    Exit ex = new Exit();
    ex.init(context);
    Debug d = new Debug(context);
    Toast t  = new Toast(context);
    Clear c = new Clear();

    Window w = new Window(context);
    //d.init(context);
    synchronized (this.cmds) {
      m.initIn(this.cmds);
      e.initIn(this.cmds);
      ex.initIn(this.cmds);
      d.initIn(this.cmds);
      t.initIn(this.cmds);
      c.initIn(this.cmds);
      w.initIn(this.cmds);
    }
  }
  public String getLoaded() {
    StringBuilder outBuff = new StringBuilder();
    outBuff.append("Loaded : [ ");
    for (String s : this.l.getLoaded().split(";")) {
      outBuff.append(s+" ");
    }
    outBuff.append("]");
    return outBuff.toString();
  }

  private String help() {
    StringBuilder outBuff = new StringBuilder();
    outBuff.append("Available Commands : [");
    for (Command c : this.cmds) {
      outBuff.append(c.getName()+" ");
    }
    outBuff.append("]");
    return outBuff.toString();
  }

  public String Handle(String raw) {
    List<String> root = new ArrayList<String>();
    root.addAll(Arrays.asList(raw.split(" ")));
    String rtrn = new String("<...>");
    if(root.isEmpty()) return help();
    if (root.get(0).equals("HELP")) return help();
    for (int i = 0;i<cmds.size() ;i++ ) {
      //rtrn = this.cmds.get(i).run(root);
      //System.out.println(root.get(0) +" -- "+this.cmds.get(i).getName());
      if(this.cmds.get(i).getName().equals(root.get(0))) {
        //System.out.println(this.cmds.get(i).getName());
        //root.remove(0);
        rtrn = this.cmds.get(i).run(root);
      }
    }
    /*
    switch (root[0]) {
      case "LOADED": rtrn=getLoaded(); break;
      default: rtrn = "Unknown command :"+root[0];


    }*/
    return rtrn;

  }

}

class Debug implements Command {
  String cmd="DEBUG";
  List<String> args=null;
  GameCanvas context = null;
  public Debug(GameCanvas context) {init(context);}
  public void init(GameCanvas context) { this.context = context;  }
  public void initIn(ArrayList<Command> cmds) { cmds.add(this); }
  public String getName() { return this.cmd; }

  public String run(List<String> args) {
    if (args.size() <= 1) return "< Debug >\n  Toggle debug view;  DIAG : toggle diagonals;  0 / 1 : toggle debug mode";
    switch (args.get(1)) {
        case "0": this.context.setDebug(true); break;
        case "FALSE": this.context.setDebug(true); break;
        case "NO": this.context.setDebug(true); break;
        case "DISABLE": this.context.setDebug(true); break;
        case "1": this.context.setDebug(false); break;
        case "TRUE": this.context.setDebug(false); break;
        case "YES": this.context.setDebug(false); break;
        case "ENABLE": this.context.setDebug(false); break;
        case "DIAG": this.context.setDebugDiag(!this.context.isDiagDebugged());
        default: return "< Debug >\n  Toggle debug view";
    }
    this.context.setDebug(!this.context.isDebugged());
    args.remove("DEBUG");
    return "Debug : "+(this.context.isDebugged()?"Enabled":"Diabled");
  }

}

class Echo implements Command {
  String cmd="ECHO";
  List<String> args=null;
  GameCanvas context = null;

  public void init(GameCanvas context) { this.context = context;  }
  public void initIn(ArrayList<Command> cmds) { cmds.add(this); }
  public String getName() { return this.cmd; }

  public String run(List<String> args) {
    if (args.size() <= 1) return "< Echo >\n  Echo some text";
    args.remove("ECHO");
    return String.join(" ", args);
  }
}
class Clear implements Command {
  String cmd="CLEAR";
  List<String> args=null;
  GameCanvas context = null;

  public void init(GameCanvas context) { this.context = context;  }
  public void initIn(ArrayList<Command> cmds) { cmds.add(this); }
  public String getName() { return this.cmd; }

  public String run(List<String> args) {
    if (args.size() > 1) return "< Clear >\n  Clear the buffer";
    return "\\c";
  }
}



class Exit implements Command {
  String cmd="EXIT";
  List<String> args=null;
  GameCanvas context = null;

  public void init(GameCanvas context) { this.context = context;  }
  public void initIn(ArrayList<Command> cmds) { cmds.add(this); }
  public String getName() { return this.cmd; }

  public String run(List<String> args) {
    this.context.stop();
    System.exit(0);
    return "Done.";
  }
}
