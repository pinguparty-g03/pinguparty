package com.pingu.party.engine.console ;

import java.util.ArrayList;

public class Buffer extends ArrayList<String> {
  private int lines;
  private final String LB = ";"; //Line Break symbol
	public Buffer() {
		super();
    this.lines = 0;
	}

  public void put(String str) {
    for (String s : str.split(";")) {
      add(s);
      this.lines++;
    }
  }

  public String[] getLasts(int sz) {
    if (sz > this.size()) sz = this.size();
    String[] rtrn = new String[sz];
    for (int i = this.lines-1;i>=this.lines-sz;i--) {
      rtrn[this.lines-(i+1)] = this.get(i);
    }
    return rtrn;
  }

  public void empty() {
    clear();
    this.lines = 0;
  }
}
