package com.pingu.party.engine.console.base;

import java.util.ArrayList;
import java.util.List;
import com.pingu.party.engine.core.GameCanvas;
import com.pingu.party.engine.console.Command;
import com.pingu.party.engine.loader.Loader;

public class Modules implements Command {
  String cmd="MODULES";
  List<String> args=null;
  GameCanvas context = null;
  private Loader l = null;
  private String hlpmsg = "< Modules Manager >;  LOADED : Print the loaded modules;  RELOAD : Reload all the modules";

  public Modules(String cmd, GameCanvas context) {
    this.cmd= cmd;
    init(context);
  }

  public void init(GameCanvas context) {
    this.context = context;
    this.l = context.getLoader();
    //TODO: init;
  }

  public void initIn(ArrayList<Command> cmds) {
    cmds.add(this);
    //System.out.println("INITIALISED");
  }

  public String getName() {
    return this.cmd;
  }

  public String getLoaded() {
    StringBuilder outBuff = new StringBuilder();
    outBuff.append("Loaded : [ ");
    for (String s : this.l.getLoaded().split(";")) {
      outBuff.append(s+" ");
    }
    outBuff.append("]");
    return outBuff.toString();
  }

  public String reload() {
    StringBuilder outBuff = new StringBuilder();
    try {
      this.l.reload(context);
      outBuff.append("Reload : Success !");
    } catch (Exception e) {
      outBuff.append("Reload failed : "+e.toString());
    }
    return outBuff.toString();
  }

  public String run(List<String> args) {
    //System.out.println(args.toString() + "OK");
    if (args.size() <= 1) return this.hlpmsg;
    switch (args.get(1)) {
      case "LOADED": return getLoaded();
      case "RELOAD": return reload();
      case "HELP": return this.hlpmsg;
      default: return "Unknown command : "+args.get(1);
    }
  }
}
