package com.pingu.party.engine.console.base;

import com.pingu.party.engine.game.Test.*;
import com.pingu.party.activities.*;

import javax.swing.SwingUtilities;
import com.pingu.party.engine.core.WindowMaker;
import com.pingu.party.engine.game.Test.TV;

import java.util.ArrayList;
import java.util.List;
import com.pingu.party.engine.core.GameCanvas;
import com.pingu.party.engine.console.Command;
import com.pingu.party.engine.loader.Loader;
import com.pingu.party.launcher.LaunchUI;

public class Window implements Command {
  String cmd="WINDOW";
  List<String> args=null;
  GameCanvas context = null;
  private Loader l = null;
  private String hlpmsg = "< Window Manager >;  Available : MAIN TV ANIM PERF LOCAL ONLINE PROFILE STATS OPTIONS PARTIE END JOIN PROFILELIST HELP;";

  private WindowMaker w;

  public Window(String cmd, GameCanvas context) {
    this.cmd= cmd;
    init(context);
  }
  public Window(GameCanvas context) {
    init(context);
  }

  public void init(GameCanvas context) {
    this.context = context;
    this.l = context.getLoader();
    this.w = (WindowMaker) SwingUtilities.getAncestorOfClass(WindowMaker.class, context);
    //TODO: init;
  }

  public void initIn(ArrayList<Command> cmds) {
    cmds.add(this);
    //System.out.println("INITIALISED");set
  }

  public String getName() {
    return this.cmd;
  }

  public String getLoaded() {
    StringBuilder outBuff = new StringBuilder();
    outBuff.append("Loaded : [ ");
    for (String s : this.l.getLoaded().split(";")) {
      outBuff.append(s+" ");
    }
    outBuff.append("]");
    return outBuff.toString();
  }

  public String reload() {
    StringBuilder outBuff = new StringBuilder();
    try {
      this.l.reload(context);
      outBuff.append("Reload : Success !");
    } catch (Exception e) {
      outBuff.append("Reload failed : "+e.toString());
    }
    return outBuff.toString();
  }

  private boolean set(GameCanvas gc) {
    try {
  		if(this.w == null) {
  			this.w = WindowMaker.make(gc);
  		} else {
  			this.w.set(gc);
  		}
  		return true;
  		//WindowMaker.make(new GameCanvas());
  		//WindowMaker.make(new TV());
  		//WindowMaker.make(new Pixels());
  		//WindowMaker.make(new Empty());
  		//WindowMaker.make(new Menu());
  		//WindowMaker.make(new Bouncy());
  	} catch (Exception e) {
  		return false;
  	}
  }

  public String run(List<String> args) {

    //System.out.println(args.toString() + "OK");
    if (args.size() <= 1) return this.hlpmsg;
    switch (args.get(1)) {
      case "MAIN" : return (set(new Menu()))?"Created !":"Failed";
      case "TV": return (set(new TV()))?"Created !":"Failed";
      case "ANIM": return (set(new Pixels()))?"Created !":"Failed";
      case "PERF": return (set(new Bouncy()))?"Created !":"Failed";
      case "BLANK": return (set(new Empty()))?"Created !":"Failed";
      case "LOCAL": return (set(new Local()))?"Created !":"Failed";
      case "ONLINE": return (set(new Online()))?"Created !":"Failed";
      case "PROFILE": return (set(new EditProfile()))?"Created !":"Failed";
      case "STATS": return (set(new ShowStats()))?"Created !":"Failed";
      case "SPLASH": return (set(new Splash()))?"Created !":"Failed";
      case "OPTIONS": return (set(new Options()))?"Created !":"Failed";
      case "PARTIE": return (set(new Partie()))?"Created !":"Failed";
      case "END": return (set(new End()))?"Created !":"Failed";
      case "JOIN": return (set(new Join()))?"Created !":"Failed";
      case "PROFILELIST": return (set(new ProfileList()))?"Created !":"Failed";
      case "GRAPH": return (set(new Graph()))?"Created !":"Failed";
      case "HELP": return this.hlpmsg;
      default: return "Unknown command : "+args.get(1);
    }
  }
}
