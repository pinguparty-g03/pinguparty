package com.pingu.party.engine.console.base;

import com.pingu.party.engine.logger.Logger;

import java.util.ArrayList;
import java.util.List;
import com.pingu.party.engine.core.GameCanvas;
import com.pingu.party.engine.console.Command;
import com.pingu.party.gui.toaster.Toaster;

public class Toast implements Command {
  String cmd="TOAST";
  List<String> args=null;
  GameCanvas context = null;
  private boolean usable = false;
  private String hlpmsg = "< Toast Manager >;  0-3 : Alert level;  <text> : Toast some text";

  public Toast(GameCanvas context) {
    init(context);
  }

  public void init(GameCanvas context) {
    this.context = context;
  }

  public void initIn(ArrayList<Command> cmds) {
    cmds.add(this);
  }

  public String getName() {
    return this.cmd;
  }

  private String Toast(String text, int level) {
    if(text==null ||text.isEmpty()) text = this.cmd;
    System.out.println("#"+text);
    Logger l = this.context.getLogger();
    switch (level) {
      case 0: l.debug(text, 500); break;
      case 1: l.info(text, 500); break;
      case 2: l.warn(text, 500); break;
      case 3: l.error(text, 500); break;
      default: l.info(text, 500);
    }
    return "Toasted !";
  }

  public String run(List<String> args) {
    //if(!this.usable) return "Unloaded Toaster into game Logger";
    //System.out.println(args.toString() + "OK");
    if (args.size() <= 1) return this.hlpmsg;
    switch (args.get(1)) {
      case "HELP": return this.hlpmsg;
      case "0": if(args.size()>2) {
        args.remove(0);
        args.remove(0);
        return Toast(String.join(" ", args), 0);
      }
      return Toast(this.cmd, 0);
      case "1": if(args.size()>2) {
                  args.remove(0);
                  args.remove(0);
                  return Toast(String.join(" ", args), 1);
                }
                return Toast(this.cmd, 1);
      case "2": if(args.size()>2) {
                  args.remove(0);
                  args.remove(0);
                  return Toast(String.join(" ", args), 2);
                }
                return Toast(this.cmd, 2);
      case "3": if(args.size()>2) {
                  args.remove(0);
                  args.remove(0);
                  return Toast(String.join(" ", args), 3);
                }
                return Toast(this.cmd, 3);
      default: if(args.size()>1) {
        args.remove(0);
        return Toast(String.join(" ", args), 1);
      }
      return "Unknown command : "+args.get(1);
    }
  }

}
