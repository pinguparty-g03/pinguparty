package com.pingu.party.engine.console;

import java.util.ArrayList;
import java.util.List;
import com.pingu.party.engine.core.GameCanvas;

public interface Command {
  String cmd="";
  List<String> args=null;
  GameCanvas context = null;

  public void init(GameCanvas context);

  public void initIn(ArrayList<Command> cmds);

  public String getName();

  public String run(List<String> args);

}
