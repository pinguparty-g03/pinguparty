package com.pingu.party.engine.console;

import java.awt.Font;

import com.pingu.party.engine.config.Keys;
import com.pingu.party.launcher.Utils.Font.Roboto;

import com.pingu.party.engine.input.Input;

import javax.swing.JLabel;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import com.pingu.party.engine.config.Config;

import com.pingu.party.engine.core.GameCanvas;

import com.pingu.party.engine.input.Binding;

import java.awt.Color;
import java.awt.Graphics2D;

import com.pingu.party.engine.loader.Module;
import java.util.ArrayList;

import com.pingu.party.engine.console.Buffer;

/* added, remove after  */

import com.pingu.party.engine.config.ini.Ini;
import java.io.FileInputStream;
import java.io.File;

public class Console implements Module {
  protected boolean visible = false;
  protected boolean busy = false;
  private String secret = "Console000";
  /** Mod fancy name */
  public String name;
  /** Mod unique id */
  final double id;
  /** Load order value */
  protected int LO;

  private Font consoleFont;

  /** Mod Description */
  public String desc;
  /** Mod requiements (id list) */
  public double[] requiements;
  protected Buffer buffer;
  private int renderlines;
  private int startAt;
  private final int maxlines = 25;

  protected ArrayList<String> history; // Comands history
  private int historyC;
  private ConsoleHandler handler;

  private Color bg = new Color(12, 30, 37, 128);
  private Color fg = new Color(192, 192, 192);

  private int blink = 0;

  public boolean waiting = false;

  private JLabel intext = new JLabel("");
  private StringBuilder inBuff;

  private ArrayList<Integer> KRel;
  private Input InHandler;

  private int toggleKeycode = Keys.get("KEY_CONSOLE");

  private boolean inputFocus = false;

  private GameCanvas context;

  private Ini conf; //TODO : REMOVE MEEEE

  public Console() {
    System.out.println("NEW CONSOLE");
    this.buffer = new Buffer();
    this.history = new ArrayList<>();
    this.id = 6737877;
    this.name = "Console";
    this.desc = "An embed console";

    this.KRel = new ArrayList<>();
    this.renderlines = 0;
    this.startAt = 0;

    this.consoleFont = new Roboto().get_regular(14);

    this.inBuff = new StringBuilder();

    //this.conf = new Ini();
    /*this.conf.add("Screen");
    this.conf.putIn("Screen", "WIDTH", "1440");*/

    /*try {
      this.conf.read(new FileInputStream(new File("config/Engine.ini")));
      //this.conf.read(getClass().getClassLoader().getResourceAsStream("config/Engine.ini"));
    } catch (Exception e) {
      System.out.println("Error : " + e);
    }*/
    //this.conf.print();

    //String name, String des, double id, double[] requiements
  }

  public void load(GameCanvas gc) {
    this.buffer.put("..:: Console ::..;;Console have been loaded !;Use >help to read more . . .");
    this.renderlines = 4;

    this.historyC = 0;
    this.context = gc;

    this.handler = new ConsoleHandler(gc);

    this.InHandler = gc.getInputHandler();
    this.inputFocus = this.InHandler.getInputFocus();

    // TODO: verbose log
    //this.context.log("Console loaded !", 0);
  }
  public void unload(GameCanvas gc){
    this.visible = false;
    this.blink = 0;
    this.waiting = false;

    InHandler = null;
    this.inputFocus = this.InHandler.releaseInputFocus();

    this.Unbind(gc);
    this.context.log("Console unloaded . . .", 0);
  }
  public String getName() {
    return this.name;
  }
  public int getID() {
    return 6737877; // Sorry 🤷
  }
  public void setOrder(int o) {
    this.LO = o;
  }
  public int getOrder() {
    return this.LO;
  }

  public void ConsoleToggleBind(GameCanvas gc) {
    Runnable r = (() -> {
      if(!Config.ALLOW_CONSOLE) return;
      if(this.busy) return;
      this.busy = true;
      try {
        Thread.sleep(500);
      } catch (Exception e) { gc.log(""+e.toString(), 2);}
      this.visible = !this.visible;
      if(this.visible && !this.inputFocus) this.inputFocus = this.InHandler.getInputFocus();
      if(this.visible && !this.inputFocus)  this.inputFocus = true;
      if(!this.visible && this.inputFocus) this.inputFocus = this.InHandler.releaseInputFocus();
      if(!this.visible &&!this.inputFocus)  this.inputFocus = false;

      this.busy = false;
    });
    Binding b = new Binding(this.toggleKeycode,r);
    gc.addBinding(b);
  }

  /** Testing */

  public void Unbind(GameCanvas gc) {
    gc.removeBinding(82);
    gc.removeBinding(this.toggleKeycode);
  }

  public void testBindAction(GameCanvas gc) {
    Runnable r = (() -> {
      if(this.waiting) return;
      this.waiting = true;
      gc.log(this.secret, 0);
      try {
        Thread.sleep(5000);
      } catch (Exception e) { gc.log(""+e.toString(), 0);}
      gc.log(this.secret, 0);
      this.waiting = false;
    });
    Binding b = new Binding(82,r);
    gc.addBinding(b);
  }

  /** end test */

  @Override
  public void render(GameCanvas gc) {
    if (this.visible) {
      if(!gc.isPaused()) {
        //gc.pause();
        //gc.lockInput();
      }
      /*if (!this.busy) {
        if(gc.isKeyPressed(178)) {
          if (gc.isInputLocked()) { gc.UnlockInput(); }
          this.visible = false;
        }
      }*/
      this.blink += 1;
      if (this.blink > 50) this.blink = 0;
      gc.getG2D().setFont(this.consoleFont);
      gc.getG2D().setColor(this.bg);
      gc.getG2D().fillRect(0, gc.getHeight()-(17+(this.renderlines*16)), gc.getWidth(), 17+(this.renderlines*16));
      gc.getG2D().setColor(this.fg);
      if(this.inBuff.toString().isEmpty())
        gc.getG2D().drawString(">"+" "+(this.blink>25?"_":" "), 2, gc.getHeight()-4);
      else
        gc.getG2D().drawString(">"+" "+this.inBuff.toString()+(this.blink>25?"_":" "), 2, gc.getHeight()-4);
      if (this.renderlines != 0) {
        int ln = 16;
        for (String s : this.buffer.getLasts(this.renderlines)) {
          gc.getG2D().drawString(s, 2, gc.getHeight()-ln-4);
          ln+=16;
        }
      }
    }
    else {
      if(gc.isPaused() && !this.busy) {
        try {
          Thread.sleep(500);
        } catch (Exception e) { gc.log(""+e.toString(), 2);}
      }
    }
    if (this.waiting) {
      gc.getG2D().setColor(this.fg);
      gc.getG2D().drawString("initializing . . . ", gc.getWidth()-76, gc.getHeight()-4);
    }
  }

  @Override
  public void freeze(GameCanvas gc) {
    //this.visible = false;
    this.blink = 0;
  }

  @Override
  public void init(GameCanvas gc) {
    if (Config.ALLOW_CONSOLE) {
      testBindAction(gc);
    }
    ConsoleToggleBind(gc);
  }

  @Override
  public void resume(GameCanvas gc) {
    if (Config.ALLOW_CONSOLE) {
      //stuff this.visible = true;
    }

  }

  @Override
  public void update(GameCanvas gc) {
    if(this.inputFocus) {
      if(this.visible) {
        this.KRel = this.InHandler.getReleased();
        for (int it=0; it<this.KRel.size();it++) {
          int i = this.KRel.get(it);
          if((i>='A'&&i<='Z')) {
            this.InHandler.consumeKeyRelased(i);
            this.inBuff.append((char)i);
            this.intext.setText(this.inBuff.toString());
          } else if (i>=96&&i<=105) {
            this.InHandler.consumeKeyRelased(i);
            this.inBuff.append(i-96);
            this.intext.setText(this.inBuff.toString());
          } else if (i==32) {
            this.InHandler.consumeKeyRelased(i);
            this.inBuff.append(" ");
            this.intext.setText(this.inBuff.toString());
          } else if (i==111) {
            this.InHandler.consumeKeyRelased(i);
            this.inBuff.append("/");
            this.intext.setText(this.inBuff.toString());
          } else if (i==106) {
            this.InHandler.consumeKeyRelased(i);
            this.inBuff.append("*");
            this.intext.setText(this.inBuff.toString());
          } else if (i==107) {
            this.InHandler.consumeKeyRelased(i);
            this.inBuff.append("+");
            this.intext.setText(this.inBuff.toString());
          } else if (i==109) {
            this.InHandler.consumeKeyRelased(i);
            this.inBuff.append("-");
            this.intext.setText(this.inBuff.toString());
          } else if (i==8) {
            this.InHandler.consumeKeyRelased(i);
            if(this.inBuff.length()<1) this.inBuff.setLength(0);
            if(this.inBuff.length()>=1) this.inBuff.delete(this.inBuff.length()-1, this.inBuff.length());
            this.intext.setText(this.inBuff.toString());
          } else if (i==38) { // Up key = Hystory up
            this.InHandler.consumeKeyRelased(i);

            this.inBuff.setLength(0);
            this.historyC = (this.historyC<=0)?(this.history.size()-1):this.historyC-1;
            if(this.history.size() >0) this.inBuff.append(this.history.get(this.historyC%this.history.size()));
            this.intext.setText(this.inBuff.toString());
          } else if (i==40) { // Down key = Hystory down
            this.InHandler.consumeKeyRelased(i);

            this.inBuff.setLength(0);
            if(this.history.size()>0) {
              this.historyC = (this.historyC+1)%this.history.size();
              this.inBuff.append(this.history.get(this.historyC%this.history.size()));
            }
            this.intext.setText(this.inBuff.toString());
          } else if (i==10) {
            this.InHandler.consumeKeyRelased(i);
            this.renderlines = (this.renderlines<this.maxlines)?this.renderlines+1:this.maxlines;
            exec(this.inBuff.toString());
            this.inBuff.setLength(0);
            this.intext.setText("");
          } else if(i==33) {// page up
            this.InHandler.consumeKeyRelased(i);

          }else if(i==34) {// page down
            this.InHandler.consumeKeyRelased(i);

          }
        }
      } else {
        this.InHandler.cleanReleased();
      }
    }
  }

  public final String getLine(int index) {
    return (index<=this.buffer.size())?this.buffer.get(index):"";
  }


  public final Module getModule() {
    return this;
  }

  private String postParse(String out) {
    switch(out) {
      case "\\c": this.history.clear();
                  this.historyC = 0;
                  this.renderlines = 0;
                  this.buffer.empty();
                  break;
      default: return out;

    }
    return "";
  }

  public void exec(String raw) {
    String out = postParse(this.handler.Handle(raw));
    this.history.add(raw);
    this.historyC++;
    if (out != "") {
      this.buffer.put(out);
    }
  }

}
