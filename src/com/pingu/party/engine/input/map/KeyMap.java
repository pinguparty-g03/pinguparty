package com.pingu.party.engine.input.map ;

import java.awt.Toolkit;

public interface KeyMap {
  public static final int KEY_NULL = 0x00;

  public static int KEY_FULLSCREEN = 122;
  public static int KEY_CONSOLE = 178;

  public static int KEY_ENTER = 10;
  public static int KEY_BACKSPACE = 8;
  public static int KEY_SHIFT = 16;
  public static int KEY_CTRL = 17;
  public static int KEY_ALT = 18;

  public static int KEY_SPACE = 32;
  public static int KEY_EQU = 61;
  public static int KEY_SEMI = 59;
  public static int KEY_COM = 44;

  public static int KEY_0 = 96;
  public static int KEY_1 = 97;
  public static int KEY_2 = 98;
  public static int KEY_3 = 99;
  public static int KEY_4 = 100;
  public static int KEY_5 = 101;
  public static int KEY_6 = 102;
  public static int KEY_7 = 103;
  public static int KEY_8 = 104;
  public static int KEY_9 = 105;

  public static int KEY_A = 65;
  //...
  public static int KEY_Z = 90;

  public static int KEY_STAR = 106;
  public static int KEY_ADD = 107;
  public static int KEY_SUB = 109;
  public static int KEY_DOT = 110;
  public static int KEY_DIV = 111;

  public static int KEY_AND = 150; // &
  public static int KEY_EAC = 150; // é
  public static int KEY_DQU = 152; // "
  public static int KEY_QUO = 222; // '
  public static int KEY_OBRA = 519; // (
  public static int KEY_EAD = 232; // è
  public static int KEY_UND = 523; // _
  public static int KEY_CED = 199; // ç
  public static int KEY_AAC = 224; // à

  public static int KEY_CBRA = 522; // )
  public static int KEY_COL = 513; // :
  public static int KEY_EXC = 517; // !

  public static int IntDiff = KEY_0-48;
  public static int AaDiff = 97-KEY_A;


  public static String getText(int keyCode) {
    return Toolkit.getProperty("AWT.unknown", "Unknown");
  }
}
