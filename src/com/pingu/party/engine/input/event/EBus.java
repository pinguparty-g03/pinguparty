package com.pingu.party.engine.input.event;

import java.awt.Point;
import java.util.ArrayList;

/** Event Bus */
public class EBus {
  /** Mouse EBus */
  private MouseButtonBus MBBus;
  private final MouseMotionBus MMBus;
  private final MouseScrollBus MSBus;

  /** Keyboard EBus */
  private KeyInputBus KIBus;

  /** Gamepad EBus */
  private final ArrayList<GamepadButtonEvent> GBEBus = null;
  private final ArrayList<GamepadStickEvent> GSEBus = null;

  /* Toggler */
  private boolean listenMotion;

  public EBus() {
    this.MBBus = new MouseButtonBus();
    this.MMBus = new MouseMotionBus();
    this.MSBus = new MouseScrollBus();
    this.KIBus = new KeyInputBus();

    this.listenMotion = true;
    System.out.println("Event bus created !");
  }

  public String getLog() {
    return ""+this.MBBus.genLog()+this.KIBus.genLog()+this.MMBus.genLog()+this.MSBus.genLog();
  }

  public void clearAll() {
    this.MBBus.clear();
    this.MMBus.clear();
    this.KIBus.clear();
    this.MSBus.clear();
  }

  /** Bus-specific functions */

  public void addKeyInputEvent(KeyInputEvent kie) {
    this.KIBus.add(kie);
  }
  public boolean hasKeyCode(int code) {
    return this.KIBus.hasKeyCode(code);
  }
  public boolean hasKeyCodePressed(int code) {
    return (this.KIBus.has(new KeyInputEvent(code, true)) && this.KIBus.has(new KeyInputEvent(code, false)));
  }
  public boolean hasKeyCodeReleased(int code) {
    return (this.KIBus.hasR(code));
  }

  public ArrayList<Integer> getReleased() {
    return this.KIBus.getReleased();
  }

  public void cleanReleased() {
    this.KIBus.cleanReleased();
  }

  public void consumeKeyRelased(int code) {
    this.KIBus.consumeKeyRelased(code);
  }

  public void lockMouse() {
    this.MSBus.lock();
    this.MMBus.lock();
    this.MBBus.lock();
    this.listenMotion = false;
  }

  public void UnlockMouse() {
    this.MSBus.unlock();
    this.MMBus.unlock();
    this.MBBus.unlock();
    this.listenMotion = true;
  }

  public boolean isMouseLocked() {
    return (!this.listenMotion?false:this.MSBus.getLock()&&this.MMBus.getLock()&&this.MBBus.getLock());
  }

  public Point getMouseLocation() {
    return this.MMBus.getLastXY();
  }

  public Point getLastClic() {
    return this.MBBus.getLastXY();
  }

  public int getLastScroll() {
    return this.MSBus.getLastScroll();
  }

  public boolean hasClicked() {
    return this.MBBus.isEmpty();
  }

  public void consumeClicked() {
    this.MBBus.consume();
  }

  public boolean hasScrolled() {
    return this.MSBus.isEmpty();
  }

  public void consumeScrolled() {
    this.MSBus.consume();
  }

  public boolean hasMoved() {
    return this.MMBus.isEmpty();
  }
  
  public Point getMove() {
    return this.MMBus.getLastXY();
  }

  public void consumeMoved() {
    this.MMBus.clear();
  }

  public void addMouseButtonEvent(MouseButtonEvent mbe) {
    this.MBBus.add(mbe);
  }
  public void addMouseScrollEvent(MouseScrollEvent mse) {
    this.MSBus.add(mse);
  }
  public void addMouseMotionEvent(MouseMotionEvent mme) {
    this.MMBus.add(mme);
  }
  public void addMouseMotionEventRaw(int x, int y, boolean d) {
    if (this.listenMotion) {
      this.MMBus.add(x, y, d);
    }
  }

}
