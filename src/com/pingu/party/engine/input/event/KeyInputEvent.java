package com.pingu.party.engine.input.event ;

import java.awt.event.KeyEvent;
import java.awt.Point;

public class KeyInputEvent implements IOEvent<KeyEvent> {

  private int keyCode;
  private boolean pressed;

	public KeyInputEvent(int keyCode, boolean pressed) {
    this.keyCode = keyCode;
    this.pressed = pressed;
  }

  @Override
  public String getName() {
    return "KeyInputEvent:"+this.keyCode+(this.pressed?"+":"-");
  }

  @Override
  public int getID() {
    return this.keyCode;
  }

  public boolean isPressed() {
    return this.pressed;
  }

  public int getkeyCode() {
    return this.keyCode;
  }
}
