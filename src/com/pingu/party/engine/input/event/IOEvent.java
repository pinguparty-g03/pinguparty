package com.pingu.party.engine.input.event;

/**   @param <T> create IOEvent from event */
public interface IOEvent<T> {
  /** @return unique event id */
  public int getID();
  /** @return string that may be used to search event => CLASS:id@values+
  values may be an array, separated by "," and "+" mean that the event is active whereas - mean the opposite
   */
  public String getName();
}
