package com.pingu.party.engine.input.event;

import java.awt.Point;

import java.util.ArrayList;

public class MouseMotionBus implements Bus<MouseMotionEvent> {
    private boolean locked = false;
    private int x, y;
    private boolean d;
    private ArrayList<MouseMotionEvent> EStack;

    public MouseMotionBus() {
      this.x = 0;
      this.y = 0;
      this.d = false;
      this.EStack = new ArrayList<MouseMotionEvent>();
    }

    public int getScroll() {
      int rtrn = 0;
      synchronized(this.EStack) {
        for (MouseMotionEvent e : this.EStack) {
          rtrn += e.getID();
        }
      }
      return rtrn;
    }

    public Point getLastXY() {
      return new Point(this.x, this.y);
    }

    public String genLog() {
      final StringBuilder logList = new StringBuilder();
      logList.append("["+getScroll()+"]");
      synchronized(this.EStack) {
        for (MouseMotionEvent e : this.EStack) {
          logList.append(e.getName() + ";");
        }
      }
      return logList.toString();
    }

    public boolean isEmpty() {
      synchronized(this.EStack) {
          if (this.EStack.size()>0) return true;
      }
      return false;
    }

    @Override
    public void clear() {
      if (!this.locked) {
        synchronized(this.EStack) {
            this.EStack.clear();
        }
      }
    }

    @Override
    public boolean has(MouseMotionEvent mbe) {
      synchronized(this.EStack) {
        for (MouseMotionEvent e : this.EStack) {
          if (mbe.getID() == e.getID()) return true;
        }
      }
      return false;
    }

    @Override
    public int indexOf(MouseMotionEvent mbe) {
      int rtrn = -1;
      synchronized(this.EStack) {
        rtrn = this.EStack.indexOf(mbe);
      }
      return rtrn;
    }

    @Override
    public void add(MouseMotionEvent mbe) {
      if (!this.locked) {
        synchronized(this.EStack) {
          this.EStack.add(mbe);
        }
        synchronized (mbe) {
          this.x = mbe.getX();
          this.y = mbe.getY();
          this.d = mbe.isDragged();
        }
      }
    }

    public void add(int x, int y, boolean dragged) {
      if (!this.locked) {
        synchronized(this.EStack) {
          this.EStack.add(new MouseMotionEvent(this.x, this.y, x, y, dragged));
        }
        this.x = x;
        this.y = y;
        this.d = dragged;
      }
    }


    @Override
    public void lock() {
      this.locked = true;
    }

    @Override
    public void unlock() {
      this.locked = false; // TODO: await for unlock
    }

    @Override
    public boolean getLock() {
      return this.locked;
    }

}
