package com.pingu.party.engine.input.event;

import java.awt.Point;
import java.util.ArrayList;

public class MouseButtonBus implements Bus<MouseButtonEvent> {
    private boolean locked = false;
    private ArrayList<MouseButtonEvent> EStack;
    Point last;
    private boolean gotAction = false;

    public MouseButtonBus() {
      this.EStack = new ArrayList<MouseButtonEvent>();
    }

    public String genLog() {
      final StringBuilder logList = new StringBuilder();
      synchronized(this.EStack) {
        for (MouseButtonEvent e : this.EStack) {
          logList.append(e.getName() + ";");
        }
      }
      return logList.toString();
    }

    public Point getLastXY() {
      return this.last;
    }

    @Override
    public void clear() {
      if (!this.locked) {
        synchronized(this.EStack) {
            this.EStack.clear();
        }
      }
    }

    public boolean isEmpty() {
      return this.gotAction;
    }

    public void consume() {
      synchronized(this.EStack) {
        if(this.EStack.size()>=1) this.EStack.remove(this.EStack.size()-1);
        else this.EStack.clear();
        this.gotAction = false;
      }
    }

    @Override
    public boolean has(MouseButtonEvent mbe) {
      synchronized(this.EStack) {
        for (MouseButtonEvent e : this.EStack) {
          if (mbe.getID() == e.getID()) return true;
        }
      }
      return false;
    }

    @Override
    public int indexOf(MouseButtonEvent mbe) {
      int rtrn = -1;
      synchronized(this.EStack) {
        rtrn = this.EStack.indexOf(mbe);
      }
      return rtrn;
    }

    @Override
    public void add(MouseButtonEvent mbe) {
      this.last = mbe.getXY();
      this.gotAction = true;
      if (!this.locked) {
        synchronized(this.EStack) {
          this.EStack.add(mbe);
        }
      }
    }

    @Override
    public void lock() {
      this.locked = true;
    }

    @Override
    public void unlock() {
      this.locked = false; // TODO: await for unlock
    }

    @Override
    public boolean getLock() {
      return this.locked;
    }

}
