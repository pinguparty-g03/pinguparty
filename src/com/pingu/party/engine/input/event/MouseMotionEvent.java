package com.pingu.party.engine.input.event ;

import java.awt.event.MouseEvent;
import java.awt.Point;
import com.pingu.party.engine.math.Vector2D;
import com.pingu.party.engine.config.Config;

public class MouseMotionEvent implements IOEvent<MouseEvent> {
  private boolean locked = false;

  private int x, y, dx, dy;
  protected boolean dragged;
	public MouseMotionEvent(int x, int y, int dx, int dy, boolean dragged) {
    this. x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.dragged = dragged;
  }

  /** D mean dragged, H hovered */
  @Override
  public String getName() {
    return "MouseMotionEvent:"+this.x+"-"+this.y+">"+this.dx+"-"+this.dy+(this.dragged?"D":"H");
  }

  /** ID = surface form origin */
  @Override
  public int getID() {
    return (Config.WIDTH*(this.dx-1)+this.dy);
  }

  public boolean isDragged() {
    return this.dragged;
  }

  public int getX() {
    return this.x;
  }

  public int getY() {
    return this.y;
  }

  public int getDX() {
    return this.dx;
  }

  public int getDY() {
    return this.dy;
  }

  public Vector2D getMove() {
    return new Vector2D(this.dx-this.x, this.dy-this.y);
  }

  public Point getDestXY() {
    return new Point(this.dx, this.dy);
  }

  public Point getOriginXY() {
    return new Point(this.x, this.y);
  }
}
