package com.pingu.party.engine.input.event ;

import java.awt.event.MouseWheelEvent;
import java.awt.Point;

public class MouseScrollEvent implements IOEvent<MouseWheelEvent> {
  //implements IOEvent<MouseWheelEvent> {
  private boolean locked = false;

  private int velocity;
  private boolean down;
	public MouseScrollEvent(boolean down, int vel) {
    this.down = down;
    this.velocity = vel;
  }
  public MouseScrollEvent(int wheelRotation) {
    this.down = (wheelRotation<0?true:false);
    this.velocity = Math.abs(wheelRotation);
  }

  @Override
  public String getName() {
    return "MouseScrollEvent:"+(this.down?"↑":"↓")+this.velocity;
  }

  @Override
  public int getID() {
    return (this.down?this.velocity*-1:this.velocity);
  }

  public int getVelocity() {
    return this.velocity;
  }

  public boolean isDown() {
    return this.down;
  }

  public boolean isUp() {
    return !this.down;
  }

}
