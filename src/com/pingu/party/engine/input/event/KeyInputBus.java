package com.pingu.party.engine.input.event;

import java.util.ArrayList;

public class KeyInputBus implements Bus<KeyInputEvent> {
    private boolean locked = false;
    private ArrayList<KeyInputEvent> EStack; // Stacking event, more like a swap
    private ArrayList<Integer> EActive; // Keeping track of active keys, as string value
    private ArrayList<Integer> EReleased; // Keeping track of active keys, as string value

    public KeyInputBus() {
      this.EStack = new ArrayList<KeyInputEvent>();
      this.EActive = new ArrayList<>();
      this.EReleased = new ArrayList<>();
    }

    public String genLog() {
      final StringBuilder logList = new StringBuilder();
      if (!this.locked) {
        synchronized(this.EStack) {
          for (KeyInputEvent i : this.EStack) {
            logList.append(i.getName() + ";");
          }
        }
      }
      return logList.toString();
    }

    public void Reset() {
      if (!this.locked) {
        synchronized(this.EStack) {
            this.EActive.clear();
        }
      }
    }

    @Override
    public void clear() {
      if (!this.locked) {
        synchronized(this.EStack) {
            this.EStack.clear();
        }
      }
    }

    @Override
    public boolean has(KeyInputEvent kie) {
      if (!this.locked) {
        synchronized(this.EStack) {
          for (Integer i : this.EActive) {
            if (kie.getkeyCode() == i) return true;
          }
        }
      }
      return false;
    }

    public boolean hasR(int code) {
      if (!this.locked) {
        synchronized(this.EReleased) {
          for (Integer i : this.EReleased) {
            if (code == i) return true;
          }
        }
      }
      return false;
    }

    public ArrayList<Integer> getReleased() {
      synchronized(this.EReleased) {
        return this.EReleased;
      }
    }

    public void cleanReleased() {
      if (!this.locked) {
        synchronized(this.EReleased) {
          this.EReleased.clear();
        }
      }
    }

    public void consumeKeyRelased(int code) {
      synchronized(this.EReleased) {
        if(!hasR(code)) return;
        this.EReleased.remove((Object)code);
      }
    }

    public boolean hasKeyCode(int code) {
      if (!this.locked) {
        synchronized(this.EActive) {
          for (Integer i : this.EActive) {
            if (code == i) return true;
          }
        }
      }
      return false;
    }

    @Override
    public int indexOf(KeyInputEvent kie) {
      int rtrn = -1;
      rtrn = this.EActive.indexOf(kie.getkeyCode());
      return rtrn;
    }

    @Override
    public void add(KeyInputEvent kie) {
      if (!this.locked) {
        synchronized(this.EStack) {
          this.EStack.add(kie);
        }
        if (kie.isPressed()) {
          synchronized(this.EActive) {
            if (!has(kie)) {
              this.EActive.add((Integer)kie.getkeyCode());
            }
          }
        } else {
          synchronized(this.EActive) {
            if (has(kie)) {
              this.EActive.add((Integer)kie.getkeyCode());
            }
            while(this.EActive.remove((Integer)kie.getkeyCode())) {}
          }
          synchronized(this.EReleased) {
            if(!hasR(kie.getkeyCode())) {
              this.EReleased.add(kie.getkeyCode());
            }
          }
        }
      }
    }

    @Override
    public void lock() {
      this.locked = true;
    }

    @Override
    public void unlock() {
      this.locked = false; // TODO: await for unlock
    }

    @Override
    public boolean getLock() {
      return this.locked;
    }

}
