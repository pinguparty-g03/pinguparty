package com.pingu.party.engine.input.event;

/**   @param <T> create generic bus */
public interface Bus<T> {
  /** clear serialized EBus */
  public void clear();

  /** @param event add event to serialized EBus  */
  public void add(T event);

  /** @return boolean @param event if bus has event */
  public boolean has(T event);

  /** @return int @param event return position in event stac, or -1 */
  public int indexOf(T event);

  /** ask for bus lock */
  public void lock();

  /** ask for bus unlock */
  public void unlock();

  /** @return Boolean Lock state */
  public boolean getLock();

  /**
  TODO: Ideas
  public void setRate(int rate); => set max events rate, like a Shadowing
  public int getRate();
  */
}
