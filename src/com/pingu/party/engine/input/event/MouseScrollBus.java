package com.pingu.party.engine.input.event;

import java.util.ArrayList;

public class MouseScrollBus implements Bus<MouseScrollEvent> {
    private boolean locked = false;
    private ArrayList<MouseScrollEvent> EStack;

    private boolean gotAction = false;

    public MouseScrollBus() {
      this.EStack = new ArrayList<MouseScrollEvent>();
    }

    public int getScroll() {
      int rtrn = 0;
      synchronized(this.EStack) {
        for (MouseScrollEvent e : this.EStack) {
          rtrn += e.getID();
        }
      }
      return rtrn;
    }

    public boolean isEmpty() {
      return this.gotAction;
    }

    public void consume() {
      synchronized(this.EStack) {
        if(this.EStack.size()>=1) this.EStack.remove(this.EStack.size()-1);
        else this.EStack.clear();
        this.gotAction = false;
      }
    }

    public String genLog() {
      if (this.EStack.size() == 0) return "";
      final StringBuilder logList = new StringBuilder();
      logList.append("["+getScroll()+"]");
      synchronized(this.EStack) {
        for (MouseScrollEvent e : this.EStack) {
          logList.append(e.getName() + ";");
        }
      }
      return logList.toString();
    }

    public int getLastScroll() {
      synchronized(this.EStack) {
        if(this.EStack.size()>=1) return this.EStack.get(this.EStack.size()-1).getID();
      }
      return 0;
    }

    @Override
    public void clear() {
      if (!this.locked) {
        synchronized(this.EStack) {
            this.EStack.clear();
        }
      }
    }

    @Override
    public boolean has(MouseScrollEvent mbe) {
      synchronized(this.EStack) {
        for (MouseScrollEvent e : this.EStack) {
          if (mbe.getID() == e.getID()) return true;
        }
      }
      return false;
    }

    @Override
    public int indexOf(MouseScrollEvent mbe) {
      int rtrn = -1;
      synchronized(this.EStack) {
        rtrn = this.EStack.indexOf(mbe);
      }
      return rtrn;
    }

    @Override
    public void add(MouseScrollEvent mbe) {
      if (!this.locked) {
        this.gotAction = true;
        synchronized(this.EStack) {
          this.EStack.add(mbe);
        }
      }
    }

    @Override
    public void lock() {
      this.locked = true;
    }

    @Override
    public void unlock() {
      this.locked = false; // TODO: await for unlock
    }

    @Override
    public boolean getLock() {
      return this.locked;
    }

}
