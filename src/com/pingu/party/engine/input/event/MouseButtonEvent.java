package com.pingu.party.engine.input.event ;

import java.awt.event.MouseEvent;
import java.awt.Point;

public class MouseButtonEvent implements IOEvent<MouseEvent> {

  private int x, y, id;
  protected boolean pr;
	public MouseButtonEvent(int x, int y, boolean pressed, int id) {
    this. x = x;
    this.y = y;
    this.id = id;
    this.pr = pressed;
  }

  @Override
  public String getName() {
    return "MouseButtonEvent:"+this.id+"@"+this.x+","+this.y+(this.pr?"+":"-");
  }

  @Override
  public int getID() {
    return this.id;
  }

  public int getButtonIndex() {
    return this.id;
  }

  public boolean isPressed() {
    return this.pr;
  }

  public int getX() {
    return this.x;
  }

  public int getY() {
    return this.y;
  }

  public Point getXY() {
    return new Point(this.x, this.y);
  }
}
