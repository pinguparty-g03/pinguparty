package com.pingu.party.engine.input ;

import javax.swing.SwingWorker;

public class Binding {
  private int code;
  private int priority;
  private Runnable action;

  public Binding(int code, Runnable action) {
    this.code = code;
    this.action = action;
    this.priority = Thread.NORM_PRIORITY;
  }

  public int getCode() {
    return this.code;
  }

  public Runnable action() {
    return this.action;
  }

  public void run() {
    SwingWorker<String, Object> sw = new SwingWorker<String, Object>() {
      @Override
      public String doInBackground() {
        action.run();
        return "r";
      }
    };
    Thread t1 = new Thread(sw);
    t1.setPriority(this.priority);
    t1.start();
  }

  public void runBlocking() {
    this.run();
  }
}
