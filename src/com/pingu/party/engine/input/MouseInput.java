package com.pingu.party.engine.input ;

public interface MouseInput {

  public static final int MOUSE_X = 0;
  public static final int MOUSE_Y = 1;
  public static final int MOUSE_WHEEL = 2;

  public static final int BUTTON_LEFT = 0;
  public static final int BUTTON_RIGHT = 1;
  public static final int BUTTON_MIDDLE = 2;

  public boolean isClicked();
  public boolean isClicked(int button);

  public void setCursorVisible(boolean visible);
  public boolean isCursorVisible();

  //public void setCursor(Cursor cur); // TODO: Future ?


}
