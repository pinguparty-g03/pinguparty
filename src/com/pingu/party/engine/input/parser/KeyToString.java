package com.pingu.party.engine.input.parser;

import com.pingu.party.engine.input.map.KeyMap;

public class KeyToString {

  public static char getText(int keyCode) {
    //TODO: if >A <Z
    //AaDiff
    if(keyCode>=KeyMap.KEY_A && keyCode<=KeyMap.KEY_Z) {
      return (char)(keyCode+(KeyMap.AaDiff));
    }
    if(keyCode>=KeyMap.KEY_0 && keyCode<=KeyMap.KEY_9) {
      return (char)(keyCode-(KeyMap.IntDiff));
    }
    if(keyCode == KeyMap.KEY_STAR) {
      return '*';
    } if (keyCode == KeyMap.KEY_ADD) {
      return '+';
    } if (keyCode == KeyMap.KEY_SPACE) {
      return ' ';
    } if (keyCode == KeyMap.KEY_COL) {
      return ':';
    } if (keyCode == KeyMap.KEY_SEMI) {
      return '.';
    } if (keyCode == KeyMap.KEY_COM) {
      return ',';
    } if (keyCode == KeyMap.KEY_EXC) {
      return '!';
    } if (keyCode == KeyMap.KEY_SUB) {
      return '-';
    } if (keyCode == KeyMap.KEY_DOT) {
      return '.';
    } if (keyCode == KeyMap.KEY_DIV) {
      return '/';
    } if (keyCode == KeyMap.KEY_AND) {
      return '&';
    } if (keyCode == KeyMap.KEY_EAC) {
      return 'e';
    } if (keyCode == KeyMap.KEY_DQU) {
      return '"';
    } if (keyCode == KeyMap.KEY_QUO) {
      return '\'';
    } if (keyCode == KeyMap.KEY_OBRA) {
      return '(';
    } if (keyCode == KeyMap.KEY_EAD) {
      return 'e';
    } if (keyCode == KeyMap.KEY_UND) {
      return '_';
    } if (keyCode == KeyMap.KEY_CED) {
      return 'c';
    } if (keyCode == KeyMap.KEY_AAC) {
      return 'a';
    } if (keyCode == KeyMap.KEY_CBRA) {
      return ')';
    }
    //return (char) keyCode;
    return 0;
  }
}
