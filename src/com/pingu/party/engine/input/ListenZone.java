package com.pingu.party.engine.input ;

import javax.swing.SwingWorker;

public class ListenZone {
  private int priority;
  private Runnable action;

  private boolean active = true;

  private int x, y, w, h;
  private int dx, dy;

  public ListenZone(int x, int y, int w, int h, Runnable action) {
    this.x = x;
    this.dx = 0;
    this.y = y;
    this.dy = 0;
    this.w = w;
    this.h = h;
    this.action = action;
    this.priority = Thread.NORM_PRIORITY;
  }

  public void setAction(Runnable action) {
    this.action = action;
  }

  public boolean isIn(int x, int y) {
    return ((x-(this.x+this.dx)>0 && this.x+this.dx+this.w-x>0)&&(y-(this.y+this.dy)>0 && this.y+this.dy+this.h-y>0));
  }

  public boolean isActive() {
    return this.active;
  }

  public void disable() {
    this.active = false;
  }

  public void enable() {
    this.active = true;
  }

  public Runnable action() {
    return this.action;
  }

  public void run() {
    SwingWorker<String, Object> sw = new SwingWorker<String, Object>() {
      @Override
      public String doInBackground() {
        action.run();
        return "r";
      }
    };
    Thread t1 = new Thread(sw);
    t1.setPriority(this.priority);
    t1.start();
  }

  public void runBlocking() {
    this.run();
  }

  public void setXY(int x, int y) {
    this.x = x;
    this.y = y;
  }
  public void setX(int x) {
    this.x = x;
  }
  public void setY(int y) {
    this.y = y;
  }
  public void setDX(int x) {
    this.dx = x;
  }
  public void setDY(int y) {
    this.dy = y;
  }

  public int getX() {
    return this.x;
  }
  public int getY() {
    return this.y;
  }
  public int getDX() {
    return this.dx;
  }
  public int getDY() {
    return this.dy;
  }
  public int getWidth() {
    return this.w;
  }
  public int getHeight() {
    return this.h;
  }

	/**
	* Create string representation of ListenZone for printing
	* @return
	*/
	@Override
	public String toString() {
		return "ListenZone [ "+this.x+this.dx+"=>"+(this.x+this.dx+this.w)+", "+this.y+this.dy+"=>"+(this.y+this.dy+this.h)+"]";
	}
}
