package com.pingu.party.engine.input ;


import java.util.concurrent.atomic.AtomicBoolean;
import com.pingu.party.engine.input.event.KeyInputEvent;
import com.pingu.party.engine.input.event.MouseButtonEvent;
import com.pingu.party.engine.input.event.MouseScrollEvent;
import com.pingu.party.engine.input.event.MouseMotionEvent;

import com.pingu.party.engine.input.event.EBus;

import java.util.ArrayList;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.Component;
import java.awt.Point;
import java.awt.Cursor;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Input implements KeyListener, MouseInput, MouseListener, MouseWheelListener, MouseMotionListener {
  public EBus EventBus;
  /** Toggled on action */
  private boolean actionPerformed = false;

  private boolean visible = true;
  private Cursor hidenCur;
  private Component cp;

  private int wheel;
  private Point location;

  private AtomicBoolean inputFocus;

  //TODO : Implements every functions below
  // Idea : Use a queue for events, and exec them one by one (for multiple clicks / buttons )

  public static Input initInput(Component c) {
    return new Input(c);
  }

  public void lockMouse() {
    this.EventBus.lockMouse();
  }
  public void UnlockMouse() {
    this.EventBus.UnlockMouse();
  }
  public boolean isMouseLocked() {
    return this.EventBus.isMouseLocked();
  }

  public Point getMouseLocation() {
    this.location = this.EventBus.getMouseLocation();
    return this.location;
  }

  public Point getLastClic() {
    return this.EventBus.getLastClic();
  }
  public int getLastScroll() {
    return this.EventBus.getLastScroll();
  }

  public boolean isInputFocusable() {
    return this.inputFocus.get();
  }

  public boolean getInputFocus() {
    return this.inputFocus.compareAndSet(false, true);
  }

  public boolean releaseInputFocus() {
    this.inputFocus.set(false);
    return this.isInputFocusable();
  }

  public Input(Component c) {
    if (this.cp != null) {
      this.cp.removeMouseListener(this);
      this.cp.removeMouseMotionListener(this);
      this.cp.removeMouseWheelListener(this);
      this.cp.removeKeyListener(this);
      if (this.EventBus != null) {
        this.EventBus.clearAll();
        System.out.println("Event bus wasn't null !");
      } else {
        this.EventBus = new EBus();
        System.out.println("Event bus is null . . . ");
      }
    }
    this.EventBus = new EBus();
    this.cp = c;
    this.cp.addMouseListener(this);
    this.cp.addMouseMotionListener(this);
    this.cp.addMouseWheelListener(this);
    this.cp.addKeyListener(this);
    //this.cp.setFocusTraversalKeysEnabled(false); // Unfocus Traversal keys (like f2, tab ...) maybe need a change
    System.out.print("Input initialized.");

    this.inputFocus = new AtomicBoolean(false);
    this.location = new Point();
  }

  public void freeze(Component c) {
    this.EventBus.lockMouse(); //TODO: advanced actions on Component ?
  }
  public void resume(Component c) {
    this.EventBus.UnlockMouse();
  }

  public void mouseExited(MouseEvent me) {}

  public boolean isClicked() {
    return false; //TODO: change me !!!
  }
  public boolean isClicked(int button) {
    return false; //TODO: change me !!!
  }

  public boolean isCursorVisible() {
    return this.visible;
  }

  public boolean hasClicked() {
    return this.EventBus.hasClicked();
  }
  public void consumeClicked() {
    this.EventBus.consumeClicked();
  }

  public boolean hasScrolled() {
    return this.EventBus.hasScrolled();
  }
  public void consumeScrolled() {
    this.EventBus.consumeScrolled();
  }

  public boolean hasMoved() {
    return this.EventBus.hasMoved();
  }

  public Point getMove() {
    return this.EventBus.getMove();
  }

  public void consumeMoved() {
    this.EventBus.consumeMoved();
  }

  public boolean hasKeyPressed(int code) {
    return this.EventBus.hasKeyCode(code);
  }
  public boolean hasKeyEvent(int code) {
    return this.EventBus.hasKeyCodePressed(code);
  }
  public boolean hasKeyCodeReleased(int code) {
    return this.EventBus.hasKeyCodeReleased(code);
  }
  public void consumeKeyRelased(int code) {
    this.EventBus.consumeKeyRelased(code);
  }
  public ArrayList<Integer> getReleased() {
    return this.EventBus.getReleased();
  }
  public void cleanReleased() {
    this.EventBus.cleanReleased();
  }

  private Cursor makeTransparentCur() {
    if (this.hidenCur == null) {
      BufferedImage curimg = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
      this.hidenCur = Toolkit.getDefaultToolkit().createCustomCursor(curimg, new Point(0, 0), "Hidden Cursor");
    }
    return this.hidenCur;
  }

  public String getIOBusLog() {
    return this.EventBus.getLog();
  }

  public boolean IOAction() {
    return this.actionPerformed;
  }

  public void IOTick() {
    this.actionPerformed = false;
    this.EventBus.clearAll();
  }

  public void IOReset() {
    this.actionPerformed = false;
    this.EventBus.clearAll();
  }

  public void setCursorVisible(boolean visible) {
    if (this.visible != visible) {
      this.visible = visible;
      this.cp.setCursor((visible)?(null):(makeTransparentCur()));

    }
  }

  private static int getMouseButtonCode(MouseEvent me) {
    switch (me.getButton()) {
      case MouseEvent.BUTTON1: return MouseInput.BUTTON_LEFT;
      case MouseEvent.BUTTON2: return MouseInput.BUTTON_MIDDLE;
      case MouseEvent.BUTTON3: return MouseInput.BUTTON_RIGHT;
      default: return -1;
    }
  }

  public void mouseClicked(MouseEvent me) {
    //MouseButtonEvent mbe = new MouseButtonEvent(me.getX(), me.getY(), false, getMouseButtonCode(me));
    //this.actionPerformed = true;
  }

  public void mousePressed(MouseEvent me) {
    this.EventBus.addMouseButtonEvent(new MouseButtonEvent(me.getX(), me.getY(), true, getMouseButtonCode(me)));
    this.actionPerformed = true;
  }
  public void mouseDragged(MouseEvent me) {
    this.EventBus.addMouseMotionEventRaw(me.getX(),me.getY(), true);
    this.actionPerformed = true;
  }
  public void mouseReleased(MouseEvent me) {}
  public void mouseEntered(MouseEvent me) {}
  public void mouseMoved(MouseEvent me) {
    this.EventBus.addMouseMotionEventRaw(me.getX(),me.getY(), false);
    this.actionPerformed = true;
  }
  public void mouseWheelMoved(MouseWheelEvent mwe) {
    this.EventBus.addMouseScrollEvent(new MouseScrollEvent(mwe.getWheelRotation()));
    this.actionPerformed = true;
  }
  public void keyPressed(KeyEvent ke) {
    this.EventBus.addKeyInputEvent(new KeyInputEvent((ke.getExtendedKeyCode()%16777216), true));
    this.actionPerformed = true;
  }
  public void keyReleased(KeyEvent ke) {
    this.EventBus.addKeyInputEvent(new KeyInputEvent((ke.getExtendedKeyCode()%16777216), false));
    this.actionPerformed = true;
  }
  public void keyTyped(KeyEvent ke) {this.actionPerformed = true;}

}
