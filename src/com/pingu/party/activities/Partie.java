package com.pingu.party.activities;

import javax.swing.SwingUtilities;
import com.pingu.party.engine.structs.SScores;

import com.pingu.party.game.Game_Console;
import com.pingu.party.game.Card;
import com.pingu.party.game.Player;
import com.pingu.party.game.Game;
import com.pingu.party.game.AIOne;
import com.pingu.party.game.Board;
import com.pingu.party.game.Init;

import com.pingu.party.network.MultiThreadServer;
import com.pingu.party.network.GameServer;
import com.pingu.party.network.clientThread;
import com.pingu.party.network.GameClient;

import com.pingu.party.engine.core.StringManager;
import javax.swing.JOptionPane;

import com.pingu.party.gui.widget.WButton;
import com.pingu.party.gui.widget.WLabel;
import com.pingu.party.gui.widget.WTilesHand;
import com.pingu.party.engine.loader.Loader;
import com.pingu.party.gui.toaster.Toaster;
import com.pingu.party.engine.core.AssetManager;

import com.pingu.party.engine.input.ListenZone;
import java.util.HashMap;
import java.util.ArrayList;

import com.pingu.party.engine.config.Config;

import com.pingu.party.gui.widget.WTilesPile;
import com.pingu.party.gui.widget.WTile;
import com.pingu.party.gui.widget.WImage;
import com.pingu.party.activities.base.Viewport;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Image;
import java.awt.Color;
import com.pingu.party.gui.widget.Widget;
import com.pingu.party.engine.core.GameCanvas;
import java.awt.Graphics2D;

import com.pingu.party.engine.game.Profile;
import com.pingu.party.engine.game.ProfileManager;

public class Partie extends GameCanvas implements Viewport {
  private Color almostGrey = new Color(72, 87, 92, 204);
  private Widget w;
  // Colors
  private Color deepBlue = new Color(87, 161, 211);
  private Color deepWhite = new Color(193, 198, 200);
  private Color testGrey = new Color(132, 142, 146);
  private Color softBlue = new Color(118, 158, 174);
  private Color titleBlue = new Color(74, 111, 125);

  private WTilesPile wtp;
  private WTilesHand wth;

  private static WLabel LabelName;

  private static WLabel LabelManche;
  private static int cntManche;
  private static WLabel LabelTour;
  private static int cntTour;

  private static WButton skipReplay;

  private static int playersCount = 2;
  private static ArrayList<WLabel> LabelTimes;
  private static ArrayList<Integer> cntTimes;

  private static boolean blitz = false;
  private static int times=0;
  
  private static boolean replay=false;

  private int dec;

  Toaster t;
  Loader l;

  protected static final ArrayList<Widget> Widgets = new ArrayList<Widget>();

  protected static final HashMap<String, ListenZone> actions = new HashMap<>();
  protected int[][] tilesTraductor;
  protected int nbPlayer;
  protected int nbAI;
  
  private static Init I;
  protected boolean AI;
  protected int port;
  protected String ip;
  
  protected Profile p;
  
  boolean AIornot1[];
  String playerName1[];
  
  String fileName;
	
  ArrayList<Profile> ListProfile;
  
  public void changeDis(int i)
  {
	  System.out.println("Disc : "+i);
	  this.wtp.setTileIndex(36, i);
  }
  public void stopp()
  {
	  try { Thread.sleep(300); } catch (Exception e) { }
	  setActivity(new Menu());
  }
  public Widget addDeco(GameClient access)
  {
	 Runnable rh = (() -> {
      access.close();
    });
	Widget w = new Widget(0,0, 160,80);
    w.setHorizontalAlign(2);
	WButton askInput = new WButton(16, 16, 64, 64, 0);
    askInput.setBackground(new WImage(AssetManager.getImage("kickH"))); //button image
    askInput.setAction(rh); // bind the action to the button
    actions.put("askInput", askInput.getActionZone()); // Register the action
    askInput.setAction(actions.get("askInput").action()); // and put back the registered action into button
    addClickBinding(askInput.getActionZone()); // finnaly set the button as clickable
    w.add(askInput);
	return w;
  }
  public void Endo(SScores scores)
  {
	  setActivity(new End(scores));
  }
  public void toDisplayTempString(String S)
  {
	  Widget w = new Widget(0,0, 160,80);
      w.setHorizontalAlign(2);
	  Logger.info(S, 500);
  }
  public void changeName(String name)
  {
	  LabelName.setCaption(name);
  } 
  private void setInputNumber()
  {
		StringManager.add("str_enter_poss","Enter the Possibility Number :");
		StringManager.add("str_enter_color","Enter a Color :");
  }
  public int getInputNumber(String key)
  {
	  String number = JOptionPane.showInputDialog(StringManager.get(key));
	  int i;
	  try{
			i=Integer.parseInt(number);
		}
	  catch(Exception e)
		{
			i=9;
		}
	  return i;
  }/*
  private void initHand() {
    this.w = AssetManager.getWidget("TitleBar");
  }*/
  public Widget initHand(int nbPlayer)
  { 
      int x = (Config.WIDTH<1400)?0:(Config.WIDTH - (1400))/2;
      int y = Config.HEIGHT-200;
      Widget w = new Widget(x, y, 140 ,1400);
	  this.wth = new WTilesHand(0,0,140, 1400);
	  int nbCard=0;
	  	if(nbPlayer==2)
			nbCard=14;
		else if(nbPlayer==3)
			nbCard=12;
		else if(nbPlayer==4)
			nbCard=9;
		else if(nbPlayer==5)
			nbCard=7;
		else if(nbPlayer==6)
			nbCard=6;
	  for(int i=0;i<nbCard;i++)
	  {
		  this.wth.add(new WTile(0));
	  }
		w.add(this.wth);
		return w;
  }
    public Widget initHand()
  { 
      int x = (Config.WIDTH<1400)?0:(Config.WIDTH - (1400))/2;
      int y = Config.HEIGHT-200;
      Widget w = new Widget(x, y, 140 ,1400);
	  this.wth = new WTilesHand(0,0,140, 1400);
	  for(int i=0;i<14;i++)
	  {
		  this.wth.add(new WTile(0));
	  }
		w.add(this.wth);
		return w;
  }
  public void toDisplayHand(String s)
  {
	  for(int i=0;i<s.length();i++)
	  {
		 this.wth.setTileIndex(i,s.charAt(i)-48);
	  }
  }
  public void toDisplayHand(Player P)
  {
	  for(int i=0;i<P.getMaxCard();i++)
	  {
			  this.wth.setTileIndex(i, P.getCardColor(i));
	  }  
  }
  public Widget statusTest() {
    Widget w = new Widget(0,80,160,80);
    w.setHorizontalAlign(2);
    cntManche = 0;
    LabelManche = new WLabel("Manche : "+cntManche, 24, 0);
    w.add(LabelManche);
    cntTour = 0;
    LabelTour = new WLabel("Tour : "+cntTour, 24, 24);
    w.add(LabelTour);

    for (int i = 0; i < nbPlayer; i++) {
      //int min = cntTimes.get(i)/60;
      //int sec = cntTimes.get(i)%60;
	  
      if(blitz) {
         // WLabel LabelTime = new WLabel(playerName1[i] + " : "+min+":"+((sec<10)?"0"+sec:sec), 24, 48 + i*24);
		  WLabel LabelTime = new WLabel("",24, 48 + i*24);
          LabelTimes.add(LabelTime);
          w.add(LabelTimes.get(i));
      }
    }

    return w;
  }

  public boolean setPlayerTime(int player,String pseudo, int time) {
    if(player>=nbPlayer || !blitz) return false;
    int min = time/60;
    int sec = time%60;

    String s = pseudo + " : "+min+":"+((sec<10)?"0"+sec:sec);
    LabelTimes.get(player).setCaption(s);
    return true;
  }

    private Widget replayAction() {

        Widget w = new Widget(BaseActivity.dec,BaseActivity.SizeC+88+72, 160,48);
        w.setHorizontalAlign(2);

        skipReplay = new WButton(4, 4, 160, 40);

        skipReplay.setBackground(new WImage(AssetManager.getImage("Bskip")));
        w.add(skipReplay);

        skipReplay.hide();
        if(replay) skipReplay.show();

        scaleViewport();

        return w;

    }

  public Widget inputTest() {
    Widget w = new Widget(0,0, 160,80);
    w.setHorizontalAlign(2);

   /* Runnable rh = (() -> {
      // YOUR ACTION GOES HERE
      // show a popup
      String nname = JOptionPane.showInputDialog(StringManager.get("str_enter_name"));
      LabelName.setCaption(nname);
      // change static integer
      cntManche++;
      cntTour+=2;
      // change the caption of a static WLabel
      LabelManche.setCaption("Manche : "+cntManche);
      LabelTour.setCaption("Tour : "+cntTour);
      // toast a temporary message
      Logger.info("LOL NOPE", 500);
      Logger.warn("STOP RIGHT THERE", 500);
    });

    WButton askInput = new WButton(16, 16, 64, 64, 0);
    askInput.setBackground(new WImage(AssetManager.getImage("addH"))); //button image
    askInput.setAction(rh); // bind the action to the button
    actions.put("askInput", askInput.getActionZone()); // Register the action
    askInput.setAction(actions.get("askInput").action()); // and put back the registered action into button
    addClickBinding(askInput.getActionZone()); // finnaly set the button as clickable
    w.add(askInput); // and add the button to the widget*/
    // WHY ALL THIS MESS FOR A CLIC ?
    // because the clic actions are observers, and need to be refreshed and accessed on a global context
    // ignoring the "actions"- related function will make it unaccessible for actions handler

   // LabelName = new WLabel("Players : "+playerCnt, 80, 16);
   // w.add(LabelName);
	LabelName = new WLabel("MonSuperTexte", 80, 16);
    w.add(LabelName);

    scaleViewport();

    return w;
  }
  public Widget initTiles() {
    Widget w = new Widget(this.dec,64, 800+128, 330+128);
    w.setHorizontalAlign(1);

    this.wtp = new WTilesPile(64, 64, 800+64, 330+64);

    /*
      HOW INDEXES WORKS ?
      color is from 0 to 6 : Transparent, RED, VIOLET, BLUE, YELLOW, GREEN, GREY
     */
	int nint=0;
	WTile W1;
	tilesTraductor = new int[36][];
	 int g=8;
	 for(int i=0;i<8;i++,g--)
	  {
		  for(int j=0;j<g;j++)
		  {
			 W1=new WTile(0);
			 wtp.add(W1);
			 tilesTraductor[nint]=new int[2];
			 tilesTraductor[nint][0]=i;
			 tilesTraductor[nint][1]=j;
			 nint++;
		  }
	  }
	wtp.add(new WTile(0));
    w.add(this.wtp);
    scaleViewport();
    return w;
  }
  public void changeTandR(int turn,int round)
  {
	 LabelManche.setCaption("Manche : "+round);
     LabelTour.setCaption("Tour : "+turn);
  }
  public void toDisplayBoard(Game G)
  {
	  cntTour=G.getTurn();
	  cntManche=G.getRound();
	  LabelManche.setCaption("Manche : "+cntManche);
      LabelTour.setCaption("Tour : "+cntTour);
	  int nbplayer=G.getNumberPlayer();
	  int o,g;
	  if(nbplayer==2)
	  {
		  g=7;
		  o=7;
	  }
	  else
	  {
		  g=8;
		  o=8;
	  }
	  for(int i=o-1,jj=0;((i!=-1)&&(jj<g));i--,jj++)
      {
          for(int j=0;j<jj;j++)
          {
              this.wtp.setTileIndex(getTraduction(tilesTraductor,i,j), G.getColorBoard(i,j));
			  if(!(G.getColorBoard(i,j)==0))
				try { Thread.sleep(40); } catch (Exception e) { }
          }
      }
	  if(nbplayer==5)
	  {
		  this.wtp.setTileIndex(36, G.getDis());
	  }
  }
  public void toDisplayBoard(Board B,int nb)
  {
	 /* cntTour=G.getTurn();
	  cntManche=G.getRound();
	  LabelManche.setCaption("Manche : "+cntManche);
      LabelTour.setCaption("Tour : "+cntTour);*/
	  int nbplayer=nb;
	  int o,g;
	  if(nbplayer==2)
	  {
		  g=7;
		  o=7;
	  }
	  else
	  {
		  g=8;
		  o=8;
	  }
	  for(int i=0;i<o;i++,g--)
	  {
		  for(int j=g-1;j!=-1;j--)
		  {
			  this.wtp.setTileIndex(getTraduction(tilesTraductor,i,j), B.getColorBoard(i,j));
			  if(B.getColorBoard(i,j)!=0)
				try { Thread.sleep(20); } catch (Exception e) { }
		  }
	  }
	  if(nbplayer==5)
	  {
		  this.wtp.setTileIndex(36, B.getDis());
	  }
  }
  private int getTraduction(int[][] tilesTraductors,int x,int y)
  {
	  for(int i=0;i<tilesTraductors.length;i++)
	  {
		if((tilesTraductors[i][0]==x)&&(tilesTraductors[i][1]==y))
		{
			return i;
		}
	  }
	  return 0;
  }
  private void addThread() {
    Runnable r = (() -> {
      try { Thread.sleep(240); } catch (Exception e) { }
      this.wtp.setTileIndex(21, 5);
      try { Thread.sleep(60); } catch (Exception e) { }
      this.wtp.setTileIndex(22, 1);
      try { Thread.sleep(60); } catch (Exception e) { }
      this.wtp.setTileIndex(23, 3);
      try { Thread.sleep(60); } catch (Exception e) { }
      this.wtp.setTileIndex(24, 4);
      try { Thread.sleep(60); } catch (Exception e) { }
      this.wtp.setTileIndex(25, 2);
      //next line
      try { Thread.sleep(60); } catch (Exception e) { }
      this.wtp.setTileIndex(26, 5);
      try { Thread.sleep(60); } catch (Exception e) { }
      this.wtp.setTileIndex(27, 1);
      try { Thread.sleep(60); } catch (Exception e) { }
      this.wtp.setTileIndex(28, 3);
      try { Thread.sleep(60); } catch (Exception e) { }
      this.wtp.setTileIndex(29, 2);
      //next line
      try { Thread.sleep(60); } catch (Exception e) { }
      this.wtp.setTileIndex(30, 5);
      try { Thread.sleep(60); } catch (Exception e) { }
      this.wtp.setTileIndex(31, 1);
      try { Thread.sleep(60); } catch (Exception e) { }
      this.wtp.setTileIndex(32, 2);
      //next line
      try { Thread.sleep(60); } catch (Exception e) { }
      this.wtp.setTileIndex(33, 1);
      try { Thread.sleep(60); } catch (Exception e) { }
      this.wtp.setTileIndex(34, 2);
      //next line
      try { Thread.sleep(120); } catch (Exception e) { }
      this.wtp.setTileIndex(35, 1);

    });
    r.run();
  }
  private void movehand() {
      Runnable r = (() -> {
        this.wtp.setTileIndex(15,this.wth.get(0).getTileIndex());
        try { Thread.sleep(180); } catch (Exception e) { }
        this.wtp.setTileIndex(16,this.wth.get(0).getTileIndex());
        try { Thread.sleep(180); } catch (Exception e) { }
        this.wtp.setTileIndex(17,this.wth.get(0).getTileIndex());
        try { Thread.sleep(180); } catch (Exception e) { }
        this.wtp.setTileIndex(18,this.wth.get(0).getTileIndex());
        try { Thread.sleep(180); } catch (Exception e) { }
        this.wtp.setTileIndex(19,this.wth.get(0).getTileIndex());
        try { Thread.sleep(180); } catch (Exception e) { }
        this.wtp.setTileIndex(20,this.wth.get(0).getTileIndex());
        try { Thread.sleep(180); } catch (Exception e) { }

      });
      r.run();
    }
  private void afterInitAdd() {
    Runnable r = (() -> {
      this.wtp.setTileIndex(15, 0); // changes after creation
      try { Thread.sleep(60); } catch (Exception e) { }
      this.wtp.setTileIndex(16, 2);
      try { Thread.sleep(60); } catch (Exception e) { }
      this.wtp.setTileIndex(17, 4);
      try { Thread.sleep(60); } catch (Exception e) { }
      this.wtp.setTileIndex(18, 1);
      try { Thread.sleep(60); } catch (Exception e) { }
      this.wtp.setTileIndex(19, 0);
      try { Thread.sleep(60); } catch (Exception e) { }
      this.wtp.setTileIndex(20, 3);
      try { Thread.sleep(60); } catch (Exception e) { }
    });
    r.run();
  }
  public void initViewport() {
    this.w = new Widget();

    this.dec = (int)(Config.WIDTH - (800+128))/2;

    addToViewport(this.w);
	setInputNumber();
	
	I=new Init(nbPlayer,nbAI);
	I.initialization(this,AIornot1,ListProfile,times,blitz,replay);
	//I.initialization(this,AIornot1,ListProfile,times,blitz,false);

    addToViewport(initTiles());
    addToViewport(initHand(I.getNumberPlayer()));
	//System.out.println("Area secured 3");
    addToViewport(inputTest());
	//	System.out.println("Area secured 4");
    addToViewport(statusTest());

    addToViewport(replayAction());

	//System.out.println("Area secured 5");
	for(int i=0;i<nbPlayer;i++)
	{
		setPlayerTime(i, playerName1[i],cntTimes.get(i));
		//LabelTimes.set(i,"hoho");
	}
	
	
		//System.out.println("Area secured OUFFFF");
	OnInit();
	//I.startGame();
	SwingUtilities.invokeLater(new Runnable(){
		public void run()
		{
			try { Thread.sleep(30); } catch (Exception e) { }
			I.startGame();
		}
	});
    OnRescale();
  }
  public Widget blitzInit(int nbPlayer, boolean bl)
  {
	this.nbPlayer=nbPlayer;
	Widget w = new Widget(0,80,160,80);
    w.setHorizontalAlign(2);
    cntManche = 0;
    LabelManche = new WLabel("Manche : "+cntManche, 24, 0);
    w.add(LabelManche);
    cntTour = 0;
    LabelTour = new WLabel("Tour : "+cntTour, 24, 24);
    w.add(LabelTour);
	
	System.out.println("Config"+ nbPlayer+" "+bl);
	  if(bl) {
		  blitz=true;
			for (int i = 0; i < nbPlayer; i++) 
			{
			System.out.println("Test3");
			 // WLabel LabelTime = new WLabel(playerName1[i] + " : "+min+":"+((sec<10)?"0"+sec:sec), 24, 48 + i*24);
			  WLabel LabelTime = new WLabel("",24, 48 + i*24);
			  LabelTimes.add(LabelTime);
			  w.add(LabelTimes.get(i));
			}
	  }
	  return w;
  }
  public void initViewport(int i) {
	if(i==1)
	{
		this.w = new Widget();

		this.dec = (int)(Config.WIDTH - (800+128))/2;

		  addToViewport(this.w);

		  setInputNumber();
		  addToViewport(initTiles());
		  addToViewport(initHand());
		  addToViewport(inputTest());
		  addToViewport(statusTest());

		  GameClient I=new GameClient();
		  addToViewport(addDeco(I));
		  //Init I=new Init(nbPlayer,nbAI);
		  //I.initialization(this);
		OnInit();
		//I.startGame();
		I.init(ip,port,p,this);
		OnRescale();
	}
	else
	{
		this.w = new Widget();

		this.dec = (int)(Config.WIDTH - (800+128))/2;

		addToViewport(this.w);
		setInputNumber();
		
		I=new Init();
		
		addToViewport(initTiles());
		//addToViewport(initHand(I.getNumberPlayer()));
		addToViewport(initHand(2));
		addToViewport(inputTest());
		//addToViewport(statusTest());
		/*
		for(int i=0;i<nbPlayer;i++)
		{
			setPlayerTime(i, playerName1[i],cntTimes.get(i));
		}*/
		
		
		OnInit();
		
		I.initialization(this,fileName);
		/*SwingUtilities.invokeLater(new Runnable(){
			public void run()
			{
				try { Thread.sleep(30); } catch (Exception e) { }
				I.startGame();
			}
		});*/
		OnRescale();	
	}
  }

    @Override
  public void OnRescale() {
    refreshViewport();
    scaleViewport();
  }

  @Override
  public void OnInit() {
    this.l = getLoader();
    this.t = new Toaster();
    Logger = this.t;
    this.l.load(this.t);
    this.t.showAll();
  }

  public void refreshViewport() {
    this.dec = (int)(Config.WIDTH - (800+128))/2;
    this.w.scale();
    this.wtp.recalc();
  }

  public void init(int i) {
	  if(i==1)
		initViewport();
	  else if(i==2)
		initViewport(1);
	  else if(i==3)
		initViewport(2);
    //setEventDriven(true);
  }
  public Partie() {
    super();
    init(1);
  }
  public Partie(int players,int IA) 
  {
    super();
	nbPlayer=players;
	nbAI=IA;
    init(1);
  }

  public Partie(int players, boolean isAI[], ArrayList<Profile> playerNames, int IA, int time, boolean b, boolean replay) {
      super();
      blitz = b;
	  times=time;
	  
	  this.replay=replay;
	  
      cntTimes = new ArrayList<>(players);
      LabelTimes = new ArrayList<>(players);
      for (int i = 0; i < players; i++) {
          cntTimes.add(time);
      }
      nbPlayer=players;
      nbAI=IA;
      AIornot1=isAI;
	  
	  playerName1=new String[nbPlayer];
	  for(int i=0;i<playerNames.size();i++)
	  {
		  playerName1[i]=playerNames.get(i).getName();
	  }
	  //System.out.println("Hey :"+playerNames.size());
	  ListProfile=playerNames;
      //playerName1=playerNames;
      init(1);
  }

   
  public Partie(int players,boolean AIornot[],String playerName[],int IA) 
  {
    super();
	nbPlayer=players;
	nbAI=IA;
	AIornot1=AIornot;
	playerName1=playerName;
    init(1);
  }
  public Partie(String ip,int port,Profile po)
  {
	  super();
	  AI=po.getAI();
	  this.port=port;
	  this.ip=ip;
	  this.p=po;
	  init(2);
  }
  
  public void setLabelTimes(int players)
  {
      LabelTimes = new ArrayList<>(players);
  }
  public Partie(String fileName)
  {
	  super();

	  this.fileName=fileName;
	  init(3);
  }
  public void render(Graphics2D g2d) {
  };

}
