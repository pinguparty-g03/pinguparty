package com.pingu.party.activities;

import com.pingu.party.gui.widget.WTitledText;
import com.pingu.party.gui.widget.WIconLayout;

import com.pingu.party.engine.structs.SScores;

import com.pingu.party.engine.core.AssetManager;

import com.pingu.party.engine.core.AssetManager;

import com.pingu.party.gui.widget.WCardWrapper;
import com.pingu.party.gui.widget.WButton;

import com.pingu.party.engine.input.ListenZone;
import java.util.HashMap;
import com.pingu.party.gui.widget.WImage;

import com.pingu.party.gui.widget.WStatusIcon;
import com.pingu.party.gui.widget.WList;
import com.pingu.party.gui.widget.WListItem;
import com.pingu.party.network.structures.IP;
import com.pingu.party.network.structures.Server;
import java.util.ArrayList;
import com.pingu.party.gui.widget.WProgress;
import com.pingu.party.launcher.Utils.Font.Roboto;
import com.pingu.party.gui.widget.WLabel;
import com.pingu.party.gui.widget.WCardTitle;
import com.pingu.party.activities.base.Viewport;
import com.pingu.party.gui.widget.WBackground;
import com.pingu.party.gui.widget.WTitle;
import com.pingu.party.gui.widget.WPoly;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Image;
import java.awt.Color;
import com.pingu.party.launcher.Utils.UIConsts;
import com.pingu.party.gui.widget.WTitleBar;

import com.pingu.party.gui.widget.Widget;
import com.pingu.party.engine.core.GameCanvas;
import com.pingu.party.engine.config.Config;
import java.awt.Graphics2D;

public class End extends GameCanvas implements Viewport {
  private Color almostGrey = new Color(72, 87, 92, 204);
  private Widget w;
  // Colors
  private Color deepBlue = new Color(87, 161, 211);
  private Color deepWhite = new Color(193, 198, 200);
  private Color testGrey = new Color(132, 142, 146);
  private Color softBlue = new Color(118, 158, 174);
  private Color titleBlue = new Color(74, 111, 125);

  // Calculated var
  private int dec;
  private int playersC = 6;

  private SScores scores;
  private static WList scoresList;

  // Bindings map
  private HashMap<String, ListenZone> actions = new HashMap<>();

  public Widget createToolbarButtons() {
    Widget wi = new Widget(0,0, 192, 80, false);
    wi.setHorizontalAlign(2);

    Runnable rh = (() -> {
      setActivity(new Options());
    });
    Runnable rq = (() -> {
      System.exit(0);
    });

    WButton setb = new WButton(0, 0, 64, 64, 80);
    WImage bg = new WImage("res/UI/menu/buttons/Ht/blank.png");
    setb.setBackground(bg);
    setb.setAction(rh);
    this.actions.put("Settings", setb.getActionZone());
    setb.setAction(this.actions.get("Settings").action());
    addClickBinding(setb.getActionZone());

    if(Config.FULLSCREEN) {
      WButton quitb = new WButton(72, 0, 64, 64, 80);
      quitb.setBackground(bg);
      quitb.setAction(rq);
      this.actions.put("QUIT", quitb.getActionZone());
      quitb.setAction(this.actions.get("QUIT").action());
      addClickBinding(quitb.getActionZone());
      wi.add(quitb);
      wi.setXY((Config.WIDTH-152),8);
    } else {
      wi.setXY((Config.WIDTH-80),8);
    }
    wi.add(setb);
    return wi;
  }

  private void createScores() {
    scoresList = new WList(0,0,playersC*64, 0, 8);
    WStatusIcon wsi;
    WIconLayout wil;
    WTitledText wtt;

    String cheapSeparator = "        ";

    for (int i = 0;i<scores.getPos() ;i++ ) {
      WListItem wli = new WListItem(0,0,dec,64);
      wsi = new WStatusIcon(12,12, "res/UI/menu/serv.png");
      wtt = new WTitledText(6,8,256,24,0, (scores.getScore(i).getPos()+1)+" -   "+scores.getScore(i).getName());
      WLabel Sip = new WLabel("Score : "+scores.getScore(i).getScore() + cheapSeparator+"Wins : "+scores.getScore(i).getWins()+cheapSeparator+"Green card : "+scores.getScore(i).getGreenCards(), 0, 0, 12, UIConsts.COLOR_MIDGREY, 16);
      WListItem wttChild = new WListItem(6,0,256,24);
      wttChild.setInner(Sip);
      wtt.add(wttChild);
      /*WListItem wttChild2 = new WListItem(6,0,256,24);
      wttChild2.setInner(new WLabel("Wins :"+scores.getScore(i).getWins(), 0, 0, 12, UIConsts.COLOR_MIDGREY, 16));
      wtt.add(wttChild2);*/
      wil = new WIconLayout(12, 0, wsi, wtt);
      wil.setIconWidth(64);
      wli.setInner(wil);

      scoresList.add(wli);
      System.out.println(scores.getScore(i).toString());
    }
    scoresList.recalc();
  }

  public Widget createEndCard() {
    int wid = playersC*64+8+88+64;
    Widget wi = new Widget(96,168, this.dec, wid, false);
    wi.setHorizontalAlign(1);

    WCardWrapper body = new WCardWrapper(0, 0,this.dec, wid, AssetManager.getImage("endT"));


    Runnable rb = (() -> {
      setActivity(new Menu());
    });
    WButton returnb = new WButton(12, wid-72-64, 326, 64, 0);
    returnb.setBackground(new WImage(0, 0, 326, 64, AssetManager.getImage("returnBHT")));

    returnb.setAction(rb);
    this.actions.put("Return", returnb.getActionZone());
    returnb.setAction(this.actions.get("Return").action());
    addClickBinding(returnb.getActionZone());

    body.addToContainer(returnb);

    createScores();
    body.addToContainer(scoresList);

    wi.add(body);
    //wi.add(returnb);

    return wi;
  }

  public Widget imaGottaGoFast() {
    //TODO: sorry I had to go reaaaaly fast
    Widget w = new Widget(0,0, 1440, 959);

    Image ic = null;
    try {
      ic = ImageIO.read(new File("res/UI/LOLNOPE/end.png"));
    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }
    w.add(new WImage(0, 0, 1440, 959, ic));


    return w;
  }

  public void initViewport() {
    this.w = AssetManager.getWidget("TitleBar");
    //this.w = new Widget();
    //createTitleBar();

    this.dec = Config.WIDTH - 96 - 96;

    addToViewport(this.w);
    //addToViewport(imaGottaGoFast());
    addToViewport(createToolbarButtons());
    addToViewport(createEndCard());
  }

  @Override
  public void OnRescale() {
    refreshViewport();
    scaleViewport();
  }

  public void refreshViewport() {
    this.w.scale();
  }

  public void init() {
    initViewport();
    //setEventDriven(true);
  }


  public End() {
    super();
    init();
  }
  public End(SScores scores) {
    super();
    this.scores = scores;
    playersC = scores.getPos();

    //System.out.println(scores.toString());
    init();
  }

  public void render(Graphics2D g2d) {
  };

}
