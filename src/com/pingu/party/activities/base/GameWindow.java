package com.pingu.party.activities.base;

import com.pingu.party.engine.logger.Logger;
import com.pingu.party.engine.input.Input;
import com.pingu.party.engine.loader.Loader;
import java.awt.Component;
import com.pingu.party.engine.core.GameCanvas;
import java.util.ArrayList;

public class GameWindow extends Component {
  public ArrayList<Activity> reg;

  private Loader LO;
  Input IHandler = null;
  protected Logger Logger;

  public GameWindow() {
    this.reg = new ArrayList<Activity>();
  }

  public Activity Init() {
    synchronized (this.reg) {
      //this.reg.get(0).launch();
      if(this.reg.size()>0) {
        //this.reg.get(0).launch();
        //System.out.println("Launch : "+this.reg.get(0).getID());
        return this.reg.get(0);
      }
    }
    return null;
  }

  public void show(Activity ac) {
    synchronized (this.reg) {
      for (Activity a : this.reg) {
        if (a.getID() == ac.getID()) {
          //TODO: set activity
          return;
        }
      }
      register(ac);
    }
  }
  public void register(Activity ac) {
    synchronized (this.reg) {
      this.reg.add(ac);
      //System.out.println("Added : "+ac.getID());
    }
  }
}
