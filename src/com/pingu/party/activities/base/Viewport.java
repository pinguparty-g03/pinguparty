package com.pingu.party.activities.base;

public interface Viewport {

  public void initViewport();
  public void refreshViewport();

}
