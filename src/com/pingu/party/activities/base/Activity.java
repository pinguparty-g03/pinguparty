package com.pingu.party.activities.base;

import com.pingu.party.engine.core.GameCanvas;

public class Activity {
  GameCanvas gc = null;
  private int ID;

  public Activity(GameCanvas gc, int ID) {
    this.ID = ID;
    this.gc = gc;
  }

  public int getID() {
    return this.ID;
  }

  public final void launch() {
    this.gc.start();
  }

  public GameCanvas getCanvas() {
    return this.gc;
  }

  protected void onCreate() {}
  protected void onDestroy() {}
  protected void onStart() {}
  protected void onResume() {}
  protected void onPause() {}
  protected void onStop() {}
}
