package com.pingu.party.activities;

import com.pingu.party.engine.core.AssetManager;
import com.pingu.party.engine.game.Profile;
import com.pingu.party.engine.game.ProfileManager;
import com.pingu.party.engine.input.ListenZone;
import com.pingu.party.engine.utils.Lambda;
import com.pingu.party.gui.widget.*;
import com.pingu.party.launcher.Utils.Font.MaterialIcon;
import com.pingu.party.launcher.Utils.UIConsts;
import com.pingu.party.network.structures.IP;
import com.pingu.party.network.structures.Server;

import com.pingu.party.network.MultiThreadServer;
import com.pingu.party.network.GameServer;
import com.pingu.party.network.clientThread;
import com.pingu.party.network.DataForProfilesAndStats;
import com.pingu.party.network.GameClient;

import java.awt.*;
import java.util.ArrayList;

public class Online2 extends BaseActivity {

    private static WInput wp;

    //private static WCheckbox wcb;
    private static IP server;
    private static WSelector<Profile> wsp;
    private static ProfileManager pm;

    private static WList usersList;
    private static int sz;

    private static Font icoFont = new MaterialIcon().get(24);

    private static Integer blitz = null;
    private static ArrayList<Profile> clients;
    private static ArrayList<WTitledText> cNames;
    private static ArrayList<WLabel> cSubs;
    private static ArrayList<WStatusIcon> cStatsIco;

    private static ArrayList<WButton> ckick;

    private static final String emptyName = "<...>";
	
	private static MultiThreadServer MTS;

    public static void joinServer(Profile p) {
        removeBinds();
		MTS.launchs();
        setActivity(new Partie((server.toString()),Integer.parseInt(wp.getCaption()),p));
        //setActivity(new Partie((server.toString()),Integer.parseInt(wp.getCaption()),p.getAI()));
    }

    public static void removeBinds() {
        removeClickBinding(actions.get("Return"));
        removeClickBinding(actions.get("Settings"));
        removeClickBinding(actions.get("Join"));
        removeClickBinding(actions.get("Port"));
        removeClickBinding(actions.get("IP"));
        removeClickBinding(actions.get("isIA"));
        removeClickBinding(actions.get("P_Bnext"));
        removeClickBinding(actions.get("P_Bprev"));

        for (int i = 0; i < 6; i++) {
            ListenZone lz = actions.get("kick_"+i);
            if(lz != null) removeClickBinding(lz);
        }
        TicksListener.clear();
    }

    private static void initProfiles() {
        pm = new ProfileManager();
    }

    public static void newWin() {
        removeBinds();
        setCard(Menu.make_card(0, 0));
        Menu.wPlay = Menu.createPlayButtons();
        pushAdded();
    }

    public static boolean doCheck() {
        //actions before check
        if(wp.isFocused()) wp.toggleFocus();

        //values check
        try {
            int i = Integer.parseInt(wp.getCaption());
            if(i != 0) System.out.println("Port is : "+i);
        } catch (Exception e) {
            Logger.warn("Invalid Port !", 200);
            return false;
        }

        System.out.println("User : "+wsp.getCurrent().getName());
        if(wsp.getCurrent().getAI()) System.out.println("AI");
        else System.out.println("Regular user");

        //allow screen change
        return true;
    }

    private static void setUsersCard(Profile p, int index) {
        if (index >= cNames.size() || index < 0) return;

        String bigName;
        String subName;

        if(p!=null) {
            bigName = p.getName();
            subName = "profile";
            cStatsIco.get(index).setStatus(Server.Status.OPEN);
            cStatsIco.get(index).setStatusIcon( AssetManager.getImage("pingu2"));
        } else {
            bigName = emptyName;
            subName = "";
            cStatsIco.get(index).setStatus(Server.Status.OFFLINE);
            cStatsIco.get(index).setStatusIcon( AssetManager.getImage("serv"));
        }


        cNames.get(index).setText(bigName);
        cSubs.get(index).setCaption(subName);
        usersList.recalc();
    }
    
	public static void setUsersCard(String name,boolean online, int index) {
        if (index >= cNames.size() || index < 0) return;

        String bigName=name;
        String subName;

        if(online) {
            subName = "profile";
            cStatsIco.get(index).setStatus(Server.Status.OPEN);
            cStatsIco.get(index).setStatusIcon( AssetManager.getImage("pingu2"));
        } else {
            bigName = emptyName;
            subName = "";
            cStatsIco.get(index).setStatus(Server.Status.OFFLINE);
            cStatsIco.get(index).setStatusIcon( AssetManager.getImage("serv"));
        }


        cNames.get(index).setText(bigName);
        cSubs.get(index).setCaption(subName);
        usersList.recalc();
    }
	
    private static void makeUsersCard(Profile p, int i) {
        String bigName;
        String subName;
        WStatusIcon wsi;
        WIconActionLayout wil;
        WTitledText wtt;

        if(p!=null) {
            bigName = p.getName();
            subName = p.getAIType();
        } else {
            bigName = emptyName;
            subName = "";
        }

        WListItem wli = new WListItem(0,0,dec,64);
        WLabel Sip;
        if(i==0) {
            wsi = new WStatusIcon(12,12, AssetManager.getImage("own3"));
            wsi.setStatus(Server.Status.OPEN);
            wtt = new WTitledText(6,8,256,24,0, "HOST "+bigName);
            Sip = new WLabel("Local player", 0, 0, 12, UIConsts.COLOR_MIDGREY, 16);
        } else {
            if(p != null) {
                wsi = new WStatusIcon(12,12, AssetManager.getImage("pingu1"));
                wsi.setStatus(Server.Status.OPEN);
            } else {
                wsi = new WStatusIcon(12,12, "res/UI/menu/serv.png");
            }
            wtt = new WTitledText(6,8,256,24,0, bigName);
            Sip = new WLabel(subName, 0, 0, 12, UIConsts.COLOR_MIDGREY, 16);
        }

        cNames.add(i, wtt);
        wtt = cNames.get(i);
        cSubs.add(i, Sip);
        Sip = cSubs.get(i);
        cStatsIco.add(i, wsi);
        wsi = cStatsIco.get(i);
        WListItem wttChild = new WListItem(6,0,256,24);
        wttChild.setInner(Sip);
        wtt.add(wttChild);

        wil = new WIconActionLayout(12, 0, wsi, wtt);
        wil.setIconWidth(64);
        wli.setInner(wil);

        usersList.add(wli);
        usersList.recalc();
        sz++;

        //setUsersCard(0, pm.getList().get(4));
    }

    private static void addUser() {
        int cnt = clients.size()-1;
        if(sz>=cnt) return;

        for (int i = 0; i <= cnt; i++) {
            Profile pr = clients.get(i);
            makeUsersCard(pr, i);
        }

    }

    @Override
    public WCardWrapper makeC(int dec, int wid) {
        return make_card(dec, wid);
    }

    /**
     *
     * @param dec @dec
     * @param wid @wid
     * @param slots Profile slots : null = open, size = nbPlayer
     * @param b Integer : null if false, time in seconds else
     * @return @Card
     */
    public static WCardWrapper make_card_with(int dec, int wid, ArrayList<Profile> slots, Integer b) {
        sz = 0;
        blitz = b;
        clients = slots;
        if(blitz != null) System.out.println("Mode blitz = "+(b/60)+":"+(b%60));
        for (Profile p : slots) {
            if(p==null) System.out.println("-  <Open>");
            else System.out.println("-  "+p.getName());
        }
		
		
		//MTS=new MultiThreadServer(this);
		MTS=new MultiThreadServer();
		String[] arg=new String[1];
		arg[0]=clients.size()+"";
		Thread serverThread = new Thread() 
        {
            @Override
            public void run() 
            {
            	DataForProfilesAndStats data = new DataForProfilesAndStats();
            	for(Profile p : clients)
            	{
            		if(p != null) data.addOldProfileAndStat(p.getUUID());
            	}
            	MTS.init(data.getList());
				MTS.main(arg);
			}
		};
		serverThread.start();
		
        return make_card(dec, wid);
    }

    public static WCardWrapper make_card(int dec, int wid) {
        initProfiles();
        WCardWrapper body = new WCardWrapper(0, 0,dec, wid, AssetManager.getImage("VersusT"));

        Runnable rb = (() -> {
            removeBinds();
            newWin();
            //setActivity(new Menu());
        });
        WButton returnb = new WButton(12, wid-72-64, 326, 64, 0);
        returnb.setBackground(new WImage(0, 0, 326, 64, AssetManager.getImage("returnBHT")));

        returnb.setAction(rb);
        actions.put("Return", returnb.getActionZone());
        returnb.setAction(actions.get("Return").action());
        addClickBinding(returnb.getActionZone());

        body.addToContainer(returnb);

        Runnable rs = (() -> {
			System.out.println("Launch");
            if(true)
			{	
				joinServer(clients.get(0));
			}
        });

        WButton startb = new WButton(dec-12-326, wid-72-64, 326, 64, 0);
        startb.setBackground(new WImage(0, 0, 326, 64, AssetManager.getImage("joinBHT")));

        startb.setAction(rs);
        actions.put("Join", startb.getActionZone());
        startb.setAction(actions.get("Join").action());
        addClickBinding(startb.getActionZone());

        body.addToContainer(startb);

        // RIGHT BAR

        int y = 16;
        int x2 = dec-338-12;

        WLabel PortIcoLabel = new WLabel("\ue1e1",x2, y+6,icoFont, UIConsts.COLOR_MIDGREY, 16);
        body.addToContainer(PortIcoLabel);
        WLabel PortLabel = new WLabel("Port",x2+32, y, UIConsts.COLOR_MIDGREY, 16);
        body.addToContainer(PortLabel);

        wp = new WInput("4242", x2+32, y+32);

        Runnable r2 = (() -> {
            if(!wp.isFocused() && !canInputListen()) {
                System.out.println("asking");
                askInputListen();
            }
            if(wp.isFocused()) {
                relaseInputListen();
            }
            wp.toggleFocus();
            //if(!canInputListen() && wi.isFosuced()) askInputListen();
            System.out.println(inputBuffer);
        });
        wp.setAction(r2);

        actions.put("Port", wp.getActionZone());
        wp.setAction(actions.get("Port").action());
        addClickBinding(wp.getActionZone());

        body.addToContainer(wp);

        Lambda<String> k = ((String s) -> {
            //System.out.print("o");

            if(!canInputListen() && wp.isFocused()) wp.toggleFocus();
            if((wp.isFocused()) && wp.getCaption() != s) wp.setCaption(s);
            //wi.listen();
        });

        TicksListener.add(k);

        y+= 96;

        WLabel BlitzIcoLabel;
        WLabel BlitzDetailsLabel;
        if(blitz != null) {
            BlitzIcoLabel = new WLabel("\ue3e7",x2, y+6,icoFont, UIConsts.COLOR_MIDGREY, 16);

            int m = blitz/60;
            int s = blitz%60;
            String cap = ""+((m<10)?"0"+m:m)+":"+((s<10)?"0"+s:s);

            BlitzDetailsLabel = new WLabel("Timeout is "+cap,x2+32, y+32, UIConsts.COLOR_FOREGROUND, 16);
        } else {
            BlitzIcoLabel = new WLabel("\ue3e6",x2, y+6,icoFont, UIConsts.COLOR_MIDGREY, 16);

            BlitzDetailsLabel = new WLabel("Blitz mode is disabled",x2+32, y+32, UIConsts.COLOR_FOREGROUND, 16);
        }
        body.addToContainer(BlitzIcoLabel);
        WLabel BlitzLabel = new WLabel("Blitz",x2+32, y, UIConsts.COLOR_MIDGREY, 16);
        body.addToContainer(BlitzLabel);

        body.addToContainer(BlitzDetailsLabel);

        // USERS LIST + KICK

        usersList = new WList(0,0,x2-12, 0, 8);

        body.addToContainer(usersList);

        cNames = new ArrayList<>();
        cSubs = new ArrayList<>();
        cStatsIco = new ArrayList<>();

        ckick = new ArrayList<>(6);

        int pos = 0;
        int py = 8;
        for (Profile p : clients) {
            if(p == null) {
                WButton wq = new WButton(dec-338-12-48-32, py+8, 48, 48);
                wq.setBackground(new WImage(AssetManager.getImage("delH")));

                int po = pos;

                wq.setAction(() -> {doKick(po);});
                actions.put("kick_"+pos, wq.getActionZone());
                wq.setAction(actions.get("kick_"+pos).action()); //default action : open
                addClickBinding(wq.getActionZone());

                ckick.add(wq);
                body.addToContainer(wq);

                removeClickBinding(wq.getActionZone());
                wq.hide();
            }
            pos++;
            py += 64;
        }

        //ckick.get(ckick.size()-1).show();

        addUser();

        //TEST TODO: REMOVE_ME

       /* if(ckick.size()>0) {
            addUserAt(ckick.size()-1, new ProfileManager().getRandomProfile());
        }*/

        // ENDS HERE

        //TODO : change with this !
        //setUsersCard(0, new ProfileManager().getList().get(4));

        return body;
    }
    
    private static Integer getClientEmptyPos(int p) {
        if(p>=ckick.size() || p<0) return null;
        int i;
        for (i = 0; i < clients.size(); i++) {
            if(clients.get(i) == null) p--;
            if(p<0) return i;
        }
        return null;
    }

    private static Integer getEmptyClientPos(int p) {
        if(p>=clients.size() || p<0) return null;
        int i;
        int cnt = -1;
        for (i = 0; i <= p; i++) {
            if(clients.get(i) == null) cnt++;
            if(p<0) return i;
        }
        return (cnt>-1)?cnt:null;
    }

    /**
     * Add User Ay Position
     * @param pos position in list (0-6)
     * @param p Profile instance / null for empty
     */
    private static void addUserAt(int pos, Profile p) {
        if(pos>=ckick.size() || pos<0) return;
        WButton wq = ckick.get(pos);
        wq.show();
        addClickBinding(wq.getActionZone());
        Integer i = getClientEmptyPos(pos);
        if(i != null) {
            setUsersCard(p, i);
        }
    }

    private static void doKick(int po) {
        System.out.println("I am here "+po+" "+ckick.size());
        Integer p = getEmptyClientPos(po);
        if(p == null) return;
        int pos = p;

        if(pos>ckick.size() || pos<0) return;
        ckick.get(pos).hide();
        removeClickBinding(ckick.get(pos).getActionZone());
        System.out.println("Kick "+po);
        setUsersCard(null, po);

    }

    @Override
    public void BeforeAdd() {
        SizeC = 4*64;

    }

    public Online2() {
        super();
    }


    public Online2(ArrayList<Profile> slots, boolean checked) {
        super();
    }

}
