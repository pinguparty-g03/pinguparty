package com.pingu.party.activities;

import com.pingu.party.engine.animation.Animation;
import com.pingu.party.engine.loader.Loader;
import com.pingu.party.gui.toaster.Toaster;
import java.util.Map;

import com.pingu.party.engine.core.AssetManager;

import javax.swing.JOptionPane;
import java.util.List;

import com.pingu.party.engine.config.LoadInfo;

import com.pingu.party.gui.widget.WIconLayout;
import com.pingu.party.gui.widget.WCardWrapper;
import com.pingu.party.gui.widget.WButton;
import com.pingu.party.engine.input.ListenZone;
import java.util.HashMap;
import com.pingu.party.gui.widget.WImage;

import com.pingu.party.gui.widget.WStatusIcon;
import com.pingu.party.gui.widget.WList;
import com.pingu.party.gui.widget.WListItem;
import com.pingu.party.network.structures.IP;
import com.pingu.party.network.structures.Server;
import java.util.ArrayList;
import com.pingu.party.gui.widget.WProgress;
import com.pingu.party.launcher.Utils.Font.Roboto;
import com.pingu.party.gui.widget.WLabel;
import com.pingu.party.gui.widget.WCardTitle;
import com.pingu.party.activities.base.Viewport;
import com.pingu.party.gui.widget.WBackground;
import com.pingu.party.gui.widget.WTitle;
import com.pingu.party.gui.widget.WPoly;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Image;
import java.awt.Color;
import com.pingu.party.launcher.Utils.UIConsts;
import com.pingu.party.gui.widget.WTitleBar;

import com.pingu.party.gui.widget.Widget;
import com.pingu.party.engine.core.GameCanvas;
import com.pingu.party.engine.config.Config;
import java.awt.Graphics2D;

public class Splash extends GameCanvas implements Viewport {
  private Color Cbg = new Color(233, 238, 241);
  private Color Cbg2 = new Color(242, 247, 253);
  private Widget w;

  // Calculated var
  static int dec;
  static int ver;
  private int wd;
  private int hd;
  static int SizeC = 4*64;

  static WCardWrapper win;
  static Widget WCard;

  static boolean drawcard = true;
  private static int inited = 0;

  Toaster t;
  Loader l;

  //modify me !
  static String titleName = "pinguT";

  protected static final ArrayList<Widget> Widgets = new ArrayList<Widget>();

  protected static final HashMap<String, ListenZone> actions = new HashMap<>();

  Animation aLogo = new Animation(0, true, Animation.playType.NORMAL);


  private void setUpAnim() {
    for (int i = 1; i<96;i++ ) {
      if(i==75) this.aLogo.addKey(Config.FPS*2,AssetManager.getImage("splash"+i));
      else this.aLogo.addKey((int)Config.FPS/30,AssetManager.getImage("splash"+i));
    }
    this.aLogo.addKey(Config.FPS*5, AssetManager.getImage("splash95"));
  }

  public static void removeAllBinds() {
    for (Map.Entry<String, ListenZone> e : actions.entrySet()) {
      String name = e.getKey();
      ListenZone lz = e.getValue();
      removeClickBinding(lz);
    }
    removeClickBinding(actions.get("Settings"));
    removeClickBinding(actions.get("QUIT"));
    removeClickBinding(actions.get("Return"));
  }

  public void initViewport() {
    dec = (Config.WIDTH - 720)/2;
    ver = (Config.HEIGHT - 480)/2;
    this.wd = Config.WIDTH;
    this.hd = Config.HEIGHT;

    setUpAnim();
    this.aLogo.play();
  }

  @Override
  public void onStop() {
    this.aLogo.pause();
  }

  @Override
  public void onRemuse() {
    this.aLogo.play();
  }

  @Override
  public void OnRescale() {
    refreshViewport();
    scaleViewport();
  }

  @Override
  public void OnInit() {
    this.l = getLoader();
    this.t = new Toaster();
    Logger = this.t;
    this.l.load(this.t);
    this.t.showAll();
  }

  public void BeforeAdd() {

  }

  public void refreshViewport() {
    //this.w.scale();
    dec = (Config.WIDTH - 720)/2;
    ver = (Config.HEIGHT - 480)/2;
    this.wd = Config.WIDTH;
    this.hd = Config.HEIGHT;
  }

  public void init() {
    initViewport();
    //setEventDriven(true);
  }

  public Splash() {
    super();
    init();
  }
  public void render(Graphics2D g2d) {
    if(!this.aLogo.isPaused() && g2d != null) {
      g2d.setColor(this.Cbg);
      g2d.fillRect(0,0,this.wd,this.hd);
      this.aLogo.next();
      g2d.drawImage(this.aLogo.getCurrentImage(), dec,ver, this);
    }
  }

}
