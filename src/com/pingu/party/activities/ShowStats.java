package com.pingu.party.activities;

import java.util.ArrayList;

import com.pingu.party.engine.core.StringManager;

import java.awt.Font;
import com.pingu.party.engine.core.AssetManager;
import java.util.List;
import com.pingu.party.engine.config.Config;
import com.pingu.party.gui.widget.WImage;
import com.pingu.party.gui.widget.WLabel;
import com.pingu.party.gui.widget.WList;
import com.pingu.party.gui.widget.WBox;
import com.pingu.party.gui.widget.Widget;
import com.pingu.party.launcher.Utils.Font.Roboto;
import com.pingu.party.launcher.Utils.UIConsts;
import java.awt.Image;
import com.pingu.party.gui.widget.WButton;
import com.pingu.party.gui.widget.WSortButton;
import com.pingu.party.gui.widget.WCardWrapper;


import com.pingu.party.stats.Stat;
import com.pingu.party.stats.StatManager;
import com.pingu.party.engine.config.LoadInfo;

/**
 * @author bastien
 * @author florian
 *
 */
public class ShowStats extends BaseActivity {
  private Widget w;
  // Calculated var
  private static int dec;
  private static boolean percent = false;

  private static WButton modes;
  private static WButton bStats;

   private static WList statsList;
   private static ArrayList<Image> sortIco;
   private static ArrayList<WSortButton> sortBtns;

   private void initStat() {
	   LoadInfo.STATS = new StatManager().getList();
   }

   private static void loadStat(List<Stat> s) {
	   LoadInfo.STATS = s;
   }

   public static void removeBinds() {
    removeClickBinding(actions.get("Return"));
   	removeClickBinding(actions.get("Settings"));
   	removeClickBinding(actions.get("Sort"));
   	removeClickBinding(actions.get("Sort2"));
   	removeClickBinding(actions.get("Sort3"));
   	removeClickBinding(actions.get("Sort4"));
   	removeClickBinding(actions.get("Sort5"));
   	removeClickBinding(actions.get("Sort6"));
   	removeClickBinding(actions.get("Sort7"));
    removeClickBinding(actions.get("Modes"));
    removeClickBinding(actions.get("Graph"));
   }

   public static void newWin() {
     setCard(Menu.make_card(0, 0));
     Menu.wPlay = Menu.createPlayButtons();
   	pushAdded();
   }

   @Override
   public WCardWrapper makeC(int dec, int wid) {
     return make_card(dec, wid);
   }

    public static WCardWrapper make_card_from(List<Stat> s, boolean p) {
      percent = p;
      return make_card_from(s);
    }

   public static WCardWrapper make_card_from(List<Stat> s) {
       loadStat(s);
       dec = Config.WIDTH - 96 - 96;
       removeBinds();
       return make_card(dec, SizeC+88+96);

   }

   public static WCardWrapper make_card(int dec, int wid) {
     WCardWrapper body = new WCardWrapper(0, 0,dec, wid, AssetManager.getImage("statsT"));


     Runnable rperc = (() -> {
       percent = !percent;
       setCard(make_card_from(LoadInfo.STATS));
     });
     modes = new WButton(dec-64, -64, 64, 64, 0);
     if(percent) {
       modes.setBackground(new WImage(0, 0, 64, 64, AssetManager.getImage("BmodesH")));
     } else {
       modes.setBackground(new WImage(0, 0, 64, 64, AssetManager.getImage("BmodesH2")));
     }

     modes.setAction(rperc);
     if(actions.containsKey("Modes")) {
       actions.remove("Modes");
       removeClickBinding(actions.get("Modes"));
     }
     actions.put("Modes", modes.getActionZone());
     modes.setAction(actions.get("Modes").action());
     addClickBinding(modes.getActionZone());

     body.addToContainer(modes);

     Runnable rStats = (() -> {
       removeBinds();
       setCard(Graph.make_card(BaseActivity.dec, BaseActivity.SizeC+88+72));
     });

     bStats = new WButton(dec-64*2, -64, 64, 64, 0);
     bStats.setBackground(new WImage(0,0,64,64,AssetManager.getImage("BstatsH")));
     bStats.setAction(rStats);
     if(actions.containsKey("Graph")) {
       actions.remove("Graph");
       removeClickBinding(actions.get("Graph"));
     }
     actions.put("Graph", bStats.getActionZone());
     bStats.setAction(actions.get("Graph").action());
     addClickBinding(bStats.getActionZone());

     body.addToContainer(bStats);

     Image sortIcon = AssetManager.getImage("sortD");
     Image sortIconR = AssetManager.getImage("sortR");
     Image sortIconN = AssetManager.getImage("sort");
     //Image sortIcon = AssetManager.getImage("sort");
     Image sortLIcon = AssetManager.getImage("sortD-136");
     Image sortLIconR = AssetManager.getImage("sortR-136");
     Image sortLIconN = AssetManager.getImage("sort-136");
     Image sortSIcon = AssetManager.getImage("sortD-96");
     Image sortSIconR = AssetManager.getImage("sortR-96");
     Image sortSIconN = AssetManager.getImage("sort-96");
     //Image sortLIcon = AssetManager.getImage("sort-136");

     sortIco = new ArrayList<>();
     sortBtns = new ArrayList<>();

     WBox contain = new WBox(24, 16, dec-48, (LoadInfo.STATS.size()*38-104), UIConsts.COLOR_BUTTON_BORDER);
     WBox banner = new WBox(24, 28, dec-48, 24, 0, UIConsts.COLOR_BUTTON);
     body.addToContainer(contain);
     body.addToContainer(banner);
     int i = 28;
     int p = 24;
     // button
     Runnable rname = (() -> {
      LoadInfo.STATS = new StatManager().sortName();
        setCard(make_card_from(LoadInfo.STATS));
        sortBtns.get(0).setState((sortBtns.get(0).getState()+1)%3);
         //setActivity(new ShowStats(LoadInfo.STATS));
     });
     WSortButton sortName = new  WSortButton(p, i, 128+8, 24, 0);
     sortName.setMuteSize("136");

     sortName.setAction(rname);
     actions.put("Sort", sortName.getActionZone());
     sortName.setAction(actions.get("Sort").action());
     addClickBinding(sortName.getActionZone());
     sortBtns.add(0, sortName);
     body.addToContainer(sortBtns.get(0)); // Add sort button
     body.addToContainer(new WLabel(StringManager.get("str_name"), p+8, i));

     p+=128+8;
     Runnable rplayedgames = (() -> {
     	  LoadInfo.STATS = new StatManager().sortPlayedGames();
        setCard(make_card_from(LoadInfo.STATS));
        sortBtns.get(1).setState((sortBtns.get(1).getState()+1)%3);
        //setActivity(new ShowStats(LoadInfo.STATS));
     });
     WSortButton sortPlayed = new WSortButton(p, i, 96, 24, 0);
     sortPlayed.setMuteSize("96");

     //sortPlayed.setBackground(new WImage(dec-54, i, 48, 32, AssetManager.getImage("sort-96")));
     sortIco.add(0, sortSIcon);
     //sortPlayed.setBackground(new WImage(dec-54, i, 48, 32, sortIco.get(0)));

     sortPlayed.setAction(rplayedgames);
     actions.put("Sort2", sortPlayed.getActionZone());
     sortPlayed.setAction(actions.get("Sort2").action());
     addClickBinding(sortPlayed.getActionZone());
     sortBtns.add(1, sortPlayed);
     body.addToContainer(sortBtns.get(1)); // Add sort button
     body.addToContainer(new WLabel(StringManager.get("str_played"), p+8, i));
     body.addToContainer(new WBox(p+2, 52, 2, (LoadInfo.STATS.size()*38-140), 0, UIConsts.COLOR_BUTTON));

     p+=96;
     Runnable rwon = (() -> {
        LoadInfo.STATS = new StatManager().sortWin();
        setCard(make_card_from(LoadInfo.STATS));
        sortBtns.get(2).setState((sortBtns.get(2).getState()+1)%3);
        //setActivity(new ShowStats(LoadInfo.STATS));
     });
     WSortButton sortWon = new WSortButton(p, i, 64, 24, 0);
     sortIco.add(1, sortIcon);
     //sortWon.setBackground(new WImage(dec-54, i, 48, 32, sortIco.get(1)));

     sortWon.setAction(rwon);
     actions.put("Sort3",sortWon.getActionZone());
     sortWon.setAction(actions.get("Sort3").action());
     addClickBinding(sortWon.getActionZone());
     sortBtns.add(2, sortWon);
     body.addToContainer(sortBtns.get(2)); // Add sort button
     body.addToContainer(new WLabel(StringManager.get("str_won"), p+8, i));
     body.addToContainer(new WBox(p+2, 52, 2, (LoadInfo.STATS.size()*38-140), 0, UIConsts.COLOR_BUTTON));


     p+=64;
     Runnable rlost = (() -> {
     	LoadInfo.STATS = new StatManager().sortLoose();
        setCard(make_card_from(LoadInfo.STATS));
        sortBtns.get(3).setState((sortBtns.get(3).getState()+1)%3);
         //setActivity(new ShowStats(LoadInfo.STATS));
     });
     WSortButton sortLost = new WSortButton(p, i, 64, 24, 0);
     sortIco.add(2, sortIcon);
     //sortLost.setBackground(new WImage(dec-54, i, 48, 32, sortIco.get(2)));

     sortLost.setAction(rlost);
     actions.put("Sort4",sortLost.getActionZone());
     sortLost.setAction(actions.get("Sort4").action());
     addClickBinding(sortLost.getActionZone());
     sortBtns.add(3, sortLost);
     body.addToContainer(sortBtns.get(3)); // Add sort button
     body.addToContainer(new WLabel(StringManager.get("str_lost"), p+8, i));
     body.addToContainer(new WBox(p+2, 52, 2, (LoadInfo.STATS.size()*38-140), 0, UIConsts.COLOR_BUTTON));

     p+=64;
     Runnable rleft = (() -> {
     	LoadInfo.STATS = new StatManager().sortQuit();
        setCard(make_card_from(LoadInfo.STATS));
        sortBtns.get(4).setState((sortBtns.get(4).getState()+1)%3);
        // setActivity(new ShowStats(LoadInfo.STATS));
     });
     WSortButton sortLeft = new WSortButton(p, i, 64, 24, 0);
     sortIco.add(3, sortIcon);
     //sortLeft.setBackground(new WImage(dec-54, i, 48, 32, sortIco.get(3)));

     sortLeft.setAction(rleft);
     actions.put("Sort5",sortLeft.getActionZone());
     sortLeft.setAction(actions.get("Sort5").action());
     addClickBinding(sortLeft.getActionZone());
     sortBtns.add(4, sortLeft);
     body.addToContainer(sortBtns.get(4)); // Add sort button
     body.addToContainer(new WLabel(StringManager.get("str_left"), p+8, i));
     body.addToContainer(new WBox(p+2, 52, 2, (LoadInfo.STATS.size()*38-140), 0, UIConsts.COLOR_BUTTON));

     p+=64;
     Runnable rtime = (() -> {
     	LoadInfo.STATS = new StatManager().sortTime();
        setCard(make_card_from(LoadInfo.STATS));
        sortBtns.get(5).setState((sortBtns.get(5).getState()+1)%3);
         //setActivity(new ShowStats(LoadInfo.STATS));
     });
     WSortButton sortTime = new WSortButton(p, i, 136, 24, 0);
     sortTime.setMuteSize("136");
     sortIco.add(4, sortLIcon);
     //sortTime.setBackground(new WImage(dec-54, i, 48, 32, sortIco.get(4)));

     sortTime.setAction(rtime);
     actions.put("Sort6",sortTime.getActionZone());
     sortTime.setAction(actions.get("Sort6").action());
     addClickBinding(sortTime.getActionZone());
     sortBtns.add(5, sortTime);
     body.addToContainer(sortBtns.get(5)); // Add sort button
     body.addToContainer(new WLabel(StringManager.get("str_time"), p+8, i));
     body.addToContainer(new WBox(p+2, 52, 2, (LoadInfo.STATS.size()*38-140), 0, UIConsts.COLOR_BUTTON));

     p+=136;
     Runnable relo = (() -> {
       LoadInfo.STATS = new StatManager().sortElo();
       setCard(make_card_from(LoadInfo.STATS));
       sortBtns.get(6).setState((sortBtns.get(6).getState()+1)%3);
       //setActivity(new ShowStats(LoadInfo.STATS));
     });
     WSortButton sortElo = new WSortButton(p, i, 64, 24, 0);
     sortIco.add(5, sortIcon);
     //sortElo.setBackground(new WImage(dec-54, i, 48, 32, sortIco.get(5)));

     sortElo.setAction(relo);
     actions.put("Sort7",sortElo.getActionZone());
     sortElo.setAction(actions.get("Sort7").action());
     addClickBinding(sortElo.getActionZone());
     sortBtns.add(6, sortElo);
     body.addToContainer(sortBtns.get(6)); // Add sort button
     body.addToContainer(new WLabel(StringManager.get("str_elo"), p+8, i));
     body.addToContainer(new WBox(p+2, 52, 2, (LoadInfo.STATS.size()*38-140), 0, UIConsts.COLOR_BUTTON));

     //i += 24;
     i += 8;
     statsList = new WList(0,0,480, 0, 8); // Create the list element
     i += 24;
     int y;
     for (Stat s : LoadInfo.STATS) {
       int totalGames = s.getPlayedGames();
       if(i%40==0) body.addToContainer(new WBox(24, i, dec-48, 20, 0, UIConsts.COLOR_SEMI_BUTTON));
       y= 32;
       body.addToContainer(new WLabel(s.getName(), y, i, UIConsts.COLOR_MIDGREY));
       y += 128;
       body.addToContainer(new WLabel(""+s.getPlayedGames(), y+8, i, UIConsts.COLOR_MIDGREY));
       y += 96;
       if(percent) {
         int w = (int)((s.getWin() * 100.0f) / totalGames);
         body.addToContainer(new WLabel(""+w+"%", y+8, i, UIConsts.COLOR_MIDGREY));
       } else body.addToContainer(new WLabel(""+s.getWin(), y+8, i, UIConsts.COLOR_MIDGREY));
       y += 64;
       if(percent) {
         int w = (int)((s.getLoose() * 100.0f) / totalGames);
         body.addToContainer(new WLabel(""+w+"%", y+8, i, UIConsts.COLOR_MIDGREY));
       } else body.addToContainer(new WLabel(""+s.getLoose(), y+8, i, UIConsts.COLOR_MIDGREY));
       y += 64;
       if(percent) {
         int w = (int)((s.getQuit() * 100.0f) / totalGames);
         body.addToContainer(new WLabel(""+w+"%", y+8, i, UIConsts.COLOR_MIDGREY));
       } else body.addToContainer(new WLabel(""+s.getQuit(), y+8, i, UIConsts.COLOR_MIDGREY));
       y += 64;
       body.addToContainer(new WLabel(""+s.getStringTime(), y+8, i, UIConsts.COLOR_MIDGREY));
       y += 136;
       body.addToContainer(new WLabel(""+s.getElo(), y+8, i, UIConsts.COLOR_MIDGREY));
       i+= 20;
     }
     i += 50 ;

     Runnable rb = (() -> {
       removeBinds();
   		newWin();
       //setActivity(new Menu());
     });
     WButton returnb = new WButton(12, wid-72-64, 326, 64, 0);
     returnb.setBackground(new WImage(0, 0, 326, 64, AssetManager.getImage("returnBHT")));

     returnb.setAction(rb);
     actions.put("Return", returnb.getActionZone());
     returnb.setAction(actions.get("Return").action());
     addClickBinding(returnb.getActionZone());

     body.addToContainer(returnb);

     return body;
   }

  public Widget createStatsCard() {
    int wid = LoadInfo.STATS.size()*40+32+8+16+88+64;
    Widget wi = new Widget(96,168, dec, wid, false);
    wi.setHorizontalAlign(1);

    Image sortIcon = AssetManager.getImage("checked");

    WCardWrapper body = new WCardWrapper(0, 0,dec, wid, AssetManager.getImage("statsT"));

    WBox contain = new WBox(24, 10, dec-48, (LoadInfo.STATS.size()*38+24+16), UIConsts.COLOR_BUTTON_BORDER);
    body.addToContainer(contain);
    int i = 24;

    // button
    Runnable rname = (() -> {
      LoadInfo.STATS = new StatManager().sortName();
      setCard(make_card_from(LoadInfo.STATS));
      //setActivity(new ShowStats(LoadInfo.STATS));
    });
    WButton sortName = new WButton(32, i, 128, 64, 0);
    sortName.setBackground(new WImage(dec-54, i, 48, 48, sortIcon));

    sortName.setAction(rname);
    actions.put("Sort", sortName.getActionZone());
    sortName.setAction(actions.get("Sort").action());
    addClickBinding(sortName.getActionZone());
    body.addToContainer(sortName); // Add sort button

    // button
    Runnable rplayedgames = (() -> {
      LoadInfo.STATS = new StatManager().sortPlayedGames();
      setCard(make_card_from(LoadInfo.STATS));
      //setActivity(new ShowStats(LoadInfo.STATS));
    });
    WButton sortPlayed = new WButton(160, i, 64, 64, 0);
    sortPlayed.setBackground(new WImage(dec-54, i, 48, 48, sortIcon));

    sortPlayed.setAction(rplayedgames);
    actions.put("Sort2", sortPlayed.getActionZone());
    sortPlayed.setAction(actions.get("Sort2").action());
    addClickBinding(sortPlayed.getActionZone());
    body.addToContainer(sortPlayed); // Add sort button


    // button
    Runnable rwon = (() -> {
      LoadInfo.STATS = new StatManager().sortWin();
      setCard(make_card_from(LoadInfo.STATS));
      //setActivity(new ShowStats(LoadInfo.STATS));
    });
    WButton sortWon = new WButton(224, i, 64, 64, 0);
    sortWon.setBackground(new WImage(dec-54, i, 48, 48, sortIcon));

    sortWon.setAction(rwon);
    actions.put("Sort3",sortWon.getActionZone());
    sortWon.setAction(actions.get("Sort3").action());
    addClickBinding(sortWon.getActionZone());
    body.addToContainer(sortWon); // Add sort button


    //button
    Runnable rlost = (() -> {
      LoadInfo.STATS = new StatManager().sortLoose();
      setCard(make_card_from(LoadInfo.STATS));
      //setActivity(new ShowStats(LoadInfo.STATS));
    });
    WButton sortLost = new WButton(288, i, 64, 64, 0);
    sortLost.setBackground(new WImage(dec-54, i, 48, 48, sortIcon));

    sortLost.setAction(rlost);
    actions.put("Sort4",sortLost.getActionZone());
    sortLost.setAction(actions.get("Sort4").action());
    addClickBinding(sortLost.getActionZone());
    body.addToContainer(sortLost); // Add sort button


    //button
    Runnable rleft = (() -> {
      LoadInfo.STATS = new StatManager().sortQuit();
      setCard(make_card_from(LoadInfo.STATS));
      //setActivity(new ShowStats(LoadInfo.STATS));
    });
    WButton sortLeft = new WButton(352, i, 64, 64, 0);
    sortLeft.setBackground(new WImage(dec-54, i, 48, 48, sortIcon));

    sortLeft.setAction(rleft);
    actions.put("Sort5",sortLeft.getActionZone());
    sortLeft.setAction(actions.get("Sort5").action());
    addClickBinding(sortLeft.getActionZone());
    body.addToContainer(sortLeft); // Add sort button


    //button
    Runnable rtime = (() -> {
      LoadInfo.STATS = new StatManager().sortTime();
      setCard(make_card_from(LoadInfo.STATS));
      //setActivity(new ShowStats(LoadInfo.STATS));
    });
    WButton sortTime = new WButton(416, i, 96, 64, 0);
    sortTime.setBackground(new WImage(dec-54, i, 48, 48, sortIcon));

    sortTime.setAction(rtime);
    actions.put("Sort6",sortTime.getActionZone());
    sortTime.setAction(actions.get("Sort6").action());
    addClickBinding(sortTime.getActionZone());
    body.addToContainer(sortTime); // Add sort button



    //button
    Runnable relo = (() -> {
      LoadInfo.STATS = new StatManager().sortElo();
      setCard(make_card_from(LoadInfo.STATS));
      //setActivity(new ShowStats(LoadInfo.STATS));
    });
    WButton sortElo = new WButton(512, i, 64, 64, 0);
    sortElo.setBackground(new WImage(dec-54, i, 48, 48, sortIcon));

    sortElo.setAction(relo);
    actions.put("Sort7",sortElo.getActionZone());
    sortElo.setAction(actions.get("Sort7").action());
    addClickBinding(sortElo.getActionZone());
    body.addToContainer(sortElo); // Add sort button


    // title
    i += 24;
    body.addToContainer(new WLabel("name", 32, i));
    body.addToContainer(new WLabel("played", 160, i));
    body.addToContainer(new WLabel("won", 224, i));
    body.addToContainer(new WLabel("lost", 288, i));
    body.addToContainer(new WLabel("left", 352, i));
    body.addToContainer(new WLabel("time", 416, i));
    body.addToContainer(new WLabel("elo", 512, i));

    statsList = new WList(0,0,480, 0, 8); // Create the list element
    i += 24;
    int y;
    for (Stat s : LoadInfo.STATS) {
      y= 32;
      body.addToContainer(new WLabel(s.getName(), y, i, UIConsts.COLOR_MIDGREY));
      y += 128;
      body.addToContainer(new WLabel(""+s.getPlayedGames(), y, i, UIConsts.COLOR_MIDGREY));
      y += 64;
      body.addToContainer(new WLabel(""+s.getWin(), y, i, UIConsts.COLOR_MIDGREY));
      y += 64;
      body.addToContainer(new WLabel(""+s.getLoose(), y, i, UIConsts.COLOR_MIDGREY));
      y += 64;
      body.addToContainer(new WLabel(""+s.getQuit(), y, i, UIConsts.COLOR_MIDGREY));
      y += 64;
      body.addToContainer(new WLabel(""+s.getStringTime(), y, i, UIConsts.COLOR_MIDGREY));
      y += 96;
      body.addToContainer(new WLabel(""+s.getElo(), y, i, UIConsts.COLOR_MIDGREY));
      i+= 20;
    }
    i += 50 ;
    Runnable rb = (() -> {
      removeBinds();
      newWin();
    });
    WButton returnb = new WButton(12, i, 326, 64, 0);
    returnb.setBackground(new WImage(0, 0, 326, 64, AssetManager.getImage("returnBHT")));

    returnb.setAction(rb);
    actions.put("Return", returnb.getActionZone());
    returnb.setAction(actions.get("Return").action());
    addClickBinding(returnb.getActionZone());

    body.addToContainer(returnb); // Add return button

    wi.add(body);
    return wi;

  }

  public void initViewport() {
    this.w = AssetManager.getWidget("TitleBar");
    //this.w = new Widget();
    //createTitleBar();

    dec = Config.WIDTH - 96 - 96;

    addToViewport(this.w);
    //addToViewport(imaGottaGoFast());
    addToViewport(createToolbarButtons());
    addToViewport(createStatsCard());
  }

  @Override
  public void BeforeAdd() {
    SizeC = LoadInfo.STATS.size()*40+24;
    initStat();
  }


  public ShowStats(List<Stat> s) {
    super();
    loadStat(s);
    init();
  }

  public ShowStats() {
    super();
    initStat();
    init();
  }
}
