package com.pingu.party.activities;

import com.pingu.party.engine.core.StringManager;

import com.pingu.party.engine.core.AssetManager;

import javax.swing.JOptionPane;
import java.util.Arrays;
import java.util.List;

import com.pingu.party.engine.config.LoadInfo;

import com.pingu.party.gui.widget.WIconLayout;
import com.pingu.party.gui.widget.WCardWrapper;
import com.pingu.party.gui.widget.WButton;

import com.pingu.party.engine.input.ListenZone;
import java.util.HashMap;
import com.pingu.party.gui.widget.WImage;

import com.pingu.party.gui.widget.WStatusIcon;
import com.pingu.party.gui.widget.WList;
import com.pingu.party.gui.widget.WListItem;
import com.pingu.party.network.structures.IP;
import com.pingu.party.network.structures.Server;
import java.util.ArrayList;
import com.pingu.party.gui.widget.WProgress;
import com.pingu.party.launcher.Utils.Font.Roboto;
import com.pingu.party.gui.widget.WLabel;
import com.pingu.party.gui.widget.WCardTitle;
import com.pingu.party.activities.base.Viewport;
import com.pingu.party.gui.widget.WBackground;
import com.pingu.party.gui.widget.WTitle;
import com.pingu.party.gui.widget.WPoly;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Image;
import java.awt.Color;
import com.pingu.party.launcher.Utils.UIConsts;
import com.pingu.party.gui.widget.WTitleBar;

import com.pingu.party.gui.widget.Widget;
import com.pingu.party.engine.core.GameCanvas;
import com.pingu.party.engine.config.Config;
import java.awt.Graphics2D;


import com.pingu.party.engine.game.ProfileManager;
import com.pingu.party.engine.game.Profile;

/**
 * @author bastien
 * @author florian
 *
 */
public class ProfileList extends BaseActivity {
  private Color almostGrey = new Color(72, 87, 92, 204);
  private Widget w;

  private static WLabel nameProfile1;
  private static WLabel nameSubProfile1;
  private static WImage imageProfile1;
  private static WLabel nameProfile2;
  private static WLabel nameSubProfile2;
  private static WImage imageProfile2;
  private static WLabel nameProfile3;
  private static WLabel nameSubProfile3;
  private static WImage imageProfile3;
  private static WLabel nameProfile4;
  private static WLabel nameSubProfile4;
  private static WImage imageProfile4;
  private static WLabel nameProfile5;
  private static WLabel nameSubProfile5;
  private static WImage imageProfile5;
  private static WLabel nameProfile6;
  private static WLabel nameSubProfile6;
  private static WImage imageProfile6;
  private static WButton selectProfile1;
  private static WButton selectProfile2;
  private static WButton selectProfile3;
  private static WButton selectProfile4;
  private static WButton selectProfile5;
  private static WButton selectProfile6;

  private static WLabel nameProfile7;
  private static WLabel nameSubProfile7;
  private static WImage imageProfile7;
  private static WLabel nameProfile8;
  private static WLabel nameSubProfile8;
  private static WImage imageProfile8;
  private static WLabel nameProfile9;
  private static WLabel nameSubProfile9;
  private static WImage imageProfile9;
  private static WLabel nameProfile10;
  private static WLabel nameSubProfile10;
  private static WImage imageProfile10;
  private static WLabel nameProfile11;
  private static WLabel nameSubProfile11;
  private static WImage imageProfile11;
  private static WButton selectProfile7;
  private static WButton selectProfile8;
  private static WButton selectProfile9;
  private static WButton selectProfile10;
  private static WButton selectProfile11;

  private static WButton profilePB;
  private static WButton profileNB;

  private static int deb = 0;

  private int profilsC = 7; // Profiles count => display size

  private int n;


  Widget card;

   private static WList profileList;
   private static ProfileManager manageProfiles;

   public static void removeBinds() {
     removeClickBinding(actions.get("Return"));
   	removeClickBinding(actions.get("Settings"));
   	removeClickBinding(actions.get("Edit Profile"));
   	removeClickBinding(actions.get("Create"));
   	removeClickBinding(actions.get("Delete"));
   	removeClickBinding(actions.get("Change"));

   removeClickBinding(actions.get("selectProfile1"));
   removeClickBinding(actions.get("selectProfile2"));
   removeClickBinding(actions.get("selectProfile3"));
   removeClickBinding(actions.get("selectProfile4"));
   removeClickBinding(actions.get("selectProfile5"));
   removeClickBinding(actions.get("selectProfile6"));

   removeClickBinding(actions.get("selectProfile7"));
   removeClickBinding(actions.get("selectProfile8"));
   removeClickBinding(actions.get("selectProfile9"));
   removeClickBinding(actions.get("selectProfile10"));
   removeClickBinding(actions.get("selectProfile11"));
   }

   public static void newWin() {
     setCard(Menu.make_card(0, 0));
     Menu.wPlay = Menu.createPlayButtons();
   	pushAdded();
   }

   @Override
   public WCardWrapper makeC(int dec, int wid) {
     return make_card(dec, wid);
   }

  private void initProfiles() {
    manageProfiles = new ProfileManager();
  }

  public static WCardWrapper make_card(int dec, int wid) {
    wid = 6*64+88+72;
    WCardWrapper body = new WCardWrapper(0, 0,dec, wid, AssetManager.getImage("profilsT"));

    Runnable rche = (() -> {
      removeBinds();
      setCard(EditProfile.make_card_from(LoadInfo.ACTUALPROFILE));
      //setActivity(new EditProfile(LoadInfo.ACTUALPROFILE));
      //setActivity(new EditProfile());
    });

    WButton edit_image = new WButton(dec-54, -54, 48, 48, 0);
    edit_image.setBackground(new WImage(0, 0, 48, 48, AssetManager.getImage("nextH")));
    edit_image.setAction(rche);
    actions.put("Edit Profile", edit_image.getActionZone());
    edit_image.setAction(actions.get("Edit Profile").action());
    addClickBinding(edit_image.getActionZone());
    body.addToContainer(edit_image);
    //body.setAction(edit_image);

    Runnable rb = (() -> {
      removeBinds();
      newWin();
      //setActivity(new Menu());
    });
    WButton returnb = new WButton(12, wid-72-64, 326, 64, 0);
    returnb.setBackground(new WImage(0, 0, 326, 64, AssetManager.getImage("returnBHT")));

    returnb.setAction(rb);
    actions.put("Return", returnb.getActionZone());
    returnb.setAction(actions.get("Return").action());
    addClickBinding(returnb.getActionZone());

    body.addToContainer(returnb);

      Runnable rnew = (() -> {
      	LoadInfo.ACTUALPROFILE = LoadInfo.NEWPROFILE;
        removeBinds();
        setCard(EditProfile.make_card(BaseActivity.dec, BaseActivity.SizeC+88+72));
        //setActivity(new EditProfile());
      });
      WButton create = new WButton(dec-54-54-54, -54, 48, 48, 0);
      create.setBackground(new WImage(0, 0, 48, 48, AssetManager.getImage("addH")));

      create.setAction(rnew);
      actions.put("Create", create.getActionZone());
      create.setAction(actions.get("Create").action());
      addClickBinding(create.getActionZone());

      body.addToContainer(create); // Add create button

      Runnable rdel = (() -> {
        if(LoadInfo.ACTUALPROFILE != null && LoadInfo.PROFILES.get(1) != null) {
          Profile.deleteUser(LoadInfo.ACTUALPROFILE.getUUID());
          LoadInfo.ACTUALPROFILE = LoadInfo.PROFILES.get(0);
        }
        removeBinds();
        setCard(ProfileList.make_card(dec, SizeC+88+72));
        scaleViewport();
        rescaleAll();
        //setActivity(new ProfileList());
      });
        WButton delet = new WButton(dec-54-54, -54, 48, 48, 0);
        delet.setBackground(new WImage(0, 0, 48, 48, AssetManager.getImage("minH")));

        delet.setAction(rdel);
        actions.put("Delete", delet.getActionZone());
        delet.setAction(actions.get("Delete").action());
        addClickBinding(delet.getActionZone());

        body.addToContainer(delet); // Add return button

    Runnable rch = (() -> {
      List<String> choices = new ArrayList<>();
      manageProfiles.getList().forEach(p -> choices.add(p.getName()));
      String[] choic = choices.toArray(new String[0]);
      Object[] buttons = {"OK"};
      Profile input = (Profile) JOptionPane.showInputDialog(null, StringManager.get("str_choose")+":",StringManager.get("str_current_profile"), JOptionPane.QUESTION_MESSAGE, null, manageProfiles.getList().toArray(), manageProfiles.getList().get(0));
      removeBinds();
      if(input == null) {
          setCard(ProfileList.make_card(dec, SizeC+88+72));
          return;
      }
      LoadInfo.ACTUALPROFILE = input;
      Menu.refresh();
      System.out.println(input);
      //setActivity(new ProfileList());

      setCard(ProfileList.make_card(dec, SizeC+88+72));
    });

    WButton choiceb = new WButton(dec-12-326, wid-72-64, 326, 64, 0);
    choiceb.setBackground(new WImage(0, 0, 326, 64, AssetManager.getImage("changeBHT")));

    choiceb.setAction(rch);
    actions.put("Change", choiceb.getActionZone());
    choiceb.setAction(actions.get("Change").action());
    addClickBinding(choiceb.getActionZone());

    body.addToContainer(choiceb); // Add return button

    profileList = new WList(0,0,480, 0, 8);

    WStatusIcon wsi;
    WIconLayout wil;
    int i = 16;
    LoadInfo.launch();

    int y = 16;
    int x = 24;
    int liSize = 64;

    nameProfile1 = new WLabel("profile 1", x+48+8, y-8, 24);
    //(String s, int x, int y, Color c, int fsize)
    nameSubProfile1 = new WLabel("Ai 1", x+48+8, y+24, UIConsts.COLOR_MIDGREY, 16);
    imageProfile1 = new WImage(x, y,  AssetManager.getImage("pingu"+1));
    selectProfile1 = new WButton(480-40, y+4, 48, 48);
    selectProfile1.setBackground(new WImage(AssetManager.getImage("check")));

    y += liSize;

    nameProfile2 = new WLabel("profile 2", x+48+8, y-8, 24);
    nameSubProfile2 = new WLabel("Ai 1", x+48+8, y+24, UIConsts.COLOR_MIDGREY, 16);
    imageProfile2 = new WImage(x, y,  AssetManager.getImage("pingu"+1));
    selectProfile2 = new WButton(480-40, y+4, 48, 48);
    selectProfile2.setBackground(new WImage(AssetManager.getImage("check")));

    y += liSize;

    nameProfile3 = new WLabel("profile 3", x+48+8, y-8, 24);
    nameSubProfile3 = new WLabel("Ai 1", x+48+8, y+24, UIConsts.COLOR_MIDGREY, 16);
    imageProfile3 = new WImage(x, y,  AssetManager.getImage("pingu"+1));
    selectProfile3 = new WButton(480-40, y+4, 48, 48);
    selectProfile3.setBackground(new WImage(AssetManager.getImage("check")));

    y += liSize;

    nameProfile4 = new WLabel("profile 4", x+48+8, y-8, 24);
    nameSubProfile4 = new WLabel("Ai 1", x+48+8, y+24, UIConsts.COLOR_MIDGREY, 16);
    imageProfile4 = new WImage(x, y,  AssetManager.getImage("pingu"+1));
    selectProfile4 = new WButton(480-40, y+4, 48, 48);
    selectProfile4.setBackground(new WImage(AssetManager.getImage("check")));

    y += liSize;

    nameProfile5 = new WLabel("profile 5", x+48+8, y-8, 24);
    nameSubProfile5 = new WLabel("Ai 1", x+48+8, y+24, UIConsts.COLOR_MIDGREY, 16);
    imageProfile5 = new WImage(x, y,  AssetManager.getImage("pingu"+1));
    selectProfile5 = new WButton(480-40, y+4, 48, 48);
    selectProfile5.setBackground(new WImage(AssetManager.getImage("check")));

    y += liSize;

    nameProfile6 = new WLabel("profile 6", x+48+8, y-8, 24);
    nameSubProfile6 = new WLabel("Ai 1", x+48+8, y+24, UIConsts.COLOR_MIDGREY, 16);
    imageProfile6 = new WImage(x, y,  AssetManager.getImage("pingu"+1));
    selectProfile6 = new WButton(480-40, y+4, 48, 48);
    selectProfile6.setBackground(new WImage(AssetManager.getImage("check")));

    y += liSize;

    y = 16;
    x = 480+24;
    liSize = 64;

    nameProfile7 = new WLabel("profile 7", x+48+8, y-8, 24);
    nameSubProfile7 = new WLabel("Ai 1", x+48+8, y+24, UIConsts.COLOR_MIDGREY, 16);
    imageProfile7 = new WImage(x, y,  AssetManager.getImage("pingu"+1));
    selectProfile7 = new WButton(x+480-40, y+4, 48, 48);
    selectProfile7.setBackground(new WImage(AssetManager.getImage("check")));

    y += liSize;

    nameProfile8 = new WLabel("profile 8", x+48+8, y-8, 24);
    nameSubProfile8 = new WLabel("Ai 1", x+48+8, y+24, UIConsts.COLOR_MIDGREY, 16);
    imageProfile8 = new WImage(x, y,  AssetManager.getImage("pingu"+1));
    selectProfile8 = new WButton(x+480-40, y+4, 48, 48);
    selectProfile8.setBackground(new WImage(AssetManager.getImage("check")));

    y += liSize;

    nameProfile9 = new WLabel("profile 9", x+48+8, y-8, 24);
    nameSubProfile9 = new WLabel("Ai 1", x+48+8, y+24, UIConsts.COLOR_MIDGREY, 16);
    imageProfile9 = new WImage(x, y,  AssetManager.getImage("pingu"+1));
    selectProfile9 = new WButton(x+480-40, y+4, 48, 48);
    selectProfile9.setBackground(new WImage(AssetManager.getImage("check")));

    y += liSize;

    nameProfile10 = new WLabel("profile 10", x+48+8, y-8, 24);
    nameSubProfile10 = new WLabel("Ai 1", x+48+8, y+24, UIConsts.COLOR_MIDGREY, 16);
    imageProfile10 = new WImage(x, y,  AssetManager.getImage("pingu"+1));
    selectProfile10 = new WButton(x+480-40, y+4, 48, 48);
    selectProfile10.setBackground(new WImage(AssetManager.getImage("check")));

    y += liSize;

    nameProfile11 = new WLabel("profile 11", x+48+8, y-8, 24);
    nameSubProfile11 = new WLabel("Ai 1", x+48+8, y+24, UIConsts.COLOR_MIDGREY, 16);
    imageProfile11 = new WImage(x, y,  AssetManager.getImage("pingu"+1));
    selectProfile11 = new WButton(x+480-40, y+4, 48, 48);
    selectProfile11.setBackground(new WImage(AssetManager.getImage("check")));

    y += liSize;


    body.addToContainer(nameProfile1);
    body.addToContainer(nameSubProfile1);
    body.addToContainer(imageProfile1);
    body.addToContainer(selectProfile1);
    body.addToContainer(nameProfile2);
    body.addToContainer(nameSubProfile2);
    body.addToContainer(imageProfile2);
    body.addToContainer(selectProfile2);
    body.addToContainer(nameProfile3);
    body.addToContainer(nameSubProfile3);
    body.addToContainer(imageProfile3);
    body.addToContainer(selectProfile3);
    body.addToContainer(nameProfile4);
    body.addToContainer(nameSubProfile4);
    body.addToContainer(imageProfile4);
    body.addToContainer(selectProfile4);
    body.addToContainer(nameProfile5);
    body.addToContainer(nameSubProfile5);
    body.addToContainer(imageProfile5);
    body.addToContainer(selectProfile5);
    body.addToContainer(nameProfile6);
    body.addToContainer(nameSubProfile6);
    body.addToContainer(imageProfile6);
    body.addToContainer(selectProfile6);

    body.addToContainer(nameProfile7);
    body.addToContainer(nameSubProfile7);
    body.addToContainer(imageProfile7);
    body.addToContainer(selectProfile7);
    body.addToContainer(nameProfile8);
    body.addToContainer(nameSubProfile8);
    body.addToContainer(imageProfile8);
    body.addToContainer(selectProfile8);
    body.addToContainer(nameProfile9);
    body.addToContainer(nameSubProfile9);
    body.addToContainer(imageProfile9);
    body.addToContainer(selectProfile9);
    body.addToContainer(nameProfile10);
    body.addToContainer(nameSubProfile10);
    body.addToContainer(imageProfile10);
    body.addToContainer(selectProfile10);
    body.addToContainer(nameProfile11);
    body.addToContainer(nameSubProfile11);
    body.addToContainer(imageProfile11);
    body.addToContainer(selectProfile11);

    if(manageProfiles == null) manageProfiles = new ProfileManager();

    makeProfiles();

    /*
     * AI | distant | Existant
     * 1 1 1
     *
     * AI = 5
     * distant = 3
     * AI Distant = 7
     */


    /*for (Profile p : manageProfiles.getList()) {
      //if(p.getName().compareTo(LoadInfo.getProfile().getName()) == 0) {
      if(p.getName().compareTo(LoadInfo.ACTUALPROFILE.getName()) == 0) {
      //if(p.getName().compareTo("Robert") == 0) {
        body.addToContainer(new WImage(dec-54, i, 48, 48, AssetManager.getImage("checked")));
      } else {
        body.addToContainer(new WImage(dec-54, i, 48, 48, AssetManager.getImage("check")));
      }
      //body.addToContainer(changeb);
      WListItem wli = new WListItem(0,0,480,64); // Create a list item

      int type = 1;

      try {
        if(p.getDistant()) type += 2;
        if(p.getAI()) type += 4;
      } catch (Exception e) {

      }
      switch (type) {
        case 1:
          type = 1; //green, local
          break;
        case 3:
          type = 2; //brown, remote
          break;
        case 5:
          type = 5; //green, AI
          break;
        case 7:
          type = 3; //blue, remote AI
          break;
        default:
          type = 4; // Grey
      }
      wsi = new WStatusIcon(12,12,  AssetManager.getImage("pingu"+type)); // Icon
      WLabel Sip = new WLabel(p.getName(), 0, 8, 12, UIConsts.COLOR_MIDGREY, 16); //Text component
      wil = new WIconLayout(12, 0, wsi, Sip); // adding the text and the icon together
      wli.setInner(wil); // setting the elements into the list Item
      profileList.add(wli); //push into the list
      i+=64;
    }*/

    body.addToContainer(profileList);

    return body;
  }

  private static void applyChanges() {
    Menu.refresh();
    removeBinds();
    //setActivity(new ProfileList());
    setCard(ProfileList.make_card(dec, SizeC+88+72));
  }

  private static int getType(Profile p) {
    int type = 1;

    try {
      if(p.getDistant()) type += 2;
      if(p.getAI()) type += 4;
    } catch (Exception e) {

    }
    switch (type) {
      case 1:
        type = 1; //green, local
        break;
      case 3:
        type = 2; //brown, remote
        break;
      case 5:
        type = 5; //green, AI
        break;
      case 7:
        type = 3; //blue, remote AI
        break;
      default:
        type = 4; // Grey
    }
    return type;
  }

  private static void makeProfiles() {
    removeClickBinding(actions.get("selectProfile1"));
    removeClickBinding(actions.get("selectProfile2"));
    removeClickBinding(actions.get("selectProfile3"));
    removeClickBinding(actions.get("selectProfile4"));
    removeClickBinding(actions.get("selectProfile5"));
    removeClickBinding(actions.get("selectProfile6"));

    removeClickBinding(actions.get("selectProfile7"));
    removeClickBinding(actions.get("selectProfile8"));
    removeClickBinding(actions.get("selectProfile9"));
    removeClickBinding(actions.get("selectProfile10"));
    removeClickBinding(actions.get("selectProfile11"));

    selectProfile1.hide();
    selectProfile2.hide();
    selectProfile3.hide();
    selectProfile4.hide();
    selectProfile5.hide();
    selectProfile6.hide();

    selectProfile7.hide();
    selectProfile8.hide();
    selectProfile9.hide();
    selectProfile10.hide();
    selectProfile11.hide();

    nameProfile1.hide();
    nameProfile2.hide();
    nameProfile3.hide();
    nameProfile4.hide();
    nameProfile5.hide();
    nameProfile6.hide();

    nameProfile7.hide();
    nameProfile8.hide();
    nameProfile9.hide();
    nameProfile10.hide();
    nameProfile11.hide();

    nameSubProfile1.hide();
    nameSubProfile2.hide();
    nameSubProfile3.hide();
    nameSubProfile4.hide();
    nameSubProfile5.hide();
    nameSubProfile6.hide();

    nameSubProfile7.hide();
    nameSubProfile8.hide();
    nameSubProfile9.hide();
    nameSubProfile10.hide();
    nameSubProfile11.hide();

    imageProfile1.hide();
    imageProfile2.hide();
    imageProfile3.hide();
    imageProfile4.hide();
    imageProfile5.hide();
    imageProfile6.hide();

    imageProfile7.hide();
    imageProfile8.hide();
    imageProfile9.hide();
    imageProfile10.hide();
    imageProfile11.hide();

    int i = 0;
    ArrayList<Profile> profiles = new ArrayList<>();
    manageProfiles.getList().stream().forEach(x -> profiles.add(x));

    if(profiles != null && profiles.size() > deb + 0) {
      int sz = profiles.size() - deb;

      if(sz >= 11) {
        Profile p = profiles.get(deb + 10);
        nameSubProfile11.setCaption((p.getAI())?p.getAIType():"Human");
        nameSubProfile11.show();
        nameProfile11.setCaption(p.getName());
        nameProfile11.show();
        if(LoadInfo.ACTUALPROFILE == p) {
          selectProfile11.setBackground(new WImage(AssetManager.getImage("checked")));
          removeClickBinding(actions.get("selectProfile11"));
        } else {
          selectProfile11.setAction(() -> {
            LoadInfo.ACTUALPROFILE = p;
            applyChanges();
          });
          actions.put("selectProfile11", selectProfile11.getActionZone());
          addClickBinding(actions.get("selectProfile11"));
        }
        selectProfile11.show();
        imageProfile11.setImg(AssetManager.getImage("pingu"+getType(p)));
        imageProfile11.show();
      }
      if(sz >= 10) {
        Profile p = profiles.get(deb + 9);
        nameSubProfile10.setCaption((p.getAI())?p.getAIType():"Human");
        nameSubProfile10.show();
        nameProfile10.setCaption(p.getName());
        nameProfile10.show();
        if(LoadInfo.ACTUALPROFILE == p) {
          selectProfile10.setBackground(new WImage(AssetManager.getImage("checked")));
          removeClickBinding(actions.get("selectProfile10"));
        } else {
          selectProfile10.setAction(() -> {
            LoadInfo.ACTUALPROFILE = p;
            applyChanges();
          });
          actions.put("selectProfile10", selectProfile10.getActionZone());
          addClickBinding(actions.get("selectProfile10"));
        }
        selectProfile10.show();
        imageProfile10.setImg(AssetManager.getImage("pingu"+getType(p)));
        imageProfile10.show();
      }
      if(sz >= 9) {
        Profile p = profiles.get(deb + 8);
        nameSubProfile9.setCaption((p.getAI())?p.getAIType():"Human");
        nameSubProfile9.show();
        nameProfile9.setCaption(p.getName());
        nameProfile9.show();
        if(LoadInfo.ACTUALPROFILE == p) {
          selectProfile9.setBackground(new WImage(AssetManager.getImage("checked")));
          removeClickBinding(actions.get("selectProfile9"));
        } else {
          selectProfile9.setAction(() -> {
            LoadInfo.ACTUALPROFILE = p;
            applyChanges();
          });
          actions.put("selectProfile9", selectProfile9.getActionZone());
          addClickBinding(actions.get("selectProfile9"));
        }
        selectProfile9.show();
        imageProfile9.setImg(AssetManager.getImage("pingu"+getType(p)));
        imageProfile9.show();
      }
      if(sz >= 8) {
        Profile p = profiles.get(deb + 7);
        nameSubProfile8.setCaption((p.getAI())?p.getAIType():"Human");
        nameSubProfile8.show();
        nameProfile8.setCaption(p.getName());
        nameProfile8.show();
        if(LoadInfo.ACTUALPROFILE == p) {
          selectProfile8.setBackground(new WImage(AssetManager.getImage("checked")));
          removeClickBinding(actions.get("selectProfile8"));
        } else {
          selectProfile8.setAction(() -> {
            LoadInfo.ACTUALPROFILE = p;
            applyChanges();
          });
          actions.put("selectProfile8", selectProfile8.getActionZone());
          addClickBinding(actions.get("selectProfile8"));
        }
        selectProfile8.show();
        imageProfile8.setImg(AssetManager.getImage("pingu"+getType(p)));
        imageProfile8.show();
      }
      if(sz >= 7) {
        Profile p = profiles.get(deb + 6);
        nameSubProfile7.setCaption((p.getAI())?p.getAIType():"Human");
        nameSubProfile7.show();
        nameProfile7.setCaption(p.getName());
        nameProfile7.show();
        if(LoadInfo.ACTUALPROFILE == p) {
          selectProfile7.setBackground(new WImage(AssetManager.getImage("checked")));
          removeClickBinding(actions.get("selectProfile7"));
        } else {
          selectProfile7.setAction(() -> {
            LoadInfo.ACTUALPROFILE = p;
            applyChanges();
          });
          actions.put("selectProfile7", selectProfile7.getActionZone());
          addClickBinding(actions.get("selectProfile7"));
        }
        selectProfile7.show();
        imageProfile7.setImg(AssetManager.getImage("pingu"+getType(p)));
        imageProfile7.show();
      }
      if(sz >= 6) {
        Profile p = profiles.get(deb + 5);
        nameSubProfile6.setCaption((p.getAI())?p.getAIType():"Human");
        nameSubProfile6.show();
        nameProfile6.setCaption(p.getName());
        nameProfile6.show();
        if(LoadInfo.ACTUALPROFILE == p) {
          selectProfile6.setBackground(new WImage(AssetManager.getImage("checked")));
          removeClickBinding(actions.get("selectProfile6"));
        } else {
          selectProfile6.setAction(() -> {
            LoadInfo.ACTUALPROFILE = p;
            applyChanges();
          });
          actions.put("selectProfile6", selectProfile6.getActionZone());
          addClickBinding(actions.get("selectProfile6"));
        }
        selectProfile6.show();
        imageProfile6.setImg(AssetManager.getImage("pingu"+getType(p)));
        imageProfile6.show();
      }
      if(sz >= 5) {
        Profile p = profiles.get(deb + 4);
        nameSubProfile5.setCaption((p.getAI())?p.getAIType():"Human");
        nameSubProfile5.show();
        nameProfile5.setCaption(p.getName());
        nameProfile5.show();
        if(LoadInfo.ACTUALPROFILE == p) {
          selectProfile5.setBackground(new WImage(AssetManager.getImage("checked")));
          removeClickBinding(actions.get("selectProfile5"));
        } else {
          selectProfile5.setAction(() -> {
            LoadInfo.ACTUALPROFILE = p;
            applyChanges();
          });
          actions.put("selectProfile5", selectProfile5.getActionZone());
          addClickBinding(actions.get("selectProfile5"));
        }
        selectProfile5.show();
        imageProfile5.setImg(AssetManager.getImage("pingu"+getType(p)));
        imageProfile5.show();
      }
      if(sz >= 4) {
        Profile p = profiles.get(deb + 3);
        nameSubProfile4.setCaption((p.getAI())?p.getAIType():"Human");
        nameSubProfile4.show();
        nameProfile4.setCaption(p.getName());
        nameProfile4.show();
        if(LoadInfo.ACTUALPROFILE == p) {
          selectProfile4.setBackground(new WImage(AssetManager.getImage("checked")));
          removeClickBinding(actions.get("selectProfile4"));
        } else {
          selectProfile4.setAction(() -> {
            LoadInfo.ACTUALPROFILE = p;
            applyChanges();
          });
          actions.put("selectProfile4", selectProfile4.getActionZone());
          addClickBinding(actions.get("selectProfile4"));
        }
        selectProfile4.show();
        imageProfile4.setImg(AssetManager.getImage("pingu"+getType(p)));
        imageProfile4.show();
      }
      if(sz >= 3) {
        Profile p = profiles.get(deb + 2);
        nameSubProfile3.setCaption((p.getAI())?p.getAIType():"Human");
        nameSubProfile3.show();
        nameProfile3.setCaption(p.getName());
        nameProfile3.show();
        if(LoadInfo.ACTUALPROFILE == p) {
          selectProfile3.setBackground(new WImage(AssetManager.getImage("checked")));
          removeClickBinding(actions.get("selectProfile3"));
        } else {
          selectProfile3.setAction(() -> {
            LoadInfo.ACTUALPROFILE = p;
            applyChanges();
          });
          actions.put("selectProfile3", selectProfile3.getActionZone());
          addClickBinding(actions.get("selectProfile3"));
        }
        selectProfile3.show();
        imageProfile3.setImg(AssetManager.getImage("pingu"+getType(p)));
        imageProfile3.show();
      }
      if(sz >= 2) {
        Profile p = profiles.get(deb + 1);
        nameSubProfile2.setCaption((p.getAI())?p.getAIType():"Human");
        nameSubProfile2.show();
        nameProfile2.setCaption(p.getName());
        nameProfile2.show();
        if(LoadInfo.ACTUALPROFILE == p) {
          selectProfile2.setBackground(new WImage(AssetManager.getImage("checked")));
          removeClickBinding(actions.get("selectProfile2"));
        } else {
          selectProfile2.setAction(() -> {
            LoadInfo.ACTUALPROFILE = p;
            applyChanges();
          });
          actions.put("selectProfile2", selectProfile2.getActionZone());
          addClickBinding(actions.get("selectProfile2"));
        }
        selectProfile2.show();
        imageProfile2.setImg(AssetManager.getImage("pingu"+getType(p)));
        imageProfile2.show();
      }
      if(sz >= 1) {
        Profile p = profiles.get(deb);
        nameSubProfile1.setCaption((p.getAI())?p.getAIType():"Human");
        nameSubProfile1.show();
        nameProfile1.setCaption(p.getName());
        nameProfile1.show();
        if(LoadInfo.ACTUALPROFILE == p) {
          selectProfile1.setBackground(new WImage(AssetManager.getImage("checked")));
          removeClickBinding(actions.get("selectProfile1"));
        } else {
          selectProfile1.setAction(() -> {
            LoadInfo.ACTUALPROFILE = p;
            applyChanges();
          });
          actions.put("selectProfile1", selectProfile1.getActionZone());
          addClickBinding(actions.get("selectProfile1"));
        }
        selectProfile1.show();
        imageProfile1.setImg(AssetManager.getImage("pingu"+getType(p)));
        imageProfile1.show();
      }

    }

  }

  @Override
  public void BeforeAdd() {
    SizeC = profilsC*64;
    if(LoadInfo.ACTUALPROFILE == null) LoadInfo.ACTUALPROFILE = LoadInfo.PROFILES.get(0);
    initProfiles(); // FAKE PROFILE FILLER => CHANGE ME
  }

  public ProfileList() {
    super();
    //init();
  }
}
