package com.pingu.party.activities;

import com.pingu.party.engine.game.Profile;
import com.pingu.party.engine.game.ProfileManager;

import com.pingu.party.engine.input.ListenZone;
import java.util.Map;

import com.pingu.party.engine.core.AssetManager;
import com.pingu.party.gui.widget.WButton;
import com.pingu.party.gui.widget.WCardWrapper;
import com.pingu.party.gui.widget.WCheckbox;
import com.pingu.party.gui.widget.WIconActionLayout;
import com.pingu.party.gui.widget.WIconLayout;
import com.pingu.party.gui.widget.WImage;
import com.pingu.party.gui.widget.WLabel;
import com.pingu.party.gui.widget.WList;
import com.pingu.party.gui.widget.WListItem;
import com.pingu.party.gui.widget.WSelector;
import com.pingu.party.gui.widget.WStatusIcon;
import com.pingu.party.gui.widget.WTitledText;
import com.pingu.party.launcher.Utils.UIConsts;
import java.util.ArrayList;

import com.pingu.party.engine.config.Config;
public class Local extends BaseActivity {

  private static WSelector<String> ws;
  private static WSelector<String> wia;
  private static ArrayList<WSelector<Profile>> wias;
  private static WList usersList;
  private static int sz=0;
  private static ArrayList<Boolean> clicked = new ArrayList<>(7);
  private static WCheckbox wBlitz;
  private static WSelector<String> wTime;
  private static String sTime = "05:30";

  private static ProfileManager pm;

  private static int time = 330;

  private static void startPartie(int players) {
   // int iac = Integer.parseInt(wia.getCurrent().toString());
    int cnt = Integer.parseInt(ws.getCurrent().toString());
    int p = 0;

    int min = time/60;
    int sec = time%60;
    boolean blitz = wBlitz.isChecked();

    String s = "Time : "+min+":"+sec+" , "+((blitz)?"Blitz Mode":"Normal mode");
    System.out.println(s);
    Logger.info(s, 2000);

   // System.out.println(iac);
    /*for (WSelector<Profile> w : wias) {
      if(p>=cnt) break;
      System.out.println(w.getCurrent().getName() + " AI:"+w.getCurrent().getAI());
      p++;
    }*/
	int nbAI=0;
	String playerName[]=new String[cnt];
	boolean AiorNot[]=new boolean[cnt];

  ArrayList<String> SelectedUUID = new ArrayList<>();
  int doLaunch = 1;

  for(WSelector<Profile> w : wias)
	{
		if(p>=cnt) break;
    if(SelectedUUID.contains(w.getCurrent().getUUID())) doLaunch = 0;
    SelectedUUID.add(w.getCurrent().getUUID());
		if(w.getCurrent().getAI())
			nbAI++;
		//playerName[p]=w.getCurrent().getName();
		AiorNot[p]=w.getCurrent().getAI();
		p++;
	}
	
	ArrayList<Profile> SelectedProfile = new ArrayList<>();
	p=0;
	for(WSelector<Profile> w : wias)
	{
		if(p>=cnt) break;
		SelectedProfile.add(w.getCurrent());
		p++;
	}
	//System.out.println("Hey :"+Selected.size());
	boolean gg=false;
	if(Config.get("DO_REPLAY")==1)
		gg=true;
    //if(doLaunch == 1) setActivity(new Partie(players,AiorNot,SelectedProfile,nbAI,time,blitz,gg));
    if(doLaunch == 1) setActivity(new Partie(players,AiorNot,SelectedProfile,nbAI,time,blitz,gg));
    else Logger.warn("Duplicated profile selected", 2000);
  }

  public static void removeBinds() {
    removeClickBinding(actions.get("Return"));
    removeClickBinding(actions.get("Start"));
    removeClickBinding(actions.get("AI_Bnext"));
    removeClickBinding(actions.get("Bnext"));
    removeClickBinding(actions.get("AI_Bprev"));
    removeClickBinding(actions.get("Bprev"));
    removeClickBinding(actions.get("isBlitz"));
    removeClickBinding(actions.get("Tnext"));
    removeClickBinding(actions.get("Tprev"));
    for (WSelector<Profile> w : wias) {
      if(hasClickBinding(w.getNextZone())) removeClickBinding(w.getNextZone());
      if(hasClickBinding(w.getPrevZone())) removeClickBinding(w.getPrevZone());
    }
    for (Map.Entry<String, ListenZone> e : actions.entrySet()) {
      String name = e.getKey();
      if(name.startsWith("check")) {
        ListenZone lz = e.getValue();
        removeClickBinding(lz);
        //lz.disable();
      }
    }
    //removeClickBinding(actions.get("Settings"));
  }

  public static void newWin() {
    setCard(Menu.make_card(0, 0));
    Menu.wPlay = Menu.createPlayButtons();
  	pushAdded();
  }

  private static void removeUser(int index) {
    int cur = Integer.parseInt(wia.getCurrent().toString());
    int cnt = Integer.parseInt(ws.getCurrent().toString());
    if(sz<=2) return;
    int p = 0;
    for (WSelector<Profile> w : wias) {
      if(p>=cnt){
        if(hasClickBinding(w.getNextZone())) removeClickBinding(w.getNextZone());
        if(hasClickBinding(w.getPrevZone())) removeClickBinding(w.getPrevZone());
        w.hide();
      }
      p++;
    }
    if(actions.containsKey("check"+sz)) removeClickBinding(actions.remove("check"+sz));
    sz--;
    if(cur>sz) checkIACount(-1);
    usersList.remove(usersList.getItemsCount()-1);
    usersList.recalc();
  }

  private static void plop(int i) {
    System.out.println("Index  : "+i);
    if(clicked.isEmpty()) {
      System.out.println("EMPTY");
      for (int y = 0; y< 7;y++ ) {
        clicked.add(false);
      }
    }
    clicked.set(i, !(clicked.get(i)));
    System.out.println("Val  : "+clicked.get(i));
  }

  private static void initProfiles() {
    pm = new ProfileManager();
  }

  private static void makeUsersCard(String name) {
    WStatusIcon wsi;
    WIconActionLayout wil;
    WTitledText wtt;

    WListItem wli = new WListItem(0,0,dec,64);
    wsi = new WStatusIcon(12,12, "res/UI/menu/serv.png");
    wtt = new WTitledText(6,8,256,24,0, ""+sz);
    WLabel Sip = new WLabel("S:"+name, 0, 0, 12, UIConsts.COLOR_MIDGREY, 16);
    WListItem wttChild = new WListItem(6,0,256,24);
    wttChild.setInner(Sip);
    wtt.add(wttChild);
    WListItem wttChild2 = new WListItem(6,0,256,24);
    wttChild2.setInner(new WLabel("s:"+name, 0, 0, 12, UIConsts.COLOR_MIDGREY, 16));

    wil = new WIconActionLayout(12, 0, wsi, wtt);
    wil.setIconWidth(64);
    wli.setInner(wil);

    /*WCheckbox wcb = new WCheckbox(8,8,48,48,false);
    int i = sz;
    wcb.setAction(()->{
      wcb.toggle();
      plop(i);
    });
    if(actions.containsKey("check"+sz)) actions.remove("check"+sz);
    actions.put("check"+sz, wcb.getActionZone());
    wcb.setAction(actions.get("check"+sz).action());
    addClickBinding(wcb.getActionZone());

    wil.setAction(wcb);*/

    usersList.add(wli);
    usersList.recalc();
  }

  private static void addUser() {
    int cnt = Integer.parseInt(ws.getCurrent().toString());
    if(sz>=cnt) return;
    int p = 0;
    for (WSelector<Profile> w : wias) {
      if(p<cnt){
        if(! hasClickBinding(w.getNextZone())) addClickBinding(w.getNextZone());
        if(! hasClickBinding(w.getPrevZone())) addClickBinding(w.getPrevZone());

        System.out.println("Pos : "+p + " at "+w.getPos());

        int i = w.getPos();
        if(i!=p) {
          for (int j = i; j < p; j++) {

            w.step(1);
          }
        }

        System.out.println("Pos : "+p + " at "+w.getPos());
        //if(p<w.getSize()) w.step(p);
        w.show();
      }
      p++;
    }
    //if(usersList.getItemsCount()>=ws.getSize()) return;

    cnt -= sz;
    while(cnt-->0) {
      makeUsersCard(""+sz++);
    }

  }

  private static void timeStep(int t) {
    if (!(time<20 && t<0)) {
      if(wTime.getPos()<1) wTime.step(1);
      time += t;
      if(time==10 && t<0) wTime.step(-1);
      int m = time/60;
      int s = time%60;
      sTime = ""+((m<10)?"0"+m:m)+":"+((s<10)?"0"+s:s);
      wTime.hackCaption(sTime);
    }
  }

  private static void wstep(WSelector<Profile> i, int v) {
    if(pm.getList().size() <6) {
      i.step(v);
    } else {
      i.step(v);
    }
  }

  private static boolean checkIACount(int i) {
    int cnt = Integer.parseInt(ws.getCurrent().toString());
    int cur = Integer.parseInt(wia.getCurrent().toString());
    if(cur>cnt) {
      int d = cur-cnt;
      while(d-->0) {
        wia.step(-1);
      }
    }
    if(cur+i>cnt || cur+i<0) return false;
    return true;
  }

  public static WCardWrapper make_card(int dec, int wid) {
    initProfiles();

    WCardWrapper body = new WCardWrapper(0, 0,dec, wid, AssetManager.getImage("SoloT"));
    Runnable rb = (() -> {
      newWin();
      removeBinds();
      //setActivity(new Menu());
    });
    WButton returnb = new WButton(12, wid-72-64, 326, 64, 0);
    returnb.setBackground(new WImage(0, 0, 326, 64, AssetManager.getImage("returnBHT")));

    returnb.setAction(rb);
    actions.put("Return", returnb.getActionZone());
    returnb.setAction(actions.get("Return").action());
    addClickBinding(returnb.getActionZone());

    body.addToContainer(returnb);

    Runnable rs = (() -> {
      int pl = Integer.parseInt(ws.getCurrent().toString());
      startPartie(pl);
    });

    WButton startb = new WButton(dec-12-326, wid-72-64, 326, 64, 0);
    startb.setBackground(new WImage(0, 0, 326, 64, AssetManager.getImage("startBHT")));

    startb.setAction(rs);
    actions.put("Start", startb.getActionZone());
    startb.setAction(actions.get("Start").action());
    addClickBinding(startb.getActionZone());

    body.addToContainer(startb);

    WLabel wBlitzText = new WLabel("Mode Blitz", 80, wid-72-64-50, 16, UIConsts.COLOR_FOREGROUND, 24);
    body.addToContainer(wBlitzText);

    wBlitz = new WCheckbox(20, wid-72-64*2, 48, 48);
    wBlitz.setAction(()->{
      if (wBlitz.isChecked()) {
        wTime.hide();
        removeClickBinding(wTime.getNextZone());
        removeClickBinding(wTime.getPrevZone());
      } else {
        wTime.show();
        addClickBinding(wTime.getNextZone());
        addClickBinding(wTime.getPrevZone());
      }
      wBlitz.toggle();
    });
    actions.put("isBlitz", wBlitz.getActionZone());
    wBlitz.setAction(actions.get("isBlitz").action());
    addClickBinding(wBlitz.getActionZone());
    body.addToContainer(wBlitz);

    wTime = new WSelector<String>(dec-338-12, wid-72-64*2, false, false);
    wTime.setNextAction(() -> { timeStep(10); });
    actions.put("Tnext", wTime.getNextZone());
    wTime.setNextAction(actions.get("Tnext").action());
    //addClickBinding(wTime.getNextZone());
    wTime.setPrevAction(() -> { timeStep(-10); });
    actions.put("Tprev", wTime.getPrevZone());
    wTime.setPrevAction(actions.get("Tprev").action());
    //addClickBinding(wTime.getPrevZone());

    wTime.add(sTime);
    wTime.add(sTime);
    wTime.add(sTime);
    wTime.step(1);
    // Sorry for that dirty hack

    wTime.hide();

    body.addToContainer(wTime);

    ws = new WSelector<String>(dec-338-12, -64, true, false);

    ws.setNextAction( () -> {ws.step(1);addUser();});
    actions.put("Bnext", ws.getNextZone());
    ws.setNextAction(actions.get("Bnext").action());
    addClickBinding(ws.getNextZone());

    ws.setPrevAction( () -> {ws.step(-1);removeUser(ws.getPos());});
    actions.put("Bprev", ws.getPrevZone());
    ws.setPrevAction(actions.get("Bprev").action());
    addClickBinding(ws.getPrevZone());

    ws.add("2");
    ws.add("3");
    ws.add("4");
    ws.add("5");
    ws.add("6");
    sz=0;
    //ws.add("SomeVeryLong");

    //getCurrent
    body.addToContainer(ws);

    wias = new ArrayList<WSelector<Profile>>();

    int py = 8;
    int pl = Integer.parseInt(ws.getCurrent().toString());
    for (int i=0;i<6 ;i++) {
      WSelector<Profile> w = new WSelector<Profile>(dec-338-12, py+8, false, false);

      w.setNextAction( () -> wstep(w, 1));
      actions.put("P_Bnext_"+i, w.getNextZone());
      w.setNextAction(actions.get("P_Bnext_"+i).action());
      addClickBinding(w.getNextZone());

      w.setPrevAction( () -> wstep(w, -1));
      actions.put("P_Bprev_"+i, w.getPrevZone());
      w.setPrevAction(actions.get("P_Bprev_"+i).action());
      addClickBinding(w.getPrevZone());

      //TODO: Change Me !
      pm.getList().forEach(p -> {
        w.add(p);
      });

      /*w.add("Profile 0");
      w.add("Profile 1");
      w.add("Profile 2");
      w.add("Profile 3");
      w.add("Profile 4");
      w.add("Profile 5");
      w.add("Profile 6");*/

      if(i>=pl) {
        removeClickBinding(w.getNextZone());
        removeClickBinding(w.getPrevZone());
        w.hide();
      }
      body.addToContainer(w);
      wias.add(w);
      py += 64;
    }

    wia = new WSelector<String>(dec-338-12, 8, false, false);

    wia.setNextAction( () -> {if(checkIACount(1)) wia.step(1);});
    actions.put("AI_Bnext", wia.getNextZone());
    wia.setNextAction(actions.get("AI_Bnext").action());
    addClickBinding(wia.getNextZone());

    wia.setPrevAction( () -> {if(checkIACount(-1)) wia.step(-1);});
    actions.put("AI_Bprev", wia.getPrevZone());
    wia.setPrevAction(actions.get("AI_Bprev").action());
    addClickBinding(wia.getPrevZone());

    wia.add("0");
    wia.add("1");
    wia.add("2");
    wia.add("3");
    wia.add("4");
    wia.add("5");
    wia.add("6");

    //body.addToContainer(wia);

    usersList = new WList(0,0,dec, 0, 8);

    body.addToContainer(usersList);

    addUser();

    return body;
  }

  @Override
  public void BeforeAdd() {
    SizeC = 10*64;
  }

  public Local() {
    super();
  }

}
