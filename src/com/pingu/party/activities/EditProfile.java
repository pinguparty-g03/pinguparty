package com.pingu.party.activities;

import com.pingu.party.engine.core.StringManager;

import com.pingu.party.engine.core.AssetManager;

import com.pingu.party.engine.config.LoadInfo;

import com.pingu.party.engine.game.Profile;
import com.pingu.party.engine.game.ProfileManager;

import javax.swing.JOptionPane;

import java.util.HashMap;
import com.pingu.party.engine.input.ListenZone;
import com.pingu.party.activities.base.Viewport;
import com.pingu.party.engine.config.Config;
import com.pingu.party.engine.core.GameCanvas;
import com.pingu.party.gui.widget.WBackground;
import com.pingu.party.gui.widget.WCardTitle;
import com.pingu.party.gui.widget.WImage;
import com.pingu.party.gui.widget.WLabel;
import com.pingu.party.gui.widget.WList;
import com.pingu.party.gui.widget.WListItem;
import com.pingu.party.gui.widget.WPoly;
import com.pingu.party.gui.widget.WBox;
import com.pingu.party.gui.widget.WProgress;
import com.pingu.party.gui.widget.WStatusIcon;
import com.pingu.party.gui.widget.WTitle;
import com.pingu.party.gui.widget.WTitleBar;
import com.pingu.party.gui.widget.Widget;
import com.pingu.party.launcher.Utils.Font.Roboto;
import com.pingu.party.launcher.Utils.UIConsts;
import com.pingu.party.network.structures.IP;
import com.pingu.party.network.structures.Server;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import com.pingu.party.gui.widget.WButton;
import com.pingu.party.gui.widget.WCardWrapper;

class Stat {
  public String lolTest;
  public Stat(String s) {
    this.lolTest = s;
  }
}

/**
 * @author bastien
 * @author florian
 *
 */
public class EditProfile extends BaseActivity {
  private Color almostGrey = new Color(72, 87, 92, 204);
  private Widget w;
  private Widget WCard;

  // Calculated var
  private static int dec;

  public static Profile editing;

    private static String name ;
    private static String uuid ;
    private static String image;
    private static Stat stats;
    protected static boolean distant;
    protected static boolean imAI;
    protected static int level;

    private void fillIt() {
      name = "Name";
      uuid = "uuid";
      image = "res/UI/pingus/pingu1.png";
      stats = new Stat("pingu");
      distant = false;
      imAI = false;
      level = 42;
    }

    private static WLabel LabelName;
    private static WLabel LabelUuid;
    private static WLabel LabelImage;
    private static WLabel LabelLevel;

    public static void removeBinds() {
      removeClickBinding(actions.get("Return"));
    	removeClickBinding(actions.get("Settings"));
      removeClickBinding(actions.get("Edit"));
    	removeClickBinding(actions.get("Edit Image"));
    	removeClickBinding(actions.get("Edit Level"));
    	removeClickBinding(actions.get("create"));
    }

    @Override
    public WCardWrapper makeC(int dec, int wid) {
      return make_card(dec, wid);
    }


    public static WCardWrapper make_card_from(Profile p) {
        editing = p;
        dec = Config.WIDTH - 96 - 96;
        removeBinds();
        return make_card(dec, SizeC+88+96);

    }

    public static WCardWrapper make_card(int dec, int wid) {
      editing = LoadInfo.ACTUALPROFILE;
      WCardWrapper body = new WCardWrapper(0, 0,dec, wid, AssetManager.getImage("editT"));
      Runnable rb = (() -> {
        removeBinds();
        setCard(ProfileList.make_card(BaseActivity.dec, BaseActivity.SizeC+88+72));
        //setActivity(new ProfileList());
      });
      WButton returnb = new WButton(12, wid-72-64, 326, 64, 0);
      returnb.setBackground(new WImage(0, 0, 326, 64, AssetManager.getImage("returnBHT")));

      returnb.setAction(rb);
      actions.put("Return", returnb.getActionZone());
      returnb.setAction(actions.get("Return").action());
      addClickBinding(returnb.getActionZone());

      body.addToContainer(returnb); // Add return button

      if(!editing.getDistant() || editing.getUUID().equals("uuid")) {
        Runnable rp = (() -> {
              String nname = JOptionPane.showInputDialog(StringManager.get("str_enter_name"));
              if(JOptionPane.showConfirmDialog(null, StringManager.get("str_change")+" `"+editing.getName()+"` "+StringManager.get("str_to")+" `"+nname+"` ?", StringManager.get("str_confirm"), JOptionPane.YES_NO_OPTION) == 0) {
                if(!editing.getUUID().equals("uuid")) LoadInfo.ACTUALPROFILE.setName(nname); //SETNAME
                LabelName.setCaption(StringManager.get("str_name") +" : "+nname);
              }
            });

            WButton edit_name = new WButton(dec-54, 16, 48, 48, 0);
            edit_name.setBackground(new WImage(0, 0, 48, 48, AssetManager.getImage("nextH")));
            edit_name.setAction(rp);
            actions.put("Edit", edit_name.getActionZone());
            edit_name.setAction(actions.get("Edit").action());
            addClickBinding(edit_name.getActionZone());

            body.addToContainer(edit_name); // Add return button
      }

      int i = 24;
      LabelName = new WLabel(StringManager.get("str_name")+" : "+editing.getName(), 40, i, UIConsts.COLOR_FOREGROUND, 24);

      body.addToContainer(LabelName);

      i += 64;
      if(!editing.getDistant() || editing.getUUID().equals("uuid")) {
      	Runnable ri = (() -> {
      	      String nimg = JOptionPane.showInputDialog(StringManager.get("str_enter_the_image_path")+" :");
      	      if(JOptionPane.showConfirmDialog(null, StringManager.get("str_change")+" `"+editing.getImage()+"` "+StringManager.get("str_to")+" `"+nimg+"` ?", StringManager.get("str_confirm"), JOptionPane.YES_NO_OPTION) == 0) {
      	    	  if(!editing.getUUID().equals("uuid")) LoadInfo.ACTUALPROFILE.setImage(nimg); //SetImage
      	        LabelImage.setCaption(StringManager.get("str_image") +" : "+nimg);

      	      }
      	    });

      	    // button modif
      	    WButton edit_image = new WButton(dec-54, i-8, 48, 48, 0);
      	    edit_image.setBackground(new WImage(0, 0, 48, 48, AssetManager.getImage("nextH")));
      	    edit_image.setAction(ri);
      	    actions.put("Edit Image", edit_image.getActionZone());
      	    edit_image.setAction(actions.get("Edit Image").action());
      	    addClickBinding(edit_image.getActionZone());

      	    body.addToContainer(edit_image); // Add return button
      	    ///
      }

      LabelImage = new WLabel(StringManager.get("str_image")+": "+editing.getImage(), 40, i, UIConsts.COLOR_FOREGROUND, 24);
      body.addToContainer(LabelImage);

      i += 64;
      if(editing.getAI() && !editing.getDistant()) {
      	Runnable rlevel = (() -> {
      	      String nlvl = JOptionPane.showInputDialog(StringManager.get("str_enter_level"));
      	      if(JOptionPane.showConfirmDialog(null, StringManager.get("str_change")+" `"+editing.getImage()+"` "+StringManager.get("str_to")+" `"+nlvl+"` ?", StringManager.get("str_confirm"), JOptionPane.YES_NO_OPTION) == 0) {
      	    	  LoadInfo.ACTUALPROFILE.setLevel(Integer.parseInt(nlvl)); //SetLevel
      	        LabelImage.setCaption("Image : "+nlvl);
      	      }
      	    });
      	// button modif
          WButton edit_level = new WButton(dec-54, i-8, 48, 48, 0);
          edit_level.setBackground(new WImage(0, 0, 48, 48, AssetManager.getImage("nextH")));
          edit_level.setAction(rlevel);
          actions.put("Edit Level", edit_level.getActionZone());
          edit_level.setAction(actions.get("Edit Level").action());
          addClickBinding(edit_level.getActionZone());

          body.addToContainer(edit_level); // Add level button

      	LabelLevel = new WLabel(StringManager.get("str_level")+" : "+editing.getLevel(), 40, i, UIConsts.COLOR_FOREGROUND, 24);
      	body.addToContainer(LabelLevel);

      }
      if(editing.getUUID().equals("uuid"))
  	{
          Runnable valid = (() -> {
            removeBinds();
          	LoadInfo.PROFILEMANAGER.createProfile(editing.getName(),editing.getImage());
            BaseActivity.setCard(ProfileList.make_card(BaseActivity.dec, BaseActivity.SizeC+88+72));

              //setActivity(new ProfileList());
              //setActivity(new EditProfile());
            });
      	WButton create = new WButton(dec-54, i+16, 48, 48, 0);
      	create.setBackground(new WImage(0, 0, 48, 48, AssetManager.getImage("addH")));
      	create.setAction(valid);
          actions.put("create", create.getActionZone());
          create.setAction(actions.get("create").action());
          addClickBinding(create.getActionZone());
          body.addToContainer(create);
  	}

      return body;
    }

  public Widget createEditCard() {
    int wid = 256+32+8+16+88+64;
    Widget wi = new Widget(96,168, dec, wid, false);
    wi.setHorizontalAlign(1);

    WCardWrapper body = new WCardWrapper(0, 0,dec, wid, AssetManager.getImage("editT"));

    Runnable rb = (() -> {
      removeFromViewport(WCard);
      removeBinds();
      setCard(ProfileList.make_card(BaseActivity.dec, BaseActivity.SizeC+88+72));
      //setActivity(new ProfileList());
    });
    WButton returnb = new WButton(12, wid-72-64, 326, 64, 0);
    returnb.setBackground(new WImage(0, 0, 326, 64, AssetManager.getImage("returnBHT")));

    returnb.setAction(rb);
    actions.put("Return", returnb.getActionZone());
    returnb.setAction(actions.get("Return").action());
    addClickBinding(returnb.getActionZone());

    body.addToContainer(returnb); // Add return button

    Image edit_ico = null;
    try {
      edit_ico = ImageIO.read(new File("res/UI/menu/buttons/Ht/nextH.png"));
    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }
    if(!editing.getDistant() || editing.getUUID().equals("uuid")) {
    	Runnable rp = (() -> {
    	      String nname = JOptionPane.showInputDialog("Enter name :");
    	      if(JOptionPane.showConfirmDialog(null, StringManager.get("str_change")+" `"+editing.getName()+"` "+StringManager.get("str_to")+" `"+nname+"` ?", StringManager.get("str_confirm"), JOptionPane.YES_NO_OPTION) == 0) {
    	    	  if(!editing.getUUID().equals("uuid")) LoadInfo.ACTUALPROFILE.setName(nname); //SETNAME
    	        LabelName.setCaption("Name : "+nname);
    	      }
    	    });

    	    WButton edit_name = new WButton(dec-54, 16, 48, 48, 0);
    	    edit_name.setBackground(new WImage(0, 0, 48, 48, edit_ico));
    	    edit_name.setAction(rp);
    	    actions.put("Edit", edit_name.getActionZone());
    	    edit_name.setAction(actions.get("Edit").action());
    	    addClickBinding(edit_name.getActionZone());

    	    body.addToContainer(edit_name); // Add return button
    }


    int i = 24;
    LabelName = new WLabel(StringManager.get("str_name")+" : "+editing.getName(), 40, i, UIConsts.COLOR_FOREGROUND, 24);

    body.addToContainer(LabelName);

    i += 64;
    if(!editing.getDistant() || editing.getUUID().equals("uuid")) {
    	Runnable ri = (() -> {
    	      String nimg = JOptionPane.showInputDialog("Enter the new Image path :");
    	      if(JOptionPane.showConfirmDialog(null, StringManager.get("str_change")+" `"+editing.getImage()+"` "+StringManager.get("str_to")+" `"+nimg+"` ?", StringManager.get("str_confirm"), JOptionPane.YES_NO_OPTION) == 0) {
    	    	  if(!editing.getUUID().equals("uuid")) LoadInfo.ACTUALPROFILE.setImage(nimg); //SetImage
    	        LabelImage.setCaption("Image : "+nimg);

    	      }
    	    });

    	    // button modif
    	    WButton edit_image = new WButton(dec-54, i-8, 48, 48, 0);
    	    edit_image.setBackground(new WImage(0, 0, 48, 48, edit_ico));
    	    edit_image.setAction(ri);
    	    actions.put("Edit Image", edit_image.getActionZone());
    	    edit_image.setAction(actions.get("Edit Image").action());
    	    addClickBinding(edit_image.getActionZone());

    	    body.addToContainer(edit_image); // Add return button
    	    ///
    }

    LabelImage = new WLabel(StringManager.get("str_image")+" : "+editing.getImage(), 40, i, UIConsts.COLOR_FOREGROUND, 24);
    body.addToContainer(LabelImage);

    i += 64;
    if(editing.getAI() && !editing.getDistant()) {
    	Runnable rlevel = (() -> {
    	      String nlvl = JOptionPane.showInputDialog("Enter the level :");
    	      if(JOptionPane.showConfirmDialog(null, StringManager.get("str_change")+" `"+editing.getImage()+"` "+StringManager.get("str_to")+" `"+nlvl+"` ?", StringManager.get("str_confirm"), JOptionPane.YES_NO_OPTION) == 0) {
    	    	  LoadInfo.ACTUALPROFILE.setLevel(Integer.parseInt(nlvl)); //SetLevel
    	        LabelImage.setCaption("Image : "+nlvl);
    	      }
    	    });
    	// button modif
        WButton edit_level = new WButton(dec-54, i-8, 48, 48, 0);
        edit_level.setBackground(new WImage(0, 0, 48, 48, edit_ico));
        edit_level.setAction(rlevel);
        actions.put("Edit Level", edit_level.getActionZone());
        edit_level.setAction(actions.get("Edit Level").action());
        addClickBinding(edit_level.getActionZone());

        body.addToContainer(edit_level); // Add level button

    	LabelLevel = new WLabel(StringManager.get("str_level")+" : "+editing.getLevel(), 40, i, UIConsts.COLOR_FOREGROUND, 24);
    	body.addToContainer(LabelLevel);

    }
    if(editing.getUUID().equals("uuid"))
	{
    	Image creatBT = null;
        try {
          creatBT = ImageIO.read(new File("res/UI/menu/buttons/Ht/addH.png"));
        } catch (Exception e) {
          Logger.warn("Load failed : "+e, 500);
        }
        Runnable valid = (() -> {
          removeFromViewport(WCard);
        	LoadInfo.PROFILEMANAGER.createProfile(editing.getName(),editing.getImage());
          BaseActivity.setCard(ProfileList.make_card(BaseActivity.dec, BaseActivity.SizeC+88+72));

            //setActivity(new ProfileList());
            //setActivity(new EditProfile());
          });
    	WButton create = new WButton(dec-54, i+16, 48, 48, 0);
    	create.setBackground(new WImage(0, 0, 48, 48, creatBT));
    	create.setAction(valid);
        actions.put("create", create.getActionZone());
        create.setAction(actions.get("create").action());
        addClickBinding(create.getActionZone());
        body.addToContainer(create);


	}
    wi.add(body);
    return wi;

  }

  public Widget imaGottaGoFast() {
    //TODO: sorry I had to go reaaaaly fast
    Widget w = new Widget(0,0, 1440, 959);

    Image ic = null;
    try {
      ic = ImageIO.read(new File("res/UI/LOLNOPE/profile.png"));
    } catch (Exception e) {
      Logger.warn(StringManager.get("str_load_failed")+" : "+e, 500);
    }
    w.add(new WImage(0, 0, 1440, 959, ic));


    return w;
  }

  public void initViewport() {
    this.w = AssetManager.getWidget("TitleBar");
    //this.w = new Widget();
    //createTitleBar();

    dec = Config.WIDTH - 96 - 96;


    if(editing != null) System.out.println(editing);

    addToViewport(this.w);
    //this.addToViewport(imaGottaGoFast());
    addToViewport(createToolbarButtons());
    WCard = createEditCard();
    addToViewport(WCard);
  }

  @Override
  public void OnRescale() {
    refreshViewport();
    scaleViewport(); // Rescale widgets on resize
  }

  public void refreshViewport() {
    this.w.scale();
  }

  public void init() {
    initViewport();
    //setEventDriven(true);
  }

  @Override
  public void BeforeAdd() {
    //SizeC = 2*64;
    SizeC = 4*64+40; //256+32+8+16+88+64
  }

  public EditProfile(String nam, String uui, String imag, Stat stat, boolean distan, boolean iAI, int leve) {
    super();
    name = nam;
    uuid = uui;
    image = imag;
    stats = stat;
    distant = distan;
    imAI = iAI;
    level = leve;
    init();
  }

  public EditProfile() {
    super();
    fillIt();
    editing = LoadInfo.ACTUALPROFILE;
    init();
  }
  public EditProfile(Profile p) {
    super();
    fillIt();
    editing = p;
    init();
  }

  public EditProfile(boolean b) {
	    super();
	    fillIt();
	    //editing = p;
	    init();
	  }

  public void render(Graphics2D g2d) {
  };

}
