package com.pingu.party.activities;

import com.pingu.party.engine.core.AssetManager;

import java.util.HashMap;
import com.pingu.party.gui.widget.WPopup;

import com.pingu.party.engine.input.ListenZone;

import com.pingu.party.gui.widget.WTitledText;
import com.pingu.party.gui.widget.WIconLayout;

import com.pingu.party.gui.widget.WImage;
import com.pingu.party.gui.widget.WButton;

import com.pingu.party.gui.widget.WStatusIcon;
import com.pingu.party.gui.widget.WList;
import com.pingu.party.gui.widget.WListItem;
import com.pingu.party.network.structures.IP;
import com.pingu.party.network.structures.Server;
import com.pingu.party.stats.StatManager;

import java.util.ArrayList;
import com.pingu.party.gui.widget.WProgress;
import com.pingu.party.launcher.Utils.Font.Roboto;
import com.pingu.party.gui.widget.WLabel;
import com.pingu.party.gui.widget.WCardTitle;
import com.pingu.party.activities.base.Viewport;
import com.pingu.party.gui.widget.WBackground;
import com.pingu.party.gui.widget.WTitle;
import com.pingu.party.gui.widget.WPoly;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Image;
import java.awt.Color;
import com.pingu.party.launcher.Utils.UIConsts;
import com.pingu.party.gui.widget.WTitleBar;

import com.pingu.party.gui.widget.Widget;
import com.pingu.party.engine.core.GameCanvas;
import com.pingu.party.engine.config.Config;
import com.pingu.party.engine.config.LoadInfo;

import java.awt.Graphics2D;

import javax.swing.SwingUtilities;
import com.pingu.party.engine.core.WindowMaker;
import com.pingu.party.engine.game.Profile;

public class Menu_old extends GameCanvas implements Viewport {
  private Color almostGrey = new Color(72, 87, 92, 204);
  private Widget w;

  /* temp vars */

  // Bindings map
  private HashMap<String, ListenZone> actions = new HashMap<>();

  // Calculated var
  private int dec;

  private WPopup popup;

  // Level card
  private int pCount = 145; //Parties count
  private int xp = 1680;
  private int level = 42;

  // Servers card
  private ArrayList<Server> servers = new ArrayList<Server>();
  private WList servLis;

  // Colors
  private Color deepBlue = new Color(87, 161, 211);
  private Color deepWhite = new Color(193, 198, 200);
  private Color testGrey = new Color(132, 142, 146);
  private Color softBlue = new Color(118, 158, 174);
  private Color titleBlue = new Color(74, 111, 125);

  private WProgress progr;

  private void createTitleBar() {
    WTitleBar wtb = new WTitleBar(80, this.almostGrey);

    WBackground wb = new WBackground("bg");
    this.w.add(wb);

    //w.add(wtb);
    Image im = null;
    try {
      im = ImageIO.read(new File("res/UI/menu/logo-linear.png"));

    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }
    //this.w.add(wct);
    int tx = (Config.WIDTH/5)*2-im.getWidth(null);
    if(tx<0) tx=0;
    WTitle wt = new WTitle(tx, 0, im);
    wtb.addTitle(wt);
    wtb.addButton();
    this.w.add(wtb);
  }

  public Widget createStatsCard() {
	System.out.println("selected => "+LoadInfo.ACTUALPROFILE);
    Widget wi = new Widget(this.dec,120, 480, 120, false);
    wi.setHorizontalAlign(2);
    wi.setHorizontalMargin(48);

    Image ic = null;
    try {
      ic = ImageIO.read(new File("res/UI/menu/titleico.png"));

    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }


    WCardTitle statsC = new WCardTitle(0, 0, 480, 120, ic, "Stats");
    statsC.addToContainer(new WLabel(""+this.pCount, 16, 0,new Roboto().get_boldcondensed(40), this.deepBlue, 40));
    statsC.addToContainer(new WLabel("Parties", 82, 28,new Roboto().get_medium(13), this.testGrey, 12));
    statsC.addToContainer(new WPoly(480-168, 0, 168, 96, 56, this.softBlue));
    statsC.addToContainer(new WLabel(""+this.level, 480-95, 17,new Roboto().get_boldcondensed(76), this.deepWhite, 58));
    statsC.addToContainer(new WLabel(""+this.xp, 209, 50,new Roboto().get_boldcondensed(15), this.deepBlue, 12));
    statsC.addToContainer(new WLabel("/"+this.level*60, 242, 50,new Roboto().get_boldcondensed(15), this.testGrey, 12));
    this.progr = new WProgress(12, 72, 480-168-24, 16, this.deepBlue, this.titleBlue);
    statsC.addToContainer(this.progr);
    wi.add(statsC);
    this.progr.setProgress(2);
    return wi;
  }

  public Widget createServerCard() {

    this.servers.add(new Server("Server 1",new IP(10,0,0,1)));
    this.servers.add(new Server("Server 2",new IP(10,0,0,2)));
    this.servers.add(new Server("Random lol",new IP(94,142,12,45)));

    Widget wi = new Widget(this.dec,264, 480, 300, false);
    wi.setHorizontalAlign(2);
    wi.setHorizontalMargin(48);
    Image ic = null;
    try {
      ic = ImageIO.read(new File("res/UI/menu/titleico.png"));
    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }

    WCardTitle servC = new WCardTitle(0, 0, 480, 24+16+this.servers.size()*64, ic, "Servers");
    this.servLis = new WList(0,0,480, 0, 8);


    WStatusIcon wsi;
    WIconLayout wil;
    WTitledText wtt;
    for (Server s : this.servers) {
      WListItem wli = new WListItem(0,0,480,64);
      wsi = new WStatusIcon(12,12, "res/UI/menu/serv.png",s.getStatus());
      wtt = new WTitledText(6,8,256,24,0, s.getName()); //int x, int y, int w, int h, int m, String s
      WLabel Sip = new WLabel(s.getIPText(), 0, 0, 12, UIConsts.COLOR_MIDGREY, 16); //(s,x, y, fsize-(fsize/6), tc, fsize);
      WListItem wttChild = new WListItem(6,0,256,24); //int x, int y, int w, int h
      wttChild.setInner(Sip);
      wtt.add(wttChild);
      WListItem wttChild2 = new WListItem(6,0,256,24);
      wttChild2.setInner(new WLabel(s.getStatus().toString(), 0, 0, 12, UIConsts.COLOR_MIDGREY, 16));
      //wtt.add(wttChild2);
      wil = new WIconLayout(12, 0, wsi, wtt);
      //wil = new WIconLayout(12, 0, wsi, Sip);
      wil.setIconWidth(64);
      wli.setInner(wil);
      this.servLis.add(wli);
      /*
      WListItem wli = new WListItem(0,0,480,64,new Color(0,0,0,42));
      wli.setInner(new WStatusIcon(24,12, "res/UI/menu/serv.png",s.getStatus()));
      this.servLis.add(wli);*/
    }

    //TEST REPLACEMENTS
  /*  wsi = new WStatusIcon(12,12, "res/UI/menu/serv.png",this.servers.get(0).getStatus());
    wil = new WIconLayout(12, 0, wsi, new WLabel( this.servers.get(0).toString(), 0, 0));
    wil.setIconWidth(64);
    WListItem wli = new WListItem(0,0,480,64,new Color(0,0,0,42));
    wli.setInner(wil);
    this.servLis.add(wli);*/
    //TEST R END

    servC.addToContainer(this.servLis);

    wi.add(servC);

    // TEST ONLY, FIX ME WITH COMPOSITES LAYOUT
    //wi.add(new WLabel( this.servers.get(0).toString(), 40, 26));
  //  wi.add(new WLabel( this.servers.get(1).toString(), 40, 58));
  //  wi.add(new WLabel( this.servers.get(2).toString(), 40, 90));
    //TEST END
    return wi;
  }

  public Widget createPlayButtons() {
    //TODO: sorry I had to go reaaaaly fast
    //Widget wi = new Widget(96,288, 480, 256);
    Widget wi = new Widget(96,152, 480, 256);

    //SOLO BUTTON

    Image ic = null;
    try {
      ic = ImageIO.read(new File("res/UI/menu/buttons/button-solo.png"));
    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }
    Image hostHT = null;
    //wi.add(new WImage(0, 0, 480, 120, ic));

    Runnable r = (() -> {
      System.out.println("Solo");
      setActivity(new Local());
    });

    WButton wb = new WButton(0, 0, 480, 120, 0);

    wb.setAction(r);
    this.actions.put("Solo", wb.getActionZone());
    wb.setAction(this.actions.get("Solo").action());

    wb.setBackground(new WImage(0, 0, 480, 120, ic));
    addClickBinding(wb.getActionZone());
    wi.add(wb);
    // ListenZone b = new ListenZone(96, 288, 480 , 120, r);
    // addClickBinding(b);

    //VERSUS BUTTON

    Image ic2 = null;
    try {
      ic2 = ImageIO.read(new File("res/UI/menu/buttons/button-vs.png"));
    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }

    Runnable r2 = (() -> {
      showPopup();
    });

    //WButton wb2 = new WButton(0, 136, 480, 120, 0);
    WButton wb2 = new WButton(0, 136, 480, 120, 0);

    wb2.setAction(r2);
    this.actions.put("Versus", wb2.getActionZone());
    wb2.setAction(this.actions.get("Versus").action());

    wb2.setBackground(new WImage(0, 0, 480, 120, ic2));
    addClickBinding(wb2.getActionZone());
    wi.add(wb2);

    // Stats button => I did not choosed to add it
    Image ic3 = null;
    try {
      ic3 = ImageIO.read(new File("res/UI/menu/buttons/button-stats.png"));
    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }
    Runnable r3 = (() -> {
    	LoadInfo.STATS = new StatManager().getList();
      setActivity(new ShowStats());
    });

    WButton wb3 = new WButton(0, 272, 480, 120, 0);
    wb3.setAction(r3);
    this.actions.put("Stats", wb3.getActionZone());
    wb3.setAction(this.actions.get("Stats").action());

    wb3.setBackground(new WImage(0, 0, 480, 120, ic3));
    addClickBinding(wb3.getActionZone());
    wi.add(wb3);

    // Profiles button => I did not choosed to add it too
    Image ic4 = null;
    try {
      ic4 = ImageIO.read(new File("res/UI/menu/buttons/button-profile.png"));
    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }
    Runnable r4 = (() -> {
      setActivity(new ProfileList());
    });

    WButton wb4 = new WButton(0, 408, 480, 120, 0);
    wb4.setAction(r4);
    this.actions.put("Profiles", wb4.getActionZone());
    wb4.setAction(this.actions.get("Profiles").action());

    wb4.setBackground(new WImage(0, 0, 480, 120, ic4));
    addClickBinding(wb4.getActionZone());
    wi.add(wb4);

    return wi;
  }

  public Widget createToolbarButtons() {
    Widget wi;
    if(Config.FULLSCREEN) {
       wi = new Widget(0,0, 224, 80, false);
    } else {
      wi = new Widget(0,0, 152, 80, false);
    }
    wi.setHorizontalAlign(2);

    Runnable rh = (() -> {
      setActivity(new Options());
    });
    Runnable rprofiles = (() -> {
      setActivity(new ProfileList());
    });
    Runnable rq = (() -> {
      System.exit(0);
    });

    WImage bg = new WImage("res/UI/menu/buttons/Ht/blank.png");

  if(Config.FULLSCREEN) {
    WButton quitb = new WButton(144, 0, 64, 64, 80);
    quitb.setBackground(bg);
    quitb.setAction(rq);
    this.actions.put("QUIT", quitb.getActionZone());
    quitb.setAction(this.actions.get("QUIT").action());
    addClickBinding(quitb.getActionZone());
    wi.setXY((Config.WIDTH-224),8);
    wi.add(quitb);
    //wi.setXY((Config.WIDTH-152),8);
  } else {
    //wi.setXY((Config.WIDTH-80),8);
    wi.setXY((Config.WIDTH-152),8);
  }
  WButton profb = new WButton(0, 0, 64, 64, 80);
  WImage pbg = new WImage("res/UI/menu/buttons/profile-rbtn.png");
  profb.setBackground(pbg);
  profb.setAction(rprofiles);
  this.actions.put("Profiles", profb.getActionZone());
  profb.setAction(this.actions.get("Profiles").action());
  addClickBinding(profb.getActionZone());

  WButton setb = new WButton(72, 0, 64, 64, 80);
  setb.setBackground(bg);
  setb.setAction(rh);
  this.actions.put("Settings", setb.getActionZone());
  setb.setAction(this.actions.get("Settings").action());
  addClickBinding(setb.getActionZone());
  wi.add(setb);
  wi.add(profb);
  return wi;


  }

  public Widget createPopup() {
    // widget
    this.popup = new WPopup(576,360);

    Image ic = null;
    try {
      ic = ImageIO.read(new File("res/UI/menu/buttons/btn256-e.png"));
    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }

    Image hostHT = null;
    try {
      hostHT = ImageIO.read(new File("res/UI/menu/buttons/Ht/hostHT.png"));
    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }
    Image joinHT = null;
    try {
      joinHT = ImageIO.read(new File("res/UI/menu/buttons/Ht/joinHT.png"));
    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }

    Runnable rh = (() -> {
      //System.out.println("Host Hover");
    });
    Runnable rc = (() -> {
      System.out.println("Clicked Host");
      setActivity(new Online());
    });

    Runnable rh2 = (() -> {
      //System.out.println("Join Hover");
    });
    Runnable rc2 = (() -> {
      System.out.println("Clicked Join");
      setActivity(new Join());
    });


    WButton wb = new WButton(16, 16, 256, 256, 0);
    wb.setBackground(new WImage(0, 0, 256, 256, ic));

    wb.setHoverAction(rh);
    this.actions.put("HoverHost", wb.getHoverZone());
    wb.setHoverAction(this.actions.get("HoverHost").action());

    wb.setAction(rc);
    this.actions.put("ClickedHost", wb.getActionZone());
    wb.setAction(this.actions.get("ClickedHost").action());

    wb.getHoverZone().disable();
    wb.getActionZone().disable();
    this.addHoverBinding(wb.getHoverZone());
    addClickBinding(wb.getActionZone());

    this.popup.add(wb);

    this.popup.add(new WImage(0, 288, 288, 64, hostHT));
    this.popup.add(new WImage(288, 288, 288, 64, joinHT));

    WButton wb2 = new WButton(304, 16, 256, 256, 0);
    wb2.setBackground(new WImage(0, 0, 256, 256, ic));

    wb2.setHoverAction(rh2);
    this.actions.put("HoverJoin", wb2.getHoverZone());
    wb2.setHoverAction(this.actions.get("HoverJoin").action());

    wb2.setAction(rc2);
    this.actions.put("ClickedJoin", wb2.getActionZone());
    wb2.setAction(this.actions.get("ClickedJoin").action());

    wb2.getHoverZone().disable();
    wb2.getActionZone().disable();
    this.addHoverBinding(wb2.getHoverZone());
    addClickBinding(wb2.getActionZone());

    this.popup.add(wb2);

    return this.popup;
  }

  private void showPopup() {
    this.actions.get("Versus").disable(); // Disable butons action
    this.actions.get("Solo").disable();  // Reactivate if, on "out click" for example

    this.actions.get("HoverHost").enable();
    this.actions.get("ClickedHost").enable();
    this.actions.get("HoverJoin").enable();
    this.actions.get("ClickedJoin").enable();
    this.popup.setVisible();
  }

  public void initViewport() {
    this.w = AssetManager.getWidget("TitleBar");
    //this.w = new Widget();
    //createTitleBar();

    this.dec = Config.WIDTH - 480 - 48;

    addToViewport(this.w);
    addToViewport(createStatsCard());
    addToViewport(createServerCard());
    addToViewport(createPlayButtons());
    addToViewport(createToolbarButtons());
    addToViewport(createPopup());

    OnRescale();

    Runnable r2 = (() -> {
      System.out.println("SCROLL EVENT !");
    });
    ListenZone b2 = new ListenZone(5, 5, 50 , 50, r2);
    this.addScrollBinding(b2);
  }

  @Override
  public void OnRescale() {
    refreshViewport();
    scaleViewport();
  }

  public void refreshViewport() {
    this.w.scale();
  }

  public void init() {
    initViewport();
    //setEventDriven(true);
  }

  public void calcProgress() {
    this.progr.setProgress((int)(this.xp/(this.level*60f)*100));
  }

  public Menu_old() {
    super();
    System.out.println(LoadInfo.ACTUALPROFILE);
    init();
    calcProgress();
  }

  public void render(Graphics2D g2d) {
  };

}
