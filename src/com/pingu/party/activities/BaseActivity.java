package com.pingu.party.activities;

import com.pingu.party.launcher.Component.ErrorDialog;

import com.pingu.party.engine.input.parser.KeyToString;

import com.pingu.party.engine.input.map.KeyMap;
import com.pingu.party.engine.utils.Lambda;

import com.pingu.party.engine.input.Input;
import com.pingu.party.engine.loader.Loader;
import com.pingu.party.gui.toaster.Toaster;
import java.util.Map;

import com.pingu.party.engine.core.AssetManager;

import javax.swing.JOptionPane;
import java.util.List;

import com.pingu.party.engine.config.LoadInfo;

import com.pingu.party.gui.widget.WIconLayout;
import com.pingu.party.gui.widget.WCardWrapper;
import com.pingu.party.gui.widget.WButton;
import com.pingu.party.engine.input.ListenZone;
import java.util.HashMap;
import com.pingu.party.gui.widget.WImage;

import com.pingu.party.gui.widget.WStatusIcon;
import com.pingu.party.gui.widget.WList;
import com.pingu.party.gui.widget.WListItem;
import com.pingu.party.network.structures.IP;
import com.pingu.party.network.structures.Server;
import java.util.ArrayList;
import com.pingu.party.gui.widget.WProgress;
import com.pingu.party.launcher.Utils.Font.Roboto;
import com.pingu.party.gui.widget.WLabel;
import com.pingu.party.gui.widget.WCardTitle;
import com.pingu.party.activities.base.Viewport;
import com.pingu.party.gui.widget.WBackground;
import com.pingu.party.gui.widget.WTitle;
import com.pingu.party.gui.widget.WPoly;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Image;
import java.awt.Color;
import com.pingu.party.launcher.Utils.UIConsts;
import com.pingu.party.gui.widget.WTitleBar;

import com.pingu.party.gui.widget.Widget;
import com.pingu.party.engine.core.GameCanvas;
import com.pingu.party.engine.config.Config;
import java.awt.Graphics2D;

public class BaseActivity extends GameCanvas implements Viewport {
  private Color almostGrey = new Color(72, 87, 92, 204);
  private Widget w;
  // Colors
  private Color deepBlue = new Color(87, 161, 211);
  private Color deepWhite = new Color(193, 198, 200);
  private Color testGrey = new Color(132, 142, 146);
  private Color softBlue = new Color(118, 158, 174);
  private Color titleBlue = new Color(74, 111, 125);

  private ArrayList<Integer> KRel;
  private static Input InHandler;
  private static boolean inputFocus;
  private static boolean stopListen = false;
  protected static String inputBuffer;

  // Calculated var
  static int dec;
  static int SizeC = 0;

  static WCardWrapper win;
  static Widget WCard;

  static boolean drawcard = true;
  private static int inited = 0;

  Toaster t;
  Loader l;

  //modify me !
  static String titleName = "pinguT";

  protected static final ArrayList<Widget> Widgets = new ArrayList<Widget>();

  protected static final HashMap<String, ListenZone> actions = new HashMap<>();

  protected static final ArrayList<Lambda<String>> TicksListener = new ArrayList<>();


  public static Widget createToolbarButtons() {
    Widget wi;
    if(Config.FULLSCREEN) {
       wi = new Widget(0,0, 152, 80, false);
    } else {
      wi = new Widget(0,0, 80, 80, false);
    }
    wi.setHorizontalAlign(2);

    Runnable rh = (() -> {
      removeAllBinds();
      clearAdded();
      setCard(Options.make_card(BaseActivity.dec, BaseActivity.SizeC+88+72));
      //setActivity(new Options());
    });
    Runnable rq = (() -> {
      //stop();
      System.exit(0);
    });

    WImage bg = new WImage("res/UI/menu/buttons/Ht/blank.png");

    if(!actions.containsKey("Settings")) {
      //System.out.println("noSet");
      WButton setb = new WButton(0, 0, 64, 64, 80);
      setb.setBackground(bg);
      setb.setAction(rh);
      actions.put("Settings", setb.getActionZone());
      setb.setAction(actions.get("Settings").action());
      addClickBinding(setb.getActionZone());
      wi.add(setb);
    } else {
      addClickBinding(actions.get("Settings"));
    }

    if(Config.FULLSCREEN) {
      if(!actions.containsKey("QUIT")) {
        WButton quitb = new WButton(72, 0, 64, 64, 80);
        quitb.setBackground(bg);
        quitb.setAction(rq);
        actions.put("QUIT", quitb.getActionZone());
        quitb.setAction(actions.get("QUIT").action());
        addClickBinding(quitb.getActionZone());
        wi.add(quitb);
      } else {
        addClickBinding(actions.get("QUIT"));

      }
      wi.setXY((Config.WIDTH-152),8);
    } else {
      wi.setXY((Config.WIDTH-80),8);
    }
    scaleViewport();
    return wi;
  }

  public static void removeAllBinds() {
    for (Map.Entry<String, ListenZone> e : actions.entrySet()) {
      String name = e.getKey();
      ListenZone lz = e.getValue();
      removeClickBinding(lz);
    }
    removeClickBinding(actions.get("Settings"));
    removeClickBinding(actions.get("QUIT"));
    removeClickBinding(actions.get("Return"));
    TicksListener.clear();
  }

  public Widget createCard() {
    clearAdded();
    int wid = SizeC+88+72;
    Widget wi = new Widget(96,168, dec, wid, false);
    wi.setHorizontalAlign(1);

    win = makeC(dec, wid);
    if(win == null) return wi;
    wi.add(win);
    return wi;
  }

  public static Widget createCard(WCardWrapper wcw) {
    int wid = SizeC+88+72;
    Widget wi = new Widget(96,168, dec, wid, false);
    wi.setHorizontalAlign(1);
    win = wcw;
    wi.add(win);
    return wi;
  }

  public static void setCard(WCardWrapper wcw) {
    clearAdded();
    addToViewport(createToolbarButtons());
    //Widgets.clear();
    removeFromViewport(WCard);
    if(wcw == null) return;
    try {
      WCard = createCard(wcw);
    } catch (Exception e) {
      ErrorDialog.handle("Card error : ",e, true, false);
      return;
    }
    if(WCard != null) addToViewport(WCard);
  }

  public static void setWidget(Widget w) {
    synchronized (Widgets) {
      Widgets.add(w);
    }
    addToViewport(w);
  }

  public WCardWrapper makeC(int dec, int wid) {
    WCardWrapper w = makeCard(dec, wid);
    //OnRescale();
    //rescale();
    return w;
  }

  public static WCardWrapper makeCard(int dec, int wid) {
    if(dec<=0 || wid <=0) return null;

    WCardWrapper body = new WCardWrapper(0, 0,dec, wid, AssetManager.getImage(titleName));


    Runnable rb = (() -> {
      setActivity(new Menu());
    });
    WButton returnb = new WButton(12, wid-72-64, 326, 64, 0);
    returnb.setBackground(new WImage(0, 0, 326, 64, AssetManager.getImage("returnBHT")));

    returnb.setAction(rb);
    actions.put("Return", returnb.getActionZone());
    returnb.setAction(actions.get("Return").action());
    addClickBinding(returnb.getActionZone());

    body.addToContainer(returnb);
    return body;
  }

  public static void addWidget(Widget w) {
    synchronized (Widgets) {
      Widgets.add(w);
    }
  }

  public static void removeWidget(Widget w) {
    synchronized (Widgets) {
      removeFromViewport(w);
      Widgets.remove(w);
    }
  }

  protected static void clearAdded() {
    synchronized (Widgets) {
      for (Widget w: Widgets) {
        removeFromViewport(w);
      }
    }
  }

  protected static void removeAdded() {
    synchronized (Widgets) {
      for (Widget w: Widgets) {
        removeFromViewport(w);
        Widgets.remove(w);
      }
    }
  }

  protected static void pushAdded() {
    synchronized (Widgets) {
      for (Widget w: Widgets) {
        addToViewport(w);
      }
    }
  }

  protected static void rescaleAll() {
    synchronized (Widgets) {
      for (Widget w: Widgets) {
        w.scale();
      }
    }
  }

  protected void setSizeC(int s) {
    SizeC = s;
  }

  protected void drawMode() {
    drawcard = true;
  }

  public static BaseActivity instance;

  public static void refreshConfig() {
    dec = Config.WIDTH - 96 - 96;
    if(instance != null) instance.OnRescale();
    //OnRescale();
  }

  public void initViewport() {
    dec = Config.WIDTH - 96 - 96;
    drawMode();
    BeforeAdd();
    if(this.w == null) {
      this.w = AssetManager.getWidget("TitleBar");
      addToViewport(this.w);
    }
    if(! actions.containsKey("Settings")) addToViewport(createToolbarButtons());
    if(drawcard) {
      WCard = createCard();
      if(WCard != null) addToViewport(WCard);
    }
    pushAdded();
    OnRescale();
    //this.w = new Widget();
    //createTitleBar();

  }

  public static boolean canInputListen() {
    return !stopListen;
  }

  public static void askInputListen() {
    stopListen = false;
    InHandler.getInputFocus();
  }

  public static void relaseInputListen() {
    stopListen = true;
    InHandler.releaseInputFocus();
  }

  public static void clearInput(String s) {
    InHandler.cleanReleased();
    inputBuffer = s;
  }

  @Override
  public void OnTick() {
    if(stopListen) {
      InHandler.cleanReleased();
      return;
    }
    if(inputFocus) {
      int i;
      this.KRel = InHandler.getReleased();
      char c;
      StringBuilder str = new StringBuilder((inputBuffer!=null)?inputBuffer:"");
      for (int it=0; it<this.KRel.size();it++) {
        i = this.KRel.get(it);
        InHandler.consumeKeyRelased(i);
        if(i==KeyMap.KEY_BACKSPACE) {
          if(str.length()<1) str.setLength(0);
          else str.delete(str.length()-1, str.length());
        } else if (i==KeyMap.KEY_ENTER) {
          relaseInputListen();
        } else {
          //System.out.print(i);
          c = KeyToString.getText((int)i);
          if(c!=0) str.append(c);
          /*char ch = (char) ((int)i);
          ch);*/
        }
        //Make String here
      }
      inputBuffer = str.toString();
      synchronized (TicksListener) {
        for (Lambda<String> l : TicksListener) {
          l.run(inputBuffer);
        }
      }
    }
  }

  @Override
  public void OnRescale() {
    refreshViewport();
    scaleViewport();
  }

  @Override
  public void OnInit() {
    this.l = getLoader();
    this.t = new Toaster();
    Logger = this.t;
    this.l.load(this.t);
    this.t.showAll();
  }

  public void BeforeAdd() {
  }

  public void refreshViewport() {
    this.w.scale();
  }

  public static void askInputFocus() {
    inputFocus = InHandler.getInputFocus();
  }

  public static boolean isInputFocused() {
    return inputFocus;
  }

  public void initHanders() {
    InHandler = getInputHandler();
    inputFocus = InHandler.getInputFocus();
    //InHandler = null;
    //this.inputFocus = this.InHandler.releaseInputFocus();
  }

  public void init() {
    SizeC = 6*64;
    initViewport();
    initHanders();
    //setEventDriven(true);
  }

  public BaseActivity() {
    super();
    init();
    instance = this;
  }
  public void render(Graphics2D g2d) {
  }

}
