package com.pingu.party.activities;

import com.pingu.party.engine.config.Config;
import com.pingu.party.engine.core.AssetManager;
import com.pingu.party.engine.core.WindowMaker;
import com.pingu.party.gui.widget.*;
import com.pingu.party.launcher.Utils.Font.MaterialIcon;
import com.pingu.party.launcher.Utils.UIConsts;
import javafx.util.Pair;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;


public class Options extends BaseActivity {

	private static final long serialVersionUID = 8787404949040699504L;

	private static Font icoFont = new MaterialIcon().get(24);

    public static void removeBinds() {
        removeClickBinding(actions.get("Return"));
        removeClickBinding(actions.get("Settings"));
        removeClickBinding(actions.get("doReplay"));
        removeClickBinding(actions.get("Lang_Bnext"));
        removeClickBinding(actions.get("Lang_Bprev"));
        removeClickBinding(actions.get("enableConsole"));
        removeClickBinding(actions.get("fullscreenToggle"));
        removeClickBinding(actions.get("fps_Bnext"));
        removeClickBinding(actions.get("fps_Bprev"));
        removeClickBinding(actions.get("ScrnSz_Bnext"));
        removeClickBinding(actions.get("ScrnSz_Bprev"));
        removeClickBinding(actions.get("Replay_Bnext"));
        removeClickBinding(actions.get("Replay_Bprev"));
    }

    private static void newWin() {
        WindowMaker.doRefresh();
        refreshConfig();
        Menu.remake();
        setCard(Menu.make_card(0, 0));
        Menu.wPlay = Menu.createPlayButtons();
        pushAdded();
    }

    @Override
    public WCardWrapper makeC(int dec, int wid) {
        WindowMaker.doRefresh();
        refreshConfig();
        Menu.remake();
        return make_card(dec, wid);
    }

    public static WCardWrapper make_card(int dec, int wid) {
        WCardWrapper body = new WCardWrapper(0, 0,dec, wid, AssetManager.getImage("optionsT"));

        Runnable rb = (() -> {
        removeBinds();
            newWin();
            //setActivity(new Menu());
        });
        WButton returnb = new WButton(12, wid-72-64, 326, 64, 0);
        returnb.setBackground(new WImage(0, 0, 326, 64, AssetManager.getImage("returnBHT")));

        returnb.setAction(rb);
        actions.put("Return", returnb.getActionZone());
        returnb.setAction(actions.get("Return").action());
        addClickBinding(returnb.getActionZone());

        body.addToContainer(returnb);


        int y = 16;
        int x2 = dec/2+16;

        // SCREEN

        WLabel screenIcoLabel = new WLabel("\ue85b",16, y+6,icoFont, UIConsts.COLOR_MIDGREY, 16);
        body.addToContainer(screenIcoLabel);
        WLabel screenLabel = new WLabel("Screen",16+32, y, UIConsts.COLOR_MIDGREY, 16);
        body.addToContainer(screenLabel);

        WSelector<Pair<String, String>> scrnSz = new WSelector<>(16+32, y+32, false, true);
        ArrayList<Pair<String, String>> Sizes =  new ArrayList<>(Arrays.asList(
                new Pair<>("1200", "800"),
                new Pair<>("1280", "1024"),
                new Pair<>("1280", "800"),
                new Pair<>("1360", "768"),
                new Pair<>("1366", "768"),
                new Pair<>("1440", "900"),
                new Pair<>("1536", "864"),
                new Pair<>("1600", "900"),
                new Pair<>("1680", "1050"),
                new Pair<>("1920", "1200"),
                new Pair<>("1920", "1080"),
                new Pair<>("2560", "1080"),
                new Pair<>("2560", "1440"),
                new Pair<>("3440", "1440"),
                new Pair<>("3840", "2160")));


        Pair<String, String> curSz = new Pair<>(String.format("%d", Config.WIDTH), String.format("%d", Config.HEIGHT));

        Sizes.forEach(scrnSz::add);

        scrnSz.setNextAction( () -> {
            scrnSz.step(1);
            Config.set("WIDTH", scrnSz.getCurrent().getKey());
            Config.set("HEIGHT", scrnSz.getCurrent().getValue());
        });
        actions.put("ScrnSz_Bnext", scrnSz.getNextZone());
        scrnSz.setNextAction(actions.get("ScrnSz_Bnext").action());
        addClickBinding(scrnSz.getNextZone());

        scrnSz.setPrevAction( () -> {
            scrnSz.step(-1);
            Config.set("WIDTH", scrnSz.getCurrent().getKey());
            Config.set("HEIGHT", scrnSz.getCurrent().getValue());
        });
        actions.put("ScrnSz_Bprev", scrnSz.getPrevZone());
        scrnSz.setPrevAction(actions.get("ScrnSz_Bprev").action());
        addClickBinding(scrnSz.getPrevZone());

        if(Sizes.contains(curSz)) {
            while(! (scrnSz.getCurrent().equals(curSz))) scrnSz.step(1);
        } else {
            scrnSz.add(curSz);
            while(! (scrnSz.getCurrent().equals(curSz))) scrnSz.step(1);
        }
        //scrnSz.hackCaption(scrnSz.getCurrent()+" x");

        body.addToContainer(scrnSz);

        // WINDOW

        WLabel WindowIcoLabel = new WLabel("\ue069",x2, y+6,icoFont, UIConsts.COLOR_MIDGREY, 16);
        body.addToContainer(WindowIcoLabel);
        WLabel WindowLabel = new WLabel("Window",x2+32, y, UIConsts.COLOR_MIDGREY, 16);
        body.addToContainer(WindowLabel);

        boolean fs = Config.FULLSCREEN;

        WCheckbox fullscreenToggle = new WCheckbox(x2+32, y+32, 48, 48, fs);
        fullscreenToggle.setAction(() -> {
            Config.set("FULLSCREEN", (Config.get("FULLSCREEN")==1)?("0"):("1"));
            fullscreenToggle.toggle();
        });

        actions.put("fullscreenToggle", fullscreenToggle.getActionZone());
        fullscreenToggle.setAction(actions.get("fullscreenToggle").action());
        addClickBinding(fullscreenToggle.getActionZone());

        body.addToContainer(fullscreenToggle);

        WLabel fullscreenLabel = new WLabel("Fullscreen",x2+32+64, y+32+12, 16);
        body.addToContainer(fullscreenLabel);


        y += 96;

        // RENDER

        WLabel RenderIcoLabel = new WLabel("\ue322",16, y+6,icoFont, UIConsts.COLOR_MIDGREY, 16);
        body.addToContainer(RenderIcoLabel);
        WLabel RenderLabel = new WLabel("Render",16+32, y, UIConsts.COLOR_MIDGREY, 16);
        body.addToContainer(RenderLabel);

        WSelector<String> fpsCnt = new WSelector<>(16+32, y+32, false, true);
        ArrayList<String> fpsCnts = new ArrayList<>(Arrays.asList("5","15","30","60","90","144"));
        String curFps = String.format("%d", Config.FPS);

        fpsCnts.forEach(fpsCnt::add);
        if(fpsCnts.contains(curFps)) {
            while(! (fpsCnt.getCurrent().equals(curFps))) fpsCnt.step(1);
        } else {
            fpsCnt.add(curFps);
            while(! (fpsCnt.getCurrent().equals(curFps))) fpsCnt.step(1);
        }
        fpsCnt.hackCaption(fpsCnt.getCurrent()+" FPS");


        fpsCnt.setNextAction( () -> {
            fpsCnt.step(1);
            Config.set("FPS", fpsCnt.getCurrent());
            fpsCnt.hackCaption(fpsCnt.getCurrent()+" FPS");
        });
        actions.put("fps_Bnext", fpsCnt.getNextZone());
        fpsCnt.setNextAction(actions.get("fps_Bnext").action());
        addClickBinding(fpsCnt.getNextZone());

        fpsCnt.setPrevAction( () -> {
            fpsCnt.step(-1);
            Config.set("FPS", fpsCnt.getCurrent());
            fpsCnt.hackCaption(fpsCnt.getCurrent()+" FPS");
        });
        actions.put("fps_Bprev", fpsCnt.getPrevZone());
        fpsCnt.setPrevAction(actions.get("fps_Bprev").action());
        addClickBinding(fpsCnt.getPrevZone());

        body.addToContainer(fpsCnt);


        // LOCALE

        WLabel LocaleIcoLabel = new WLabel("\ue153",x2, y+6,icoFont, UIConsts.COLOR_MIDGREY, 16);
        body.addToContainer(LocaleIcoLabel);
        WLabel LocaleLabel = new WLabel("Locale",x2+32, y, UIConsts.COLOR_MIDGREY, 16);
        body.addToContainer(LocaleLabel);

        WSelector<String> lang = new WSelector<>(x2+32, y+32, false, true);
        ArrayList<String> langs = new ArrayList<>(Arrays.asList("de","en","fr","jp"));
        langs.forEach(lang::add);
        while(!lang.getCurrent().equals(Config.LANG)) lang.step(1);


        lang.setNextAction( () -> {lang.step(1); Config.set("LANG", lang.getCurrent());});
        actions.put("Lang_Bnext", lang.getNextZone());
        lang.setNextAction(actions.get("Lang_Bnext").action());
        addClickBinding(lang.getNextZone());

        lang.setPrevAction( () -> {lang.step(-1); Config.set("LANG", lang.getCurrent());});
        actions.put("Lang_Bprev", lang.getPrevZone());
        lang.setPrevAction(actions.get("Lang_Bprev").action());
        addClickBinding(lang.getPrevZone());

        body.addToContainer(lang);


        y += 96;

        // GAME

        WLabel GameIcoLabel = new WLabel("\ue338",16, y+6,icoFont, UIConsts.COLOR_MIDGREY, 16);
        body.addToContainer(GameIcoLabel);
        WLabel GameLabel = new WLabel("Game",16+32, y, UIConsts.COLOR_MIDGREY, 16);
        body.addToContainer(GameLabel);

        boolean rpl = Config.DO_REPLAY;

        WCheckbox doReplay = new WCheckbox(16+32, y+32, 48, 48, rpl);
        doReplay.setAction(() -> {
            Config.set("DO_REPLAY", (Config.get("DO_REPLAY")==1)?("0"):("1"));
            doReplay.toggle();
        });

        actions.put("doReplay", doReplay.getActionZone());
        doReplay.setAction(actions.get("doReplay").action());
        addClickBinding(doReplay.getActionZone());

        body.addToContainer(doReplay);

        WLabel replayLabel = new WLabel("Enable replay",16+32+64, y+32+12, 16);
        body.addToContainer(replayLabel);


        WSelector<Integer> replay = new WSelector<>(16+32, y+32+12+48, false, false);
        ArrayList<Integer> rcounts = new ArrayList<>(Arrays.asList(1, 2, 4, 8, 12, 16, 24, 32, 48));
        rcounts.forEach(replay::add);
        if(!rcounts.contains(Config.REPLAY_COUNT)) {
            replay.add(Config.REPLAY_COUNT);
        }

        while(!replay.getCurrent().equals(Config.REPLAY_COUNT)) replay.step(1);


        replay.setNextAction( () -> {replay.step(1); Config.set("REPLAY_COUNT", ""+replay.getCurrent());});
        actions.put("Replay_Bnext", replay.getNextZone());
        replay.setNextAction(actions.get("Replay_Bnext").action());
        addClickBinding(replay.getNextZone());

        replay.setPrevAction( () -> {replay.step(-1); Config.set("REPLAY_COUNT", ""+replay.getCurrent());});
        actions.put("Replay_Bprev", replay.getPrevZone());
        replay.setPrevAction(actions.get("Replay_Bprev").action());
        addClickBinding(replay.getPrevZone());

        body.addToContainer(replay);


        // DEBUG

        WLabel DebugIcoLabel = new WLabel("\ue8be",x2, y+6,icoFont, UIConsts.COLOR_MIDGREY, 16);
        body.addToContainer(DebugIcoLabel);
        WLabel DebugLabel = new WLabel("Debug",x2+32, y, UIConsts.COLOR_MIDGREY, 16);
        body.addToContainer(DebugLabel);

        boolean dc = Config.ALLOW_CONSOLE;

        WCheckbox enableConsole = new WCheckbox(x2+32, y+32, 48, 48, dc);
        enableConsole.setAction(() -> {
            Config.set("ALLOW_CONSOLE", (Config.get("ALLOW_CONSOLE")==1)?("0"):("1"));
            enableConsole.toggle();
        });

        actions.put("enableConsole", enableConsole.getActionZone());
        enableConsole.setAction(actions.get("enableConsole").action());
        addClickBinding(enableConsole.getActionZone());

        body.addToContainer(enableConsole);

        WLabel consoleLabel = new WLabel("Enable console",x2+32+64, y+32+12, 16);
        body.addToContainer(consoleLabel);

        return body;
    }

    @Override
    public void BeforeAdd() {
        SizeC = 2*64;
    }

    public Options() {
        super();
    }

}
