package com.pingu.party.activities;

import com.pingu.party.gui.widget.WCheckbox;
import com.pingu.party.engine.utils.Lambda;

import com.pingu.party.engine.core.GameCanvas;
import com.pingu.party.gui.widget.WInput;
import com.pingu.party.network.structures.IP;
import com.pingu.party.engine.core.AssetManager;
import com.pingu.party.gui.widget.WCardWrapper;
import com.pingu.party.gui.widget.WButton;

import com.pingu.party.engine.input.ListenZone;
import java.util.HashMap;
import com.pingu.party.gui.widget.WImage;
import com.pingu.party.gui.widget.WSelector;
import com.pingu.party.engine.game.Profile;
import com.pingu.party.engine.game.ProfileManager;

public class Join extends BaseActivity {

  private static WInput wi;
  private static WInput wp;

  //private static WCheckbox wcb;
  private static IP server;
  private static WSelector<Profile> wsp;
  private static ProfileManager pm;

  public static void joinServer(Profile p) {
    removeBinds();
    //TODO: new Partie with Profile instead of boolean
    //setActivity(new Partie((server.toString()),Integer.parseInt(wp.getCaption()),p));
    setActivity(new Partie((server.toString()),Integer.parseInt(wp.getCaption()),p));
  }

  public static void removeBinds() {
    removeClickBinding(actions.get("Return"));
  	removeClickBinding(actions.get("Settings"));
  	removeClickBinding(actions.get("Join"));
    removeClickBinding(actions.get("Port"));
    removeClickBinding(actions.get("IP"));
    removeClickBinding(actions.get("isIA"));
    removeClickBinding(actions.get("P_Bnext"));
    removeClickBinding(actions.get("P_Bprev"));
    TicksListener.clear();
  }

  private static void initProfiles() {
    pm = new ProfileManager();
  }

  public static void newWin() {
    removeBinds();
    setCard(Menu.make_card(0, 0));
    Menu.wPlay = Menu.createPlayButtons();
  	pushAdded();
  }

  public static boolean doCheck() {
    //actions before check
    if(wi.isFocused()) wi.toggleFocus();
    if(wp.isFocused()) wp.toggleFocus();

    //values check
    server = IP.parseIP(wi.getCaption());
    if(server != null) System.out.println("IP is : "+server.toString());
    else {
      Logger.warn("Invalid IP !", 200);
      return false;
    }
    try {
      int i = Integer.parseInt(wp.getCaption());
      if(i != 0) System.out.println("Port is : "+i);
    } catch (Exception e) {
      Logger.warn("Invalid Port !", 200);
      return false;
    }

    System.out.println("User : "+wsp.getCurrent().getName());
    if(wsp.getCurrent().getAI()) System.out.println("AI");
    else System.out.println("Regular user");

    //allow screen change
    return true;
  }

  @Override
  public WCardWrapper makeC(int dec, int wid) {
    return make_card(dec, wid);
  }

  public static WCardWrapper make_card(int dec, int wid) {
    initProfiles();
    WCardWrapper body = new WCardWrapper(0, 0,dec, wid, AssetManager.getImage("VersusT"));

    Runnable rb = (() -> {
      removeBinds();
      newWin();
      //setActivity(new Menu());
    });
    WButton returnb = new WButton(12, wid-72-64, 326, 64, 0);
    returnb.setBackground(new WImage(0, 0, 326, 64, AssetManager.getImage("returnBHT")));

    returnb.setAction(rb);
    actions.put("Return", returnb.getActionZone());
    returnb.setAction(actions.get("Return").action());
    addClickBinding(returnb.getActionZone());

    body.addToContainer(returnb);

    Runnable rs = (() -> {
      if(doCheck()) joinServer(wsp.getCurrent());
    });

    WButton startb = new WButton(dec-12-326, wid-72-64, 326, 64, 0);
    startb.setBackground(new WImage(0, 0, 326, 64, AssetManager.getImage("joinBHT")));

    startb.setAction(rs);
    actions.put("Join", startb.getActionZone());
    startb.setAction(actions.get("Join").action());
    addClickBinding(startb.getActionZone());

    body.addToContainer(startb);

    wp = new WInput("4242", 8, 48);

    Runnable r2 = (() -> {
      if(wi.isFocused()) {
        clearInput(wp.getCaption());
        wi.toggleFocus();
      }
      if(!wp.isFocused() && !canInputListen()) {
        System.out.println("asking");
        askInputListen();
      }
      if(wp.isFocused()) {
        relaseInputListen();
      }
      wp.toggleFocus();
      //if(!canInputListen() && wi.isFosuced()) askInputListen();
      System.out.println(inputBuffer);
    });
    wp.setAction(r2);

    actions.put("Port", wp.getActionZone());
    wp.setAction(actions.get("Port").action());
    addClickBinding(wp.getActionZone());

    body.addToContainer(wp);

    wi = new WInput("127.0.0.1", 8 , 8);


    Runnable r = (() -> {
      if(wp.isFocused()) {
        clearInput(wi.getCaption());
        wp.toggleFocus();
      }
      if(!wi.isFocused() && !canInputListen()) {
        System.out.println("asking");
        askInputListen();
      }
      if(wi.isFocused()) {
        relaseInputListen();
      }
      wi.toggleFocus();
      //if(!canInputListen() && wi.isFosuced()) askInputListen();
      System.out.println(inputBuffer);
    });
    wi.setAction(r);
    //isFosuced
    actions.put("IP", wi.getActionZone());
    wi.setAction(actions.get("IP").action());
    addClickBinding(wi.getActionZone());

    server = IP.parseIP(wi.getCaption());
    if(server != null) System.out.println("IP : "+server.toString());

    body.addToContainer(wi);

    Lambda<String> k = ((String s) -> {
      //System.out.print("o");

      if(!canInputListen() && wi.isFocused()) wi.toggleFocus();
      if(!canInputListen() && wp.isFocused()) wp.toggleFocus();
      if((wi.isFocused()) && wi.getCaption() != s) wi.setCaption(s);
      if((wp.isFocused()) && wp.getCaption() != s) wp.setCaption(s);
      //wi.listen();
    });

    TicksListener.add(k);

    Lambda<String> l = (String a) -> {
      a = "a";
    };
    String[] s = {"A", "B"};
    for (String st : l.runW(s)) {
      System.out.println(st);
    }

    wsp = new WSelector<Profile>(8,88, false, false);
    wsp.setNextAction( () -> {wsp.step(1);});
    actions.put("P_Bnext", wsp.getNextZone());
    wsp.setNextAction(actions.get("P_Bnext").action());
    addClickBinding(wsp.getNextZone());

    wsp.setPrevAction( () -> {wsp.step(-1);});
    actions.put("P_Bprev", wsp.getPrevZone());
    wsp.setPrevAction(actions.get("P_Bprev").action());
    addClickBinding(wsp.getPrevZone());

    pm.getList().forEach(p -> {
      wsp.add(p);
    });

    body.addToContainer(wsp);

    /*wcb = new WCheckbox(8,152,48,48,false);
    wcb.setAction(()->{
      wcb.toggle();
      System.out.println(wcb.isChecked());
    });
    actions.put("isIA", wcb.getActionZone());
    wcb.setAction(actions.get("isIA").action());
    addClickBinding(wcb.getActionZone());
    body.addToContainer(wcb);*/

    /*Lambda<String> l = (String s) -> {
      System.out.println(s);
      s="OWO";
      wi.setCaption(s);
    };
    l.run("Lol");
    System.out.println(l.yield("owo"));*/

    return body;
  }

  @Override
  public void BeforeAdd() {
    SizeC = 4*64;

  }

  public Join() {
    super();
  }

}
