package com.pingu.party.activities;

import com.pingu.party.engine.game.Profile;

import com.pingu.party.gui.widget.WCardWrapper;

import java.io.FilenameFilter;
import java.util.Map;

import com.pingu.party.engine.core.AssetManager;
import com.pingu.party.engine.core.StringManager;

import java.util.HashMap;
import com.pingu.party.gui.widget.WPopup;

import com.pingu.party.engine.input.ListenZone;

import com.pingu.party.gui.widget.WTitledText;
import com.pingu.party.gui.widget.WIconLayout;

import com.pingu.party.gui.widget.WImage;
import com.pingu.party.gui.widget.WButton;

import com.pingu.party.gui.widget.WStatusIcon;
import com.pingu.party.gui.widget.WList;
import com.pingu.party.gui.widget.WListItem;
import com.pingu.party.launcher.Utils.RessourceComputer;
import com.pingu.party.network.structures.IP;
import com.pingu.party.network.structures.Server;
import com.pingu.party.stats.StatManager;

import java.util.ArrayList;
import com.pingu.party.gui.widget.WProgress;
import com.pingu.party.launcher.Utils.Font.Roboto;
import com.pingu.party.gui.widget.WLabel;
import com.pingu.party.gui.widget.WCardTitle;
import com.pingu.party.gui.widget.WBackground;
import com.pingu.party.gui.widget.WTitle;
import com.pingu.party.gui.widget.WPoly;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Image;
import java.awt.Color;
import com.pingu.party.launcher.Utils.UIConsts;
import com.pingu.party.gui.widget.WTitleBar;

import com.pingu.party.gui.widget.Widget;
import com.pingu.party.engine.config.Config;
import com.pingu.party.engine.config.LoadInfo;

import java.awt.Graphics2D;
import java.util.regex.Pattern;

public class Menu extends BaseActivity {
  private Color almostGrey = new Color(72, 87, 92, 204);
  private Widget w;

  // Calculated var
  private static int dec;

  static WPopup popup = null;

  // Level card
  //
  private static Profile currentProfile = LoadInfo.ACTUALPROFILE;

  private static int pCount = currentProfile.getStats().getPlayedGames(); //Parties count
  //xp = level * 60
  private static int winGames =  currentProfile.getStats().getWin();
  private static int level = pCount*winGames*((int)(pCount/10))+1;
  private static int xp = 0;

  // Servers card
  private static ArrayList<Server> servers = null;
  private static WList servLis;

  // Replay Card
  private static ArrayList<String> savedParties = null;
  private static ArrayList<WButton> PartiesPlay = null;

  // Colors
  private static Color deepBlue = new Color(87, 161, 211);
  private static Color deepWhite = new Color(193, 198, 200);
  private static Color testGrey = new Color(132, 142, 146);
  private static Color softBlue = new Color(118, 158, 174);
  private static Color titleBlue = new Color(74, 111, 125);

  private static WProgress progr;

  protected static Widget wStats = null;
  protected static Widget wServer = null;
  protected static Widget wPlay = null;
  protected static Widget wReplay = null;

  private static WCardTitle statsC;
  private static WLabel pCountLabel;
  private static WLabel levelLabel;
  private static WLabel xpLabel;
  private static WLabel totalLabel;

  private static WCardTitle savedC;
  private static WLabel savedCL1;
  private static WLabel savedCL2;
  private static WLabel savedCL3;
  private static WLabel savedCL4;
  private static WButton savedCB1;
  private static WButton savedCB2;
  private static WButton savedCB3;
  private static WButton savedCB4;
  private static WButton savedPB;
  private static WButton savedNB;

  private static int deb = 0;

  public static void refresh() {
    currentProfile = LoadInfo.ACTUALPROFILE;
    pCount = currentProfile.getStats().getPlayedGames();
    winGames = currentProfile.getStats().getWin();
    level = (winGames/10)+1;
    int plvl = (winGames/10);
    try {
      xp = winGames%10*6*level;
      System.out.println((winGames * (level*60))+ " "+pCount);
    } catch (Exception e) {
      xp = 0;
    }

    pCountLabel = new WLabel(""+pCount, 16, 0,new Roboto().get_boldcondensed(40), deepBlue, 40);
    levelLabel = new WLabel(""+level, 480-95, 17,new Roboto().get_boldcondensed(76), deepWhite, 58);
    xpLabel = new WLabel(""+xp, 209, 50,new Roboto().get_boldcondensed(15), deepBlue, 12);
    totalLabel = new WLabel("/"+level*60, 242, 50,new Roboto().get_boldcondensed(15), testGrey, 12);
  }

  public static void remake() {
    rescaleAll();
    refresh();
  }

  public static Widget createStatsCard() {
	System.out.println("selected => "+LoadInfo.ACTUALPROFILE);
    Widget wi = new Widget(dec,120, 480, 120, false);
    wi.setHorizontalAlign(2);
    wi.setHorizontalMargin(48);

    Image ic = null;
    try {
      ic = ImageIO.read(new File("res/UI/menu/titleico.png"));

    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }

    refresh();

    statsC = new WCardTitle(0, 0, 480, 120, ic, StringManager.get("str_stats"));
    statsC.addToContainer(pCountLabel);
    statsC.addToContainer(new WLabel(StringManager.get("str_games"), 82, 28,new Roboto().get_medium(13), testGrey, 12));
    statsC.addToContainer(new WPoly(480-168, 0, 168, 96, 56, softBlue));
    statsC.addToContainer(levelLabel);
    statsC.addToContainer(xpLabel);
    statsC.addToContainer(totalLabel);
    progr = new WProgress(12, 72, 480-168-24, 16, deepBlue, titleBlue);
    statsC.addToContainer(progr);
    wi.add(statsC);
    progr.setProgress(2);
    return wi;
  }

  public static Widget createServerCard() {
    if (servers == null) {
      servers = new ArrayList<Server>();
      servers.add(new Server(StringManager.get("str_remote_host")+" 1",new IP(10,0,0,1)));
      servers.add(new Server(StringManager.get("str_remote_host")+" 2",new IP(10,0,0,2)));
      servers.add(new Server("Random lol",new IP(94,142,12,45)));
    }
    Widget wi = new Widget(dec,264, 480, 300, false);
    wi.setHorizontalAlign(2);
    wi.setHorizontalMargin(48);
    Image ic = null;
    try {
      ic = ImageIO.read(new File("res/UI/menu/titleico.png"));
    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }

    WCardTitle servC = new WCardTitle(0, 0, 480, 24+16+servers.size()*64, ic, StringManager.get("str_servers"));
    servLis = new WList(0,0,480, 0, 8);


    WStatusIcon wsi;
    WIconLayout wil;
    WTitledText wtt;
    for (Server s : servers) {
      WListItem wli = new WListItem(0,0,480,64);
      wsi = new WStatusIcon(12,12, "res/UI/menu/serv.png",s.getStatus());
      wtt = new WTitledText(6,8,256,24,0, s.getName()); //int x, int y, int w, int h, int m, String s
      WLabel Sip = new WLabel(s.getIPText(), 0, 0, 12, UIConsts.COLOR_MIDGREY, 16); //(s,x, y, fsize-(fsize/6), tc, fsize);
      WListItem wttChild = new WListItem(6,0,256,24); //int x, int y, int w, int h
      wttChild.setInner(Sip);
      wtt.add(wttChild);
      WListItem wttChild2 = new WListItem(6,0,256,24);
      wttChild2.setInner(new WLabel(s.getStatus().toString(), 0, 0, 12, UIConsts.COLOR_MIDGREY, 16));
      //wtt.add(wttChild2);
      wil = new WIconLayout(12, 0, wsi, wtt);
      //wil = new WIconLayout(12, 0, wsi, Sip);
      wil.setIconWidth(64);
      wli.setInner(wil);
      servLis.add(wli);
    }
    servC.addToContainer(servLis);
    wi.add(servC);
    return wi;
  }

  private static void scanRepDir() {
      if(savedParties == null) {
          savedParties = new ArrayList<>();
      }
      File dir = new File("saves/");
      File [] files = dir.listFiles(new FilenameFilter() {
          @Override
          public boolean accept(File dir, String name) {
              return name.endsWith(".replay");
          }
      });
      savedParties.clear();
      if(files != null && files.length > 0) {
          for (File rpart : files) {
              savedParties.add(rpart.getName());
          }
      }

  }

  private static void scanSaved() {
    if(savedParties == null) {
      savedParties = new ArrayList<>();
      /*savedParties.add("Save 1");
      savedParties.add("Save 2");
      savedParties.add("Save 3");*/
    }

    System.out.println("Replay widget created " + System.nanoTime());

    File dir = new File("saves/");
    File [] files = dir.listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        return name.endsWith(".replay");
      }
    });

    savedParties.clear();
    if(files != null && files.length > 0) {
      for (File rpart : files) {
        savedParties.add(rpart.getName());
      }
    } else {
      savedParties.add(""+System.nanoTime());
    }

    int y = 8;
    int liSize = 32;

    savedC.cleanContainer();
    //TODO: clean buttons binds

    savedCL1 = new WLabel("", 8, y+2, 16);
    savedCB1 = new WButton(480-40, y+4, 24, 24);
    savedCB1.setBackground(new WImage(AssetManager.getImage("Bplay")));
    //actions.put("savedCB2", savedCB2.getActionZone());

    y += liSize;
    savedCL2 = new WLabel("", 8, y+2, 16);
    savedCB2 = new WButton(480-40, y+4, 24, 24);
    savedCB2.setBackground(new WImage(AssetManager.getImage("Bplay")));
    //actions.put("savedCB1", savedCB1.getActionZone());

    y += liSize;
    savedCL3 = new WLabel("", 8, y+2, 16);
    savedCB3 = new WButton(480-40, y+4, 24, 24);
    savedCB3.setBackground(new WImage(AssetManager.getImage("Bplay")));
    //actions.put("savedCB3", savedCB3.getActionZone());

    y += liSize;
    savedCL4 = new WLabel("", 8, y+2, 16);
    savedCB4 = new WButton(480-40, y+4, 24, 24);
    savedCB4.setBackground(new WImage(AssetManager.getImage("Bplay")));
    //actions.put("savedCB4", savedCB4.getActionZone());

    savedNB = new WButton(480-24, -24, 16, 16);
    savedPB = new WButton(480-24-24, -24, 16, 16);

    y = 8;

    makeReplayButtons();

    savedC.addToContainer(savedNB);
    savedC.addToContainer(savedPB);

    savedC.addToContainer(savedCL4);
    savedC.addToContainer(savedCB4);
    savedC.addToContainer(savedCL3);
    savedC.addToContainer(savedCB3);

    savedC.addToContainer(savedCL2);
    savedC.addToContainer(savedCB2);
    savedC.addToContainer(savedCL1);
    savedC.addToContainer(savedCB1);

    refresh();
  }

  private static void makeReplayButtons() {

    removeClickBinding(actions.get("savedCB4"));
    removeClickBinding(actions.get("savedCB3"));
    removeClickBinding(actions.get("savedCB2"));
    removeClickBinding(actions.get("savedCB1"));

    savedCB4.hide();
    savedCB3.hide();
    savedCB2.hide();
    savedCB1.hide();

    savedCL4.hide();
    savedCL3.hide();
    savedCL2.hide();
    savedCL1.hide();

    if (savedParties != null && savedParties.size() > deb + 0) {
      System.out.println(" > " + (savedParties.size() - deb));
      int sz = savedParties.size() - deb;

      if (sz >= 4) {
        savedCL4.setCaption(savedParties.get(deb + 3));
        savedCL4.show();
        savedCB4.setAction(() -> {
          System.out.println("Playing " + savedCL4.getCaption());
		  setActivity(new Partie("./saves/"+savedCL4.getCaption()));
        });
        actions.put("savedCB4", savedCB4.getActionZone());
        addClickBinding(actions.get("savedCB4"));
        savedCB4.show();
      }
      if (sz >= 3) {
        savedCL3.setCaption(savedParties.get(deb + 2));
        savedCL3.show();
        savedCB3.setAction(() -> {
          System.out.println("Playing " + savedCL3.getCaption());
		  setActivity(new Partie("./saves/"+savedCL3.getCaption()));
        });
        actions.put("savedCB3", savedCB3.getActionZone());
        addClickBinding(actions.get("savedCB3"));
        savedCB3.show();
      }
      if (sz >= 2) {
        savedCL2.setCaption(savedParties.get(deb + 1));
        savedCL2.show();
        savedCB2.setAction(() -> {
          System.out.println("Playing " + savedCL2.getCaption());
		  setActivity(new Partie("./saves/"+savedCL2.getCaption()));
        });
        actions.put("savedCB2", savedCB2.getActionZone());
        addClickBinding(actions.get("savedCB2"));
        savedCB2.show();
      }
      if (sz >= 1) {
        savedCL1.setCaption(savedParties.get(deb));
        savedCL1.show();
        savedCB1.setAction(() -> {
          System.out.println("Playing " + savedCL1.getCaption());
		  setActivity(new Partie("./saves/"+savedCL1.getCaption()));
        });
        actions.put("savedCB1", savedCB1.getActionZone());
        addClickBinding(actions.get("savedCB1"));
        savedCB1.show();
      }

      if(savedParties.size()-deb>4) {
        savedNB.setBackground(new WImage(AssetManager.getImage("Snext")));
      } else {
        savedNB.setBackground(new WImage(AssetManager.getImage("SnextD")));
      }

      savedNB.setAction(() -> {
        if(savedParties.size()-deb > 4) {
          deb += 4;
        }
        if(savedParties.size()-deb>4) {
          savedNB.setBackground(new WImage(AssetManager.getImage("Snext")));
        } else {
          savedNB.setBackground(new WImage(AssetManager.getImage("SnextD")));
        }
        makeReplayButtons();
      });

      actions.put("savedNB", savedNB.getActionZone());
      addClickBinding(actions.get("savedNB"));

      savedPB.setAction(() -> {
        if(deb>=4) {
          deb -= 4;
        }
        if(deb>=4) {
          savedPB.setBackground(new WImage(AssetManager.getImage("Sprev")));
        } else {
          savedPB.setBackground(new WImage(AssetManager.getImage("SprevD")));
        }
        makeReplayButtons();
      });

      if(deb>=4) {
        savedPB.setBackground(new WImage(AssetManager.getImage("Sprev")));
      } else {
        savedPB.setBackground(new WImage(AssetManager.getImage("SprevD")));
      }

      actions.put("savedPB", savedPB.getActionZone());
      addClickBinding(actions.get("savedPB"));
    }
  }

  public static Widget createReplay() {
    Widget wi = new Widget(dec,520, 480, 300, false);

    int definedSize = 4;

    if(PartiesPlay == null) PartiesPlay = new ArrayList<>();

    wi.setHorizontalAlign(2);
    wi.setHorizontalMargin(48);
    Image ic = null;
    try {
      ic = ImageIO.read(new File("res/UI/menu/titleico.png"));
    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }

    if(savedParties == null) {
      savedParties = new ArrayList<>();
    }

    savedC = new WCardTitle(0, 0, 480, 24+16+definedSize*32, ic, "Replay"); //TODO: StringManager

    scanSaved();

    wi.add(savedC);
    return wi;
  }

  public static Widget createPlayButtons() {
    remakeCards();
    Widget wi = new Widget(96,152, 480, 256);

    refresh();
    //SOLO BUTTON

    /*Image ic = null;
    try {
      ic = ImageIO.read(new File("res/UI/menu/buttons/button-solo.png"));
    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }*/
    //Image hostHT = null;
    //wi.add(new WImage(0, 0, 480, 120, ic));

    Runnable r = (() -> {
      System.out.println("Solo");
      pre();
      setCard(Local.make_card(BaseActivity.dec, BaseActivity.SizeC+88+72+64));
      //setActivity(new Local());
    });

    WButton wb = new WButton(0, 0, 480, 120, 0);

    wb.setAction(r);
    actions.put("Solo", wb.getActionZone());
    wb.setAction(actions.get("Solo").action());

    wb.setBackground(new WImage(0, 0, 480, 120, AssetManager.getImage("button-solo")));
    addClickBinding(wb.getActionZone());
    wi.add(wb);
    // ListenZone b = new ListenZone(96, 288, 480 , 120, r);
    // addClickBinding(b);

    //VERSUS BUTTON

    /*Image ic2 = null;
    try {
      ic2 = ImageIO.read(new File("res/UI/menu/buttons/button-vs.png"));
    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }*/

    Runnable r2 = (() -> {
      showPopup();
    });

    //WButton wb2 = new WButton(0, 136, 480, 120, 0);
    WButton wb2 = new WButton(0, 136, 480, 120, 0);

    wb2.setAction(r2);
    actions.put("Versus", wb2.getActionZone());
    wb2.setAction(actions.get("Versus").action());

    wb2.setBackground(new WImage(0, 0, 480, 120, AssetManager.getImage("button-vs")));
    addClickBinding(wb2.getActionZone());
    wi.add(wb2);

    // Stats button => I did not choosed to add it
    /*Image ic3 = null;
    try {
      ic3 = ImageIO.read(new File("res/UI/menu/buttons/button-stats.png"));
    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }*/
    Runnable r3 = (() -> {
    	LoadInfo.STATS = new StatManager().getList();
      pre();
      setCard(ShowStats.make_card(BaseActivity.dec, BaseActivity.SizeC+88+72));
      //setActivity(new ShowStats());
    });

    WButton wb3 = new WButton(0, 272, 480, 120, 0);
    wb3.setAction(r3);
    actions.put("Stats", wb3.getActionZone());
    wb3.setAction(actions.get("Stats").action());

    wb3.setBackground(new WImage(0, 0, 480, 120, AssetManager.getImage("button-stats")));
    addClickBinding(wb3.getActionZone());
    wi.add(wb3);

    // Profiles button => I did not choosed to add it too
  /*  Image ic4 = null;
    try {
      ic4 = ImageIO.read(new File("res/UI/menu/buttons/button-profile.png"));
    } catch (Exception e) {
      Logger.warn("Load failed : "+e, 500);
    }*/
    Runnable r4 = (() -> {
      pre();
      setCard(ProfileList.make_card(BaseActivity.dec, BaseActivity.SizeC+88+72));
      //setActivity(new ProfileList());
    });

    WButton wb4 = new WButton(0, 408, 480, 120, 0);
    wb4.setAction(r4);
    actions.put("Profiles", wb4.getActionZone());
    wb4.setAction(actions.get("Profiles").action());

    wb4.setBackground(new WImage(0, 0, 480, 120, AssetManager.getImage("button-profile")));
    addClickBinding(wb4.getActionZone());
    wi.add(wb4);

    return wi;
  }

  public static Widget createPopup() {

    if(popup != null) {
      popup.setHiden();
      return popup;
    }
    popup = new WPopup(576,360);


    Runnable rh = (() -> {
      //System.out.println("Host Hover");
    });
    Runnable rc = (() -> {
      System.out.println("Clicked Host");
      pre();
      setCard(Online.make_card(BaseActivity.dec, BaseActivity.SizeC+88+72+64));

      //setActivity(new Online());
    });

    Runnable rh2 = (() -> {
      //System.out.println("Join Hover");
    });
    Runnable rc2 = (() -> {
      System.out.println("Clicked Join");
      pre();
      setCard(Join.make_card(BaseActivity.dec, BaseActivity.SizeC+88+72));
    });


    WButton wb = new WButton(16, 16, 256, 256, 0);
    wb.setBackground(new WImage(0, 0, 256, 256, AssetManager.getImage("btn256-e")));

    wb.setHoverAction(rh);
    actions.put("HoverHost", wb.getHoverZone());
    wb.setHoverAction(actions.get("HoverHost").action());

    wb.setAction(rc);
    actions.put("ClickedHost", wb.getActionZone());
    wb.setAction(actions.get("ClickedHost").action());

    wb.getHoverZone().disable();
    wb.getActionZone().disable();
    addHoverBinding(wb.getHoverZone());
    addClickBinding(wb.getActionZone());

    popup.add(wb);

    popup.add(new WImage(0, 288, 288, 64, AssetManager.getImage("hostHT")));
    popup.add(new WImage(288, 288, 288, 64, AssetManager.getImage("joinHT")));

    WButton wb2 = new WButton(304, 16, 256, 256, 0);
    wb2.setBackground(new WImage(0, 0, 256, 256, AssetManager.getImage("btn256-e")));

    wb2.setHoverAction(rh2);
    actions.put("HoverJoin", wb2.getHoverZone());
    wb2.setHoverAction(actions.get("HoverJoin").action());

    wb2.setAction(rc2);
    actions.put("ClickedJoin", wb2.getActionZone());
    wb2.setAction(actions.get("ClickedJoin").action());

    wb2.getHoverZone().disable();
    wb2.getActionZone().disable();
    addHoverBinding(wb2.getHoverZone());
    addClickBinding(wb2.getActionZone());

    popup.add(wb2);

    popup.setHiden();

    return popup;
  }

  private static void pre() {
    popup.setHiden();
    removeBinds();
  }

  public static void removeAllBinds() {
    removeBinds();
    //removeClickBinding(actions.get("Settings"));
    removeClickBinding(actions.get("QUIT"));
    removeClickBinding(actions.get("Return"));
  }

  public static void removeBinds() {
    for (Map.Entry<String, ListenZone> e : actions.entrySet()) {
      String name = e.getKey();
      ListenZone lz = e.getValue();
      removeClickBinding(lz);
      //lz.disable();
    }
    removePlay();
    popup.setHiden();
  }


  private static void remakeCards() {
    if(PartiesPlay == null) return;
    for(WButton wb : PartiesPlay) {
      addClickBinding(wb.getActionZone());
    }
  }

  private static void removePlay() {
    for(WButton wb : PartiesPlay) {
      removeClickBinding(wb.getActionZone());
    }
  }

  private static void showPopup() {
    actions.get("Versus").disable(); // Disable butons action
    actions.get("Solo").disable();  // Reactivate if, on "out click" for example
    actions.get("Profiles").disable();  // Reactivate if, on "out click" for example
    actions.get("Stats").disable();  // Reactivate if, on "out click" for example

    actions.get("HoverHost").enable();
    actions.get("ClickedHost").enable();
    addClickBinding(actions.get("ClickedHost"));
    actions.get("HoverJoin").enable();
    actions.get("ClickedJoin").enable();
    addClickBinding(actions.get("ClickedJoin"));
    popup.setVisible();
  }

  public static WCardWrapper make_card(int dec, int wid) {
    //removeWidget(wReplay);
    //scanSaved();
    //savedParties.add(""+System.nanoTime());
    scanRepDir();
    makeReplayButtons();

    //addWidget(wReplay);
    return null;
  }

  @Override
  public void BeforeAdd() {
    wStats = createStatsCard();
    wServer = createServerCard();
    wPlay = createPlayButtons();
    wReplay = createReplay();
    popup = (WPopup)createPopup();
    addWidget(wStats);
    addWidget(wServer);
    addWidget(wPlay);
    addWidget(wReplay);
    addWidget(popup);
    Runnable r2 = (() -> {
      System.out.println("SCROLL EVENT !");
    });
    ListenZone b2 = new ListenZone(5, 5, 50 , 50, r2);
    addScrollBinding(b2);
  }

@Override
protected void drawMode() {
  drawcard = false;
}

  public void calcProgress() {
    progr.setProgress((int)(xp/(level*60f)*100));
  }

  public Menu() {
    super();
    System.out.println(LoadInfo.ACTUALPROFILE);
    //init();
    calcProgress();
    if(popup != null) popup.setHiden();
  }

}
