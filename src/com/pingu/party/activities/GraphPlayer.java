package com.pingu.party.activities;

import com.pingu.party.engine.config.LoadInfo;
import com.pingu.party.engine.core.AssetManager;
import com.pingu.party.engine.game.Profile;
import com.pingu.party.engine.game.ProfileManager;
import com.pingu.party.gui.widget.*;
import com.pingu.party.stats.Round;
import com.pingu.party.stats.Stat;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bastien
 * @author florian
 *
 */
public class GraphPlayer extends BaseActivity {
  private int profilsC = 7; // Profiles count => display size

  private static ArrayList<WSelector<String>> wias;
  private static WSelector<Profile> prop;

  private static WList profileList;
  private static ProfileManager pm;

  private static WHist wh;
  private static ArrayList<String> props;
  private static int curProp = 0;

  public static void removeBinds() {
    removeClickBinding(actions.get("Return"));
    removeClickBinding(actions.get("Settings"));
    removeClickBinding(actions.get("Edit Profile"));
    removeClickBinding(actions.get("Create"));
    removeClickBinding(actions.get("Delete"));
    removeClickBinding(actions.get("Change"));
    removeClickBinding(actions.get("Profile"));
    removeClickBinding(actions.get("pr_next"));
    removeClickBinding(actions.get("pr_prev"));
    removeClickBinding(actions.get("P_Bnext_0"));
    removeClickBinding(actions.get("P_Bprev_0"));
    removeClickBinding(actions.get("P_Bnext_1"));
    removeClickBinding(actions.get("P_Bprev_1"));
    removeClickBinding(actions.get("P_Bnext_2"));
    removeClickBinding(actions.get("P_Bprev_2"));
  }

  public static void newWin() {
    setCard(Menu.make_card(0, 0));
    Menu.wPlay = Menu.createPlayButtons();
    pushAdded();
  }

  public static void back() {
    setCard(ShowStats.make_card(BaseActivity.dec, BaseActivity.SizeC+88+72));
  }

  @Override
  public WCardWrapper makeC(int dec, int wid) {
    return make_card(dec, wid);
  }

  private static void initProfiles() {
    pm = new ProfileManager();
  }

  private static int getProp(Round r, int id) {
    switch (wias.get(id).getCurrent()) {
      case "score":
        return r.getScore();
      case "won":
        return r.getWon();
      case "lost":
        return r.getLost();
      case "left":
        return r.getLeft();
      case "time":
        return r.getTime();
      case "elo":
        return r.getElo();
      default:
        return r.getElo();
    }
  }

  private static void doPropStep(int v) {
    prop.step(v);

    ArrayList<Integer> axe1 = new ArrayList<>();
    ArrayList<Integer> axe2 = new ArrayList<>();
    ArrayList<Integer> axe3 = new ArrayList<>();

    for (int nb = 0; nb < 3; nb++) {
      Profile profile = prop.getCurrent();
      Stat stats = profile.getStats();
      List<Round> rounds = stats.getRounds();
      System.out.print(nb+" [");
      for (int i = 0; i < rounds.size(); i++) {
        int n = getProp(rounds.get(i), nb);
        System.out.print(" " + n);
        switch (nb) {
          case 0:
            axe1.add(n);
            break;
          case 1:
            axe2.add(n);
            break;
          case 2:
            axe3.add(n);
            break;
          default:
            break;
        }
      }
      System.out.println("] ");
    }
    /**/
    wh.setAxis(0, axe1);
    wh.setAxis(1, axe2);
    wh.setAxis(2, axe3);
  }

  private static void doStep(int id, int v) {
    wias.get(id).step(v);
    Profile profile = prop.getCurrent();
    Stat stats = profile.getStats();
    String n = wias.get(id).getCurrent();
    wh.setAxisName(id, n);

    ArrayList<Integer> axe1 = new ArrayList<>();
    List<Round> rounds = stats.getRounds();
    int lines = rounds.size();
    for (int i = 0; i < lines; i++) {
    	axe1.add(getProp(rounds.get(i),id));
    }

    wh.setAxis(id, axe1);
  }

  private static WCardWrapper makeSelector(WCardWrapper body, final int i, int py) {
    WSelector<String> w = new WSelector<>(dec-338-12, py, false, false);
    w.setNextAction( () -> {doStep(i, 1);});
    actions.put("P_Bnext_"+i, w.getNextZone());
    w.setNextAction(actions.get("P_Bnext_"+i).action());
    addClickBinding(w.getNextZone());

    w.setPrevAction( () -> {doStep(i,-1);});
    actions.put("P_Bprev_"+i, w.getPrevZone());
    w.setPrevAction(actions.get("P_Bprev_"+i).action());
    addClickBinding(w.getPrevZone());

    /*pm.getList().forEach(p -> {
      prop.add(p);
    });*/

    for (int j = 0; j < i; j++) {
     doStep(i, 1);
    }

    body.addToContainer(w);
    wias.add(w);

    return body;
  }

  private static WCardWrapper makeProfileCompare(WCardWrapper body) {
    wias = new ArrayList<WSelector<String>>();
    int py = 8;

    int pos = 0;

    props = new ArrayList<>();
    props.add("score");
    props.add("won");
    props.add("lost");
    props.add("left");
    props.add("time");
    props.add("elo");

    prop = new WSelector<>(dec-338-12, py, false, false);

    for(Profile p : pm.getList()) {
      prop.add(p);
    }
    prop.setNextAction(() -> doPropStep(1));
    actions.put("pr_next", prop.getNextZone());
    prop.setNextAction(actions.get("pr_next").action());
    addClickBinding(prop.getNextZone());

    prop.setPrevAction(() -> doPropStep(-1));
    actions.put("pr_prev", prop.getPrevZone());
    prop.setPrevAction(actions.get("pr_prev").action());
    addClickBinding(prop.getPrevZone());

    body.addToContainer(prop);

    py += 96;

    for (int i=0;i<3 ;i++) {
      WSelector<String> w = new WSelector<>(dec-338-12, py, false, false);

      props.forEach(p -> {
        w.add(p);
      });

      for (int j = 0; j < i; j++) {
        w.step(1);
      }

      body.addToContainer(w);
      wias.add(w);
      py += 64;
    }

    wias.get(0).setNextAction(() -> doStep(0, 1));
    actions.put("P_Bnext_0", wias.get(0).getNextZone());
    wias.get(0).setNextAction(actions.get("P_Bnext_0").action());
    addClickBinding(wias.get(0).getNextZone());

    wias.get(0).setPrevAction(() -> doStep(0, -1));
    actions.put("P_Bprev_0", wias.get(0).getPrevZone());
    wias.get(0).setPrevAction(actions.get("P_Bprev_0").action());
    addClickBinding(wias.get(0).getPrevZone());

    wias.get(1).setNextAction(() -> doStep(1, 1));
    actions.put("P_Bnext_1", wias.get(1).getNextZone());
    wias.get(1).setNextAction(actions.get("P_Bnext_1").action());
    addClickBinding(wias.get(1).getNextZone());

    wias.get(1).setPrevAction(() -> doStep(1, -1));
    actions.put("P_Bprev_1", wias.get(1).getPrevZone());
    wias.get(1).setPrevAction(actions.get("P_Bprev_1").action());
    addClickBinding(wias.get(1).getPrevZone());

    wias.get(2).setNextAction(() -> doStep(2, 1));
    actions.put("P_Bnext_2", wias.get(2).getNextZone());
    wias.get(2).setNextAction(actions.get("P_Bnext_2").action());
    addClickBinding(wias.get(2).getNextZone());

    wias.get(2).setPrevAction(() -> doStep(2, -1));
    actions.put("P_Bprev_2", wias.get(2).getPrevZone());
    wias.get(2).setPrevAction(actions.get("P_Bprev_2").action());
    addClickBinding(wias.get(2).getPrevZone());

    ArrayList<Integer> axe1 = new ArrayList<>();
    ArrayList<Integer> axe2 = new ArrayList<>();
    ArrayList<Integer> axe3 = new ArrayList<>();

    /*Random rand = new Random();
    int lines = 6;
    for (int i = 0; i < lines; i++) {
      axe1.add(rand.nextInt(150));
      axe2.add(rand.nextInt(150));
      axe3.add(rand.nextInt(150));
    }*/
    /* ERREUR D'AFFICHAGE
     */
    Profile profile = prop.getCurrent();
    for (int nb = 0; nb < 3; nb++) {
        Stat stats = profile.getStats();
        List<Round> rounds = stats.getRounds();
        System.out.print(nb+" [");
        for (int i = 0; i < rounds.size(); i++) {
          int n = getProp(rounds.get(i),nb);
          System.out.print(" " + n);
          switch (nb) {
            case 0:
              axe1.add(n);
              break;
            case 1:
              axe2.add(n);
              break;
            case 2:
              axe3.add(n);
              break;
            default:
              break;
          }
        }
        System.out.println("] ");
    }
     /**/

    //wh = new WHist(16, 8, 64*8, 64*6);
    wh = new WHist(16, 8, dec-338-12-32, 64*6);
    wh.addAxis(axe1, ""+wias.get(0).getCurrent());
    wh.addAxis(axe2, ""+wias.get(1).getCurrent());
    wh.addAxis(axe3, ""+wias.get(2).getCurrent());
    body.addToContainer(wh);


    Runnable rnew = (() -> {
      removeBinds();
      setCard(Graph.make_card(BaseActivity.dec, BaseActivity.SizeC+88+72));
      //setActivity(new EditProfile());
    });

    WButton create = new WButton(dec-64, -64, 64, 64, 0);
    create.setBackground(new WImage(0, 0, 64, 64, AssetManager.getImage("BstatsH")));

    create.setAction(rnew);
    actions.put("Profile", create.getActionZone());
    create.setAction(actions.get("Profile").action());
    addClickBinding(create.getActionZone());

    body.addToContainer(create);

    return body;
  }

  public static WCardWrapper make_card(int dec, int wid) {
    wid = 6*64+88+72;

    initProfiles();

    WCardWrapper body = new WCardWrapper(0, 0,dec, wid, AssetManager.getImage("statsT"));

    Runnable rb = (() -> {
      removeBinds();
      back();
    });
    WButton returnb = new WButton(12, wid-72-64, 326, 64, 0);
    returnb.setBackground(new WImage(0, 0, 326, 64, AssetManager.getImage("returnBHT")));

    returnb.setAction(rb);
    actions.put("Return", returnb.getActionZone());
    returnb.setAction(actions.get("Return").action());
    addClickBinding(returnb.getActionZone());

    body.addToContainer(returnb);

    body = makeProfileCompare(body);

    return body;
  }

  @Override
  public void BeforeAdd() {
    SizeC = profilsC*64;
    if(LoadInfo.ACTUALPROFILE == null) LoadInfo.ACTUALPROFILE = LoadInfo.PROFILES.get(0);
    initProfiles(); // FAKE PROFILE FILLER => CHANGE ME
  }

  public GraphPlayer() {
    super();
    //init();
  }
}
