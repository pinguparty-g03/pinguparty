package com.pingu.party.game;

import java.util.Vector;
import com.pingu.party.activities.Partie;


import com.pingu.party.engine.game.Profile;
import com.pingu.party.engine.game.ProfileManager;

/**
 * Class of the AI 
 * @author Remy BARRIOL
 */
public class AIOne extends Player implements AI
{
	protected int difficulty;
	/**
	*  * Constructor of the AI
	* @author Remy BARRIOL
	*/
	public AIOne(int nbPlayer,int number,Partie P)
	{
		super(nbPlayer,number,P);
		name="AI "+number;
	}

	public AIOne(Integer nbp, Partie p, Profile pr) {
		super(nbp,p,pr);
		System.out.println(this.getClass().getName()+" "+pr.getName()+" created");
		this.difficulty = pr.getLevel();
	}
	
	public AIOne(int nbPlayer,Partie P,Profile me)
	{
		super(nbPlayer,P,me);
		System.out.println(this.getClass().getName()+" "+me.getName()+" created");
		this.difficulty = me.getLevel();
	}
	
	/**
	*  * Set the action of the AI (It will choose every time the first choice it can do)
	* @param T The actual Board
	* @author Remy BARRIOL
	*/
	@SuppressWarnings("unchecked")
	public Vector possibilityPlayer(Board T,Game G)
	{
		Vector Poss=new Vector<int[]>();
		Vector Poss_Color=new Vector<int[]>();
		if(nbCard!=0)
		{
			boolean bColor[]=new boolean[5];
			bColor=booleanColor();
			for(int y=0;y<T.length(0);y++)
			{
				if(T.getColorBoard(0,y)==0)
				{
					T.changeColorBoard(0,y,6);
					int[] g1={0,y};
					Poss.add(g1);
					for(int o=0;o<5;o++)
					{
						if(bColor[o])
						{
							int[] g2={Poss.size()-1,o+1};
							Poss_Color.add(g2);
						}
					}
				}
			}
			for(int x=1;x<T.length();x++)
			{
				for(int y=0;y<T.length(x);y++)
				{
					if((T.getColorBoard(x,y)==0)&&((T.getColorBoard(x-1,y)!=0)&&(T.getColorBoard(x-1,y)!=6))&&((T.getColorBoard(x-1,y+1)!=0)&&(T.getColorBoard(x-1,y+1)!=6)))
					{
						if((bColor[T.getColorBoard(x-1,y)-1])||(bColor[T.getColorBoard(x-1,y+1)-1]))
						{
							if(T.getColorBoard(x-1,y)==T.getColorBoard(x-1,y+1))
							{
								int[] G1={x,y};
								Poss.add(G1);
								int[] G2={Poss.size()-1,T.getColorBoard(x-1,y)};
								Poss_Color.add(G2);
								T.changeColorBoard(x,y,6);
							}
							else
							{
								int[] G1={x,y};
								Poss.add(G1);
								if(bColor[T.getColorBoard(x-1,y)-1])
								{
									int[] G2={Poss.size()-1,T.getColorBoard(x-1,y)};
									Poss_Color.add(G2);
								}
								if(bColor[T.getColorBoard(x-1,y+1)-1])
								{
									int[] G3={Poss.size()-1,T.getColorBoard(x-1,y+1)};
									Poss_Color.add(G3);
								}
								T.changeColorBoard(x,y,6);
							}
						}
					}
				}
			}
			if(Poss.size()==0)
			{
				Poss.add(0);
				Poss.add(0);
				Poss.add(0);
				return Poss;
			}
			//displayHand();
			int choice=0;
			int[] hhh=(int[])Poss.get(choice);
			int x1=hhh[0];
			int y1=hhh[1];
			Poss.removeAllElements();
			for(int i=0;i<Poss_Color.size();i++)
			{
				hhh=(int[])Poss_Color.get(i);
				if(hhh[0]==choice)
				{
					Poss.add(hhh[1]);
				}
			} 
			choice=0;
			int col=(int)Poss.get(choice);
			Poss.removeAllElements();
			Poss.add(x1);
			Poss.add(y1);
			Poss.add(col);
		}
		else
		{
			Poss.add(0);
			Poss.add(0);
			Poss.add(0);
		}
		return Poss;
	}	
	@SuppressWarnings("unchecked")
	public Vector possibilityPlayer(Board T)
	{
		Vector Poss=new Vector<int[]>();
		Vector Poss_Color=new Vector<int[]>();
		if(nbCard!=0)
		{
			boolean bColor[]=new boolean[5];
			bColor=booleanColor();
			for(int y=0;y<T.length(0);y++)
			{
				if(T.getColorBoard(0,y)==0)
				{
					T.changeColorBoard(0,y,6);
					int[] g1={0,y};
					Poss.add(g1);
					for(int o=0;o<5;o++)
					{
						if(bColor[o])
						{
							int[] g2={Poss.size()-1,o+1};
							Poss_Color.add(g2);
						}
					}
				}
			}
			for(int x=1;x<T.length();x++)
			{
				for(int y=0;y<T.length(x);y++)
				{
					if((T.getColorBoard(x,y)==0)&&((T.getColorBoard(x-1,y)!=0)&&(T.getColorBoard(x-1,y)!=6))&&((T.getColorBoard(x-1,y+1)!=0)&&(T.getColorBoard(x-1,y+1)!=6)))
					{
						if((bColor[T.getColorBoard(x-1,y)-1])||(bColor[T.getColorBoard(x-1,y+1)-1]))
						{
							if(T.getColorBoard(x-1,y)==T.getColorBoard(x-1,y+1))
							{
								int[] G1={x,y};
								Poss.add(G1);
								int[] G2={Poss.size()-1,T.getColorBoard(x-1,y)};
								Poss_Color.add(G2);
								T.changeColorBoard(x,y,6);
							}
							else
							{
								int[] G1={x,y};
								Poss.add(G1);
								if(bColor[T.getColorBoard(x-1,y)-1])
								{
									int[] G2={Poss.size()-1,T.getColorBoard(x-1,y)};
									Poss_Color.add(G2);
								}
								if(bColor[T.getColorBoard(x-1,y+1)-1])
								{
									int[] G3={Poss.size()-1,T.getColorBoard(x-1,y+1)};
									Poss_Color.add(G3);
								}
								T.changeColorBoard(x,y,6);
							}
						}
					}
				}
			}
			if(Poss.size()==0)
			{
				Poss.add(0);
				Poss.add(0);
				Poss.add(0);
				return Poss;
			}
			//////////////////////////////
			partie.toDisplayBoard(T,nbpla);
			//////////////////////////////
			//T.toDisplay();
			//System.out.println("");
			//////////////////////////////
			partie.toDisplayHand(this);
			//////////////////////////////
			//displayHand();
			//displayPoss(Poss);
			int choice=0;
			int[] hhh=(int[])Poss.get(choice);
			int x1=hhh[0];
			int y1=hhh[1];
			Poss.removeAllElements();
			for(int i=0;i<Poss_Color.size();i++)
			{
				hhh=(int[])Poss_Color.get(i);
				if(hhh[0]==choice)
				{
					Poss.add(hhh[1]);
				}
			} 
			choice=0;
			int col=(int)Poss.get(choice);
			Poss.removeAllElements();
			Poss.add(x1);
			Poss.add(y1);
			Poss.add(col);
		}
		else
		{
			Poss.add(0);
			Poss.add(0);
			Poss.add(0);
		}
		return Poss;
	}
}