package com.pingu.party.game;

import java.util.Vector;
import com.pingu.party.activities.Partie;

import com.pingu.party.engine.game.Profile;

/**
 * Class of the player,which give the possibility of play in a turn 
 * @author Remy BARRIOL
 * @author Florian RIBOU
 */
public class Player
{
	protected String name;
	protected Card[] hand;
	protected boolean defeated=false;
	protected int turnOfDefeat=0;
	protected int nbCard=0;
	protected int nbMaxCard=0;
	protected int nbGreenCards = 0;
	protected int score = 0;
	protected int scoreRound = 0;	
	protected Partie partie;
	protected int nbpla;
	protected Profile profile;
	
	
	
	public String getHand()
	{
		String h="";
		for(int i=0;i<hand.length;i++)
		{
			if(hand[i].getColor()!=0)
				h+=hand[i].getColor();
		}
		return h;
	}
	public String getHand2()
	{
		String h="";
		for(int i=0;i<hand.length;i++)
		{
			h+=hand[i].getColor();
		}
		return h;
	}
	/**
	*  * Constructor of a Player
	* @param nbPlayer The number of player to determine the number of card in his hand
	* @param number The number is to determine his name and make a difference between him and the others
	* @param partie the actual game
	* @author Remy BARRIOL
	*/
	public Player(int nbPlayer,int number,Partie partie)
	{
		this.nbpla=nbPlayer;
		this.nbMaxCard = maxOfCards(nbPlayer);
		this.hand = new Card[nbMaxCard];
		for(int i=0; i < nbMaxCard; i++)
		{
			hand[i]=new Card();
		}
		this.name = "Player "+number;
		this.partie = partie;
	}
	
	public Player(int nbPlayer,Partie P,Profile me)
	{
		//System.out.println("New Player ! "+me.getName());
		this.profile = me;
		this.nbpla = nbPlayer;
		this.nbMaxCard = maxOfCards(nbPlayer);
		this.hand = new Card[nbMaxCard];
		for(int i=0; i < nbMaxCard; i++)
		{
			hand[i]=new Card();
		}
		this.name = me.getName();
		this.partie = P;
	}
	
	public Player(int nbPlayer,int number)
	{
		nbpla=nbPlayer;
		int hhh=0;
		if(nbPlayer==2)
		{
			hhh=14;
		}
		else if(nbPlayer==3)
		{
			hhh=12;
		}
		else if(nbPlayer==4)
		{
			hhh=9;
		}
		else if(nbPlayer==5)
		{
			hhh=7;
		}
		else if(nbPlayer==6)
		{
			hhh=6;
		}
		else
		{
			System.err.println("The number of players is incorrect");
		}
		nbMaxCard=hhh;
		hand=new Card[hhh];
		for(int i=0;i<hhh;i++)
		{
			hand[i]=new Card();
		}
		name="Player "+number;
	}
	
	public Profile getProfile()
	{
		return profile;
	}
	/**
	*  * Display the hand of the player
	* @author Remy BARRIOL
	*/
	public void displayHand()
	{
		System.out.println("Your hand, master : ");
		System.out.println();
		System.out.print("[ ");
		for(int i=0;i<nbCard-1;i++)
		{
			hand[i].displayCard();
			System.out.print("| ");
		}
		hand[nbCard-1].displayCard();
		System.out.println(" ]");
	}
	/**
	*  * Add a card in the hand of the player
	* @param NCard The card that we want to add
	* @author Remy BARRIOL
	*/
	public void addCard(Card NCard)
	{
		hand[nbCard]=NCard;
		nbCard++;
	}
	/**
	*  * Return the number of card the player have
	* @author Remy BARRIOL
	*/
	public int getNbCard()
	{
		return nbCard;
	}
	/**
	*  * Remove a card of a certain color in the hand of the player
	* @param C The color of the card to remove
	* @author Remy BARRIOL
	*/
	public void removeCard(int C)
	{
		int b=0;
		
		for(int i=0;i<nbCard;i++)
		{
			if(hand[i].getColor()==C)
			{
				b=i;
				i=nbCard+1;
			}
		}
		hand[b].changeColor(0);
		hand=cardAdvance(hand,b);
		nbCard--;
	}
	/**
	*  * Used in removeCard(), takes the chosen cards at the end of the table
	* @param C The table which will be modified
	* @param i The position of the Card wich has been take in removeCard()
	* @return the modified table
	* @author Remy BARRIOL
	*/
	public Card[] cardAdvance(Card []C,int i)
	{
		int t;
		for(int j=i;j<C.length-1;j++)
		{
			t=C[j].getColor();
			C[j].changeColor(C[j+1].getColor());
			C[j+1].changeColor(t);
		}
		return C;
	}
	/**
	*  * Returns the maximum number of cards the Player can have
	* @author Remy BARRIOL
	*/
	public int getMaxCard()
	{
		return nbMaxCard;
	}
	/**
	*  * Sorts the hand of the player (groups the cards by color)
	* @author Remy BARRIOL
	*/
	public void sortingHand()
	{
		Card x;
		int j;
		for(int i=1;i<hand.length;i++)
		{
			x=hand[i];
			j=i;
			while((j>0)&&(hand[j-1].getColor()>x.getColor()))
			{
				hand[j]=hand[j-1];
				j--;
			}
			hand[j]=x;
		}
	}
	/**
	*  * Change the number of cards the player have
	* @param i The number of cards that we want
	* @author Remy BARRIOL
	*/
	public void changeNbCard(int i)
	{
		nbCard=i;
	}
	/**
	*  * Shows all possibility of actions the player can do at his turn and let him the choice
	* @param T The actual Board
	* @author Remy BARRIOL
	*/
	@SuppressWarnings("unchecked")
	public Vector possibilityPlayer(Board T,Game G)
	{
		Vector Poss=new Vector<int[]>();
		Vector Poss_Color=new Vector<int[]>();
		if(nbCard!=0)
		{
			boolean bColor[]=new boolean[5];
			bColor=booleanColor();
			for(int y=0;y<T.length(0);y++)
			{
				if(T.getColorBoard(0,y)==0)
				{
					T.changeColorBoard(0,y,6);
					int[] g1={0,y};
					Poss.add(g1);
					for(int o=0;o<5;o++)
					{
						if(bColor[o])
						{
							int[] g2={Poss.size()-1,o+1};
							Poss_Color.add(g2);
						}
					}
				}
			}
			for(int x=1;x<T.length();x++)
			{
				for(int y=0;y<T.length(x);y++)
				{
					if((T.getColorBoard(x,y)==0)&&((T.getColorBoard(x-1,y)!=0)&&(T.getColorBoard(x-1,y)!=6))&&((T.getColorBoard(x-1,y+1)!=0)&&(T.getColorBoard(x-1,y+1)!=6)))
					{
						if((bColor[T.getColorBoard(x-1,y)-1])||(bColor[T.getColorBoard(x-1,y+1)-1]))
						{
							if(T.getColorBoard(x-1,y)==T.getColorBoard(x-1,y+1))
							{
								int[] G1={x,y};
								Poss.add(G1);
								int[] G2={Poss.size()-1,T.getColorBoard(x-1,y)};
								Poss_Color.add(G2);
								T.changeColorBoard(x,y,6);
							}
							else
							{
								int[] G1={x,y};
								Poss.add(G1);
								if(bColor[T.getColorBoard(x-1,y)-1])
								{
									int[] G2={Poss.size()-1,T.getColorBoard(x-1,y)};
									Poss_Color.add(G2);
								}
								if(bColor[T.getColorBoard(x-1,y+1)-1])
								{
									int[] G3={Poss.size()-1,T.getColorBoard(x-1,y+1)};
									Poss_Color.add(G3);
								}
								T.changeColorBoard(x,y,6);
							}
						}
					}
				}
			}
			if(Poss.size()==0)
			{
				Poss.add(0);
				Poss.add(0);
				Poss.add(0);
				return Poss;
			}
			//////////////////////////////
			partie.toDisplayBoard(G);
			//////////////////////////////
			//T.toDisplay();
			//System.out.println("");
			//////////////////////////////
			partie.toDisplayHand(this);
			//////////////////////////////
			//displayHand();
			displayPoss(Poss);
			int choice=theChosenOne(Poss.size(),"str_enter_poss");
			int[] hhh=(int[])Poss.get(choice);
			int x1=hhh[0];
			int y1=hhh[1];
		//	System.out.println("");
			Poss.removeAllElements();
			for(int i=0;i<Poss_Color.size();i++)
			{
				hhh=(int[])Poss_Color.get(i);
				if(hhh[0]==choice)
				{
					Poss.add(hhh[1]);
				}
			}
			displayPossColor(Poss);
			choice=theChosenOne(Poss.size(),"str_enter_color");
			int col=(int)Poss.get(choice);
			Poss.removeAllElements();
			Poss.add(x1);
			Poss.add(y1);
			Poss.add(col);
		}
		else
		{
			Poss.add(0);
			Poss.add(0);
			Poss.add(0);
		}
		return Poss;
	}
	@SuppressWarnings("unchecked")
	public Vector possibilityPlayer(Board T)
	{
		Vector Poss=new Vector<int[]>();
		Vector Poss_Color=new Vector<int[]>();
		if(nbCard!=0)
		{
			boolean bColor[]=new boolean[5];
			bColor=booleanColor();
			for(int y=0;y<T.length(0);y++)
			{
				if(T.getColorBoard(0,y)==0)
				{
					T.changeColorBoard(0,y,6);
					int[] g1={0,y};
					Poss.add(g1);
					for(int o=0;o<5;o++)
					{
						if(bColor[o])
						{
							int[] g2={Poss.size()-1,o+1};
							Poss_Color.add(g2);
						}
					}
				}
			}
			for(int x=1;x<T.length();x++)
			{
				for(int y=0;y<T.length(x);y++)
				{
					if((T.getColorBoard(x,y)==0)&&((T.getColorBoard(x-1,y)!=0)&&(T.getColorBoard(x-1,y)!=6))&&((T.getColorBoard(x-1,y+1)!=0)&&(T.getColorBoard(x-1,y+1)!=6)))
					{
						if((bColor[T.getColorBoard(x-1,y)-1])||(bColor[T.getColorBoard(x-1,y+1)-1]))
						{
							if(T.getColorBoard(x-1,y)==T.getColorBoard(x-1,y+1))
							{
								int[] G1={x,y};
								Poss.add(G1);
								int[] G2={Poss.size()-1,T.getColorBoard(x-1,y)};
								Poss_Color.add(G2);
								T.changeColorBoard(x,y,6);
							}
							else
							{
								int[] G1={x,y};
								Poss.add(G1);
								if(bColor[T.getColorBoard(x-1,y)-1])
								{
									int[] G2={Poss.size()-1,T.getColorBoard(x-1,y)};
									Poss_Color.add(G2);
								}
								if(bColor[T.getColorBoard(x-1,y+1)-1])
								{
									int[] G3={Poss.size()-1,T.getColorBoard(x-1,y+1)};
									Poss_Color.add(G3);
								}
								T.changeColorBoard(x,y,6);
							}
						}
					}
				}
			}
			if(Poss.size()==0)
			{
				Poss.add(0);
				Poss.add(0);
				Poss.add(0);
				return Poss;
			}
			//////////////////////////////
			partie.toDisplayBoard(T,nbpla);
			//////////////////////////////
			//T.toDisplay();
			//System.out.println("");
			//////////////////////////////
			partie.toDisplayHand(this);
			//////////////////////////////
			//displayHand();
			displayPoss(Poss);
			int choice=theChosenOne(Poss.size(),"str_enter_poss");
			int[] hhh=(int[])Poss.get(choice);
			int x1=hhh[0];
			int y1=hhh[1];
			Poss.removeAllElements();
			for(int i=0;i<Poss_Color.size();i++)
			{
				hhh=(int[])Poss_Color.get(i);
				if(hhh[0]==choice)
				{
					Poss.add(hhh[1]);
				}
			}
			displayPossColor(Poss);
			choice=theChosenOne(Poss.size(),"str_enter_color");
			int col=(int)Poss.get(choice);
			Poss.removeAllElements();
			Poss.add(x1);
			Poss.add(y1);
			Poss.add(col);
		}
		else
		{
			Poss.add(0);
			Poss.add(0);
			Poss.add(0);
		}
		return Poss;
	}
	/**
	*  * Return a boolean table wich indicate the color that the player own in his hand
	* @author Remy BARRIOL
	*/
	public boolean[] booleanColor()
	{
		boolean[] bColor={false,false,false,false,false};
		for(int i=0;i<hand.length;i++)
		{
			if((hand[i].getColor()>=1)&&(hand[i].getColor()<6))
			{
				bColor[hand[i].getColor()-1]=true;
			}
		}
		return bColor;
	}
	/**
	*  * Display all possibility of action in the board
	* @param V Vector wich contain all possibility of color
	* @author Remy BARRIOL
	*/
	public void displayPoss(Vector<int[]> V)
	{
		System.out.println("");
		System.out.println("Possibility :");
		for(int i=0;i<V.size();i++)
		{
			int[] h=V.get(i);
			if(i%2==0)
			{
				System.out.print("	" + i + " - [" + h[0] + "," + h[1] + "]");
			}
			else
			{
				System.out.println("	" + i + " - [" + h[0] + "," + h[1] + "]");
			}
		}
		System.out.println("");
	}
	/**
	*  * Display all possibility of color of cards 
	* @param V Vector wich contain all possibility of color
	* @author Remy BARRIOL
	*/
	public void displayPossColor(Vector<Integer> V)
	{
		System.out.println("");
		System.out.println("Possibility :");
		for(int i=0;i<V.size();i++)
		{
			int h=V.get(i);
			if(i%2==0)
			{
				System.out.print("	" + i + " - [ ");
				displayColor(h);
				System.out.print("]");
			}
			else
			{
				System.out.print("	" + i + " - [ ");
				displayColor(h);
				System.out.println("]");
			}
		}
		System.out.println("");
	}
	/**
	*  * Get by input the index of a answer
	* @param i The maximum integer of an answer
	* @author Remy BARRIOL
	*/
	public int theChosenOne(int i,String key)
	{
		int poss=partie.getInputNumber(key);
		//Scanner sc = new Scanner(System.in);
		//Pa.toDisplayTempString("Choose a possibility");
   		//poss = sc.nextInt();
		while((0>poss)||(poss>=i))
		{
			partie.toDisplayTempString("This possibility doesn't exist");
			poss = partie.getInputNumber(key);
		}
		return poss;
	}
	/**
	*  * Display the color of a cards
	* @param i The integer associated to the card that we want to display
	* @author Remy BARRIOL
	*/
	public void displayColor(int i)
	{
			if(i==1)
			{
				System.out.print("\u001B[31mR\u001B[0m ");
			}
			else if(i==2)
			{
				System.out.print("\u001B[35mP\u001B[0m ");
			}
			else if(i==3)
			{
				System.out.print("\u001B[34mB\u001B[0m ");
			}
			else if(i==4)
			{
				System.out.print("\u001B[33mY\u001B[0m ");
			}
			else if(i==5)
			{
				System.out.print("\u001B[32mG\u001B[0m ");
			}
	}
	
	/**
	 * @param nbPlayers
	 * @return the maximum cards in a hand
	 */
	public int maxOfCards(int nbPlayers)
	{
		if(nbPlayers==2)
		{
			return 14;
		}
		else if(nbPlayers==3)
		{
			return 12;
		}
		else if(nbPlayers==4)
		{
			return 9;
		}
		else if(nbPlayers==5)
		{
			return 7;
		}
		else if(nbPlayers==6)
		{
			return 6;
		}
		else
		{
			System.err.println("The number of players is incorrect");
			return 0;
		}
	}
	
	/**
	*  * Returns the name of the player
	* @return The name of the player
	* @author Remy BARRIOL
	*/
	public String getName()
	{
		return name;
	}
	/**
	*  * Increases by one the number of green card played
	* @author Remy BARRIOL
	*/
	public void increaseGreen()
	{
		nbGreenCards++;
	}
	/**
	*  * Returns the number of green card the player have played
	* @return Returns the number of green card played
	* @author Remy BARRIOL
	*/
	public int getGreen()
	{
		return nbGreenCards;
	}
	/**
	*  * Increases the player score
	* @param i The integer that we want to sum with the score
	* @author Remy BARRIOL
	*/
	public void incScore(int i)
	{
		score=score+i;
	}
	/**
	*  * Increase the number of winned rounds
	* @author Remy BARRIOL
	*/
	public void incScoreRound()
	{
		scoreRound++;
	}
	/**
	*  * Set the the turn of the defeat of the player
	* @param i The turn of his defeat
	* @author Remy BARRIOL
	*/
	public void changeTurnDeath(int i)
	{
		turnOfDefeat=i;
	}
	/**
	*  * Return the turn when the player was defeated
	* @return Return the turn of his defeat
	* @author Remy BARRIOL
	*/
	public int getTurnDeath()
	{
		return turnOfDefeat;
	}
	/**
	*  * Return the state of the player (defeated or not)
	* @return Return true if he is defeated, false if not
	* @author Remy BARRIOL
	*/
	public boolean getDefeat()
	{
		return defeated;
	}
	/**
	*  * Set the player like a not defeated player
	* @author Remy BARRIOL
	*/
	public void resetDefeat()
	{
		defeated=false;
	}
	/**
	*  * Set the player like a defeated player
	* @author Remy BARRIOL
	*/
	public void doDefeat()
	{
		if(!(this instanceof AIOne))
			partie.toDisplayTempString("You have be slain, sorry "+getName());
		defeated=true;
	}
	/**
	*  * Returns the score of the player
	* @author Remy BARRIOL
	*/
	public int getScore()
	{
		return score;
	}
	/**
	*  * Returns the number of winned rounds
	* @author Remy BARRIOL
	*/
	public int getScoreRound()
	{
		return scoreRound;
	}
	
	public int getCardColor(int i)
	{
		return hand[i].getColor();
	}

}