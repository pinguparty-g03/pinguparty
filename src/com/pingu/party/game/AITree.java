package com.pingu.party.game;

import com.pingu.party.activities.Partie;
import com.pingu.party.engine.game.Profile;
import com.pingu.party.engine.utils.AiUtils;

import java.util.ArrayList;
import java.util.Vector;

/**
 * Class of the new AI
 * @author Bastien MALEPLATE
 * @author Florian RIBOU
 */
public class AITree extends Player implements AI
{
	protected int difficulty;

	public static int SAME_RANK_A_AND_B_IN_HAND_WEIGHT = 1;
	public static int SAME_RANK_A_XOR_B_NOT_CORNER_WEIGHT = 5;
	public static int DECREASE_CORNER_WEIGHT = 3;
	public static int BLOCKING_SELF_WEIGHT = 0;
	public static int EMPTY_CASE_WEIGHT = 3;
	public static int PLAY_THIS_CHOICE_WEIGHT = 50;

	public AITree() {
		super(0, 0);
	}
	/**
	 *  * Constructor of the AI
	 */
	public AITree(int nbPlayer,int number,Partie P)
	{
		super(nbPlayer,number,P);
		this.name = "AI "+number;
	}

	public AITree(Integer nbp, Partie p, Profile pr) {
		super(nbp,p,pr);
		System.out.println(this.getClass().getName()+" "+pr.getName()+" created");
		this.difficulty = pr.getLevel();
	}

	public AITree(int nbPlayer,Partie P,Profile me)
	{
		super(nbPlayer,P,me);
		System.out.println(this.getClass().getName()+" "+me.getName()+" created");
		this.difficulty = me.getLevel();
	}

	/**
	 *  * Set the action of the AI (It will choose every time the first choice it can do)
	 * @param T The actual Board
	 */
	public Vector possibilityPlayer(Board T,Game G)
	{
		//System.out.println(this.getClass().getName()+" possibilityPlayer 1");

		ArrayList<Possibility> Possibilities = new ArrayList<>();

		/*for (int i = 0; i < T.length(); i++) {
			for (int j = 0; j < T.length(i); j++) {
				System.out.print(""+i+":"+j+" "+ AiUtils.instance.getColor(T.getColorBoard(i, j)).name()+" \t ");
			}
			System.out.println(" ");
		}*/

		Vector Poss=new Vector<int[]>();
		Vector Poss_Color=new Vector<int[]>();
		if(nbCard!=0) {
			System.out.print("Hand : ");
			for (Card c : hand) {
				System.out.print(AiUtils.instance.getColor(c.getColor()).name() + " ");
			}
			/*
			System.out.print(" ( - " + AiUtils.instance.lessPresent(hand).name() + " + " + AiUtils.instance.mostPresent(hand).name() + ")");
			System.out.println(" ");
			System.out.print("unique : ");

			for (AiUtils.colors c : AiUtils.instance.uniq(hand)) {
				System.out.print(" " + c.name());
			}
			System.out.println(" ");
			System.out.print("shorted : ");*/
			ArrayList<AiUtils.colors> shorted = AiUtils.instance.shortByPresence(hand);
			/*for (AiUtils.colors c : AiUtils.instance.shortByPresence(hand)) {
				System.out.print(" " + c.name());
			}
			System.out.println(" ");*/

			Possibility p;

			ArrayList<Possibility> possibilities = new ArrayList<>();

			if(AiUtils.instance.isBoardEmpty(T)) {
				p = AiUtils.instance.mostInterestPossibility(Possibilities);
				if(p == null) p = Possibility.instance;
				//System.out.println("AAA");
				return p.toRemy();
			} else {
				boolean b = AiUtils.instance.hasAnyRankAnyPair(T);
				if(b) {
					for (int i = 0; i <= AiUtils.instance.getHighestRankPair(T); i++) {
						possibilities.addAll(AiUtils.instance.pairXorHand(T, hand, i));
						System.out.println(""+possibilities.toString());
					}
				} else {

				}
			}


			if(possibilities.size() != 0) {
				p = AiUtils.instance.mostInterestPossibility(possibilities);
				if(p == null) {
					p = Possibility.instance;
				} else {
				    int x = p.getX();
				    int y = p.getY();
				    if(x > 0) {
				        if(T.getColorBoard(x-1, y) != 0 && T.getColorBoard(x-1, y+1) != 0) {
                            for (int i = 0; i < hand.length; i++) {
                                if(hand[i].getColor() ==  AiUtils.instance.getRemy(p.color)) {
                                    hand[i].changeColor(0);
                                    System.out.println("OK");
                                    return p.toRemy();
                                }
                            }
                        }
                    }
				}
			}

			boolean bColor[] = new boolean[5];
			bColor = booleanColor();
			for (int y = 0; y < T.length(0); y++) {
				if (T.getColorBoard(0, y) == 0) {
					T.changeColorBoard(0, y, 6);
					int[] g1 = {0, y};
					Poss.add(g1);
					for (int o = 0; o < 5; o++) {
						if (bColor[o]) {
							int[] g2 = {Poss.size() - 1, o + 1};
							//System.out.println("Color : " + AiUtils.instance.getColor(o+1)+ " ["+(g1[0])+":"+y+"]");

							Possibilities.add(new Possibility(g1[0], y, AiUtils.instance.getColor(o + 1), hand, T));

							Poss_Color.add(g2);
						}
					}
				}
			}
			for (int x = 1; x < T.length(); x++) {
				for (int y = 0; y < T.length(x); y++) {
					if ((T.getColorBoard(x, y) == 0) && ((T.getColorBoard(x - 1, y) != 0) && (T.getColorBoard(x - 1, y) != 6)) && ((T.getColorBoard(x - 1, y + 1) != 0) && (T.getColorBoard(x - 1, y + 1) != 6))) {
						if ((bColor[T.getColorBoard(x - 1, y) - 1]) || (bColor[T.getColorBoard(x - 1, y + 1) - 1])) {
							if (T.getColorBoard(x - 1, y) == T.getColorBoard(x - 1, y + 1)) {
								int[] G1 = {x, y};
								Poss.add(G1);
								int[] G2 = {Poss.size() - 1, T.getColorBoard(x - 1, y)};

								//System.out.println("Color : " + AiUtils.instance.getColor(G2[1])+ " ["+x+":"+y+"]");

								Possibilities.add(new Possibility(x, y, AiUtils.instance.getColor(G2[1]), hand, T));

								Poss_Color.add(G2);
								T.changeColorBoard(x, y, 6);
							} else {
								int[] G1 = {x, y};
								Poss.add(G1);
								if (bColor[T.getColorBoard(x - 1, y) - 1]) {
									int[] G2 = {Poss.size() - 1, T.getColorBoard(x - 1, y)};

									//System.out.println("Color : " + AiUtils.instance.getColor(G2[1])+ " ["+x+":"+y+"]");

									Possibilities.add(new Possibility(x, y, AiUtils.instance.getColor(G2[1]), hand, T));

									Poss_Color.add(G2);
								}
								if (bColor[T.getColorBoard(x - 1, y + 1) - 1]) {
									int[] G3 = {Poss.size() - 1, T.getColorBoard(x - 1, y + 1)};
									//System.out.println("Color : " + AiUtils.instance.getColor(G3[1])+ " ["+x+":"+y+"]");

									Possibilities.add(new Possibility(x, y, AiUtils.instance.getColor(G3[1]), hand, T));

									Poss_Color.add(G3);
								}
								T.changeColorBoard(x, y, 6);
							}
						}
					}
				}
			}
			/*for (Possibility p : Possibilities) {
				System.out.println(" > " + p.toString());
			}*/
			p = AiUtils.instance.mostInterestPossibility(Possibilities);
			if(p == null) {
				p = Possibility.instance;
			}
			/*
			if (p != null) {

				//System.out.println("+> " + p.toString());
			} else {
				p = Possibility.instance;
				System.out.println("-> FATAL_ERROR : No Choices");
			}*/

			return p.toRemy();

			/*

			if(Poss.size()==0)
			{
				Poss.add(0);
				Poss.add(0);
				Poss.add(0);
				return Poss;
			}
			for(int i = 0; i<Poss.size(); i++) {
                int[] a = (int[])Poss.get(i);
                for(int e : a) {
                    System.out.print(e+ " ");
                }
                System.out.println(" ");
            }
			//System.out.println("POSS "+Poss.toString());
			displayHand();
			int choice=0;
			int[] pair=(int[])Poss.get(choice);
			int x1=pair[0];
			int y1=pair[1];
			Poss.removeAllElements();

            for(int j=shorted.size(); j>0; j--) {
                int c = AiUtils.instance.getRemy(shorted.get(j-1));
                for(int i=0;i<Poss_Color.size();i++)
                {
                    pair=(int[])Poss_Color.get(i);

                    // GOMEN

                    if(pair[0]==0) {

                    } else {
                        if(pair[1] >= 6-pair[0]) {
                            //System.out.print(" "+AiUtils.instance.getColor(T.getColorBoard(pair[0]-1, pair[1]-1)).name());
                        } else if(pair[1] == 0) {
                            //System.out.print(" "+AiUtils.instance.getColor(T.getColorBoard(pair[0]-1, pair[1])).name());

                        } else {
                            //System.out.print(" "+AiUtils.instance.getColor(T.getColorBoard(pair[0]-1, pair[1])).name());
                            //System.out.print(" "+AiUtils.instance.getColor(T.getColorBoard(pair[0]-1, pair[1]-1)).name());

                                    //in the middle
                        }

                    }
                    //T.getColorBoard(pair[0], pair[1]);

                    //System.out.print(" "+pair[0]+":"+pair[1]+" ");
                    if(pair[0]==choice)
                    {
                        Poss.add(pair[1]);
                        //System.out.print("Added :  "+pair[0]+":"+pair[1]+" ");
                    }
                }
                System.out.println(shorted.get(j-1).name()+" ");
            }
			choice=0;
			int col=(int)Poss.get(choice);
			Poss.removeAllElements();
			Poss.add(x1);
			Poss.add(y1);
			Poss.add(col);*/
		}
		else
		{
			Poss.add(0);
			Poss.add(0);
			Poss.add(0);
		}
		//System.out.println("POSS "+Poss.get(0));
		return Poss;
	}
	@SuppressWarnings("unchecked")
	public Vector possibilityPlayer(Board T)
	{
		Vector Poss=new Vector<int[]>();
		Vector Poss_Color=new Vector<int[]>();
		if(nbCard!=0)
		{
			boolean bColor[]=new boolean[5];
			bColor=booleanColor();
			for(int y=0;y<T.length(0);y++)
			{
				if(T.getColorBoard(0,y)==0)
				{
					T.changeColorBoard(0,y,6);
					int[] g1={0,y};
					Poss.add(g1);
					for(int o=0;o<5;o++)
					{
						if(bColor[o])
						{
							int[] g2={Poss.size()-1,o+1};
							Poss_Color.add(g2);
						}
					}
				}
			}
			for(int x=1;x<T.length();x++)
			{
				for(int y=0;y<T.length(x);y++)
				{
					if((T.getColorBoard(x,y)==0)&&((T.getColorBoard(x-1,y)!=0)&&(T.getColorBoard(x-1,y)!=6))&&((T.getColorBoard(x-1,y+1)!=0)&&(T.getColorBoard(x-1,y+1)!=6)))
					{
						if((bColor[T.getColorBoard(x-1,y)-1])||(bColor[T.getColorBoard(x-1,y+1)-1]))
						{
							if(T.getColorBoard(x-1,y)==T.getColorBoard(x-1,y+1))
							{
								int[] G1={x,y};
								Poss.add(G1);
								int[] G2={Poss.size()-1,T.getColorBoard(x-1,y)};
								Poss_Color.add(G2);
								T.changeColorBoard(x,y,6);
							}
							else
							{
								int[] G1={x,y};
								Poss.add(G1);
								if(bColor[T.getColorBoard(x-1,y)-1])
								{
									int[] G2={Poss.size()-1,T.getColorBoard(x-1,y)};
									Poss_Color.add(G2);
								}
								if(bColor[T.getColorBoard(x-1,y+1)-1])
								{
									int[] G3={Poss.size()-1,T.getColorBoard(x-1,y+1)};
									Poss_Color.add(G3);
								}
								T.changeColorBoard(x,y,6);
							}
						}
					}
				}
			}
			if(Poss.size()==0)
			{
				Poss.add(0);
				Poss.add(0);
				Poss.add(0);
				return Poss;
			}
			//////////////////////////////
			partie.toDisplayBoard(T,nbpla);
			//////////////////////////////
			//T.toDisplay();
			//System.out.println("");
			//////////////////////////////
			partie.toDisplayHand(this);
			//////////////////////////////
			//displayHand();
			//displayPoss(Poss);
			int choice=0;
			int[] hhh=(int[])Poss.get(choice);
			int x1=hhh[0];
			int y1=hhh[1];
			Poss.removeAllElements();
			for(int i=0;i<Poss_Color.size();i++)
			{
				hhh=(int[])Poss_Color.get(i);
				if(hhh[0]==choice)
				{
					Poss.add(hhh[1]);
				}
			}
			choice=0;
			int col=(int)Poss.get(choice);
			Poss.removeAllElements();
			Poss.add(x1);
			Poss.add(y1);
			Poss.add(col);
		}
		else
		{
			Poss.add(0);
			Poss.add(0);
			Poss.add(0);
		}
		return Poss;
	}
}