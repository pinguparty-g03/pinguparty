package com.pingu.party.game;
import java.util.Scanner;
import java.util.Random;
import java.util.Vector;
import com.pingu.party.network.MultiThreadServer;
import com.pingu.party.network.GameServer;
import com.pingu.party.network.clientThread;
import com.pingu.party.network.GameClient;
import com.pingu.party.activities.Partie;
/**
 * Class of Cards of the game
 * @author Remy BARRIOL
 */
public class Card {
	private int Color;
	/**
	*  * Constructor of the Card class with a determined color
	* @param mColor An integer which affect the Card at a color 
	* @author Remy BARRIOL
	*/
	public Card(int mColor)
	{
		Color=mColor;
	}
	/**
	*  * Constructor of the Card class without a determined color(set to 0, "the uncolored card")
	* @author Remy BARRIOL
	*/
	public Card()
	{
		Color=0;
	}
	/**
	*  * Return the color of the card
	* @return The integer which represents of the card color
	* @author Remy BARRIOL
	*/
	public int getColor()
	{
		return Color;
	}
	/**
	*  * Change the color of the card
	* @param i The integer which represent the future color of the card
	* @author Remy BARRIOL
	*/
	public void changeColor(int i)
	{
		Color=i;
	}
	/**
	*  * Display the color of the card
	*/
	public void displayCard()
	{
				if(Color==0)
				{
					System.out.print("- ");
				}
				else if(Color==1)
				{
					System.out.print("\u001B[31mR\u001B[0m ");
				}
				else if(Color==2)
				{
					System.out.print("\u001B[35mP\u001B[0m ");
				}
				else if(Color==3)
				{
					System.out.print("\u001B[34mB\u001B[0m ");
				}
				else if(Color==4)
				{
					System.out.print("\u001B[33mY\u001B[0m ");
				}
				else if(Color==5)
				{
					System.out.print("\u001B[32mG\u001B[0m ");
				}
				else if(Color==6)
				{
					System.out.print("X ");
				}
	}
}
