package com.pingu.party.game;

import com.pingu.party.engine.utils.AiUtils;

import java.util.ArrayList;
import java.util.Vector;

public class Possibility {
    int x, y;
    AiUtils.colors color;

    Integer weight;

    final static int GreenWeight = -10; //Remy tolds us that you have to keep them until the end, so I changed from 5 to -10
    final static int UniqueWeight = -4;
    final static int PositionWeight = 2;
    final static int LessWeight = -2;

    public Possibility(int x, int y, AiUtils.colors color, Integer weight) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.weight = weight;
    }
    public Possibility(int x, int y, AiUtils.colors color, Card[] hand, Board T) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.weight = 0;
        weight(hand, T);
    }

    public Possibility(int x, int y, int color, Integer weight) {
        this.x = x;
        this.y = y;
        this.color = AiUtils.instance.getColor(color);
        this.weight = weight;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public AiUtils.colors getColor() {
        return color;
    }

    public void setColor(AiUtils.colors color) {
        this.color = color;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Possibility{" +
                "x=" + x +
                ", y=" + y +
                ", color=" + color +
                ", weight=" + weight +
                '}';
    }

    private Possibility() {
        this(0,0,0, 0 );
    }

    public Vector<Integer> toRemy() {
        Vector<Integer> result = new Vector<Integer>();
        result.add(x);
        result.add(y);
        result.add(AiUtils.instance.getRemy(color));
        return result;
    }

    public void weight(Card[] hand, Board T) {
        if(color == AiUtils.colors.GREEN)  weight += GreenWeight;
        if(AiUtils.instance.uniq(hand).contains(color)) weight += UniqueWeight;

        ArrayList<AiUtils.colors> shorted = AiUtils.instance.shortByPresence(hand);
        for (int j = 0; j < shorted.size(); j++) {
            if(shorted.get(j) == color) weight += j*PositionWeight;
        }
        if(AiUtils.instance.lessPresent(hand) == color) weight += LessWeight;
    }
    //Card[] hand
    public static Possibility instance = new Possibility();
}
