package com.pingu.party.game;
import java.util.Scanner;
import java.util.Random;
import java.util.Vector;
import com.pingu.party.network.MultiThreadServer;
import com.pingu.party.network.GameServer;
import com.pingu.party.network.clientThread;
import com.pingu.party.network.GameClient;
import com.pingu.party.activities.Partie;
/**
 * Class of the Board of the game, which manage all the modification on its
 * @author Remy BARRIOL
 */
public class Board {
	private Card[][] Board_T;
	private Card discarded;

	public int getDis()
	{
		if(discarded != null)
		{
			return discarded.getColor();
		}
		else
		{
			return 0;
		}

	}
	/**
	*  * Change the discarded card(It is the card unused in the case of five players)
	* @param Dis The future discarded card
	* @author Remy BARRIOL
	*/
	public void changeDis(Card Dis)
	{
		discarded=Dis;
	}
	/**
	*  * Create the Board in function of the number of players
	* @param nbPlayer The number of players
	* @author Remy BARRIOL
	*/
	public void createTable(int nbPlayer)
	{
		if(nbPlayer==2)
		{
			Board_T=new Card[7][];
			int g=7;
			for(int i=0;i<7;i++,g--)
			{
				Board_T[i]=new Card[g];
				for(int j=g-1;j!=-1;j--)
				{
					Board_T[i][j]=new Card();
				}
			}
		}
		else if((nbPlayer>=3)&&(nbPlayer<=6))
		{
			Board_T=new Card[8][];
			int g=8;
			for(int i=0;i<8;i++,g--)
			{
				Board_T[i]=new Card[g];
				for(int j=g-1;j!=-1;j--)
				{
					Board_T[i][j]=new Card();
				}
			}
		}
		else
		{
			System.err.println("The number of players is incorrect");
		}
	}
	/**
	*  * Display the Board
	* @author Remy BARRIOL
	*/
	public void toDisplay()
	{
		if(discarded!=null)
		{
			if(discarded.getColor()!=9)
			{
				System.out.println("");
				System.out.print("discarded : [ ");
				discarded.displayCard(); 
				System.out.println("]");
				System.out.println("");
			}
		}
		System.out.println("");
		int c=0;
		for(int i=Board_T.length-1;i!=-1;i--)
		{
			System.out.print(i + " ");
			for(int k=0;k<i;k++)
			{
				System.out.print(" ");
			}
			for(int j=0;j<Board_T[i].length;j++)
			{
				Board_T[i][j].displayCard();
			}
			System.out.println("");
		}
	}
	/**
	*  * Change the color of a card int the board (an unused card place is in fact an uncolored card )
	* @param x One of the coordinate of the targeted card (left to right)
	* @param y One of the coordinate of the targeted card (Bottom to Top)
	* @param i The new color of the targeted card
	* @author Remy BARRIOL
	*/
	public void changeColorBoard(int x,int y,int i)
	{
		Board_T[x][y].changeColor(i);
	}
	/**
	*  * Return the length of a line of the Board
	* @param i The index of the targeted line 
	* @return The length of the targeted line
	* @author Remy BARRIOL
	*/
	public int length(int i)
	{
		return Board_T[i].length;
	}
	/**
	*  * Return the number of line in the Board
	* @return The number line
	* @author Remy BARRIOL
	*/
	public int length()
	{
		return Board_T.length;
	}
	/**
	*  * Return the color of a card in the board
	* @param x One of the coordinate of the targeted card (left to right)
	* @param y One of the coordinate of the targeted card (Bottom to Top)
	* @return The color of the targeted card
	* @author Remy BARRIOL
	*/
	public int getColorBoard(int x,int y)
	{
		return Board_T[x][y].getColor();
	}
	/**
	*  * Remove all of the "possibility card", and set them to "uncolored cards"
	* @author Remy BARRIOL
	*/
	public void removeX()
	{
		for(int x=0;x<Board_T.length;x++)
		{
			for(int y=0;y<Board_T[x].length;y++)
			{
				if(Board_T[x][y].getColor()==6)
				{
					Board_T[x][y].changeColor(0);
				}
			}
		}
	}
}