package com.pingu.party.game;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;
import java.util.Random;
import java.util.Vector;

import com.pingu.party.engine.utils.AiUtils;
import com.pingu.party.network.MultiThreadServer;
import com.pingu.party.network.GameServer;
import com.pingu.party.network.clientThread;
import com.pingu.party.network.GameClient;
import com.pingu.party.activities.Partie;
import com.pingu.party.engine.structs.SScores;

import com.pingu.party.engine.game.Profile;
import com.pingu.party.engine.game.ProfileManager;

import com.pingu.party.activities.End;

import java.util.ArrayList;

import java.util.Timer;
import java.util.TimerTask;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.io.File;

import com.pingu.party.engine.config.Config;


/**
 * Class of the game, which manage players, rounds and turns.
 * @author Remy BARRIOL
 */
 class aTime
{

	 protected Timer timer;
	 protected int interval;
	 protected Player Pla;
	 protected Partie Pa;
	 protected int Number;
	 protected Game game;
	 protected boolean act=false;
	 public aTime(int times,Player P,Partie Pa,int number,Game g)
	 {
		Pla=P;
		this.Pa=Pa;
		timer=new Timer();
		interval=times;
		Number=number;
		game=g;
	 }
	 public Player getPlayer()
	 {
		 return Pla;
	 }
	private TimerTask aTask=new TimerTask() {
        public void run() {
            System.out.println(setInterval());
			Pa.setPlayerTime(Number,Pla.getProfile().getName(), interval);
        }
    };
	public void resume() 
	{
		this.timer = new Timer();
		aTask=new TimerTask() {
        public void run() {
            System.out.println(setInterval());
		//	System.out.println(""+(Pa==null));
			Pa.setPlayerTime(Number,Pla.getProfile().getName(), interval);
        }
    	};
		this.timer.scheduleAtFixedRate( aTask, 1000, 1000 );
	}
	/*private boolean getAlreadyActivate()
	{
		
	}*/
	private int setInterval() {
		if (interval == 1)
		{
			timer.cancel();
			Pla.doDefeat();
			if((game.getR())&&(game.getMode()==1))
				game.getReplay().appendStrToFile("3"+'\t'+Pla.getProfile().getName()+'\n');
		}
		return --interval;
	}
	public void pause()
	{
		//System.out.println("NotNot");
		act=true;
    	this.timer.cancel();
		//System.out.println("NotiNot");
	}
	public void launch()
	{
		if(act)
		{
			this.timer = new Timer();
			aTask=new TimerTask() {
			public void run() {
				System.out.println(setInterval());
				//System.out.println(""+(Pa==null));
				Pa.setPlayerTime(Number,Pla.getProfile().getName(), interval);
			}
			};
			this.timer.scheduleAtFixedRate( aTask, 1000, 1000 );
		}
		else{
			this.timer.scheduleAtFixedRate( aTask, 1000, 1000 );
		}
		//System.out.println("Yeah !!!!!!");
		//this.timer.scheduleAtFixedRate( aTask, 1000, 1000 );
	}
	public void reset(int nint)
	{
		interval=nint;
	}
}
 class aTimeReplay extends aTime
{
	String name;
	public aTimeReplay(int ih,int g,Partie p,String names)
	 {
		super(ih,null,p,g,null);
		name=names;
	 }
	private int setInterval() {
		if (interval == 1)
		{
			timer.cancel();
		}
		return --interval;
	}
	private TimerTask aTask=new TimerTask() {
        public void run() {
            //System.out.println(setInterval());
			Pa.setPlayerTime(Number,name,interval);
        }
    };
	public void resume() 
	{
		this.timer = new Timer();
		aTask=new TimerTask() {
        public void run() {
            System.out.println(setInterval());
		//	System.out.println(""+(Pa==null));
			Pa.setPlayerTime(Number,name, interval);
        }
    	};
		this.timer.scheduleAtFixedRate( aTask, 1000, 1000 );
	}
	public void launch()
	{
		if(act)
		{
			this.timer = new Timer();
			aTask=new TimerTask() {
			public void run() {
				System.out.println(setInterval());
				//System.out.println(""+(Pa==null));
				Pa.setPlayerTime(Number,name, interval);
			}
			};
			this.timer.scheduleAtFixedRate( aTask, 1000, 1000 );
		}
		else{
			this.timer.scheduleAtFixedRate( aTask, 1000, 1000 );
		}
		//System.out.println("Yeah !!!!!!");
		//this.timer.scheduleAtFixedRate( aTask, 1000, 1000 );
	}
	public String getName()
	{
		return name;
	}
	
	public void setName(String g)
	{
		name=g;
	}
}
public class Game
{
	protected int inter;
	protected boolean blitz;
	protected aTime[] Tim;
	protected aTimeReplay[] TimR=null;
	protected Card discarded;
	protected int nbPlayer = -1;
	protected int nbAI = -1;
	protected Board B=new Board();
	protected Player[] allPlayer;
	protected int nbSlain=0;
	protected int turn=0;
	protected int round=0;
	protected int lastFight=0;
	protected Partie Pa;
	protected boolean Replay;
	protected Replay R=null;
	int mode=1;
	Waiting wait;
	SScores scoresReplay;
	
	public void changeDis(int i)
	{
		Pa.changeDis(i);
	}
	public void endClass()
	{
		Pa.Endo(scoresReplay);
	}
	public void addClass(String names,int scores,int scoresRound,int green,int i)
	{
		scoresReplay.addScore(names,scores,scoresRound,green,i);
	}
	public void changeRoundandTurn(int turns,int rounds)
	{
		Pa.changeTandR(turns,rounds);
		if(round!=rounds)
		{
			B.createTable(nbPlayer);
			if(blitz)
			{
				TimR=new aTimeReplay[nbPlayer];
				for(int i=0;i<nbPlayer;i++)
				{
					TimR[i]=new aTimeReplay(inter,i,Pa,"");
				}
			}
		}
		this.round=rounds;
	}
	public void defeatReplay(String name)
	{
		Pa.toDisplayTempString(name+" have been slained");
	}
	public void configReplay(int nbPlayers,int blitzTime)
	{
		if(blitzTime!=0)
		{
			inter=blitzTime;
			blitz=true;
		}
		else
			blitz=false;
		scoresReplay=new SScores();
		nbPlayer=nbPlayers;
		B.createTable(nbPlayer);
		if(blitz)
		{
			Pa.setLabelTimes(nbPlayers);
			Pa.addToViewport(Pa.blitzInit(nbPlayer,blitz));
			TimR=new aTimeReplay[nbPlayer];
			for(int i=0;i<nbPlayer;i++)
			{
				TimR[i]=new aTimeReplay(inter,i,Pa,"");
			}
		}
	}
	public void playReplay(String action,String name,String Hand,int time)
	{
		//System.out.println("((aTimeReplay)RTim[i]).getName().equals(name)");
		aTimeReplay t=null;
		if(blitz)
		{
			for(int i=0;i<nbPlayer;i++)
			{
				//System.out.println(name+" "+TimR[i].getName());
				if(TimR[i].getName().equals(name))
				{
					t=TimR[i];
					break;
				}
				else if(TimR[i].getName().equals(""))
				{
					TimR[i].setName(name);
					t=TimR[i];
					break;
				}
			}
		}
		Pa.changeName(name);
		Pa.toDisplayHand(Hand);
		Pa.toDisplayTempString("Turn of "+name);
		Pa.toDisplayBoard(B,nbPlayer);
		wait=new Waiting(time);
		if(blitz)
		{
			t.launch();
		}
		wait.run();
		B.changeColorBoard(action.charAt(0)-48,action.charAt(1)-48,action.charAt(2)-48);
		if(blitz)
			t.pause();
		Pa.toDisplayBoard(B,nbPlayer);
	}
	public int getMode()
	{
		return mode;
	}
	public boolean getR()
	{
		return Replay;
	}
	public Replay getReplay()
	{
		return R;
	}
	public Player[] getAllPlayer()
	{
		return allPlayer;
	}
	public int getTurn()
	{
		return turn;
	}
	public int getRound()
	{
		return round;
	}
	public int getDis()
	{
		return B.getDis();
	}
	public int getNumberPlayer()
	{
		return nbPlayer;
	}
	public int getColorBoard(int i,int j)
	{
		return B.getColorBoard(i,j);
	}
	public void putPartie(Partie P)
	{
		Pa=P;
	}
	/**
	*  * Mix players of the game to randomise the order of players
	* @author Remy BARRIOL
	*/
	@SuppressWarnings("unchecked")
	public void mixPlayer()
	{
		Vector V=new Vector();
		Random rand = new Random();
		Player[] allPlayer2=new Player[nbPlayer];
		for(int i=0;i<nbPlayer;i++)
		{
			V.add(allPlayer[i]);
			allPlayer2[i]=allPlayer[i];
		}
		int mx=nbPlayer-1;
		int mn=0;
		int x;
		for(int i=0;i<nbPlayer;i++)
		{
			x=rand.nextInt(mx - mn + 1) + mn;
			allPlayer2[i]=(Player)V.get(x);
			V.remove(x);
			mx--;
		}
		for(int i=0;i<nbPlayer;i++)
		{
			allPlayer[i]=allPlayer2[i];
		}
	}
		/**
	*  * Function wich can test the Ranking in case of bug
	* @author Remy BARRIOL
	*/
	public void testRanking()
	{
		nbPlayer=6;
		allPlayer= new Player[nbPlayer];
		for(int i=0;i<nbPlayer;i++)
		{
			allPlayer[i]=new Player(nbPlayer,i+1,Pa);
		}
		B.createTable(nbPlayer);
		for(int i=0;i<nbPlayer;i++)
		{
			allPlayer[i].incScore(nbPlayer-i);
			//allPlayer[i].incScoreRound();
		}
		allPlayer[5].incScoreRound();
		allPlayer[1].incScore(1);
		allPlayer[5].incScore(15);
		allPlayer[4].incScore(2);
		allPlayer[4].increaseGreen();
		allPlayer[1].increaseGreen();
		allPlayer[0].increaseGreen();
		End();
	}
	public void distributionTest(int o)
	{
		if(o==0)
		{
			allPlayer[0].addCard(new Card(1));
			allPlayer[0].addCard(new Card(1));
			allPlayer[0].addCard(new Card(2));
			allPlayer[0].addCard(new Card(2));
			allPlayer[0].addCard(new Card(3));
			allPlayer[0].addCard(new Card(3));
			allPlayer[0].addCard(new Card(3));
			allPlayer[0].addCard(new Card(4));
			allPlayer[0].addCard(new Card(4));
			allPlayer[0].addCard(new Card(4));
			allPlayer[0].addCard(new Card(5));
			allPlayer[0].addCard(new Card(5));
			allPlayer[0].addCard(new Card(5));
			allPlayer[0].addCard(new Card(5));

			allPlayer[1].addCard(new Card(1));
			allPlayer[1].addCard(new Card(1));
			allPlayer[1].addCard(new Card(1));
			allPlayer[1].addCard(new Card(1));
			allPlayer[1].addCard(new Card(2));
			allPlayer[1].addCard(new Card(2));
			allPlayer[1].addCard(new Card(3));
			allPlayer[1].addCard(new Card(3));
			allPlayer[1].addCard(new Card(3));
			allPlayer[1].addCard(new Card(3));
			allPlayer[1].addCard(new Card(4));
			allPlayer[1].addCard(new Card(4));
			allPlayer[1].addCard(new Card(5));
			allPlayer[1].addCard(new Card(5));
		}
		else
		{
			allPlayer[0].addCard(new Card(1));
			allPlayer[0].addCard(new Card(1));
			allPlayer[0].addCard(new Card(1));
			allPlayer[0].addCard(new Card(1));
			allPlayer[0].addCard(new Card(2));
			allPlayer[0].addCard(new Card(2));
			allPlayer[0].addCard(new Card(2));
			allPlayer[0].addCard(new Card(2));
			allPlayer[0].addCard(new Card(3));
			allPlayer[0].addCard(new Card(3));
			allPlayer[0].addCard(new Card(5));
			allPlayer[0].addCard(new Card(5));
			allPlayer[0].addCard(new Card(5));
			allPlayer[0].addCard(new Card(5));

			allPlayer[1].addCard(new Card(1));
			allPlayer[1].addCard(new Card(1));
			allPlayer[1].addCard(new Card(2));
			allPlayer[1].addCard(new Card(2));
			allPlayer[1].addCard(new Card(3));
			allPlayer[1].addCard(new Card(3));
			allPlayer[1].addCard(new Card(3));
			allPlayer[1].addCard(new Card(4));
			allPlayer[1].addCard(new Card(4));
			allPlayer[1].addCard(new Card(4));
			allPlayer[1].addCard(new Card(4));
			allPlayer[1].addCard(new Card(4));
			allPlayer[1].addCard(new Card(5));
			allPlayer[1].addCard(new Card(5));
		}
		for(int i=0;i<2;i++)
		{
			allPlayer[i].sortingHand();
		}
	}
	public void mainGameTest()
	{
		int o=0;
		int min;
		while(o!=nbPlayer)
		{
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.println("Round "+o);
			System.out.println("");
			System.out.println("");
			min=999999;
			Round();
			for(int i=0;i<nbPlayer;i++)
			{
				if(min>allPlayer[i].getTurnDeath())
				{
					min=allPlayer[i].getTurnDeath();
				}
			}
			for(int i=0;i<nbPlayer;i++)
			{
				if(min==allPlayer[i].getTurnDeath())
				{
					lastFight=i;
				}
			}
			reset();
			o++;
			distributionTest(o);
		}
		End();
	}
	public void initialization(Replay file)
	{
		R=file;
		R.readAndInputThem();
	}
	/**
	*  * Initialize data of the game with the information given by the user
	* @author Remy BARRIOL
	*/
	public void initialization(int player,int ai)
	{
		nbPlayer=player;
		nbAI=ai;
		allPlayer= new Player[nbPlayer];
		int u=0;
		System.out.println("AI = "+nbAI);
		for(int i=0;i<nbAI;i++)
		{
			//TODO: Choose ai
			//allPlayer[i]=new AI(nbPlayer,i+1,Pa);
			allPlayer[i]=new AITwo(nbPlayer,i+1,Pa);
			u=i;
		}
		if(nbAI==0)
		{
			u=-1;
		}
		for(int i=u+1;i<nbPlayer;i++)
		{
			allPlayer[i]=new Player(nbPlayer,i+1,Pa);
		}
		B.createTable(nbPlayer);
		mixPlayer();
	}
	public void initialization(int player,boolean AIornot[],ArrayList<Profile> profiles,int ai,int intervalle,boolean blitzs,boolean replay)
	{
		blitz=blitzs;
		Replay=replay;
		nbPlayer=player;
		nbAI=ai;
		allPlayer= new Player[nbPlayer];
		int u=0;
		System.out.println("AI = "+nbAI);
		for(int i=0;i<nbPlayer;i++)
		{
			Profile p = profiles.get(i);
			if(!AIornot[i])
				allPlayer[i]= new Player(nbPlayer, Pa, p);
			else {
				try {
					Class c = AiUtils.instance.getAI(p.getAIType());
					allPlayer[i]= (Player) Class.forName(c.getName()).getConstructor(Integer.class, Partie.class, Profile.class).newInstance(nbPlayer, Pa, p);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					allPlayer[i]= new AIOne(nbPlayer, Pa, p);
					e.printStackTrace();
				}
			}
		}

		B.createTable(nbPlayer);
		mixPlayer();
		inter=intervalle;
		System.out.println("...a....");
		if(Replay==true)
		{
			Calendar C=new GregorianCalendar();
			R=new Replay("saves/"+C.get(Calendar.YEAR)+"_"+(C.get(Calendar.MONTH)+1)+"_"+C.get(Calendar.DAY_OF_MONTH)+"_"+(C.get(Calendar.HOUR_OF_DAY))+"_"+C.get(Calendar.MINUTE)+".replay",Config.get("REPLAY_COUNT")-1);
			String[] g=new File("saves/").list();
			File[] j=new File[g.length];
			for(int i=0;i<g.length;i++)
			{
				j[i]=new File(g[i]);
			}
			
			System.out.println("Ici !");
			//System.out.println("Post File!");
			R.removeToOld(j,0);
			System.out.println("Post Removing!");
		}
		System.out.println(".......");
		if(blitz)
		{
			Tim=new aTime[nbPlayer];
			for(int i=0;i<nbPlayer;i++)
			{
				Tim[i]=new aTime(intervalle,allPlayer[i],Pa,i,this);
			}
		}
		System.out.println("Fin !");
	}
		/**
	*  * It distribute cards between players
	* @author Remy BARRIOL
	*/
	public void distribution()
	{
		Random rand = new Random();
		int max=35;
		int min=0;
		int[] Super_Ultimate_Drawer={1,1,1,1,1,1,1,2,2,2,2,2,2,2,3,3,3,3,3,3,3,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5};
		int o=0;
		int x;
		while(o!=allPlayer[0].getMaxCard())
		{
			for(int k=0;k<nbPlayer;k++)
			{
				x=rand.nextInt(max - min + 1) + min;
				max--;
				allPlayer[k].addCard(new Card(Super_Ultimate_Drawer[x]));
				Super_Ultimate_Drawer=numberAdvance(Super_Ultimate_Drawer, x);
			}
			o++;
		}
		if(nbPlayer==5)
		{
			int color=Super_Ultimate_Drawer[0];
			B.changeDis(new Card(color));
		}
		for(int i=0;i<allPlayer.length;i++)
		{
			allPlayer[i].sortingHand();
		}
	}
	/**
	*  * Used in distribution(), takes the chosen integer at the end of the table
	* @param C The table which will be modified
	* @param i The position of the integer wich has been take in distribution()
	* @return the modified table
	* @author Remy BARRIOL
	*/
	public int[] numberAdvance(int []C,int i)
	{
		int t;
		C[i]=0;
		for(int j=i;j<C.length-1;j++)
		{
			t=C[j];
			C[j]=C[j+1];
			C[j+1]=t;
		}
		return C;
	}
		/**
	*  * reset information that have to be reset beatween turns
	* @author Remy BARRIOL
	*/
	public void reset()
	{
		for(int i=0;i<allPlayer.length;i++)
		{
			allPlayer[i].changeNbCard(0);
			allPlayer[i].changeTurnDeath(0);
			allPlayer[i].resetDefeat();
		}
		if(blitz)
		{
			for(int i=0;i<nbPlayer;i++)
				{
					Tim[i].reset(inter);
				}
		}
		nbSlain=0;
		turn=0;
		B.createTable(nbPlayer);
	}
	/**
	*  * Manages the turn of a player
	* @param j It's the position of the player who have to play in allPlayer
	* @author Remy BARRIOL
	*/
	public void Turn(int j)
	{
		//allPlayer[j].displayHand();
		/*System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("");*/
		//if(!(allPlayer[j] instanceof AI))
		if(!(AI.class.isAssignableFrom(allPlayer[j].getClass())))
			Pa.toDisplayTempString("It's your turn "+allPlayer[j].getName());
		Vector K=new Vector();
		compteur Wait=new compteur();
		if(Replay==true)
			Wait.start();
		K=allPlayer[j].possibilityPlayer(B,this);
		if(Replay==true)
			Wait.stopIt();
		int a=(int)K.get(0);
		int b=(int)K.get(1);
		int c=(int)K.get(2);
		if(c==5)
		{
			allPlayer[j].increaseGreen();
		}
		if(c==0)
		{
			//System.out.println("What ?");
			allPlayer[j].doDefeat();
			nbSlain++;
			allPlayer[j].changeTurnDeath(turn);
			if((Replay==true)&&(mode==1))
				R.appendStrToFile("3"+'\t'+allPlayer[j].getName()+'\n');
		}
		else
		{
			if(!allPlayer[j].getDefeat())
			{
				allPlayer[j].removeCard(c);
				B.changeColorBoard(a,b,c);
				if(Replay==true)
				{
					R.appendStrToFile(""+2+'\t'+allPlayer[j].getName());
					R.appendStrToFile(""+'\t'+a+b+c+'\t');
					if(mode==1)
						R.appendStrToFile(allPlayer[j].getHand()+'\t');
					R.appendStrToFile(""+Wait.getTime()+'\n');
				}
			}
			B.removeX();
		}
		//System.out.println("Hey " + j);
		/*if(blitz)
		{
			if(j==nbPlayer-1)
			{
				Tim[j].pause();
				if(!allPlayer[0].getDefeat())
					Tim[0].launch();
				else
				{
					int y=1;
					while((y<nbPlayer)&&(allPlayer[y].getDefeat()))
					{
						y++;
						if(y==nbPlayer-1)
						{
							y=-1;
							break;
						}
					}
					if(y!=-1)
						Tim[y].launch();
				}
			}
			else
			{
				int j1=j;
				Tim[j].pause();
				if(!allPlayer[j+1].getDefeat())
					Tim[j+1].launch();
				else
				{
					int y=j+1;
					while((allPlayer[y].getDefeat()))
					{
						if(y==nbPlayer-1)
						{
							y=0;
						}
						if(y==j1)
						{
							y=-1;
							break;
						}
						y++;
					}
					if(y!=-1)
						Tim[y].launch();
				}
			}
		}*/
	}
	/**
	*  * Manages the round,launches turns
	* @author Remy BARRIOL
	*/
	public void Round()
	{
		//int t=0;
		int r=0;
		int defeatCount;

		boolean allAi = (nbAI == nbPlayer);
		System.out.println("nbAI = "+nbAI);

		while(nbPlayer-1!=nbSlain && nbPlayer!=nbSlain) {
			//System.out.println("");
			System.out.println("players = "+nbPlayer+ " sl "+nbSlain);
			System.out.print("lastFight = "+lastFight);

			for (Player p : allPlayer) {
				System.out.print(" "+p.name+" "+p.defeated+"("+p.getDefeat()+")"+" "+p.profile.getName()+" // ");
			}

			defeatCount = 0;

			for (int i = 0; i < allPlayer.length; i++) {
				if(allPlayer[i].getDefeat()) defeatCount++;
			}
			System.out.print("defeatCount = "+defeatCount);
			/*if(defeatCount == nbPlayer) {
				lastFighter();
				return;
			}*/
			//lastFighter();

			System.out.println(" ");
			/*System.out.println("");
			System.out.println("");
			System.out.println("");*/
			
			if((Replay==true)&&(mode==1))
				R.appendStrToFile(""+4+'\t'+turn+'\t'+round+"\n");
			if(!allAi) Pa.toDisplayTempString("Turn "+turn);
			for(int i=lastFight;i<nbPlayer;i++) {
				if(!allPlayer[i].getDefeat()) {
					r++;
					Pa.changeName(allPlayer[i].getName());
					/*if((turn==0)&&(i==0)&&(blitz))
						Tim[0].launch();*/
					if(blitz)
						Tim[i].launch();
					Turn(i);
					if(blitz)
						Tim[i].pause();
					//B.toDisplay();
					//////////////////////////////
					Pa.toDisplayBoard(this);
					//////////////////////////////
				}
			}
			turn++;
			if(lastFight!=0) {
				turn--;
			}
			lastFight=0;
			//System.out.println("End ?");
			nbSlain=0;
			for(int i=0;i<nbPlayer;i++)
			{
				if(allPlayer[i].getDefeat())
					nbSlain++;
			}
		}
		for(int y=0;y<nbPlayer;y++) {
			allPlayer[y].incScore(allPlayer[y].getNbCard());
		}
		lastFighter();
	}
	/**
	*  * Manages the party,launches the rounds
	* @author Remy BARRIOL
	*/
	public void mainGame()
	{
		int o=0;
		int min;
		boolean allAi = (nbAI == nbPlayer);
		if(Replay==true)
		{
			R.appendStrToFile(0+"\t"+mode+"\t"+nbPlayer);
			if(blitz)
				R.appendStrToFile("\t"+inter);
			R.appendStrToFile("\n");
			if(mode==0)
			{
				for(int i=0;i<allPlayer.length;i++)
				{
					R.appendStrToFile(""+1+'\t'+allPlayer[i].getName()+"\t"+allPlayer[i].getHand2()+"\n");
				}
			}
		}
		while(o!=nbPlayer)
		{
			/*System.out.println("");
			System.out.println("");
			System.out.println("");*/
			if(!allAi) Pa.toDisplayTempString("Round "+o);
			/*System.out.println("");
			System.out.println("");*/
			/*if(o!=0)
			{
				for(int i=0;i<nbPlayer;i++)
				{
					if((Tim != null && Tim[i] != null) && blitz) {
						Tim[i].reset(inter);
					} else {
						System.out.println("DO SOMETHING !");
						End();
						return;
					}
				}
			}*/
			min=999999;
			
			//2 \t UUIDPlayer	\t HandPlayerAnteAction	\t 	xy[Color](Action)	\t	TempsAnteAction	
			if(Replay==true)
				R.appendStrToFile("7"+'\t'+B.getDis()+'\n');
			Round();
			
			for(int i=0;i<nbPlayer;i++)
			{
				if(min>allPlayer[i].getTurnDeath())
				{
					min=allPlayer[i].getTurnDeath();
				}
			}
			for(int i=0;i<nbPlayer;i++)
			{
				if(min==allPlayer[i].getTurnDeath())
				{
					lastFight=i;
				}
			}
			//System.out.println("FIN !!!");
			if(Replay==true)
				R.appendStrToFile(""+6+'\n');
			reset();
			distribution();
			o++;
			round++;
		}
		End();
	}
	/**
	*  * Make the ranking of the party with the information of players
	* @author Remy BARRIOL
	*/
	@SuppressWarnings("unchecked")
	public void End()
	{
		int PlayerManage=0;
		Vector Ranking=new Vector();
		int minScore;
		int minGeneralScore=-1;
		while(PlayerManage<nbPlayer)
		{
			minScore=9999;
			for(int i=0;i<nbPlayer;i++)
			{
				if((minScore>allPlayer[i].getScore())&&(minGeneralScore<allPlayer[i].getScore()))
				{
					minScore=allPlayer[i].getScore();
				}
			}
			minGeneralScore=minScore;
			Vector Transition1=new Vector();
			for(int i=0;i<nbPlayer;i++)
			{
				if(minScore==allPlayer[i].getScore())
				{
					Transition1.add(allPlayer[i]);
				}
			}
			if(Transition1.size()==1)
			{
				Player First[]=new Player[1];
				First[0]=(Player)Transition1.get(0);
				Transition1.removeAllElements();
				Ranking.add(First);
				PlayerManage++;
			}
			else
			{
				while(Transition1.size()>0)
				{
					Player TransitionRound[]=new Player[Transition1.size()];
					for(int i=0;i<Transition1.size();i++)
					{
						TransitionRound[i]=(Player)Transition1.get(i);
					}
					int max=-1;
					for(int i=0;i<TransitionRound.length;i++)
					{
						if(max<TransitionRound[i].getScoreRound())
						{
							max=TransitionRound[i].getScoreRound();
						}
					}
					Vector Transition2=new Vector();
					for(int i=TransitionRound.length-1;i>=0;i--)
					{
						if(max==TransitionRound[i].getScoreRound())
						{
							Transition1.remove(i);
							Transition2.add(TransitionRound[i]);
						}
					}
					if(Transition2.size()==1)
					{
						Player First[]=new Player[1];
						First[0]=(Player)Transition2.get(0);
						Transition2.removeAllElements();
						Ranking.add(First);
						PlayerManage++;
					}
					else
					{
						while(Transition2.size()>0)
						{
							Player TransitionGreen[]=new Player[Transition2.size()];
							for(int i=0;i<Transition2.size();i++)
							{
								TransitionGreen[i]=(Player)Transition2.get(i);
							}
							int minG=9999;
							for(int i=0;i<TransitionGreen.length;i++)
							{
								if(minG>TransitionGreen[i].getGreen())
								{
									minG=TransitionGreen[i].getGreen();
								}
							}
							Vector Transition3=new Vector();
							for(int i=TransitionGreen.length-1;i>=0;i--)
							{
								if(minG==TransitionGreen[i].getGreen())
								{
									Transition3.add(TransitionGreen[i]);
									Transition2.remove(i);
								}
							}
							if(Transition3.size()==1)
							{
								Player First[]=new Player[1];
								First[0]=(Player)Transition3.get(0);
								Transition3.removeAllElements();
								Ranking.add(First);
								PlayerManage++;
							}
							else
							{
								Player First[]=new Player[Transition3.size()];
								for(int i=0;i<First.length;i++)
								{
									First[i]=(Player)Transition3.get(i);
								}
								PlayerManage=PlayerManage+Transition3.size();
								Transition3.removeAllElements();
								Transition2.removeAllElements();
								Transition1.removeAllElements();
								Ranking.add(First);
							}
						}
					}
				}
			}
		}
		if(nbPlayer==PlayerManage)
		{
			Player[][] P=new Player[Ranking.size()][];
			for(int i=0;i<Ranking.size();i++)
			{
				P[i]=(Player[])Ranking.get(i);
			}
			/*System.out.println("");
			System.out.println("");*/
			//System.out.println("Ranking :");
			//System.out.println("");
			SScores scores = new SScores();
			int gg=0;
			for(int i=0;i<P.length;i++)
			{
				for(int j=0;j<P[i].length;j++)
				{
					if((Replay==true)&&(mode==1))
						R.appendStrToFile(""+5+'\t'+P[i][j].getName()+'\t'+(i+gg)+'\t'+P[i][j].getScore()+'\t'+P[i][j].getScoreRound()+'\t'+P[i][j].getGreen()+"\n");
						
					scores.addScore(P[i][j].getName(),P[i][j].getScore(),P[i][j].getScoreRound(),P[i][j].getGreen(),i+gg);
				}
				gg+=P[i].length-1;
			}
			Pa.Endo(scores);
			
		}
		else
		{
			Pa.toDisplayTempString("Some players aren't in the Ranking !");
		}
	}
		/**
	*  * Set players, who have be defeated a the last turns, winners of the rounds
	* @author Remy BARRIOL
	*/
	public void lastFighter()
	{
		int max=0;
		for(int i=0;i<nbPlayer;i++)
		{
			if(max<allPlayer[i].getTurnDeath())
			{
				max=allPlayer[i].getTurnDeath();
			}
		}
		for(int i=0;i<nbPlayer;i++)
		{
			if(max==allPlayer[i].getTurnDeath())
			{
				allPlayer[i].incScoreRound();
			}
		}
	}
}
