package com.pingu.party.game;
import java.util.Scanner;
import java.util.Random;
import java.util.Vector;
/*import com.pingu.party.network.MultiThreadServer;
import com.pingu.party.network.GameServer;
import com.pingu.party.network.clientThread;
import com.pingu.party.network.GameClient;*/
import com.pingu.party.activities.Partie;

import com.pingu.party.engine.game.Profile;
import com.pingu.party.engine.game.ProfileManager;

import java.util.ArrayList;

import com.pingu.party.engine.config.Config;

public class Init{
	int nbPlayer =-1;
	int nbAI =-1;
	Partie Pa;
	Game G;
	public Game getGame()
	{
		return G;
	}
	public Init(int players,int ais)
	{
		nbPlayer=players;
		nbAI=ais;
	}
	public Init()
	{}
	public void initialization(Partie P,boolean AIornot[],ArrayList<Profile> playerNames,int intervalle,boolean blitzs,boolean replay)
	{
		Pa=P;
		G=new Game();
		G.putPartie(Pa);
		G.initialization(nbPlayer,AIornot,playerNames,nbAI,intervalle,blitzs,replay);
	}
	public void initialization(Partie P)
	{
		Pa=P;
		int nint=1;
		if(nint==1)
		{
			G=new Game();
			G.putPartie(Pa);
			G.initialization(nbPlayer,nbAI);
		}
		else if(nint==2)
		{
			/*System.out.println("1- Server ; 2- Client");
			nint = sc.nextInt();
			while((nint!=1)&&(nint!=2))
			{
				System.out.println("There are only two possibility !");
				nint = sc.nextInt();
			}
			if(nint==1)
			{
				System.out.println("Numbers of Players :");
				nbPlayer = sc.nextInt();
				while((6<nbPlayer)||(nbPlayer<2))
				{
					System.out.println("The number of players cannot be more than 6 or negative : ");
					nbPlayer = sc.nextInt();
				}
			  String[] str = new String[1];
			  str[0]=Integer.toString(nbPlayer);
			  new MultiThreadServer().main(str);
			}
			else if(nint==2)
			{
				/*System.out.println("Enter the Server ip adress : ");*/
		/*		String str;//= sc.next();
				str="127.0.0.1";
				String[] str1=new String[1];
				str1[0] = str;
				if(str!=null)
				{	
					new GameClient().main(str1);
				}
			}*/
		}
		else
		{
				System.err.println("ERROR");
		}
	}
	public void initialization(Partie P,String fileName)
	{
		Pa=P;
		G=new Game();
		G.putPartie(Pa);
		G.initialization(new Replay(fileName,Config.get("REPLAY_COUNT"),G));
	}
	public void startGame()
	{
		G.distribution();
		//G.mixPlayer();
		G.mainGame();
	}
	public int getNumberPlayer()
	{
		return G.getNumberPlayer();
	}
	
}