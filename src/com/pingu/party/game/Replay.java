package com.pingu.party.game;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.File;

public class Replay
{
	String fileName;
	int nbFilesMax;
	Game G;
	/*
	Brouillon de Ellen Ripley, le lieutenant de première classe du l'USCSS Nostromo
	
	->Voir pour Date et Heure     							 -> Check
	->Créer dossier de sauvegarde des replays				-> Check ?
	->Modification dans Interface Graphique					-> Check
	-Foction de suppression des (i>TempsOption) fichiers.	-> Check
	
	Structure du fichier de sauveagrde potentiel :
	
	
	0 \t mode \t nbPlayer \t TimeBlitz
	
	1 \t UUID \t hand

	2 \t UUIDPlayer	\t 	xy[Color](Action) \t HandPlayerAnteAction	\t	TempsAnteAction							-> Action
	3 \t UUIDPlayer 
	4 \t  Tour \t Manche																								-> Defaite
	.......
	5 \t UUIDPlayer \t NumClassement \t Score \t ScoreRound \t NbCardGreen											-> Classement de Fin
	6			-> reset
	7 \t discarded	

	*/


	public Replay(String s,int i)
	{
		fileName=s;
		nbFilesMax=i;
	}
	public Replay(String s,int i,Game g)
	{
		fileName=s;
		nbFilesMax=i;
		G=g;
	}
	public void appendStrToFile(String str) 
    { 
        try 
		{ 
            BufferedWriter out = new BufferedWriter(new FileWriter(fileName, true)); 
            out.write(str); 
            out.close(); 
        } 
        catch (IOException e) { 
			try
			{
				File dir = new File(fileName);
				dir.createNewFile();
				BufferedWriter out = new BufferedWriter(new FileWriter(fileName, true)); 
           	 	out.write(str+"\n"); 
          		out.close();
			}
			catch(Exception er)
			{
				System.err.println("Save: "+er.getMessage());
			}
        } 
    } 
	public void readAndInputThem()
	{
		String line;
		int mode=0;
		try 
		{
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));

            while((line = bufferedReader.readLine()) != null) 
			{
                System.out.println(line);
				
				int type=line.charAt(0)-48;
				if(type==0)
				{
					//0 \t mode \t nbPlayer \t TimeBlitz
					mode=line.charAt(2)-48;
					int nbPlayer=line.charAt(4)-48;
					String secondeBlitzS="";
					for(int i=5;i<line.length();i++)
					{
						char f=line.charAt(i);
						if(f=='\n')
							break;
						else if(f!='\t')
						{
							secondeBlitzS+=f;
						}
						//System.out.println(line.charAt(i));
					}
					int secondeBlitz=0;
					if(secondeBlitzS.length()>0)
						secondeBlitz=Integer.parseInt(secondeBlitzS);
					
					//System.out.println(mode+" "+nbPlayer+" "+secondeBlitzS+" "+secondeBlitz);
					//TO DO
					//Redirection valeur
					// Game ?
					
					G.configReplay(nbPlayer,secondeBlitz);
				}
				else if(type==1)
				{
					//1 \t UUID \t hand
					int stade=0;
					String UUID="";
					String hand="";
					for(int i=2;i<line.length();i++)
					{
						char f=line.charAt(i);
						if(f=='\n')
							break;
						else if(f=='\t')
							stade++;
						else if(stade==0)
							UUID+=f;
						else if(stade==1)
							hand+=f;
						//System.out.println(line.charAt(i));
					}
					System.out.println("UUID :"+UUID+" Hand :"+hand);
					/*for(int i=0;i<hand.length();i++)
					{
						System.out.println("OH :"+hand.charAt(i));
					}*/
				}
				else if(type==2)
				{
					//2 \t UUIDPlayer \t xy[Color](Action) \t HandPlayerAnteAction \t TempsAnteAction
					int stade=0;
					String UUID="";
					String action="";
					String hand="";
					String time="";
					for(int i=2;i<line.length();i++)
					{
						char f=line.charAt(i);
						if(f=='\n')
							break;
						else if(f=='\t')
							stade++;
						else if(stade==0)
							UUID+=f;
						else if(stade==1)
							action+=f;
						else if((stade==2)&&(mode==1))
							hand+=f;
						else if((stade==2)&&(mode==0))
							time+=f;
						else if(stade==3)
							time+=f;

					}
					
					G.playReplay(action,UUID,hand,Integer.parseInt(time));
					//System.out.println("UUID : "+UUID+" Action : "+action+" Main : "+hand+" Temps : "+time);
				}
				else if(type==3)
				{
					String UUID="";
					for(int i=2;i<line.length();i++)
					{
						char f=line.charAt(i);
						if(f=='\n')
							break;
						else if(f!='\t')
							UUID+=f;
					}
					G.defeatReplay(UUID);
					//System.out.println("UUID : "+UUID);

				}
				else if(type==4)
				{
					//4 \t  Tour \t Manche
					int stade=0;
					String turn="";
					String round="";
					for(int i=2;i<line.length();i++)
					{
						char f=line.charAt(i);
						if(f=='\n')
							break;
						else if(f=='\t')
							stade++;
						else if(stade==0)
							turn+=f;
						else if(stade==1)
							round+=f;
					}
					//System.out.println("Turn  : "+turn+" Manche : "+round);
					G.changeRoundandTurn(Integer.parseInt(turn),Integer.parseInt(round));

				}
				else if(type==5)
				{
					//5 \t UUIDPlayer \t NumClassement \t Score \t ScoreRound \t NbCardGreen	
					int stade=0;
					String UUID="";
					String NumClassement="";
					String Score="";
					String ScoreRound="";
					String NbCardGreen="";
					for(int i=2;i<line.length();i++)
					{
						char f=line.charAt(i);
						if(f=='\n')
							break;
						else if(f=='\t')
							stade++;
						else if(stade==0)
							UUID+=f;
						else if(stade==1)
							NumClassement+=f;
						else if(stade==2)
							Score+=f;
						else if(stade==3)
							ScoreRound+=f;
						else if(stade==4)
							NbCardGreen+=f;
					}
					G.addClass(UUID,Integer.parseInt(Score),Integer.parseInt(ScoreRound),Integer.parseInt(NbCardGreen),Integer.parseInt(NumClassement));
					//System.out.println("UUID : "+UUID+" Num : "+NumClassement+" Score :"+Score+" ScoreRound : " + ScoreRound+" Green : "+NbCardGreen);
				}
				else if(type==6)
				{
					//G.configReplay(G.getNbPlayer());
					//affich
				}
				else if(type==7)
				{
					
					//TO DO
					int discarded=line.charAt(2)-48;
					G.changeDis(discarded);
				}
            }
            bufferedReader.close();
			G.endClass();			
        }
        catch(FileNotFoundException ex)
		{
            System.out.println("Fichier introuvable'" + fileName + "'");
        }
        catch(IOException ex)
		{
            System.out.println("Erreur lors de la lecture de '"  + fileName + "'");
        }
	}
	public void removeToOld(File[] files,int n)
	{
		String[] gfuel=new File("./saves/").list();
		if(gfuel.length<nbFilesMax)
		{
			return;
		}
		if(n!=nbFilesMax)
		{
			String dateMax="";
			int yearMax=0;
			int monthMax=0;
			int dayMax=0;
			int hourMax=0;
			int minuteMax=0;
			int yearAct=0;
			int monthAct=0; 
			int dayAct=0;
			int hourAct=0;
			int minuteAct=0;
			//int indexMax=-1;
			for(int i=0;i<files.length;i++)
			{
				
				String yearActS="";
				String monthActS=""; 
				String dayActS="";
				String hourActS="";
				String minuteActS="";
				File f=files[i];
				//if(f.isFile())
				//{
					String name=f.getName();
					int step=0;
					char c;
					for(int k=0;k<name.length();k++)
					{
						c=name.charAt(k);
						if((c=='_')||(c=='.'))
							step++;
						else if(step==0)
							yearActS+=c;
						else if(step==1)
							monthActS+=c;
						else if(step==2)
							dayActS+=c;
						else if(step==3)
							hourActS+=c;
						else if(step==4)
							minuteActS+=c;
						else if(step==5)
						{
							yearAct=Integer.parseInt(yearActS);
							monthAct=Integer.parseInt(monthActS);
							dayAct=Integer.parseInt(dayActS);
							hourAct=Integer.parseInt(hourActS);
							minuteAct=Integer.parseInt(minuteActS);
							//System.out.println(yearAct+" "+monthAct+" "+dayAct+" "+hourAct+" "+minuteAct);
							if(yearAct>yearMax)
							{
								yearMax=yearAct;
								dateMax=yearAct+"_"+monthAct+"_"+dayAct+"_"+hourAct+"_"+minuteAct;
							}
							else if(yearAct==yearMax)
							{
								if(monthAct>monthMax)
								{
									monthMax=monthAct;
									dateMax=yearAct+"_"+monthAct+"_"+dayAct+"_"+hourAct+"_"+minuteAct;
								}
								else if(monthAct==monthMax)
								{
									if(dayAct>dayMax)
									{
										dayMax=dayAct;
										dateMax=yearAct+"_"+monthAct+"_"+dayAct+"_"+hourAct+"_"+minuteAct;
									}
									else if(dayAct==dayMax)
									{
										if(hourAct>hourMax)
										{
											hourMax=hourAct;
											dateMax=yearAct+"_"+monthAct+"_"+dayAct+"_"+hourAct+"_"+minuteAct;
										}
										else if(hourAct==hourMax)
										{
											if(minuteAct>minuteMax)
											{
												minuteMax=minuteAct;
												dateMax=yearAct+"_"+monthAct+"_"+dayAct+"_"+hourAct+"_"+minuteAct;
											}
										}
									}
								}
							}
						}
					}
				//}
			}
			File[] foutput=new File[files.length-1];
			int u=0;
			for(File f : files)
			{
				//System.out.println(f.getName()+"\t"+dateMax+".replay"+ " "+f.getName().equals(dateMax+".replay"));
				if(!f.getName().equals(dateMax+".replay"))
				{
					foutput[u]=f;
					u++;
				}
			}
			removeToOld(foutput,n+1);
		}
		else
		{
			/*System.out.println("Delete this file : "+files[0].getName());		
			System.out.println(files[0].delete());*/
			for(File f : files)
			{
				f=new File("./saves/"+f.getName()); 
				try
				{
					if(f.isFile())
					{
						f=f.getCanonicalFile();
						System.out.println("Delete this file : "+f.getName());
						f.delete();
					}
					else
					{
						System.out.println("Fail to delete");
					}
				}
				catch(Exception er)
				{
					System.out.println("Delete :"+er);
				}
			}
		}
	}
}
class compteur extends Thread
{
	int second=0;
	boolean c=true;
	public void stopIt()
	{
		c=false;
	}
	public int getTime()
	{
		return second;
	}
	public void run()
		{
			while(c)
			{
				try { Thread.sleep(1000); } catch (Exception e) { }
				second++;
			}
		}
}

class Waiting
{
	int second;
	int current=0;
	boolean c=true;
	public void stop()
	{
		c=false;
	}
	public Waiting(int s)
	{
		second=s;
	}
	public void run()
		{
			while((current<second)&&(c))
			{
				try { Thread.sleep(1000); } catch (Exception e) { }
				current++;
			}
		}
}
