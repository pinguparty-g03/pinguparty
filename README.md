# Pingu Party

Implementation of the Pingu Party card game, originally by Reiner Knizia.

This software was developped as an assignment during the 2018/10 university year at the CERI, University of Avignon (France), by the following students:
* BARRIOL Rémy
* Bastien MALEPLATE
* Florian RIBOU
* Tristan WATTIN

It can be used by a human to play against artificial agents, or against distant players through a network.


## Organization
The source code is organized as follows :


```
├─── launcher                engine launcher
│    ├─── Utils              launcher utils
│    │    └─── Font          launcher fonts wrapper
│    └─── Component          launcher UI components
├─── gui                     User Interface
│    └─── toaster            Toaster UI component
├─── engine                  engine files
│    ├─── agent
│    ├─── input              Input management
│    │    ├─── map           Map wrapper for inputs
│    │    └─── event         Event handler for inputs
│    ├─── utils              Various utils
│    ├─── animation          Animation Toolkit
│    ├─── loader             Modules loader
│    ├─── data               Engine data
│    ├─── math               Math utils
│    ├─── console            Console wrapper
│    │    └─── base          Console base commands
│    ├─── game               Game instances
│    │    └─── Test          test instances
│    ├─── config             Engine config
│    └─── core               Engine core
├─── stats
└─── network
```

## Installation

### Gradle install from sources
Here is the procedure to install this software :
1. `git clone https://gitlab.com/pinguparty-g03/pinguparty.git`
2. `cd pinguparty`
3. `Gradle build`
4. `Gradle run`


## Use
In order to use the software, you must...
1. Do this
2. Do that
3. etc.

The project wiki (put a hyperlink) contains detailed instructions regarding how to use the game.


## Dependencies
The project relies on the following librarie:
* xxxxx : this library was used to...
* yyyyy: ...

## References
During development, we use the following bibliographic resources:
* Webpage x: it explains the rules of the game.
* Book xxxx: it describes how to write an artifical agent in Java.
* etc.
*
